/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once


// COptionsPagePrinting dialog

class COptionsPagePrinting : public CPropertyPage
{
	DECLARE_DYNAMIC(COptionsPagePrinting)

public:
	COptionsPagePrinting();
	virtual ~COptionsPagePrinting();

// Attributes
private:

// Dialog Data
private:
	enum { IDD = IDD_PRINTING_PAGE };
	BYTE m_nPrintSizeEdit;
	CEdit m_ctrlPrintSizeEdit;
	CString m_strPrintSampleLine1Edit, m_strPrintSampleLine2Edit;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual BOOL OnApply();
	virtual void HtmlHelp(DWORD_PTR dwData, UINT nCmd = 0x000F);

	DECLARE_MESSAGE_MAP()
	afx_msg void OnEnChangePrintsizeEdit();
	afx_msg void OnEnChangePrintsampleline1Edit();
	afx_msg void OnEnChangePrintsampleline2Edit();
	afx_msg void OnBnClickedDefaulttextButton();
	afx_msg void OnHelpButton(UINT id, NMHDR* pNotifyStruct, LRESULT* result);
};
