/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// FontRunnerView.cpp : implementation of the CFontRunnerView class
//

#include "stdafx.h"
#include "FontRunner.h"

#include "FontRunnerDoc.h"
#include "FontRunnerView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CFontRunnerView

IMPLEMENT_DYNCREATE(CFontRunnerView, CView)

BEGIN_MESSAGE_MAP(CFontRunnerView, CView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

// CFontRunnerView construction/destruction

CFontRunnerView::CFontRunnerView()
{
	// TODO: add construction code here

}

CFontRunnerView::~CFontRunnerView()
{
}

BOOL CFontRunnerView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CFontRunnerView drawing

void CFontRunnerView::OnDraw(CDC* /*pDC*/)
{
#ifdef _DEBUG
	CFontRunnerDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
#endif

	// TODO: add draw code for native data here
}


// CFontRunnerView printing

BOOL CFontRunnerView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CFontRunnerView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CFontRunnerView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}


// CFontRunnerView diagnostics

#ifdef _DEBUG
void CFontRunnerView::AssertValid() const
{
	CView::AssertValid();
}

void CFontRunnerView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CFontRunnerDoc* CFontRunnerView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CFontRunnerDoc)));
	return (CFontRunnerDoc*)m_pDocument;
}
#endif //_DEBUG


// CFontRunnerView message handlers
