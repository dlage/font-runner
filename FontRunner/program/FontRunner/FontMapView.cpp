/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// FontMapView.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "FontRunnerDoc.h"
#include "FontMapView.h"

#include "OpenTypeData.h"
#include "UnicodeData.h"
#include "FontRunnerSignals.h"
#include "FontRunnerException.h"
#include "CharDetailWnd.h"
#include "FontDetailFrame.h"
#include "ProgramOptions.h"
#include "FindCharacterFrameWnd.h"
#include "FontItem.h"
#include "FontManager.h"

#include <sstream>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CFontMapView

IMPLEMENT_DYNCREATE(CFontMapView, CScrollView)

CFontMapView::CFontMapView()
 : m_nToolTipWidth(255),
   m_pCurrentMap(NULL),
   m_nCurrentMapLength(NULL),
   m_nCurrentEncodingID(0),
   m_nScrollbarWidth(::GetSystemMetrics(SM_CXVSCROLL)),
   m_clrHot(RGB(192, 192, 192)),
   m_clrPressed(RGB(128, 128, 128)),
   m_nMouseOverIndex(0),
   m_nSelectedIndex(0),
   m_nColumns(0),
   m_bTrackingMouse(false),
   m_bMouseDown(false),
   m_nRButtonDownIndex(-1),
   m_nContextMenuIndex(-1),
   m_pCharDetailWnd(NULL),
   m_bShowingPopupMenu(false),
   m_nFontHeight(theApp.GetProgramOptions()->GetFontMapSize()),
   m_bInitialUpdated(false),
   m_sizFullMap(0, 0),
   m_clrForeground(theApp.GetProgramOptions()->GetFontMapFontColor()),
   m_clrBackground(theApp.GetProgramOptions()->GetFontMapFontBackgroundColor())
{
}

CFontMapView::~CFontMapView()
{
}

BOOL CFontMapView::Create(LPCTSTR lpszClassName,
						  LPCTSTR lpszWindowName,
						  DWORD dwStyle,
						  const RECT& rect,
						  CWnd* pParentWnd,
						  UINT nID,
						  CCreateContext* pContext)
{
	CFontRunnerSignals* pSignals = theApp.GetSignals();

	// we need to be notified of a font change
	pSignals->ConnectTo_SelectedFontChange(boost::bind(&CFontMapView::OnSelectedFontChange, this, _1));

	// we also need to be notified of a size change
	pSignals->ConnectTo_FontMapSizeChange(boost::bind(&CFontMapView::OnSizeChange, this, _1));

	// get notified of options change
	pSignals->ConnectTo_OptionsChange(boost::bind(&CFontMapView::OnOptionsChange, this));

	// get notified of unmanage event
	pSignals->ConnectTo_UnManageFontItem(boost::bind(&CFontMapView::OnUnManageFontItem, this, _1));

	// get notified of file deletion
	pSignals->ConnectTo_FontFileDeleted(boost::bind(&CFontMapView::OnFontFileDeleted, this, _1));

	// get notified of color changes
	pSignals->ConnectTo_DetailColorChange(boost::bind(&CFontMapView::OnDetailColorChange, this, _1, _2));

	// remove border from this view
	dwStyle ^= WS_BORDER;

	// return base class version
	return CView::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);
}

void CFontMapView::ResizeScrollArea()
{
	if (!m_pFontItem)
	{
		// set scroll size
		CRect rcClient;
		GetClientRect(&rcClient);
		CSize sizeScroll(rcClient.Width(), rcClient.Height());
		SetScrollSizes(MM_TEXT, sizeScroll);

		// set map size
		m_sizFullMap.SetSize(0, 0);
		return;
	}

	// need to figure out where we are
	CRect rcClient;
	GetClientRect(&rcClient);

	int nWidth = rcClient.Width() - 1;

	BOOL bHasHBar = FALSE, bHasVBar = FALSE;
	CheckScrollBars(bHasHBar, bHasVBar);
	if (!bHasVBar)
		nWidth -= m_nScrollbarWidth;

	CDC* pDC = GetDC();

	// cache font metrics for later use
	int nSavedDC = pDC->SaveDC();

	TEXTMETRIC tm;
	pDC->SelectObject(&m_fntCurrent);
	pDC->GetTextMetrics(&tm);

	// compute size of cell
	const unsigned int nPadding = 4;
	m_sizCell.SetSize(tm.tmMaxCharWidth + (nPadding * 2),
					  tm.tmHeight + (nPadding * 2));

	// how many fit on a row?
	m_nColumns = nWidth / m_sizCell.cx;

	using CruxTechnologies::OpenTypeFontData::CcmapTable;
	const CcmapTable::EncodingRecordList_Type* pMapList = m_pFontItem->GetOpenTypeData()->GetCharMaps();
	m_nCurrentMapLength = pMapList->at(m_nCurrentEncodingID).nMapLength;
	m_pCurrentMap = pMapList->at(m_nCurrentEncodingID).pMap.get();
	int nNumRows = 0;
	if (m_nColumns > 0)
		nNumRows = (m_nCurrentMapLength / m_nColumns) + 1;

	m_sizFullMap.SetSize(m_sizCell.cx * m_nColumns + 1, m_sizCell.cy * nNumRows + 1);

	// set scroll sizes
	SetScrollSizes(MM_TEXT, m_sizFullMap,
		CSize(m_sizFullMap.cx, rcClient.Height()), CSize(m_sizCell.cx, m_sizCell.cy / 3));

	// restore DC state and release
	pDC->RestoreDC(nSavedDC);
	ReleaseDC(pDC);
}

void CFontMapView::OnDraw(CDC* pDC)
{
	CRect rcClient;
	GetClientRect(&rcClient);

	// save DC state
	int nOldDC = pDC->SaveDC();

	if (m_pFontItem)
	{
		unsigned int nWidth = rcClient.Width();
		unsigned int nHeight = rcClient.Height();

		CPoint ptScroll(GetScrollPosition());

		pDC->FillSolidRect(ptScroll.x, ptScroll.y, nWidth, nHeight, m_clrBackground);

		BOOL bHasHBar = FALSE, bHasVBar = FALSE;
		CheckScrollBars(bHasHBar, bHasVBar);

		if (bHasHBar)
			nWidth -= m_nScrollbarWidth;
		if (bHasVBar)
			nHeight -= m_nScrollbarWidth;

		int nStartRow = ptScroll.y / m_sizCell.cy;

		const CPoint ptStart(0, ptScroll.y - (ptScroll.y % m_sizCell.cy));
		CPoint ptCurrent(ptStart);
		CSize m_sizCellMid(m_sizCell.cx / 2, m_sizCell.cy / 2);

		// create memory DC
		CDC memoryDC;
		memoryDC.CreateCompatibleDC(pDC);

		int nSavedMemoryDC = memoryDC.SaveDC();

		memoryDC.SetBkMode(TRANSPARENT);

		// set foreground text color
		memoryDC.SetTextColor(m_clrForeground);

		// select font
		memoryDC.SelectObject(&m_fntCurrent);

		// create cell cache bitmap
		CBitmap cellBitmap;
		cellBitmap.CreateCompatibleBitmap(pDC, m_sizCell.cx, m_sizCell.cy);

		memoryDC.SelectObject(&cellBitmap);

		unsigned int nCurrentVisibleRow = 0, nCurrentCol = 0;
		for (uint16_t nCharIndex = (uint16_t)(nStartRow * m_nColumns + 1); nCharIndex < m_nCurrentMapLength; ++nCharIndex)
		{
			// draw cell into memory buffer
			DrawCell(&memoryDC, nCharIndex);

			// blit it to screen
			pDC->BitBlt(ptCurrent.x, ptCurrent.y, m_sizCell.cx, m_sizCell.cy, &memoryDC, 0, 0, SRCCOPY);

			// move to next cell
			++nCurrentCol;
			if (nCurrentCol == m_nColumns)
			{
				if (nCurrentVisibleRow && ((nCurrentVisibleRow - 1) * m_sizCell.cy > nHeight))
					break;
			
				++nCurrentVisibleRow;
				nCurrentCol = 0;
			}

			ptCurrent.SetPoint(m_sizCell.cx * nCurrentCol, ptStart.y + (m_sizCell.cy * nCurrentVisibleRow));
		}

		memoryDC.RestoreDC(nSavedMemoryDC);
	}
	else
	{
		pDC->FillSolidRect(rcClient, m_clrBackground);

		// display a message indicating there is no font selected
		CFont fntMessage;
		fntMessage.CreateFont(25, 0, 0, 0, FW_NORMAL,
							  FALSE, FALSE, FALSE, ANSI_CHARSET,
							  OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
							  DEFAULT_QUALITY, FF_DONTCARE,
							  _T("Tahoma"));

		// select font, color and background mode
		pDC->SelectObject(&fntMessage);
		pDC->SetTextColor(RGB(0x80, 0x80, 0x80));
		pDC->SetBkMode(TRANSPARENT);

		CString strNoFont;
		strNoFont.LoadString(IDS_FONTMAPVIEW_NOSELECTION);
		pDC->DrawText(strNoFont, rcClient,
					  DT_SINGLELINE | DT_NOPREFIX | DT_CENTER | DT_VCENTER);
	}

	// restore DC state
	pDC->RestoreDC(nOldDC);
}

void CFontMapView::PreSubclassWindow()
{
	// enable tool tips
	EnableToolTips();

	CScrollView::PreSubclassWindow();
}

INT_PTR CFontMapView::OnToolHitTest(CPoint /*point*/, TOOLINFO* pTI) const
{
	if (!m_pFontItem)
		return 0;

	CPoint ptScroll = GetScrollPosition();
	CRect rcCell = MapCell(m_nMouseOverIndex);
	
	rcCell.left -= ptScroll.x;
	rcCell.right -= ptScroll.x;
	rcCell.top -= ptScroll.y;
	rcCell.bottom -= ptScroll.y;

	pTI->hwnd = m_hWnd;
	pTI->lpszText = LPSTR_TEXTCALLBACK;
	pTI->rect = rcCell;
	pTI->uId = m_nMouseOverIndex;

	return m_nMouseOverIndex;
}

uint16_t CFontMapView::HitTest(const CPoint& pt, RECT* pRect) const
{
	ASSERT(pRect);

	if (pt.x >= m_sizFullMap.cx - 1 || pt.y >= m_sizFullMap.cy - 1)
		return 0;

	// which row are we on?
	unsigned int nRow = pt.y / m_sizCell.cy;

	// which column are we on?
	unsigned int nColumn = pt.x / m_sizCell.cx;

	pRect->left = m_sizCell.cx * nColumn;
	pRect->top = m_sizCell.cy * nRow;
	pRect->right = pRect->left + m_sizCell.cx;
	pRect->bottom = pRect->top + m_sizCell.cy;

	uint16_t nReturn = (uint16_t)((nRow * m_nColumns) + nColumn + 1);

	if (nReturn >= m_nCurrentMapLength)
		return 0;

	return nReturn;
}

CRect CFontMapView::MapCell(unsigned int nCell) const
{
	int nRow = (nCell - 1) / m_nColumns;
	int nColumn = (nCell - 1) % m_nColumns;

	CPoint pt(nColumn * m_sizCell.cx, nRow * m_sizCell.cy);
	return CRect(pt.x, pt.y, pt.x + m_sizCell.cx, pt.y + m_sizCell.cy);
}

void CFontMapView::DrawCell(CDC* pDC, uint16_t nCharIndex)
{
	CRect rectCurrent(CPoint(0, 0), m_sizCell);

	// draw mouseover/mousedown
	if (nCharIndex == m_nMouseOverIndex)
		pDC->FillSolidRect(&rectCurrent, m_bMouseDown ? m_clrPressed : m_clrHot);
	else
		pDC->FillSolidRect(&rectCurrent, m_clrBackground);

	// make a solid, light gray brush
	CPen gridPen(PS_SOLID, 1, RGB(192, 192, 192));
	CPen* pOldPen = pDC->SelectObject(&gridPen);

	// draw border
	pDC->MoveTo(rectCurrent.left, rectCurrent.bottom - 1);
	pDC->LineTo(rectCurrent.right - 1, rectCurrent.bottom - 1);
	pDC->LineTo(rectCurrent.right - 1, rectCurrent.top -1);

	// draw selection border
	if (nCharIndex == m_nSelectedIndex)
	{
		CPen selectedPen(PS_SOLID, 1, RGB(0, 0, 0));
		pDC->SelectObject(&selectedPen);
		pDC->MoveTo(rectCurrent.left, rectCurrent.top);
		pDC->LineTo(rectCurrent.left, rectCurrent.bottom - 2);
		pDC->LineTo(rectCurrent.right - 2, rectCurrent.bottom - 2);
		pDC->LineTo(rectCurrent.right - 2, rectCurrent.top);
		pDC->LineTo(rectCurrent.left, rectCurrent.top);
		pDC->SelectObject(pOldPen);
	}

	pDC->SelectObject(pOldPen);

	// draw text
	CString strText((TCHAR)m_pCurrentMap[nCharIndex]);
	pDC->DrawText(strText, rectCurrent, DT_CENTER | DT_SINGLELINE | DT_VCENTER | DT_NOPREFIX);
}

void CFontMapView::ChangeCharSelection()
{
	theApp.GetSignals()->Fire_SelectedCodeChange((wchar_t)m_pCurrentMap[m_nSelectedIndex],
		m_pUnicodeData->Get(m_pCurrentMap[m_nSelectedIndex]));
}

std::string CFontMapView::GetRTFFormattedChar(int nIndex) const
{
	// have to convert font name to ANSI
	using CruxTechnologies::OpenTypeFontData::CNameRecord;
	const std::wstring* pstrFontName = m_pFontItem->GetOpenTypeData()->GetName(CNameRecord::kNameID_FullFontName);
	int nFontNameSize = ::WideCharToMultiByte(CP_ACP, 0, pstrFontName->c_str(), -1, NULL, 0, NULL, NULL);
	boost::scoped_array<char> pFontName(new char[nFontNameSize]);
	::WideCharToMultiByte(CP_ACP, 0, pstrFontName->c_str(), -1, pFontName.get(), nFontNameSize, NULL, NULL);

	// make some RTF text
	std::stringstream ssRTF;
	ssRTF << "{\\rtf1{\\fonttbl{\\f0\\fnil\\fcharset0 "
		  << pFontName.get()
		  << ";}}\\pard\\f0\\fs24 \\u"
		  << m_pCurrentMap[nIndex]
		  << "?}" << std::ends;

	return ssRTF.str();
}

void CFontMapView::CreateDisplayFont()
{
	using CruxTechnologies::OpenTypeFontData::CNameRecord;
	const std::wstring* pstrName = m_pFontItem->GetOpenTypeData()->GetName(CNameRecord::kNameID_FullFontName);

	// create font for use in table
	if (m_fntCurrent.m_hObject)
		m_fntCurrent.DeleteObject();

	LOGFONT lf;
	std::memset(&lf, 0, sizeof(LOGFONT));
	lf.lfHeight = m_nFontHeight;
	lf.lfWeight = FW_REGULAR;
	lf.lfCharSet = DEFAULT_CHARSET;
	lf.lfOutPrecision = OUT_DEFAULT_PRECIS;
	lf.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	lf.lfPitchAndFamily = DEFAULT_PITCH | FF_DONTCARE;

	switch (theApp.GetProgramOptions()->GetFontMapRenderingOption())
	{
		case CProgramOptions::kRenderingOption_UseDefault:
			lf.lfQuality = DEFAULT_QUALITY;
			break;
		case CProgramOptions::kRenderingOption_Antialiasing:
			lf.lfQuality = ANTIALIASED_QUALITY;
			break;
		case CProgramOptions::kRenderingOptions_ClearType:
			lf.lfQuality = CLEARTYPE_QUALITY;
			break;
	}

	// font name cannot exceed LF_FACESIZE
	std::wstring::size_type nNameLength = pstrName->length();
	if (nNameLength >= LF_FACESIZE)
		pstrName->_Copy_s(lf.lfFaceName, LF_FACESIZE - 1, LF_FACESIZE - 1);
	else
		pstrName->_Copy_s(lf.lfFaceName, LF_FACESIZE - 1, nNameLength);

	VERIFY(m_fntCurrent.CreateFontIndirect(&lf));

	ResizeScrollArea();
}

void CFontMapView::InvalidateAtIndex(int index, const CPoint& pt)
{
	if (index)
	{
		CRect rect = MapCell(index);
		rect -= pt;
		rect.right += 1;
		rect.bottom += 1;
		InvalidateRect(&rect);
	}
}

UINT CFontMapView::ChangeScrollPosition(int nBar, UINT nSBCode, UINT nPos)
{
	// get scroll info
	SCROLLINFO si;
	GetScrollInfo(nBar, &si, SIF_ALL);

	// save reference to point component depending on which scroll bar is changing
	int nCurrentPos = nPos;

	switch (nSBCode)
	{
		case SB_LEFT: // all the way to the left/top
			nCurrentPos = si.nMin;
			break;
		case SB_RIGHT: // all the way to the right/bottom
			nCurrentPos = si.nMax;
			break;
		case SB_LINELEFT: // line to the left/up
			if (nCurrentPos > si.nMin)
				nCurrentPos = max(si.nMin, nCurrentPos - m_sizCell.cy / 3);
			break;
		case SB_LINERIGHT: // line to the right/down
			if (nCurrentPos < si.nMax)
				nCurrentPos += min(si.nMax, nCurrentPos + m_sizCell.cy / 3);
			break;
		case SB_PAGELEFT: // page left/up
			if (nCurrentPos > si.nMin)
				nCurrentPos = max(si.nMin, nCurrentPos - (int)si.nPage);
			break;
		case SB_PAGERIGHT: // page right/down
			if (nCurrentPos < si.nMax)
				nCurrentPos = min(si.nMax, nCurrentPos + (int)si.nPage);
			break;
		case SB_THUMBPOSITION:
			nCurrentPos = si.nPos;
			break;
		case SB_THUMBTRACK:
			nCurrentPos = si.nTrackPos;
			break;
	}

	return nCurrentPos;
}

void CFontMapView::EnsureVisible(int nIndex)
{
	CRect rectClient, rectItem = MapCell(nIndex);
	GetClientRect(&rectClient);

	rectClient.OffsetRect(GetScrollPosition());

	if (rectItem.bottom > rectClient.bottom)
	{
		SCROLLINFO si;
		si.cbSize = sizeof(SCROLLINFO);
		si.fMask = SIF_POS;
		si.nPos = rectItem.bottom - rectClient.Height();
		SetScrollInfo(SB_VERT, &si);

		Invalidate();
	}
	else if (rectItem.top < rectClient.top)
	{
		SCROLLINFO si;
		si.cbSize = sizeof(SCROLLINFO);
		si.fMask = SIF_POS;
		si.nPos = rectItem.top;
		SetScrollInfo(SB_VERT, &si);

		Invalidate();
	}
}


BEGIN_MESSAGE_MAP(CFontMapView, CScrollView)
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_KEYDOWN()
	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
	ON_COMMAND(ID_FONTMAPMENU_COPYCHARACTER, OnFontMapMenu_CopyCharacter)
	ON_COMMAND(ID_FONTMAPMENU_PREVIEWCHARACTER, OnFontMapMenu_PreviewCharacter)
	ON_COMMAND(ID_FONTMAPMENU_FINDCHARACTER, OnFontMapMenu_FindCharacter)
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTW, 0, 0xFFFF, OnTTNNeedText)
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTA, 0, 0xFFFF, OnTTNNeedText)
END_MESSAGE_MAP()



// CFontMapView drawing

void CFontMapView::OnInitialUpdate()
{
	if (m_bInitialUpdated)
		return;

	CScrollView::OnInitialUpdate();

	// an arbitrary size is fine for now
	CSize sizeTotal;
	sizeTotal.cx = sizeTotal.cy = 100;
	SetScrollSizes(MM_TEXT, sizeTotal);

	// is there a selected font?
	const CFontItem* pFontItem = NULL;
	
	if (CFontRunnerDoc::GetDoc())
		pFontItem = CFontRunnerDoc::GetDoc()->GetCurrentFontItem();

	if (pFontItem)
		OnSelectedFontChange(pFontItem);

	try
	{
		if (!m_pUnicodeData)
			m_pUnicodeData.reset(new CruxTechnologies::CUnicodeCharDescriptions());
	}
	catch (std::bad_alloc&)
	{
		AfxMessageBox(IDS_ERROR_OUTOFMEMORY, MB_ICONERROR + MB_OK);
		return;
	}
	catch (CFileNotFoundException& ex)
	{
		CString strError;
		AfxFormatString1(strError, IDS_ERROR_FILENOTFOUND, ex.GetMessage());
		AfxMessageBox(strError, MB_ICONERROR + MB_OK);
		return;
	}

	// create the character detail window
	if (!m_pCharDetailWnd)
	{
		m_pCharDetailWnd = new CCharDetailWnd();
		m_pCharDetailWnd->Create(this, false);
	}

	m_bInitialUpdated = true;
}


// CFontMapView diagnostics

#ifdef _DEBUG
void CFontMapView::AssertValid() const
{
	CScrollView::AssertValid();
}

#ifndef _WIN32_WCE
void CFontMapView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}
#endif
#endif //_DEBUG


void CFontMapView::OnSelectedFontChange(const CFontItem* pData)
{
	// save pointer to the FontItem
	if (pData)
	{
		m_pFontItem.reset(theApp.GetFontManager()->AddReference(pData),
						  boost::bind(&CFontManager::RemoveReference, theApp.GetFontManager(), _1));

		CreateDisplayFont();
	}
	else
	{
		m_pFontItem.reset();
		ResizeScrollArea();
	}

	Invalidate();
}

void CFontMapView::OnSizeChange(unsigned short nSize)
{
	m_nFontHeight = (int)nSize;
	theApp.GetProgramOptions()->SetFontMapSize(nSize);
	CreateDisplayFont();
	ResizeScrollArea();
	Invalidate();
}

// CFontMapView message handlers
void CFontMapView::OnSize(UINT /*nType*/, int cx, int /*cy*/)
{
	// account for scrollbar width
	cx -= m_nScrollbarWidth;

	int nBigger = m_sizFullMap.cx + m_sizCell.cx;
	int nSmaller = m_sizFullMap.cx - m_sizCell.cx;
	if (cx < nBigger || cx > nSmaller)
		ResizeScrollArea();
	else if (m_pFontItem)
		SetScrollSizes(MM_TEXT, m_sizFullMap);
}

BOOL CFontMapView::OnEraseBkgnd(CDC* /*pDC*/)
{
	// just return FALSE to stop erasing and prevent flicker
	return FALSE;
}

void CFontMapView::OnMouseMove(UINT /*nFlags*/, CPoint point)
{
	int nOldMouseOverIndex = m_nMouseOverIndex;

	CPoint ptScroll = GetScrollPosition();
	point.Offset(ptScroll);

	CRect rectCell;
	m_nMouseOverIndex = HitTest(point, &rectCell);

	if (nOldMouseOverIndex != m_nMouseOverIndex)
	{
		InvalidateAtIndex(nOldMouseOverIndex, ptScroll);
		InvalidateAtIndex(m_nMouseOverIndex, ptScroll);
	}

	if (!m_bTrackingMouse)
	{
		// we want to know when the mouse leaves this view
		std::size_t nTMESize = sizeof(TRACKMOUSEEVENT);
		TRACKMOUSEEVENT tme;
		std::memset(&tme, 0, nTMESize);
		tme.cbSize = (DWORD)nTMESize;
		tme.dwFlags = TME_LEAVE;
		tme.hwndTrack = m_hWnd;
		
		::TrackMouseEvent(&tme);
	}
}

LRESULT CFontMapView::OnMouseLeave(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	CPoint ptScroll = GetScrollPosition();
	int nOldMouseOverIndex = m_nMouseOverIndex;

	// if the mouse was over a character, make sure it is
	// no longer drawn as "hot"
	if (m_nMouseOverIndex && !m_bShowingPopupMenu)
		m_nMouseOverIndex = 0;

	if (nOldMouseOverIndex)
	{
		CRect rectOld = MapCell(nOldMouseOverIndex);
		rectOld -= ptScroll;
		InvalidateRect(&rectOld);
	}

	return 0;
}

void CFontMapView::OnLButtonDown(UINT /*nFlags*/, CPoint /*point*/)
{
	m_bMouseDown = true;
	if (m_nMouseOverIndex)
	{
		int nOldSelectedIndex = m_nSelectedIndex;
		m_nSelectedIndex = m_nMouseOverIndex;
		InvalidateAtIndex(m_nSelectedIndex, GetScrollPosition());
		InvalidateAtIndex(nOldSelectedIndex, GetScrollPosition());
		EnsureVisible(m_nSelectedIndex);
		ChangeCharSelection();
	}
	else if (m_nSelectedIndex)
	{
		ChangeCharSelection();
		m_nSelectedIndex = 0;
	}
}

void CFontMapView::OnLButtonUp(UINT /*nFlags*/, CPoint /*point*/)
{
	m_bMouseDown = false;

	InvalidateAtIndex(m_nSelectedIndex, GetScrollPosition());
}

void CFontMapView::OnRButtonDown(UINT /*nFlags*/, CPoint /*point*/)
{
	m_nRButtonDownIndex = m_nMouseOverIndex;
}

void CFontMapView::OnRButtonUp(UINT /*nFlags*/, CPoint point)
{
	if (m_nRButtonDownIndex == m_nMouseOverIndex && m_nMouseOverIndex != 0)	
	{
		// get coordinates of click
		ClientToScreen(&point);
		
		CMenu menu;
		menu.LoadMenu(IDR_FONTMAP_MENU);

		CMenu *pSubMenu = menu.GetSubMenu(0);

		m_nContextMenuIndex = m_nRButtonDownIndex;
		m_bShowingPopupMenu = true;
		pSubMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,
								 point.x, point.y, this, NULL);
		m_bShowingPopupMenu = false;
	}

	m_nRButtonDownIndex = -1;
}

void CFontMapView::OnKeyDown(UINT nChar, UINT /*nRepCnt*/, UINT /*nFlags*/)
{
	// do nothing if no map
	if (m_pCurrentMap == NULL)
		return;

	CRect rectClient;
	GetClientRect(&rectClient);
	int nRowsVisible = rectClient.Height() / m_sizCell.cy;

	// see if control is down
	SHORT nCtrlDown = ::GetKeyState(VK_CONTROL) & 0x8000;

	// change selection based on arrow key pressed
	int nOldSelection = m_nSelectedIndex;
	switch (nChar)
	{
		case VK_LEFT:
			if (m_nSelectedIndex > 1)
				--m_nSelectedIndex;
			break;
		case VK_RIGHT:
			if (m_nSelectedIndex < (int)(m_nCurrentMapLength - 1))
				++m_nSelectedIndex;
			break;
		case VK_UP:
			if (m_nSelectedIndex / m_nColumns)
				m_nSelectedIndex = m_nSelectedIndex - m_nColumns;
			break;
		case VK_DOWN:
			if (m_nSelectedIndex + m_nColumns < m_nCurrentMapLength)
				m_nSelectedIndex = m_nSelectedIndex + m_nColumns;
			break;
		case VK_NEXT: // page down
			m_nSelectedIndex = min(m_nCurrentMapLength - 1, (int)(m_nSelectedIndex + m_nColumns * nRowsVisible));
			break;
		case VK_PRIOR: // page up
			m_nSelectedIndex = max(1, (int)(m_nSelectedIndex - m_nColumns * nRowsVisible));
			break;
		case VK_HOME: // home
			if (nCtrlDown)
				m_nSelectedIndex = 1;
			else
			{
				if (m_nSelectedIndex % m_nColumns)
					m_nSelectedIndex -= (m_nSelectedIndex % m_nColumns) - 1;
				else
					m_nSelectedIndex -= m_nColumns - 1;
			}
			break;
		case VK_END: // end
			if (nCtrlDown)
				m_nSelectedIndex = m_nCurrentMapLength - 1;
			else
			{
				if (m_nSelectedIndex % m_nColumns)
					m_nSelectedIndex = min(m_nCurrentMapLength - 1, (int)(m_nSelectedIndex + m_nColumns - (m_nSelectedIndex % m_nColumns)));
			}
			break;
	}

	if (nOldSelection != m_nSelectedIndex)
	{
		InvalidateAtIndex(m_nSelectedIndex, GetScrollPosition());
		InvalidateAtIndex(nOldSelection, GetScrollPosition());

		EnsureVisible(m_nSelectedIndex);

		ChangeCharSelection();
	}
}

BOOL CFontMapView::OnTTNNeedText(UINT /*nID*/, NMHDR* pTTTStruct, LRESULT* pResult)
{
	*pResult = 0;

	// make sure there is plenty of room for our tooltip
	::SendMessage(pTTTStruct->hwndFrom, TTM_SETMAXTIPWIDTH, 0, m_nToolTipWidth);

	// need to handle both ANSI and UNICODE versions of the message
	TOOLTIPTEXTA* pTTTA = (TOOLTIPTEXTA*)pTTTStruct;
	TOOLTIPTEXTW* pTTTW = (TOOLTIPTEXTW*)pTTTStruct;

	if (!m_pUnicodeData || !m_pCurrentMap)
		return FALSE;

	const wchar_t* wszDesc = m_pUnicodeData->Get(m_pCurrentMap[m_nMouseOverIndex]);

	CString strText;
	strText.Format(_T("%04X - %s"), m_pCurrentMap[m_nMouseOverIndex], wszDesc);

#ifndef _UNICODE
	if (pTTTStruct->code == TTN_NEEDTEXTA)
		lstrcpyn(pTTTA->szText, strText, 80);
	else
		_mbstowcsz(pTTTW->szText, strText, 80);
#else
	if (pTTTStruct->code == TTN_NEEDTEXTA)
		_wcstombsz(pTTTA->szText, strText, 80);
	else
		lstrcpyn(pTTTW->szText, strText, 80);
#endif

	return FALSE;
}

void CFontMapView::OnOptionsChange()
{
	if (!m_fntCurrent.m_hObject)
		return;

	// get logical font info from old font.  We'll
	// use it to create the new font too.
	LOGFONT lf;
	m_fntCurrent.GetLogFont(&lf);

	// delete old font
	m_fntCurrent.DeleteObject();

	// create font with new quality
	switch (theApp.GetProgramOptions()->GetFontMapRenderingOption())
	{
		case CProgramOptions::kRenderingOption_UseDefault:
			lf.lfQuality = DEFAULT_QUALITY;
			break;
		case CProgramOptions::kRenderingOption_Antialiasing:
			lf.lfQuality = ANTIALIASED_QUALITY;
			break;
		case CProgramOptions::kRenderingOptions_ClearType:
			lf.lfQuality = CLEARTYPE_QUALITY;
			break;
	}

	m_fntCurrent.CreateFontIndirect(&lf);

	Invalidate();
}

int CFontMapView::OnUnManageFontItem(const CFontItem *pFontItem)
{
	if (m_pFontItem.get() == pFontItem)
	{
		m_pFontItem.reset();

		return 1;
	}

	return 0;
}

void CFontMapView::OnFontFileDeleted(const CFontItem* pFontItem)
{
	if (m_pFontItem.get() == pFontItem)
		m_pFontItem.reset();
}

void CFontMapView::OnFontMapMenu_CopyCharacter()
{
	// attemp to open clipboard
	if (!::OpenClipboard(GetSafeHwnd()))
	{
		::AfxMessageBox(IDS_GENERAL_CLIPBOARDOPEN_ERROR);
		return;
	}

	// empty the clipboard
	::EmptyClipboard();

	// get format ID for RTF
	UINT nFormat = ::RegisterClipboardFormat(_T("Rich Text Format"));

	// get RTF
	std::string strRTF = GetRTFFormattedChar(m_nContextMenuIndex);

	// allocate global memory for transfer
	const size_t nRTFSize = strRTF.size() + 1;
	HGLOBAL hRTF = ::GlobalAlloc(GMEM_MOVEABLE | GMEM_DDESHARE | GMEM_ZEROINIT, nRTFSize);

	// stick string in global memory
	char* pszGlobal = static_cast<char*>(::GlobalLock(hRTF));
	strRTF._Copy_s(pszGlobal, nRTFSize, nRTFSize); // use MS "safe" copy
	::GlobalUnlock(hRTF);

	// now put RTF string onto the clipboard
	::SetClipboardData(nFormat, hRTF);
	::GlobalFree(hRTF);

	// also copy as unicode text

	// allocate memory for unicode transfer
	const size_t nUnicodeSize = sizeof(wchar_t) * 2; // enough for a character and a NULL terminator
	HGLOBAL hUnicode = ::GlobalAlloc(GMEM_MOVEABLE | GMEM_DDESHARE | GMEM_ZEROINIT, nUnicodeSize);

	// put unicode character in global memory
	wchar_t* pszUnicodeGlobal = static_cast<wchar_t*>(::GlobalLock(hUnicode));
	pszUnicodeGlobal[0] = m_pCurrentMap[m_nContextMenuIndex];
	::GlobalUnlock(hUnicode);

	// stick unicode text onto the clipboard
	::SetClipboardData(CF_UNICODETEXT, hUnicode);
	::GlobalFree(hUnicode);

	// done with clipboard
	::CloseClipboard();
}

void CFontMapView::OnFontMapMenu_PreviewCharacter()
{
	theApp.GetSignals()->Fire_PreviewRTFCharacter((TCHAR)m_pCurrentMap[m_nContextMenuIndex]);
}

void CFontMapView::OnFontMapMenu_FindCharacter()
{
	CFontRunnerDoc* pDoc = CFontRunnerDoc::GetDoc();

	// create find character object
	CFindCharacterFrameWnd* pFindWindow =
		new CFindCharacterFrameWnd(m_pUnicodeData->Get(m_pCurrentMap[m_nContextMenuIndex]),
								   (wchar_t)m_pCurrentMap[m_nContextMenuIndex],
								   m_pFontItem.get(),
								   pDoc->UsingFontProject() ? pDoc->GetCurrentProject() : CFontRunnerDoc::GetDoc()->GetCurrentFolderNameFQ(),
								   pDoc->UsingFontProject());
	
	// create window
	pFindWindow->Create(theApp.GetMainWnd());

	// the window will do the rest...
}

void CFontMapView::OnDetailColorChange(COLORREF color, bool bForeground)
{
	CProgramOptions* pOptions = theApp.GetProgramOptions();

	// only update if we're in font map mode
	if (pOptions->GetFontDetailMode() != CProgramOptions::kMode_FontMap)
		return;

	if (bForeground)
	{
		m_clrForeground = color;
		pOptions->SetFontMapFontColor(color);
	}
	else
	{
		m_clrBackground = color;
		pOptions->SetFontMapFontBackgroundColor(color);
	}

	Invalidate();
}

void CFontMapView::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	CScrollView::OnVScroll(nSBCode, ChangeScrollPosition(SB_VERT, nSBCode, nPos), pScrollBar);
}

void CFontMapView::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	CScrollView::OnHScroll(nSBCode, ChangeScrollPosition(SB_HORZ, nSBCode, nPos), pScrollBar);
}
