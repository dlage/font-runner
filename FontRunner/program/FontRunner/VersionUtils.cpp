/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"
#include "VersionUtils.h"

#pragma warning(push, 3) // disable some warnings so Boost headers compile cleanly

#include <boost/scoped_ptr.hpp>
#include <boost/scoped_array.hpp>
#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>

#pragma warning(pop)

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace CruxTechnologies;

// returns true if version1 is newer than version2
bool CruxTechnologies::version_newer(const version_type& version1, const version_type& version2)
{
	if (version1.major > version2.major)
		return true;
	else if (version1.major == version2.major)
	{
		// major version is the same, need at least minor to determine
		if (version1.minor > version2.minor)
			return true;
		else if (version1.minor == version2.minor)
		{
			// minor version is the same, need at least release number to determine
			if (version1.release > version2.release)
				return true;
			else if (version1.release == version2.release)
			{
				// the release numbers are the same, it comes down to build number
				if (version1.build > version2.build)
					return true;
				
				// build is less than or equal, this is not a newer release
			}
		}
	}

	return false;
}

void CruxTechnologies::string_to_version(LPCTSTR pszVersion, CruxTechnologies::version_type& version)
{
#ifdef UNICODE
	// convert to multibyte string
	int nSize = ::WideCharToMultiByte(CP_ACP, 0, pszVersion, -1, NULL, 0, NULL, NULL);
	boost::scoped_array<char> pString(new char[nSize]);
	::WideCharToMultiByte(CP_ACP, 0, pszVersion, -1, pString.get(), nSize, NULL, NULL);
	std::string strThisVersion(pString.get());
#else
	std::string strThisVersion((LPCTSTR)cstringThisVersion);
#endif

	// call std::string version
	string_to_version(strThisVersion, version);
}

void CruxTechnologies::string_to_version(const std::string& versionstring, CruxTechnologies::version_type& version)
{
	boost::regex expression("([0-9]+)\\.([0-9]+)\\.([0-9]+)\\.([0-9]+)");
	std::string::const_iterator begin = versionstring.begin();
	std::string::const_iterator end = versionstring.end();
	boost::smatch match;
	boost::match_flag_type flags = boost::match_default;

	// attempt search
	if (boost::regex_search(begin, end, match, expression, flags))
	{
		// matches 1-4 are the version components
		const int nVersionComponents = 4;

		// make strings
		std::vector<std::string> componentstring(nVersionComponents);
		for (int i = 0; i < nVersionComponents; ++i)
			componentstring[i].assign(match[i + 1].first, match[i + 1].second);

		// convert strings
		try
		{
			version.major = boost::lexical_cast<unsigned long>(componentstring[0]);
			version.minor = boost::lexical_cast<unsigned long>(componentstring[1]);
			version.release = boost::lexical_cast<unsigned long>(componentstring[2]);
			version.build = boost::lexical_cast<unsigned long>(componentstring[3]);
		}
		catch (boost::bad_lexical_cast&)
		{
		}
	}
}
