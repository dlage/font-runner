/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// UpdateCheckDlg.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "UpdateAvailableDialog.h"
#include "UpdateCheckDlg.h"

#include "OnlineRelease.h"

#include <boost/bind.hpp>
#include <boost/scoped_array.hpp>

UINT CUpdateCheckDlg::s_rwmProgressUpdate = ::RegisterWindowMessage(_T("d266e020-4fe7-11dd-ae16-0800200c9a66"));
UINT CUpdateCheckDlg::s_rwmFinished = :: RegisterWindowMessage(_T("8427ef30-5023-11dd-ae16-0800200c9a66"));

// CUpdateCheckDlg dialog
IMPLEMENT_DYNAMIC(CUpdateCheckDlg, CDialog)

CUpdateCheckDlg::CUpdateCheckDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUpdateCheckDlg::IDD, pParent),
	  m_strStatusStatic(_T("")),
	  m_bUpdateFound(false),
	  m_bDownloading(false),
	  m_bAborting(false),
	  m_bProgressInitialized(false),
	  m_pDownloadThread(NULL)
{
}

CUpdateCheckDlg::~CUpdateCheckDlg()
{
}

void CUpdateCheckDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_UPDATEICON_STATIC, m_ctrlUpdateCheckIcon);
	DDX_Control(pDX, IDC_CHECKFORUPDATE_BUTTON, m_ctrlCheckForUpdateButton);
	DDX_Text(pDX, IDC_STATUS_STATIC, m_strStatusStatic);
	DDX_Control(pDX, IDC_DOWNLOAD_PROGRESS, m_ctrlDownloadProgress);
	DDX_Control(pDX, IDCANCEL, m_ctrlCancelButton);
}

BOOL CUpdateCheckDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// don't need to clean up icon because static control takes ownership of it
	HICON hIcon = (HICON)::LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_DOWNLOADUPDATE_ICON), IMAGE_ICON, 48, 48, LR_DEFAULTCOLOR);
	m_ctrlUpdateCheckIcon.SetIcon(hIcon);
	m_ctrlUpdateCheckIcon.SetWindowPos(NULL, 0, 0, 48, 48, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOZORDER);

	m_ctrlDownloadProgress.ModifyStyle(0, PBS_MARQUEE);

	m_strStatusStatic.LoadString(IDS_UPDATECHECKDIALOG_INSTRUCTIONS);
	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

COnlineRelease::eProduct GetProduct()
{
	// which product is this?
#if defined _WIN64
	return COnlineRelease::kFontRunner64;
#else
	return COnlineRelease::kFontRunner32;
#endif
}

UINT CUpdateCheckDlg::DownloadRelease(LPVOID pParam)
{
	CUpdateCheckDlg* pDlg = reinterpret_cast<CUpdateCheckDlg*>(pParam);

	// must initialize COM in this thread
	::CoInitialize(NULL);

	// get temp path
	TCHAR szTempPath[MAX_PATH];
	::GetTempPath(MAX_PATH, szTempPath);

	// do download
	CString strCurrentVersion = theApp.GetVersionString();
	COnlineRelease onlinerelease;
	bool bSuccess =
		onlinerelease.DownloadRelease(GetProduct(),
									  strCurrentVersion,
									  szTempPath,
									  boost::bind(&CUpdateCheckDlg::Aborting, pDlg),
									  boost::bind(&CUpdateCheckDlg::OnDownloadProgress, pDlg, _1, _2));

	if (bSuccess)
		pDlg->m_strDownloadedFile = szTempPath;

	// we're done
	pDlg->PostMessage(s_rwmFinished);

	// uninitialize COM in this thread
	::CoUninitialize();

	return 0;
}

void CUpdateCheckDlg::BeginUpdate()
{
	// disable button and change text
	m_ctrlCheckForUpdateButton.EnableWindow(false);
	m_strStatusStatic.LoadString(IDS_UPDATECHECKDIALOG_DOWNLOADING);
	UpdateData(FALSE);

	m_pDownloadThread = ::AfxBeginThread(DownloadRelease, (LPVOID)this);

	// set downloading flag
	m_bDownloading = true;

	// just wait for download to finish
	return;
}


BEGIN_MESSAGE_MAP(CUpdateCheckDlg, CDialog)
	ON_BN_CLICKED(IDC_CHECKFORUPDATE_BUTTON, &CUpdateCheckDlg::OnBnClickedCheckforupdateButton)
	ON_BN_CLICKED(IDCANCEL, &CUpdateCheckDlg::OnBnClickedCancel)
	ON_REGISTERED_MESSAGE(CUpdateCheckDlg::s_rwmProgressUpdate, CUpdateCheckDlg::OnProgressMessage)
	ON_REGISTERED_MESSAGE(CUpdateCheckDlg::s_rwmFinished, CUpdateCheckDlg::OnFinishedMessage)
END_MESSAGE_MAP()


// CUpdateCheckDlg message handlers

void CUpdateCheckDlg::OnBnClickedCheckforupdateButton()
{
	m_bProgressInitialized = false;

	if (m_bUpdateFound)
		BeginUpdate();
	else
	{
		CWaitCursor wait;

		// change UI while user is waiting
		m_strStatusStatic.LoadString(IDS_UPDATECHECKDIALOG_CHECKING);
		UpdateData(FALSE);

		m_ctrlCheckForUpdateButton.EnableWindow(false);
		bool bReEnableUpdateButton = true;

		version_info_type version_info;
		COnlineRelease onlinerelease;
		if (onlinerelease.ObtainCurrentReleaseInfo(GetProduct(), version_info))
		{
			CString strThisVersion = theApp.GetVersionString();

			CruxTechnologies::version_type thisversion;
			CruxTechnologies::string_to_version(strThisVersion, thisversion);

			if (CruxTechnologies::version_newer(version_info.version, thisversion))
			{
				// get latest version
				CUpdateAvailableDialog dlg(version_info.strQuickInfoURL);
				if (dlg.DoModal() == IDOK)
					BeginUpdate();
				else
					EndDialog(IDCANCEL);
			}
			else
			{
				m_strStatusStatic.LoadString(IDS_UPDATECHECKDIALOG_UPTODATE);
				
				CString strButtonText;
				strButtonText.LoadString(IDS_UPDATECHECKDIALOG_CLOSE);
				m_ctrlCancelButton.SetWindowText(strButtonText);
			}

			bReEnableUpdateButton = false;
		}
		else
		{
			m_strStatusStatic.LoadString(IDS_UPDATECHECKDIALOG_ERROR);

			CString strButtonText;
			strButtonText.LoadString(IDS_UPDATECHECKDIALOG_TRYAGAIN);
			m_ctrlCheckForUpdateButton.SetWindowText(strButtonText);
		}

		if (bReEnableUpdateButton)
			m_ctrlCheckForUpdateButton.EnableWindow(true);
		
		UpdateData(FALSE);
	}
}

bool CUpdateCheckDlg::Aborting() const
{
	return m_bAborting;
}

void CUpdateCheckDlg::OnDownloadProgress(int nCurrent, int nTotal)
{
	PostMessage(s_rwmProgressUpdate, nCurrent, nTotal);
}

void CUpdateCheckDlg::OnBnClickedCancel()
{
	if (m_bDownloading)
	{
		m_bAborting = true;
		m_ctrlCancelButton.EnableWindow(false);
	}
	else
		OnCancel();
}

LRESULT CUpdateCheckDlg::OnProgressMessage(WPARAM wParam, LPARAM lParam)
{
	// set range if not set
	if (!m_bProgressInitialized)
	{
		m_ctrlDownloadProgress.SetRange32(0, (int)lParam);
		m_ctrlDownloadProgress.ShowWindow(SW_SHOW);
		m_bProgressInitialized = true;

		CComPtr<ITaskbarList> pTaskbarList = NULL;
		if (SUCCEEDED(::CoCreateInstance(CLSID_TaskbarList, NULL, CLSCTX_INPROC_SERVER, IID_ITaskbarList, (void**)&pTaskbarList)))
			m_pTaskbarList = pTaskbarList;

		if (m_pTaskbarList)
			m_pTaskbarList->SetProgressState(AfxGetMainWnd()->GetSafeHwnd(), TBPF_INDETERMINATE);
	}

	m_ctrlDownloadProgress.SetPos((int)wParam);

	return 0L;
}

LRESULT CUpdateCheckDlg::OnFinishedMessage(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	// wait for thread to die
	::WaitForSingleObject(m_pDownloadThread->m_hThread, 5000);
	m_pDownloadThread = NULL;

	// reset downloading flag
	m_bDownloading = false;

	// stop progress
	if (m_pTaskbarList)
		m_pTaskbarList->SetProgressState(AfxGetMainWnd()->GetSafeHwnd(), TBPF_NOPROGRESS);

	// having a downloaded file means success
	if (!m_strDownloadedFile.IsEmpty() && !m_bAborting)
	{
		// tell app to update on shutdown
		theApp.RunUpdaterOnShutdown(m_strDownloadedFile);

		// shut down
		theApp.GetMainWnd()->PostMessage(WM_CLOSE);

		// end this dialog
		OnOK();
	}
	else if (m_bAborting)
		OnCancel();
	else
	{
		// some other error
		m_strStatusStatic.LoadString(IDS_UPDATECHECKDIALOG_ERROR);

		CString strButtonText;
		strButtonText.LoadString(IDS_UPDATECHECKDIALOG_TRYAGAIN);
		m_ctrlCheckForUpdateButton.SetWindowText(strButtonText);
		m_ctrlCheckForUpdateButton.EnableWindow(true);

		UpdateData(FALSE);

		m_ctrlCancelButton.EnableWindow(true);
	}

	return 0L;
}