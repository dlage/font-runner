/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// SizeBox.cpp : implementation file
//

#include "stdafx.h"
#include "SizeBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CSizeBox

IMPLEMENT_DYNAMIC(CSizeBox, CScrollBar)

CSizeBox::CSizeBox() : m_hCursor(NULL)
{
}

CSizeBox::~CSizeBox()
{
	if (m_hCursor)
		::DestroyCursor(m_hCursor);
}

bool CSizeBox::Create(CWnd* pParentWnd, UINT nID)
{
	DWORD dwStyle = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | SBS_SIZEBOX | SBS_SIZEBOXBOTTOMRIGHTALIGN | SBS_SIZEBOXTOPLEFTALIGN;

	CRect rcControl;
	pParentWnd->GetClientRect(&rcControl);

	return (CScrollBar::Create(dwStyle, rcControl, pParentWnd, nID) == TRUE);
}

BEGIN_MESSAGE_MAP(CSizeBox, CScrollBar)
	ON_WM_CREATE()
	ON_WM_SETCURSOR()
END_MESSAGE_MAP()



// CSizeBox message handlers

int CSizeBox::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CScrollBar::OnCreate(lpCreateStruct) == -1)
		return -1;

	// grab icon
	m_hCursor = static_cast<HCURSOR>(::LoadCursor(NULL, IDC_SIZENWSE));

	return 0;
}

BOOL CSizeBox::OnSetCursor(CWnd* /*pWnd*/, UINT /*nHitTest*/, UINT /*message*/)
{
	SetCursor(m_hCursor);

	return TRUE;
}
