/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// FontRunnerDoc.h : interface of the CFontRunnerDoc class
//
#pragma once
#include <string>

class CFontItem;

class CFontRunnerDoc : public CDocument
{
protected: // create from serialization only
	CFontRunnerDoc();
	DECLARE_DYNCREATE(CFontRunnerDoc)

// Attributes
public:
	CString		m_strSystemFontsFolder;

private:
	bool			m_bUseFontProject;
	IShellFolder*	m_pCurrentShellFolder;
	LPITEMIDLIST	m_pCurrentIDL;
	CString			m_strCurrentProject;
	const CFontItem* m_pFontItem;

// Operations
public:
	void SetCurrentFolder(IShellFolder* pShellFolder, LPITEMIDLIST pidl);
	void GetCurrentFolder(IShellFolder** ppShellFolder, LPITEMIDLIST* ppidl) const;
	CString GetCurrentFolderNameFQ() const;
	CString GetCurrentFolderName() const;
	inline const CString& GetCurrentProject() const { return m_strCurrentProject; }
	const CFontItem* GetCurrentFontItem() const;

	// used for getting document from anywhere
	static CFontRunnerDoc* GetDoc();

	// open font search dialog.  If pstrSearchRoot is NULL, the current directory or project is used
	void SearchForFont(LPCTSTR lpszSearchRoot, bool bRootIsProject = false);

	void UpdateTitle();
	bool UsingFontProject() const;
	void UseFontProject(bool bUse);

// signal handlers
private:
	void OnSelectedProjectChange(const std::wstring& strProjectName);
	void OnSelectedFolderChange(const CString& strFolder, const CString* pstrFile);
	void OnSelectedFontChange(const CFontItem* pData);
	void OnFontDereferenced(const CFontItem* pData);
	void OnOptionsChange();

// Overrides
	public:
	virtual BOOL OnNewDocument();
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~CFontRunnerDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual void OnCloseDocument();
	afx_msg void OnViewOptions();
	afx_msg void OnFilePrint();
	afx_msg void OnFilePrintPreview();
	afx_msg void OnViewSearch();
};


