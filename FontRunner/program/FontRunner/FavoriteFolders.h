/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>
#include <string>


class CFavoriteFolders
{
public:
	CFavoriteFolders();
	~CFavoriteFolders();

	typedef boost::function<void (const std::wstring& str)> EnumCallbackType;

public:
	void Load(LPCTSTR szFilename);
	void Save(LPCTSTR szFilename);

	bool Empty() const;
	std::size_t Count() const;
	const std::wstring& GetAt(std::size_t nIndex) const throw(...);
	bool Exists(LPCTSTR szFolderName) const;
	void AddFolder(LPCTSTR szFolderName);
	void RemoveFolder(LPCTSTR szFolderName);
	
	// for folder list iteration
	void ForEach(EnumCallbackType) const;

private:
	struct _impl;
	boost::shared_ptr<_impl> m_pImpl;
	
};

//========================================================================================
class CFavoriteFoldersException
{
public:
	enum eError
	{
		kError_FileNotFound,
		kError_FileInvalid,
		kError_XMLSaveError
	};

	CFavoriteFoldersException(eError nError) : m_nError(nError)
	{}

	eError GetError() const
	{
		return m_nError;
	}

private:
	eError m_nError;
};