/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// ShellTreeView.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "ShellTreeView.h"

#include "FontManager.h"
#include "FontRunnerDoc.h"
#include "FontRunnerSignals.h"
#include "DropSourceHandler.h"
#include "DropTargetHandler.h"
#include "FontRunnerException.h"
#include "HRESULTException.h"
#include "pidlutils.h"
#include "ShellUtils.h"

#pragma warning(push, 3)
#include <boost/lexical_cast.hpp>
#pragma warning(pop)

#include <boost/shared_ptr.hpp>
#include <boost/scoped_array.hpp>
#include <shlwapi.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// item data for each tree item node
// The desktop folder does not have a parent and should be NULL
class CShellTreeItemData
{
public:
	CShellTreeItemData()
		: pidl(NULL),
		  fqpidl(NULL)
	{}

	~CShellTreeItemData()
	{
		if (pidl)
			::CoTaskMemFree(pidl);

		if (fqpidl)
			::CoTaskMemFree(fqpidl);
	}

public:
	CComPtr<IShellFolder>	pParent;	// parent shell folder
	LPITEMIDLIST			pidl;		// relative pidl
	LPITEMIDLIST			fqpidl;
};



// CShellTreeView

IMPLEMENT_DYNCREATE(CShellTreeView, CTreeView)

UINT CShellTreeView::s_uwmChangeNotification = ::RegisterWindowMessage(_T("CDE53232-BFE9-49a3-9D13-8EBC7B0E0FE3"));

CShellTreeView::CShellTreeView()
 : m_hDesktop(NULL),
   m_bInitialUpdateDone(false),
   m_bSelfChange(false),
   m_bDisableFolderChangeSignal(false),
   m_hContextMenuItem(NULL),
   m_pDropTarget(NULL),
   m_pDropDataObject(NULL),
   m_hDropSource(NULL),
   m_hDropTarget(NULL),
   m_bOverValidItem(false),
   m_bShellChangeHandled(false),
   m_nChangeNotifyID(0xFFFFFFFF)
{
}

CShellTreeView::~CShellTreeView()
{
}

// Callback routine for sorting the tree 
int CALLBACK CShellTreeView::TreeViewCompareProc(LPARAM lparam1, LPARAM lparam2, LPARAM /*lparamSort*/)
{
    CShellTreeItemData* pItemData1 = reinterpret_cast<CShellTreeItemData*>(lparam1);
    CShellTreeItemData* pItemData2 = reinterpret_cast<CShellTreeItemData*>(lparam2);

	DWORD dwAttributes = SFGAO_CANMOVE;

	pItemData1->pParent->GetAttributesOf(1, (const struct _ITEMIDLIST **)&pItemData1->pidl, &dwAttributes);

	// if object can be moved, it is is regular filesystem folder, then simply sort by name
	if (dwAttributes & SFGAO_CANMOVE)
	{
		TCHAR szString1[MAX_PATH], szString2[MAX_PATH];

		ShellUtils::GetName(pItemData1->pParent, pItemData1->pidl, SHGDN_INFOLDER | SHGDN_FORPARSING, szString1, MAX_PATH);
		ShellUtils::GetName(pItemData2->pParent, pItemData2->pidl, SHGDN_INFOLDER | SHGDN_FORPARSING, szString2, MAX_PATH);
		return _wcsicmp(szString1, szString2);
	}
	else
	{
		// if object is a special shell object, sort it by comparing IDs
		HRESULT   hr;

		hr = pItemData1->pParent->CompareIDs(0, pItemData1->pidl, pItemData2->pidl);

		if (FAILED(hr))
		   return 0;

		return HRESULT_CODE(hr);
	}
}

void CShellTreeView::DragEnter(IDataObject *pDataObject, DWORD /*grfKeyState*/, POINTL ptl, DWORD* pdwEffect)
{
	if (m_pDropTargetHelper)
	{
		POINT pt = { ptl.x, ptl.y };
		m_pDropTargetHelper->DragEnter(m_hWnd, pDataObject, &pt, *pdwEffect);
	}

	if (m_pDropDataObject)
		m_pDropDataObject->Release();

	// hold onto data object for future DragOver calls
	m_pDropDataObject = pDataObject;
	m_pDropDataObject->AddRef();
}

void CShellTreeView::DragOver(DWORD grfKeyState, POINTL ptl, DWORD *pdwEffect)
{
	if (m_pDropTargetHelper)
	{
		POINT pt = { ptl.x, ptl.y };
		m_pDropTargetHelper->DragOver(&pt, *pdwEffect);
	}

	CPoint ptScreen(ptl.x, ptl.y);
	CPoint ptClient(ptScreen);
	ScreenToClient(&ptClient);

	// if over a new item, get its drop target interface
	UINT nFlags = 0;
	HTREEITEM hTargetItem = GetTreeCtrl().HitTest(ptClient, &nFlags);
	if ((hTargetItem != NULL) && (hTargetItem != m_hDropTarget))
	{
		m_bOverValidItem = true;
		m_hDropTarget = hTargetItem;

		// release old drop target
		if (m_pDropTarget)
		{
			m_pDropTarget->Release();
			m_pDropTarget = NULL;
		}

		// cannot be dropped onto itself or a descendant of itself
		m_bOverValidItem = ((m_hDropSource != m_hDropTarget) &&
							(!IsAncestor(m_hDropSource, m_hDropTarget)));

		if (m_bOverValidItem)
		{
			// figure out if this is valid drop target
			CShellTreeItemData* pTargetItemData =
				reinterpret_cast<CShellTreeItemData*>(GetTreeCtrl().GetItemData(hTargetItem));
			IShellFolder* pTargetShellFolder = pTargetItemData->pParent;

			CComPtr<IShellFolder> pDesktop;

			// no parent means this is the desktop folder
			if (pTargetShellFolder == NULL)	
			{
				::SHGetDesktopFolder(&pDesktop);
				pTargetShellFolder = pDesktop;
			}

#ifdef _DEBUG
			TCHAR szName[MAX_PATH];
			ShellUtils::GetName(pTargetShellFolder, pTargetItemData->pidl, SHGDN_NORMAL, szName, MAX_PATH);
			CString strDebug;
			strDebug.Format(_T("over %s\n"), szName);
			OutputDebugString(strDebug);
#endif

			HRESULT hResult =
				pTargetShellFolder->GetUIObjectOf(
					::GetParent(GetTreeCtrl().GetSafeHwnd()),
					1,
					(const struct _ITEMIDLIST **)&pTargetItemData->pidl,
					IID_IDropTarget,
					0,
					(void**)&m_pDropTarget);

			if (FAILED(hResult))
				m_bOverValidItem = false;

			// with this new drop target, call DragEnter
			if (m_bOverValidItem)
			{
				m_pDropTarget->DragEnter(m_pDropDataObject, grfKeyState, ptl, pdwEffect);

				if (*pdwEffect == DROPEFFECT_NONE)
					GetTreeCtrl().SelectDropTarget(NULL);
				else
					GetTreeCtrl().SelectDropTarget(hTargetItem);
			}
		}
	}

	if (m_pDropTarget && m_bOverValidItem)
	{
		m_pDropTarget->DragOver(grfKeyState, ptl, pdwEffect);

		if (*pdwEffect == DROPEFFECT_NONE)
			GetTreeCtrl().SelectDropTarget(NULL);
		else
			GetTreeCtrl().SelectDropTarget(hTargetItem);
	}

	if (!m_bOverValidItem)
	{
		*pdwEffect = DROPEFFECT_NONE;
		GetTreeCtrl().SelectDropTarget(NULL);
	}
}

void CShellTreeView::DragLeave()
{
	if (m_pDropTargetHelper)
		m_pDropTargetHelper->DragLeave();

	DragCleanup();
}

void CShellTreeView::Drop(IDataObject* pDataObject, DWORD grfKeyState, POINTL ptl, DWORD *pdwEffect)
{
	try
	{
		if (m_pDropTargetHelper)
		{
			POINT pt = { ptl.x, ptl.y };
			m_pDropTargetHelper->Drop(pDataObject, &pt, *pdwEffect);
		}

		if (m_pDropTarget)
			m_pDropTarget->Drop(pDataObject, grfKeyState, ptl, pdwEffect);

		// this view's automatic shell notification mechanism will take care of the changes
	}
	catch (CFontRunnerException&)
	{
	}
	catch (CHRESULTException&)
	{
	}

	DragCleanup();
}

void CShellTreeView::DragCleanup()
{
	if (m_pDropTarget)
	{
		m_pDropTarget->Release();
		m_pDropTarget = NULL;
	}

	if (m_pDropDataObject)
	{
		m_pDropDataObject->Release();
		m_pDropDataObject = NULL;
	}

	GetTreeCtrl().SelectDropTarget(NULL);

	m_hDropSource = NULL;
	m_hDropTarget = NULL;
	m_bOverValidItem = false;
	Invalidate();
}

bool CShellTreeView::IsAncestor(HTREEITEM hParent, HTREEITEM hChild) const
{
	// see if hParent is an ancestor of hChild
	HTREEITEM hTest = GetTreeCtrl().GetParentItem(hChild);

	while (hTest)
	{
		if (hTest == hParent)
			return true;

		// move up one more level
		hTest = GetTreeCtrl().GetParentItem(hTest);
	}

	// not found
	return false;
}

bool CShellTreeView::FolderDropped(IDataObject* pDataObject) const
{
	// get filenames from dropped content
	FORMATETC fmt;
	fmt.cfFormat = CF_HDROP;
	fmt.tymed = TYMED_HGLOBAL;
	fmt.dwAspect = DVASPECT_CONTENT;

	STGMEDIUM stgm;
	pDataObject->GetData(&fmt, &stgm);

	// the DragQueryFile function will help us get the filenames
	UINT nFileCount = ::DragQueryFile((HDROP)stgm.hGlobal, 0xFFFFFFFF, NULL, 0);

	if (nFileCount == 0xFFFFFFFF)
		throw CFontRunnerException(CFontRunnerException::kCode_UnexpectedData, _T("Non-files or folders were dropped on the tree"));

	for (UINT nFile = 0; nFile < nFileCount; ++nFile)
	{
		// get size of file name
		UINT nSize = ::DragQueryFile((HDROP)stgm.hGlobal, nFile, NULL, 0);
		boost::scoped_array<TCHAR> pszFileName(new TCHAR[nSize + 1]);

		// get filename
		::DragQueryFile((HDROP)stgm.hGlobal, nFile, pszFileName.get(), nSize + 1);

		if (::PathIsDirectory(pszFileName.get()))
			return true;
	}

	return false;
}

void CShellTreeView::OnFolderUp()
{
	// get current tree item
	HTREEITEM hItem = GetTreeCtrl().GetSelectedItem();

	// get parent tree item
	HTREEITEM hParent = GetTreeCtrl().GetParentItem(hItem);

	// if parent exists, go to it
	if (hParent)
		GetTreeCtrl().Select(hParent, TVGN_CARET);
}

bool CShellTreeView::CurrentFolderHasParent() const
{
	// returns true if the current folder has a parent
	
	// get current tree item
	HTREEITEM hItem = GetTreeCtrl().GetSelectedItem();

	return (GetTreeCtrl().GetParentItem(hItem) != NULL);
}

void CShellTreeView::OnRefresh()
{
	CComPtr<IShellFolder> pDesktop;
	::SHGetDesktopFolder(&pDesktop);

	// refresh starting with desktop
	Refresh(m_hDesktop, pDesktop);
}

// to store child pidls
struct childpidl_status_type
{
	bool bHasSubFolder;
	bool bOnTree;
};

typedef std::map<LPITEMIDLIST, childpidl_status_type> childpidls;
struct find_childpidl
{
	find_childpidl(IShellFolder* pParent, LPITEMIDLIST pidl)
		: m_pParent(pParent), m_pidl(pidl)
	{}

	bool operator()(const childpidls::value_type& item) const
	{
		HRESULT hResult = m_pParent->CompareIDs(0, item.first, m_pidl);

		if (FAILED(hResult))
			return false;
		else
			return (HRESULT_CODE(hResult) == 0);
	}

private:
	IShellFolder* m_pParent;
	LPITEMIDLIST m_pidl;
};

struct save_new_pidls
{
	save_new_pidls(std::vector<LPITEMIDLIST>* pList) : m_pList(pList)
	{}

	void operator()(const childpidls::value_type& item) const
	{
		if (!item.second.bOnTree)
			m_pList->push_back(item.first);
	}

private:
	std::vector<LPITEMIDLIST>* m_pList;
};

void CShellTreeView::Refresh(HTREEITEM hItem, IShellFolder* pParent)
{
	// for convenience
	CTreeCtrl& tree = GetTreeCtrl();

	// make list of child pidls
	childpidls pidls;

	CComPtr<IEnumIDList> pEnumIDList;
	if (SUCCEEDED(pParent->EnumObjects(NULL, SHCONTF_FOLDERS, &pEnumIDList)))
	{
		LPITEMIDLIST pidl = NULL;
		DWORD dwFetched = 0;
		while (pEnumIDList->Next(1, &pidl, &dwFetched) == S_OK)
		{
			// make sure this is really a folder
			SFGAOF dwAttributes = SFGAO_HASSUBFOLDER | SFGAO_FOLDER;
			pParent->GetAttributesOf(1, (const struct _ITEMIDLIST **)&pidl, &dwAttributes);

			if (dwAttributes & SFGAO_FOLDER)
			{
				childpidl_status_type status = { ((dwAttributes & SFGAO_HASSUBFOLDER) != 0), false };
				pidls[pidl] = status;
			}
		}

		// look through list of children we know about
		HTREEITEM hChild = tree.GetNextItem(hItem, TVGN_CHILD);
		while (hChild)
		{
			// get item info
			TVITEM item;
			item.mask = TVIF_CHILDREN | TVIF_HANDLE | TVIF_PARAM | TVIF_STATE;
			item.hItem = hChild;
			tree.GetItem(&item);

			// We have everything we need from this item, so move onto the next one
			hChild = tree.GetNextSiblingItem(hChild);

			CShellTreeItemData* pItemData = reinterpret_cast<CShellTreeItemData*>(item.lParam);

			// make sure this item still exists
			childpidls::iterator itFound =
				std::find_if(pidls.begin(), pidls.end(),
					find_childpidl(pParent, pItemData->pidl));

			// item no longer exists, delete it from the tree
			if (itFound == pidls.end())
				tree.DeleteItem(item.hItem);
			else
			{
				// item is on the tree already
				itFound->second.bOnTree = true;

				if (item.state & TVIS_EXPANDEDONCE)
				{
					if (itFound->second.bHasSubFolder)
					{
						CComPtr<IShellFolder> pChildFolder;
						if (SUCCEEDED(pParent->BindToObject(pItemData->pidl, NULL, IID_IShellFolder, (void**)&pChildFolder)))
							Refresh(item.hItem, pChildFolder);
					}
					else
					{
						// there are no longer children, so delete them all
						HTREEITEM hCurrent = tree.GetNextItem(item.hItem, TVGN_CHILD);
						while (hCurrent)
						{
							HTREEITEM hDelete = hCurrent;

							// move to next item before deleting it
							hCurrent = tree.GetNextSiblingItem(hCurrent);

							// delete it
							tree.DeleteItem(hDelete);
						}

						// make sure expander button goes too
						SetExpander(item.hItem, false);
					}
				}

				// Item still exists; are there children?
				else if (item.cChildren && !itFound->second.bHasSubFolder)
				{
					// tree thinks there are unexpanded children, but there aren't
					SetExpander(item.hItem, false);
				}
				else if ((item.cChildren == 0) && itFound->second.bHasSubFolder)
				{
					// there are subfolders, but tree thinks there are no children
					SetExpander(item.hItem, true);
				}
			}
		}

		// make a list of pidls that are not on the tree
		std::vector<LPITEMIDLIST> newpidls;
		std::for_each(pidls.begin(), pidls.end(), save_new_pidls(&newpidls));

		// get item info
		TVITEM item;
		item.mask = TVIF_CHILDREN | TVIF_HANDLE | TVIF_PARAM;
		item.hItem = hItem;
		tree.GetItem(&item);

		// we need the item data of this item
		CShellTreeItemData* pData = reinterpret_cast<CShellTreeItemData*>(item.lParam);

		// insert new pidls
		std::for_each(newpidls.begin(), newpidls.end(),
			boost::bind(&CShellTreeView::InsertChildItem, this, hItem, pParent, _1, pData->fqpidl, true));

		// make sure expander is set
		if (pidls.size() && item.cChildren == 0)
		{
			item.mask = TVIF_CHILDREN | TVIF_HANDLE;
			item.hItem = hItem;
			item.cChildren = (int)pidls.size();
			tree.SetItem(&item);
		}
	}
}

void CShellTreeView::SetExpander(HTREEITEM hItem, bool bVisible)
{
	// remove the expander button
	TVITEM item;
	item.mask = TVIF_CHILDREN | TVIF_HANDLE;
	item.hItem = hItem;
	item.cChildren = bVisible ? 1 : 0;
	GetTreeCtrl().SetItem(&item);
}

HTREEITEM CShellTreeView::InsertChildItem(HTREEITEM hParent,
										  IShellFolder* pParent,
										  LPITEMIDLIST pidl,
										  LPITEMIDLIST fqpidl_parent,
										  bool bSort /* = false*/)
{
	// return item
	HTREEITEM hReturn = NULL;

	CComPtr<IShellFolder> pShellFolder;
	if (pParent)
		pShellFolder = pParent;
	else
		::SHGetDesktopFolder(&pShellFolder);

	// we're looking for folders and want to know if it has sub-folders
	DWORD dwAttributes = SFGAO_HASSUBFOLDER | SFGAO_FOLDER;
	HRESULT hResult = pShellFolder->GetAttributesOf(1, (const struct _ITEMIDLIST **)&pidl, &dwAttributes);
	if (SUCCEEDED(hResult) && (dwAttributes & (SFGAO_HASSUBFOLDER | SFGAO_FOLDER)))
	{
		// only add folders to the tree
		if (dwAttributes & SFGAO_FOLDER)
		{
			TV_ITEM	tvi;
			ZeroMemory(&tvi, sizeof(TV_ITEM));
			tvi.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_PARAM;
			
			if (dwAttributes & SFGAO_HASSUBFOLDER)
			{
				tvi.cChildren = 1;
				tvi.mask |= TVIF_CHILDREN;
			}

			CShellTreeItemData* pItemData = new CShellTreeItemData();
			tvi.lParam = (LPARAM)pItemData;

			// Get friendly name to put in tree view control
			TCHAR szName[MAX_PATH];
			ShellUtils::GetName(pShellFolder, pidl, SHGDN_NORMAL, szName, MAX_PATH);
			tvi.pszText = szName;
			tvi.cchTextMax = MAX_PATH;

			// save parent shell folder
			pItemData->pParent = pParent;
			pItemData->pidl = pidl;
			pItemData->fqpidl = pidlutils::concatpidls(fqpidl_parent, pidl);

			// get normal icon
			SHFILEINFO sfi;
			::SHGetFileInfo((LPCWSTR)pItemData->fqpidl, 0, &sfi, sizeof(SHFILEINFO),
							SHGFI_PIDL | SHGFI_SYSICONINDEX | SHGFI_SMALLICON);
			tvi.iImage = sfi.iIcon;

			// get selected icon
			::SHGetFileInfo((LPCWSTR)pItemData->fqpidl, 0, &sfi, sizeof(SHFILEINFO),
							SHGFI_PIDL | SHGFI_SYSICONINDEX | SHGFI_SMALLICON | SHGFI_OPENICON);
			tvi.iSelectedImage = sfi.iIcon;

			// insert tree item
			TV_INSERTSTRUCT	tvis;
			ZeroMemory(&tvis, sizeof(TV_INSERTSTRUCT));
			tvis.item = tvi;
			tvis.hInsertAfter = bSort ? TVI_SORT : TVI_FIRST;
			tvis.hParent = hParent;

			if (::ILIsEqual(m_pidlControlPanel.get(), pItemData->fqpidl) ||
				::ILIsEqual(m_pidlInternet.get(), pItemData->fqpidl))
				delete pItemData;
			else
				hReturn = GetTreeCtrl().InsertItem(&tvis);
		}
	}

	return hReturn;
}

bool CShellTreeView::ShowContextMenu(HTREEITEM hItem, LPSHELLFOLDER lpsfParent, LPITEMIDLIST lpi, const CPoint& point)
{
	SFGAOF rgf = SFGAO_FILESYSTEM | SFGAO_CANCOPY | SFGAO_CANDELETE |
				 SFGAO_CANMOVE | SFGAO_CANRENAME | SFGAO_HASSUBFOLDER;

	CComPtr<IShellFolder> pParent(lpsfParent);

	if (!pParent)
		::SHGetDesktopFolder(&pParent);

	HRESULT hResult = pParent->GetAttributesOf(1, (const struct _ITEMIDLIST **)&lpi, &rgf);

	CMenu* pMenu = m_menuPopup.GetSubMenu(0);
	CString strText;

	// save tree item handle for object right-clicked on
	m_hContextMenuItem = hItem;

	// customize menu for this object
	if (SUCCEEDED(hResult))
	{
		if (rgf & SFGAO_HASSUBFOLDER)
		{
			UINT nExpandedState = GetTreeCtrl().GetItemState(hItem, TVIS_EXPANDED);
			strText.LoadString((nExpandedState & TVIS_EXPANDED) ? IDS_EXPLORERVIEW_COLLAPSE : IDS_EXPLORERVIEW_EXPAND);

			MENUITEMINFO mii;
			::memset(&mii, 0, sizeof(MENUITEMINFO));
			mii.cbSize = sizeof(MENUITEMINFO);
			mii.fMask = MIIM_STATE | MIIM_STRING;
			mii.fState = MFS_DEFAULT;
			mii.cch = strText.GetLength();
			mii.dwTypeData = strText.GetBuffer();
			pMenu->SetMenuItemInfo(ID_FOLDERMENU_EXPAND, &mii, FALSE);
			strText.ReleaseBuffer();
		}
		else
			// no subfolder, disable expand/collapse item
			pMenu->EnableMenuItem(ID_FOLDERMENU_EXPAND, MF_BYCOMMAND | MF_GRAYED);

		UINT nEnable = (rgf & SFGAO_FILESYSTEM) ? MF_ENABLED : MF_GRAYED;
		pMenu->EnableMenuItem(ID_FOLDERMENU_SEARCHFORFONTS, MF_BYCOMMAND | nEnable);
		pMenu->EnableMenuItem(ID_FOLDERMENU_CREATENEWFOLDER, MF_BYCOMMAND | nEnable);

		nEnable = (rgf & SFGAO_CANDELETE) ? MF_ENABLED : MF_GRAYED;
		pMenu->EnableMenuItem(ID_FOLDERMENU_DELETE, MF_BYCOMMAND | nEnable);

		nEnable = (rgf & SFGAO_CANRENAME) ? MF_ENABLED : MF_GRAYED;
		pMenu->EnableMenuItem(ID_FOLDERMENU_RENAME, MF_BYCOMMAND | nEnable);
	}

	return (pMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,
								  point.x, point.y, this) != 0);
}

HRESULT CShellTreeView::InvokeContextMenuCommand(LPCSTR lpCommand) const
{
	HTREEITEM hItem = NULL;

	if (m_hContextMenuItem)
		hItem = m_hContextMenuItem;
	else
		hItem = GetTreeCtrl().GetSelectedItem();

	// get item data
	CShellTreeItemData* pItemData =
			reinterpret_cast<CShellTreeItemData*>(GetTreeCtrl().GetItemData(hItem));

	IContextMenu* pContextMenu = NULL;
	CComPtr<IShellFolder> pParent(pItemData->pParent);

	if (!pParent)
		::SHGetDesktopFolder(&pParent);

	HRESULT hResult = pParent->GetUIObjectOf(GetSafeHwnd(), 1, (const struct _ITEMIDLIST**)&pItemData->pidl,
											 IID_IContextMenu, NULL, (void**)&pContextMenu);

	// if we successfully got an IContextMenu interface, use it to
	// invoke whichever command was passed-in
	if (SUCCEEDED(hResult))
	{
		CMINVOKECOMMANDINFO ici;
		memset(&ici, 0, sizeof(CMINVOKECOMMANDINFO));
		ici.cbSize = sizeof(CMINVOKECOMMANDINFO);
		ici.hwnd = GetSafeHwnd();
		ici.lpVerb = lpCommand;
		ici.nShow = SW_SHOWNORMAL;
		hResult = pContextMenu->InvokeCommand(&ici);

		pContextMenu->Release();
	}

	return hResult;
}

void CShellTreeView::FillNode(HTREEITEM hNode)
{
	// make sure there are no children before filling
	HTREEITEM hChild = GetTreeCtrl().GetNextItem(hNode, TVGN_CHILD);
	while (hChild)
	{
		GetTreeCtrl().DeleteItem(hChild);
		hChild = GetTreeCtrl().GetNextItem(hNode, TVGN_CHILD);
	}

	// get item data
	CShellTreeItemData* pItemData =
		reinterpret_cast<CShellTreeItemData*>(GetTreeCtrl().GetItemData(hNode));

	// if shell folder is NULL, then we're dealing the with deskop object
	CComPtr<IShellFolder> pParent;
	if (pItemData->pParent)
		pItemData->pParent->BindToObject(pItemData->pidl, 0, IID_IShellFolder, (void**)&pParent);
	else
		::SHGetDesktopFolder(&pParent);

	CComPtr<IEnumIDList> pEnumIDList;
	if (SUCCEEDED(pParent->EnumObjects(theApp.GetMainWnd()->GetSafeHwnd(), SHCONTF_FOLDERS, &pEnumIDList)))
	{
		LPITEMIDLIST pItemIDList = NULL;
		while (pEnumIDList->Next(1, &pItemIDList, NULL) == S_OK)
			InsertChildItem(hNode, pParent, pItemIDList, pItemData->fqpidl);
	}
}

void CShellTreeView::CreateDragBitmap(const POINT& pt, SHDRAGIMAGE* pshdi)
{
	CPoint ptClient(pt);
	ScreenToClient(&ptClient);

	// get item being dragged
	HTREEITEM hItem = GetTreeCtrl().HitTest(ptClient);

	if (!hItem)
		return;

	// get item rect
	CRect rcItem;
	GetTreeCtrl().GetItemRect(hItem, &rcItem, TRUE);
	
	// widen to get folder too.
	// (this is bad, but I think we can rely on the icon size being 16x16 w/ 3-pixel padding)
	rcItem.left -= 19;

	// get DC
	CDC* pDC = GetDC();
	
	// create a compatible DC to draw into
	CDC dcMemory;
	dcMemory.CreateCompatibleDC(pDC);

	// create a bitmap
	CBitmap bmpImage;
	bmpImage.CreateCompatibleBitmap(pDC, rcItem.Width(), rcItem.Height());

	// select it into our memory DC
	CBitmap* pOldBitmap = dcMemory.SelectObject(&bmpImage);

	// paint background
	COLORREF clrMask = RGB(0xFF, 0x00, 0xFF);
	dcMemory.FillSolidRect(&rcItem, clrMask);

	dcMemory.BitBlt(0, 0, rcItem.Width(), rcItem.Height(), pDC,
					rcItem.left, rcItem.top, SRCCOPY);

	// have to deselect the image
	dcMemory.SelectObject(pOldBitmap);

	// done, release DC
	ReleaseDC(pDC);

	CPoint ptDrag(ptClient.x - rcItem.left, ptClient.y - rcItem.top);

	// fill out drag image struct
	pshdi->sizeDragImage.cx = rcItem.Width();
	pshdi->sizeDragImage.cy = rcItem.Height();
	pshdi->ptOffset = ptDrag;
	pshdi->hbmpDragImage = (HBITMAP)bmpImage.Detach();
	pshdi->crColorKey = clrMask;
}

HTREEITEM CShellTreeView::FindTreeItem(HTREEITEM hRoot, LPSHELLFOLDER pShellFolder, LPCITEMIDLIST pidl)
{
	// returns a child of hRoot that contains a PIDL matching pidl

	// NULL in, NULL out
	if (!pShellFolder || !pidl)
		return NULL;

	// get first child item
	HTREEITEM hTestItem = GetTreeCtrl().GetChildItem(hRoot);

	while (hTestItem)
	{
		// get tree view item data from this item
		CShellTreeItemData* pItemData =
			reinterpret_cast<CShellTreeItemData*>(GetTreeCtrl().GetItemData(hTestItem));

		if (pidlutils::pidls_are_equal(pShellFolder, pItemData->pidl, pidl))
			return hTestItem;

		// move to next item
		hTestItem = GetTreeCtrl().GetNextItem(hTestItem, TVGN_NEXT);
	}

	// loop finished without finding anything, just return NULL
	return NULL;
}

HTREEITEM CShellTreeView::NavigateToFolder(const CString& strFolder)
{
	HTREEITEM hReturn = NULL;

	// can't do anything if file does not exist
	if (!::PathFileExists(strFolder))
		return NULL;

	try
	{
		CheckResultFunctor CheckResult;

		CComPtr<IShellFolder> pShellFolder;
		CheckResult(::SHGetDesktopFolder(&pShellFolder));

		// convert path into PIDL
		pidl_ptr pidlFolder;
		CheckResult(pShellFolder->ParseDisplayName(theApp.GetMainWnd()->GetSafeHwnd(),
												   NULL,
												   (LPOLESTR)(LPCTSTR)strFolder,
												   NULL,
												   &pidlFolder,
												   NULL));

		LPITEMIDLIST pidlParse = pidlFolder.get();

		// now that we have a fully-qualified PIDL, walk the tree until we get where we want
		HTREEITEM hRoot = m_hDesktop;

		bool bDone = false, bSuccess = false;
		while (!bDone)
		{
			// make sure item is expaneded
			GetTreeCtrl().Expand(hRoot, TVE_EXPAND);

			HTREEITEM hItem = FindTreeItem(hRoot, pShellFolder, pidlParse);

			if (hItem)
			{
				// a temporary raw pointer to a new shell folder
				IShellFolder* pRawSF = NULL;
				
				// get new item's data
				CShellTreeItemData* pItemData =
					reinterpret_cast<CShellTreeItemData*>(GetTreeCtrl().GetItemData(hItem));

				// get shell folder for new item
				CheckResult(pShellFolder->BindToObject(pItemData->pidl, NULL, IID_IShellFolder, (void**)&pRawSF));
				pShellFolder = pRawSF; // this will release the old pointer

				// move to next pidl
				pidlParse = ::ILGetNext(pidlParse);

				// check to see if we're done
				bSuccess = bDone = ((pidlParse == NULL) || (pidlParse->mkid.cb == 0));

				// new root for next search is this item
				hRoot = hItem;
			}
			else
				bDone = true; // can't be found
		}

		if (bSuccess)
			hReturn = hRoot;
	}
	catch (CHRESULTException&)
	{
	}

	return hReturn;
}

void CShellTreeView::SelectFolder(const CString& strFolder)
{
	HTREEITEM hSelect = NavigateToFolder(strFolder);

	if (hSelect)
		GetTreeCtrl().SelectItem(hSelect);
}

void CShellTreeView::UpdateTree(LPCITEMIDLIST pidl1, LPCITEMIDLIST pidl2, eChangeType nChange)
{
	switch (nChange)
	{
		case kChange_Add:
			UpdateTree(pidl1, nChange);
			break;
		case kChange_Remove:
			UpdateTree(pidl1, nChange);
			break;
		case kChange_Modify:
			// remove old item
			UpdateTree(pidl1, kChange_Remove);

			// add new item
			UpdateTree(pidl2, kChange_Add);
			break;
	}
}

void CShellTreeView::UpdateTree(LPCITEMIDLIST pidl, CShellTreeView::eChangeType nChange)
{
	// only adds & removes are supported in this overload
	ASSERT(nChange != kChange_Modify);

	// grab desktop folder pointer
	CComPtr<IShellFolder> pFolder;
	::SHGetDesktopFolder(&pFolder);

	// start at desktop (root)
	HTREEITEM hRoot = m_hDesktop;

	// pidl for parsing
	LPCITEMIDLIST pidlParse = pidl;

	// find at first level
	HTREEITEM hItem = FindTreeItem(hRoot, pFolder, pidl);

	while (hItem && pidlParse->mkid.cb)
	{
		// get some information about the item in question
		TVITEM testitem;
		testitem.hItem = hItem;
		testitem.mask = TVIF_CHILDREN | TVIF_HANDLE | TVIF_PARAM | TVIF_STATE;
		testitem.stateMask = TVIS_DROPHILITED | TVIS_EXPANDEDONCE;
		GetTreeCtrl().GetItem(&testitem);

		// don't traverse down branch unless it has been expanded once
		if (testitem.state & TVIS_EXPANDEDONCE)
		{
			// save parent
			HTREEITEM hParent = hItem;

			// convert lparam to item data
			CShellTreeItemData* pItemData = reinterpret_cast<CShellTreeItemData*>(testitem.lParam);

			// get shell folder interface pointer for this item
			CComPtr<IShellFolder> pNew;
			pFolder->BindToObject(pItemData->pidl, NULL, IID_IShellFolder, (void**)&pNew);

			// save new folder in smart pointer
			pFolder = pNew;

			pidlParse = ::ILGetNext(pidlParse);

			if (pidlParse)
			{
				HTREEITEM hOld = hItem;
				hItem = FindTreeItem(hParent, pFolder, pidlParse);
				if (hItem)
					hParent = hOld;
			}
			else
				hItem = NULL;

			// if this is the last item and we're looking to remove something,
			// then this item is it
			LPITEMIDLIST pidlNext = ::ILGetNext(pidlParse);
			if (nChange == kChange_Remove && pidlNext->mkid.cb == 0 && hItem)
			{
				GetTreeCtrl().DeleteItem(hItem);

				// if parent has no more items, make sure we get rid of the
				// expand/collapse button
				if (GetTreeCtrl().GetChildItem(hParent) == NULL)
					SetExpander(hParent, false);

				hItem = NULL;
			}

			if (!hItem && nChange == kChange_Add)
			{
				// pidl from SHChangeNotify message can be "abnormal"
				LPITEMIDLIST pNormal = pidlutils::to_normal(pFolder, pidlParse);

				// insert normal pidl onto tree
				if (pNormal)
					InsertChildItem(hParent, pFolder, pNormal, pItemData->fqpidl, true);

				hItem = NULL;
			}
		}
		else
		{
			// if item was added to a node that thinks it has no children, correct it
			if (testitem.cChildren == 0 && nChange == kChange_Add)
				SetExpander(hItem, true);

			hItem = NULL; // don't even bother with branches that weren't expanded
		}
	}
}


BEGIN_MESSAGE_MAP(CShellTreeView, CTreeView)
	ON_WM_PAINT()
	ON_WM_DESTROY()
	ON_WM_CONTEXTMENU()
	ON_NOTIFY_REFLECT(NM_CLICK, OnClick)
	ON_NOTIFY_REFLECT(NM_RCLICK, OnRclick)
	ON_NOTIFY_REFLECT(TVN_ITEMEXPANDING, OnItemexpanding)
	ON_NOTIFY_REFLECT(TVN_SELCHANGED, OnSelchanged)
	ON_NOTIFY_REFLECT(TVN_DELETEITEM, OnDeleteitem)
	ON_NOTIFY_REFLECT(TVN_BEGINDRAG, OnBeginDrag)
	ON_NOTIFY_REFLECT(TVN_ENDLABELEDIT, OnEndLabelEdit)
	ON_COMMAND(ID_FOLDERMENU_EXPAND, OnFolderMenu_Expand)
	ON_COMMAND(ID_FOLDERMENU_OPEN, OnFolderMenu_Open)
	ON_COMMAND(ID_FOLDERMENU_EXPLORE, OnFolderMenu_Explore)
	ON_COMMAND(ID_FOLDERMENU_SEARCHFORFONTS, OnFolderMenu_Search)
	ON_COMMAND(ID_FOLDERMENU_CREATENEWFOLDER, OnFolderMenu_NewFolder)
	ON_COMMAND(ID_FOLDERMENU_DELETE, OnFolderMenu_Delete)
	ON_COMMAND(ID_FOLDERMENU_RENAME, OnFolderMenu_Rename)
	ON_COMMAND(ID_FOLDERMENU_PROPERTIES, OnFolderMenu_Properties)
	ON_REGISTERED_MESSAGE(CShellTreeView::s_uwmChangeNotification, OnChangeNotify)
END_MESSAGE_MAP()


// CShellTreeView diagnostics

#ifdef _DEBUG
void CShellTreeView::AssertValid() const
{
	CTreeView::AssertValid();
}

#ifndef _WIN32_WCE
void CShellTreeView::Dump(CDumpContext& dc) const
{
	CTreeView::Dump(dc);
}
#endif
#endif //_DEBUG


// CShellTreeView message handlers

void CShellTreeView::OnInitialUpdate()
{
	if (m_bInitialUpdateDone)
		return;

	// call base class
	CTreeView::OnInitialUpdate();

	// set tree control styles
	GetTreeCtrl().ModifyStyle(TVS_DISABLEDRAGDROP,
							  TVS_HASBUTTONS | TVS_EDITLABELS  | WS_VISIBLE | TVS_HASLINES | TVS_LINESATROOT);

	// load popup menu
	m_menuPopup.LoadMenu(IDR_FOLDER_MENU);

	// we need the desktop folder PIDL
	LPITEMIDLIST pidlDesktop = NULL;
	::SHGetFolderLocation(NULL, CSIDL_DESKTOP, NULL, 0, &pidlDesktop);

	// get the image list
	SHFILEINFO sfi;
	HIMAGELIST hImageList =
		(HIMAGELIST)::SHGetFileInfo((LPCWSTR)pidlDesktop,
									0,
									&sfi,
									sizeof(SHFILEINFO),
									SHGFI_SYSICONINDEX | SHGFI_SMALLICON | SHGFI_PIDL);

	// attach it to the tree control
	if (hImageList)
		::SendMessage(GetTreeCtrl().GetSafeHwnd(), TVM_SETIMAGELIST, (WPARAM)TVSIL_NORMAL, (LPARAM)hImageList);

	// hook up to signals
	CFontRunnerSignals* pSignals = theApp.GetSignals();
	pSignals->ConnectTo_SelectedFolderChange(boost::bind(&CShellTreeView::OnSelectedFolderChange, this, _1, _2));
	pSignals->ConnectTo_NavigateToFolder(boost::bind(&CShellTreeView::OnNavigateToFolder, this, _1));
	pSignals->ConnectTo_Refresh(boost::bind(&CShellTreeView::OnRefresh, this));
	pSignals->ConnectTo_FolderUpQuery(boost::bind(&CShellTreeView::CurrentFolderHasParent, this));
	pSignals->ConnectTo_FolderUp(boost::bind(&CShellTreeView::OnFolderUp, this));

	// create drop target helper
	::CoCreateInstance(CLSID_DragDropHelper, NULL, CLSCTX_INPROC_SERVER,
					   IID_IDropTargetHelper, (void**)&m_pDropTargetHelper);

	// create a drop target handler and connect this class to it
	CDropTargetHandler* pDropTarget = new CDropTargetHandler();
	pDropTarget->Connect(
		boost::bind(&CShellTreeView::DragEnter, this, _1, _2, _3, _4),
		boost::bind(&CShellTreeView::DragOver, this, _1, _2, _3),
		boost::bind(&CShellTreeView::DragLeave, this),
		boost::bind(&CShellTreeView::Drop, this, _1, _2, _3, _4));

	// register for drag and drops
	::RegisterDragDrop(GetSafeHwnd(), pDropTarget);

	// insert desktop item
	m_hDesktop = InsertChildItem(TVI_ROOT, NULL, pidlDesktop, NULL);

	// register for shell update notifications
	const int InterruptLevel = 0x01;
	const int ShellLevel = 0x02;
	SHChangeNotifyEntry cne;
	cne.pidl = pidlDesktop;
	cne.fRecursive = true;

	m_nChangeNotifyID =
		::SHChangeNotifyRegister(GetSafeHwnd(),
								 InterruptLevel | ShellLevel,
								 SHCNE_DRIVEADD | SHCNE_DRIVEREMOVED | SHCNE_MEDIAINSERTED | SHCNE_MEDIAREMOVED |
								 SHCNE_MKDIR | SHCNE_RENAMEFOLDER | SHCNE_RMDIR,
								 s_uwmChangeNotification, 1, &cne);

	// cache PIDLs of things we don't want in the tree
	::SHGetFolderLocation(NULL, CSIDL_CONTROLS, NULL, 0, &m_pidlControlPanel);
	::SHGetFolderLocation(NULL, CSIDL_INTERNET, NULL, 0, &m_pidlInternet);

	m_bInitialUpdateDone = true;
}

void CShellTreeView::OnPaint()
{
	// makes painting during drag and drop more reliable
	if (m_pDropDataObject && m_pDropTargetHelper)
		m_pDropTargetHelper->Show(FALSE);

	CTreeView::OnPaint();

	if (m_pDropDataObject && m_pDropTargetHelper)
		m_pDropTargetHelper->Show(TRUE);
}

void CShellTreeView::OnDestroy()
{
	// this should also release the m_pDropTarget object
	::RevokeDragDrop(GetSafeHwnd());

	// de-register for shell notifications
	::SHChangeNotifyDeregister(m_nChangeNotifyID);

	// need to call base class OnDestroy too
	CTreeView::OnDestroy();
}

void CShellTreeView::OnContextMenu(CWnd* /*pWnd*/, CPoint /*pos*/)
{
	// show context menu for currently selected item
	HTREEITEM hCurrent = GetTreeCtrl().GetSelectedItem();

	CRect rectItem;
	GetTreeCtrl().GetItemRect(hCurrent, &rectItem, TRUE);
	CPoint ptScreen(rectItem.left, rectItem.bottom);
	ClientToScreen(&ptScreen);

	CShellTreeItemData* pItemData =
		reinterpret_cast<CShellTreeItemData*>(GetTreeCtrl().GetItemData(hCurrent));
	ShowContextMenu(hCurrent,
					pItemData->pParent,
					pItemData->pidl,
					ptScreen);
}

void CShellTreeView::OnClick(NMHDR* /*pNMHDR*/, LRESULT* pResult)
{
	if (CFontRunnerDoc::GetDoc()->UsingFontProject())
	{
		DWORD dwPoint = ::GetMessagePos();
		CPoint ptScreen(GET_X_LPARAM(dwPoint), GET_Y_LPARAM(dwPoint));
		CPoint ptClient(ptScreen);

		ScreenToClient(&ptClient);

		// do a hit test
		HTREEITEM hHit = GetTreeCtrl().HitTest(ptClient);
		HTREEITEM hSelected = GetTreeCtrl().GetSelectedItem();

		if (hHit == hSelected)
		{
			CFontRunnerDoc::GetDoc()->UseFontProject(false);
			CShellTreeItemData* pItemData =
				reinterpret_cast<CShellTreeItemData*>(GetTreeCtrl().GetItemData(hSelected));

			theApp.GetSignals()->Fire_SelectedFolderChange(ShellUtils::GetName(pItemData->pParent,
																   pItemData->pidl,
																   SHGDN_NORMAL | SHGDN_FORPARSING));
		}
	}

	*pResult = 0;
}

void CShellTreeView::OnRclick(NMHDR* /*pNMHDR*/, LRESULT* pResult) 
{
	// get screen and client coordinates of click
	CPoint ptScreen, ptClient;
	GetCursorPos(&ptScreen);
	ptClient = ptScreen;
	ScreenToClient(&ptClient);
	
	// select item clicked on
	HTREEITEM hHitItem = GetTreeCtrl().HitTest(ptClient);
	if (hHitItem)
	{
		// get item data from right-clicked item
		CShellTreeItemData* pItemData =
			reinterpret_cast<CShellTreeItemData*>(GetTreeCtrl().GetItemData(hHitItem));

		ShowContextMenu(hHitItem,
						pItemData->pParent,
						pItemData->pidl,
						ptScreen);

		*pResult = 1;
	}
	
	*pResult = 0;
}

void CShellTreeView::OnItemexpanding(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = reinterpret_cast<NM_TREEVIEW*>(pNMHDR);

	if ((pNMTreeView->itemNew.state & TVIS_EXPANDEDONCE))
		return;

	FillNode(pNMTreeView->itemNew.hItem);

	TV_SORTCB tvscb;
	tvscb.hParent     = pNMTreeView->itemNew.hItem;
	tvscb.lParam      = 0;
	tvscb.lpfnCompare = TreeViewCompareProc;

	GetTreeCtrl().SortChildrenCB(&tvscb);

	*pResult = 0;
}

void CShellTreeView::OnSelchanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	NM_TREEVIEW* pNMTreeView = reinterpret_cast<NM_TREEVIEW*>(pNMHDR);

	CShellTreeItemData* pItemData =
			reinterpret_cast<CShellTreeItemData*>(pNMTreeView->itemNew.lParam);

	CComPtr<IShellFolder> pParent;
	if (pItemData->pParent)
		pParent = pItemData->pParent;
	else
		::SHGetDesktopFolder(&pParent);

	// update doc
	CFontRunnerDoc* pDoc = CFontRunnerDoc::GetDoc();
	pDoc->SetCurrentFolder(pParent, pItemData->pidl);
	
	// refresh font list with new directory info
	m_bSelfChange = true;
	if (!m_bDisableFolderChangeSignal && theApp.InitDone())
		theApp.GetSignals()->Fire_SelectedFolderChange(pDoc->GetCurrentFolderNameFQ());
	m_bSelfChange = false;

	// get shell folder for this item
	CComPtr<IShellFolder> pShellFolder;
	pParent->BindToObject(pItemData->pidl, 0, IID_IShellFolder, (void**)&pShellFolder);
	
	*pResult = 0;
}

void CShellTreeView::OnDeleteitem(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = reinterpret_cast<NM_TREEVIEW*>(pNMHDR);

	// just delete item data
	delete reinterpret_cast<CShellTreeItemData*>(pNMTreeView->itemOld.lParam);

	*pResult = 0;
}

void CShellTreeView::OnBeginDrag(NMHDR* pNMHDR, LRESULT* pResult)
{
	NM_TREEVIEW* pNMTreeView = reinterpret_cast<NM_TREEVIEW*>(pNMHDR);

	CShellTreeItemData* pSourceItemData =
		reinterpret_cast<CShellTreeItemData*>(pNMTreeView->itemNew.lParam);

	// save drop source item handle
	m_hDropSource = pNMTreeView->itemNew.hItem;

	// get some pointers we'll need
	IShellFolder* pSourceShellFolder = pSourceItemData->pParent;

#ifdef _DEBUG
	TCHAR szName[MAX_PATH];
	ShellUtils::GetName(pSourceShellFolder, pSourceItemData->pidl, SHGDN_NORMAL, szName, MAX_PATH);
	CString strDebug;
	strDebug.Format(_T("Dragging %s\n"), szName);
	OutputDebugString(strDebug);
#endif
	
	POINT pt = pNMTreeView->ptDrag;
	ClientToScreen(&pt);
	
	// get object from source (drag) object
	DWORD dwEffect = 0;
	IDataObject* pSourceData = NULL;
	pSourceShellFolder->GetUIObjectOf(::GetParent(GetTreeCtrl().GetSafeHwnd()),
		1, (const struct _ITEMIDLIST**)&pSourceItemData->pidl, IID_IDataObject, 0,
		(void**)&pSourceData);

	if (pSourceData)
	{
		CComPtr<IDragSourceHelper> pDragSourceHelper;
		::CoCreateInstance(CLSID_DragDropHelper, NULL, CLSCTX_INPROC_SERVER,
						   IID_IDragSourceHelper, (void**)&pDragSourceHelper);

		SHDRAGIMAGE shdi;
		CreateDragBitmap(pt, &shdi);

		if (FAILED(pDragSourceHelper->InitializeFromBitmap(&shdi, pSourceData)))
			::DeleteObject(shdi.hbmpDragImage); // have to delete bitmap if call fails

		CDropSourceHandler dropSource;
		::DoDragDrop(pSourceData, &dropSource, DROPEFFECT_COPY | DROPEFFECT_MOVE | DROPEFFECT_LINK, &dwEffect);

		pSourceData->Release();
		m_hDropSource = NULL;
	}

	*pResult = 0;
}

void CShellTreeView::OnFolderMenu_Expand()
{
	GetTreeCtrl().Expand(m_hContextMenuItem, TVE_TOGGLE);
	m_hContextMenuItem = NULL;
}

void CShellTreeView::OnFolderMenu_Open()
{
	// get item data
	CShellTreeItemData* pItemData =
		reinterpret_cast<CShellTreeItemData*>(GetTreeCtrl().GetItemData(m_hContextMenuItem));

	CComPtr<IShellFolder> pParent(pItemData->pParent);

	if (!pParent)
		::SHGetDesktopFolder(&pParent);

	// get path from tree item
	CString strPathName = ShellUtils::GetName(pParent, pItemData->pidl, SHGDN_NORMAL | SHGDN_FORPARSING);
	::ShellExecute(NULL, _T("open"), strPathName, NULL, NULL, SW_SHOW);
	m_hContextMenuItem = NULL;
}

void CShellTreeView::OnFolderMenu_Explore()
{
	// get item data
	CShellTreeItemData* pItemData =
		reinterpret_cast<CShellTreeItemData*>(GetTreeCtrl().GetItemData(m_hContextMenuItem));

	CComPtr<IShellFolder> pParent(pItemData->pParent);

	if (!pParent)
		::SHGetDesktopFolder(&pParent);

	// get path from tree item
	CString strPathName = ShellUtils::GetName(pParent, pItemData->pidl, SHGDN_NORMAL | SHGDN_FORPARSING);
	::ShellExecute(NULL, _T("explore"), strPathName, NULL, NULL, SW_SHOW);
	m_hContextMenuItem = NULL;
}

void CShellTreeView::OnFolderMenu_Search()
{
	// get item data
	CShellTreeItemData* pItemData =
		reinterpret_cast<CShellTreeItemData*>(GetTreeCtrl().GetItemData(m_hContextMenuItem));

	CComPtr<IShellFolder> pParent(pItemData->pParent);

	if (!pParent)
		::SHGetDesktopFolder(&pParent);

	// get path from tree item
	CString strPathName = ShellUtils::GetName(pParent, pItemData->pidl, SHGDN_NORMAL | SHGDN_FORPARSING);
	CFontRunnerDoc* pDoc = CFontRunnerDoc::GetDoc();
	pDoc->SearchForFont(strPathName);
	m_hContextMenuItem = NULL;
}

void CShellTreeView::OnFolderMenu_NewFolder()
{
	CShellTreeItemData* pItemData =
		reinterpret_cast<CShellTreeItemData*>(GetTreeCtrl().GetItemData(m_hContextMenuItem));

	CComPtr<IShellFolder> pParentFolder(pItemData->pParent);

	if (!pParentFolder)
		::SHGetDesktopFolder(&pParentFolder);

	CString strPath = ShellUtils::GetName(pParentFolder, pItemData->pidl, SHGDN_NORMAL | SHGDN_FORPARSING);
	
	CString strNewFolder;
	strNewFolder.LoadString(IDS_EXPLORERVIEW_NEWFOLDER);
	CString strFullPath = strPath + _T("\\") + strNewFolder;

	// if folder already exists, use "New Folder (x)"
	int nAttempt = 1;
	const int nMaxAttempts = 256;
	while (::PathFileExists(strFullPath) && ++nAttempt < nMaxAttempts)
	{
		std::wstring strAttempt = boost::lexical_cast<std::wstring>(nAttempt);
		::AfxFormatString1(strNewFolder, IDS_EXPLORERVIEW_NEWFOLDERX, strAttempt.c_str());
		strFullPath = strPath + _T("\\") + strNewFolder;
	}

	if (nAttempt == nMaxAttempts)
	{
		m_hContextMenuItem = NULL;
		return;
	}

	// ignore shell notifications until label editing is finished
	m_bShellChangeHandled = true;
	m_strShellChange = strFullPath;

	// create it
	if (::CreateDirectory(strFullPath, NULL))
	{
		IShellFolder* pShellFolder = NULL;

		// if pItemData doesn't have a valid parent, that means its the desktop
		// we already took that into account above
		if (pItemData->pParent)
			pItemData->pParent->BindToObject(pItemData->pidl, NULL, IID_IShellFolder, (void**)&pShellFolder);
		else
			pShellFolder = pParentFolder;

		LPITEMIDLIST pidl = NULL;
		pShellFolder->ParseDisplayName(GetSafeHwnd(), NULL, (LPOLESTR)(LPCTSTR)strNewFolder, NULL, &pidl, NULL);

		HTREEITEM hInsert = 
			InsertChildItem(m_hContextMenuItem, pShellFolder, pidl, pItemData->fqpidl, true);

		pShellFolder->Release();

		if (hInsert)
			GetTreeCtrl().EditLabel(hInsert);
		m_hContextMenuItem = NULL;
	}
	else
	{
		m_bShellChangeHandled = false;
		m_strShellChange.Empty();
	}
}

void CShellTreeView::OnFolderMenu_Delete()
{
	// if no context menu item, use selected
	HTREEITEM hItem = m_hContextMenuItem;
	if (hItem == NULL)
		hItem = GetTreeCtrl().GetSelectedItem();

	// if still no item, we can't figure out what the user wants to delete
	if (hItem == NULL)
		return;

	// are any fonts in use by this folder?
	CShellTreeItemData* pItemData = reinterpret_cast<CShellTreeItemData*>(GetTreeCtrl().GetItemData(hItem));
	if (pItemData)
	{
		TCHAR szPath[MAX_PATH];
		::SHGetPathFromIDList(pItemData->fqpidl, szPath);

		if (theApp.GetFontManager()->PathInUse(szPath))
		{
			// warn user
			int nResult = ::AfxMessageBox(IDS_EXPLORERVIEW_DELETEINUSE, MB_YESNO + MB_ICONQUESTION);
			if (nResult == IDNO)
				return;
		}

		// unmanage any fonts that are about to be deleted
		theApp.GetFontManager()->UnManageFontsInPath(szPath);
	}

	InvokeContextMenuCommand("delete");
	m_hContextMenuItem = NULL;
}

void CShellTreeView::OnFolderMenu_Rename()
{
	GetTreeCtrl().EditLabel(m_hContextMenuItem);
	m_hContextMenuItem = NULL;
}

void CShellTreeView::OnFolderMenu_Properties()
{
	InvokeContextMenuCommand("properties");
	m_hContextMenuItem = NULL;
}

void CShellTreeView::OnEndLabelEdit(NMHDR* pNMHDR, LRESULT *pResult)
{
	NMTVDISPINFO* pInfo = (NMTVDISPINFO*)pNMHDR;

	if (pInfo->item.pszText)
	{
		CShellTreeItemData* pItemData =
			reinterpret_cast<CShellTreeItemData*>(GetTreeCtrl().GetItemData(pInfo->item.hItem));

		CString strOldName = ShellUtils::GetName(pItemData->pParent, pItemData->pidl, SHGDN_NORMAL | SHGDN_FORPARSING);
		TCHAR szTemp[MAX_PATH];
		::wcscpy_s(szTemp, MAX_PATH, strOldName);
		::PathRemoveFileSpec(szTemp);
		CString strNewName(szTemp);
		strNewName.Format(_T("%s\\%s"), szTemp, pInfo->item.pszText);

		// file name buffers must have double-nulls
		size_t nOldSize = strOldName.GetLength() + 2;
		size_t nNewSize = strNewName.GetLength() + 2;
		boost::scoped_array<TCHAR> pFrom(new TCHAR[nOldSize]);
		boost::scoped_array<TCHAR> pTo(new TCHAR[nNewSize]);
		::wcscpy_s(pFrom.get(), nOldSize, strOldName);
		::wcscpy_s(pTo.get(), nNewSize, strNewName);
		pFrom[nOldSize - 1] = NULL;
		pTo[nNewSize - 1] = NULL;

		// use shell to rename file
		SHFILEOPSTRUCT sfo;
		::memset(&sfo, 0, sizeof(SHFILEOPSTRUCT));
		sfo.hwnd = GetSafeHwnd();
		sfo.wFunc = FO_RENAME;
		sfo.pFrom = pFrom.get();
		sfo.pTo = pTo.get();
		
		if (::SHFileOperation(&sfo) == 0)
		{
			*pResult = TRUE;
			m_bShellChangeHandled = true;
			m_strShellChange = pTo.get();

			// get item data
			CShellTreeItemData* pItemData = reinterpret_cast<CShellTreeItemData*>(GetTreeCtrl().GetItemData(pInfo->item.hItem));

			// add new renamed item
			LPWSTR pszFileName = ::PathFindFileName(pTo.get());
			LPITEMIDLIST pidl = NULL;
			pItemData->pParent->ParseDisplayName(NULL, NULL, pszFileName, NULL, &pidl, NULL);
			HTREEITEM hParent = GetTreeCtrl().GetParentItem(pInfo->item.hItem);
			InsertChildItem(hParent, pItemData->pParent, pidl, pItemData->fqpidl);

			// delete old item
			GetTreeCtrl().DeleteItem(pInfo->item.hItem);
		}
		else
		{
			// Re-edit the label on error.  The shell should take care of dealing with the user
			GetTreeCtrl().EditLabel(pInfo->item.hItem);
			*pResult = FALSE;
		}
	}
	else
		*pResult = FALSE;
}

void CShellTreeView::OnSelectedFolderChange(const CString& strFolder, const CString* /*pstrFile*/)
{
	if (!m_bSelfChange)
	{
		m_bDisableFolderChangeSignal = true;
		SelectFolder(strFolder);
		m_bDisableFolderChangeSignal = false;
	}
}

void CShellTreeView::OnNavigateToFolder(const CString& strFolder)
{
	NavigateToFolder(strFolder);
}

LRESULT CShellTreeView::OnChangeNotify(WPARAM wParam, LPARAM lParam)
{
	LPCITEMIDLIST* pidls = reinterpret_cast<LPCITEMIDLIST*>(wParam);

	try
	{
		// default change type is modify
		eChangeType nChange = kChange_Modify;

		if (lParam & SHCNE_MKDIR || lParam & SHCNE_DRIVEADD)
			nChange = kChange_Add;

		if (lParam & SHCNE_RMDIR || lParam & SHCNE_DRIVEREMOVED)
			nChange = kChange_Remove;

		if (m_bShellChangeHandled)
		{
			LPCITEMIDLIST pidlCheck = NULL;

			CComPtr<IShellFolder> pDesktop;
				::SHGetDesktopFolder(&pDesktop);

			if (nChange == kChange_Add || nChange == kChange_Remove)
				pidlCheck = pidls[0];
			else
				pidlCheck = pidls[1];

			TCHAR szPath[MAX_PATH];
			ShellUtils::GetName(pDesktop, pidlCheck, SHGDN_FORPARSING, szPath, MAX_PATH);

			if (m_strShellChange.CompareNoCase(szPath) == 0)
			{
				// this shell change was handled -- reset vars
				m_bShellChangeHandled = false;
				m_strShellChange.Empty();

				return 0L;
			}
		}

		// update tree
		UpdateTree(pidls[0], pidls[1], nChange);
	}
	catch (CHRESULTException& e)
	{
#ifdef _DEBUG
		CString strDebug;
		strDebug.Format(_T("Could not update tree from shell notification.  HRESULT is 0x%08x.\n"), e.GetHRESULT());
		OutputDebugString(strDebug);
#else
		e;
#endif
	}

	return 0L;
}
