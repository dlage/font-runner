/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "FontListView.h"
#include "FindStatusBar.h"
#include <vector>
#include <boost/bind.hpp>
#include <boost/function.hpp>

class CFontItem;
class CPrettyToolbar;
class CSearchControllerBase;

// CFindFrameBase frame

class CFindFrameBase : public CMiniFrameWnd
{
protected:
	CFindFrameBase(LPCTSTR lpszSearchRoot = NULL, bool bProject = false);
	virtual ~CFindFrameBase();

// definitions
protected:
	struct FindComboData
	{
		enum data_type
		{
			PIDL_type,
			browse_type,
			favorites_type,
			projects_type
		};

		FindComboData(LPITEMIDLIST _pidl, data_type _type = PIDL_type)
			: pidl(_pidl),
			  nType(_type)
		{}

		FindComboData(LPCTSTR szProjectName)
			: pidl(NULL),
			nType(projects_type),
			strProjectName(szProjectName)
		{}

		~FindComboData()
		{
			if (pidl)
				::CoTaskMemFree(pidl);
		}

		LPITEMIDLIST pidl;
		data_type nType;
		CString strProjectName;
	};

	struct SearchThreadData
	{
		CSearchControllerBase* pController;
		CWnd* pWnd;
		LPITEMIDLIST pidlRoot;
		bool bSearchRemovableDisks;
	};

	struct SearchProjectData
	{
		CSearchControllerBase* pController;
		CWnd* pWnd;
		CString strProjectName;
	};

	// WPARAM = pointer to CFontItem
	static UINT s_rwmFontFound;

	// WPARAM and LPARAM not used
	static UINT s_rwmFinished;

	// WPARAM is folder name (LPCTSTR)
	static UINT s_rwmEnteringFolder;

	// constants
	enum { kToolbarButtonSize = 24 };

// operations
protected:
	virtual bool Create(CWnd* pParentWnd, UINT nTitle);
	virtual bool Create(CWnd* pParentWnd, LPCTSTR lpszTitle);
	CFontListView* GetFontListView() { return m_pFontListView; }

	// must override for font list view mode
	virtual CFontListView::eMode GetFontListViewMode() const = 0;

	// inserts current folder into combo box
	static int ShowCurrent(IShellFolder* pFolder, LPITEMIDLIST pidlParseCurrent, LPITEMIDLIST pidlFQItem, COMBOBOXEXITEM& cbi, CComboBoxEx* pCombo, CImageList* pImageList);

	// Initializes a folder-drop down using the MFC CComboBoxEx class
	// Returns index of current folder in the dropdown list, -1 on failure
	// Note: You should handle CBEN_DELETEITEM to delete the FindComboData
	// pointer being held in the LPARAM.  Otherwise, you need to clean
	// them up another way.
	static int InitFolderCombo(CComboBoxEx* pCombo,	// pointer to combo box
						CImageList* pImageList, // pointer to image list
						const CString& strCurrentFolder, // current folder
						bool bAddFonts = true,
						bool bAddBrowseItem = true,
						bool bAddFavorites = true,
						bool bAddProjects = true,
						bool bCurrentIsProject = false);

	static bool GetSearchPath(LPCITEMIDLIST pidl, LPTSTR pszPath = NULL, bool* pbMyComputer = NULL);

	// This SHBrowseForFolder callback function sets a default selection
	// and only enables the OK button if a real file path is selected.
	// Just set the BROWSEINFO lParam member to a char pointer containing
	// the full path of the default location or NULL if none.
	static int CALLBACK FolderBrowserCallback(HWND hwnd, UINT uMsg, LPARAM lParam, LPARAM lpData);

	static unsigned int __cdecl SearchThreadFunction(void* pParam);
	static unsigned int __cdecl SearchProjectFunction(void* pParam);


	// common selection change handler for look-in combo
	void LookInSelChange();

// overrides
protected:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	virtual void PostNcDestroy();

// signal handlers
private:
	void OnFontRunnerShutdown();

protected:
	int m_nLookInSelection;
	bool m_bLookInValid;
	bool m_bLookInMyComputer;
	bool m_bSearching;
	bool m_bCanceled;
	CReBar		m_wndRebar;
	CFindStatusBar m_wndStatusBar;
	CImageList m_ilLookIn;
	CComboBoxEx m_wndLookInCombo;
	boost::shared_ptr<CPrettyToolbar> m_pwndSearchToolbar;
	CString m_strCurrentFolder;
	bool m_bCurrentFolderIsProject;

private:
	CFontListView* m_pFontListView;
};

// Base class for search controller
class CSearchControllerBase
{
public:
	virtual bool Searching() const = 0;
	virtual void EnteringFolder(LPCTSTR szPath) = 0;
	virtual void Found(CFontItem* pItem) = 0;
};

// Search controller that allows member functions to be used directly
template <class T>
class CSearchController : public CSearchControllerBase
{
public:
	// function pointer types
	typedef bool (T::*searching_ptrtype)() const;
	typedef void (T::*enteringfolder_ptrtype)(LPCTSTR szPath);
	typedef void (T::*found_ptrtype)(CFontItem*);
	typedef boost::function<bool (void)> searching_functiontype;
	typedef boost::function<void (LPCTSTR)> enteringfolder_functiontype;
	typedef boost::function<void (CFontItem*)> found_functiontype;

public:
	CSearchController(searching_ptrtype pSearching,
					  enteringfolder_ptrtype pEntering,
					  found_ptrtype pFound, T* pClass)
		: searching(boost::bind(pSearching, pClass)),
		  enteringfolder(boost::bind(pEntering, pClass, _1)),
		  found(boost::bind(pFound, pClass, _1))
	{}

	bool Searching() const
	{
		return searching();
	}

	void EnteringFolder(LPCTSTR szPath)
	{
		enteringfolder(szPath);
	}

	void Found(CFontItem* pItem)
	{
		found(pItem);
	}

private:
	searching_functiontype searching;
	enteringfolder_functiontype enteringfolder;
	found_functiontype found;
};

// function object for searching a folder
struct SearchFolder
{
	SearchFolder(CSearchControllerBase* pData) : m_pController(pData)
	{}

	typedef std::vector<CString> directorylist_type;

	void operator()(LPCTSTR szSearchFolder) const;

private:
	CSearchControllerBase* m_pController;
};

// function object for seaching a project
struct SearchProject
{
	SearchProject(CSearchControllerBase* pData) : m_pController(pData)
	{}

	void operator()(const CString& strProjectName) const;

private:
	CSearchControllerBase* m_pController;
};
