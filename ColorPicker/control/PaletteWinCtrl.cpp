#include "StdAfx.h"
#include "PaletteWinctrl.h"
#include "PaletteCtrl.h"

CPaletteWinCtrl::CPaletteWinCtrl()
{
	m_hwndParent = NULL;

	// init color map
	m_prgbMap = NULL;

	m_bCached = false;

	// colors
	m_clrOver = RGB(255,255,255);
	m_clrCurrent = RGB(255,255,255);

	// mouse not captured
	m_bMouseDown = false;
}

CPaletteWinCtrl::~CPaletteWinCtrl()
{
	// make sure RGB map is deleted
	if (m_prgbMap)
		delete [] m_prgbMap;
}

void CPaletteWinCtrl::InitPalette(CPaletteCtrl* pParent)
{
	LONG_PTR dwStyle = GetWindowLongPtr(GWL_STYLE);
    SetWindowLong(GWL_STYLE, (LONG)dwStyle | SS_NOTIFY);

	// save parent class pointer
	m_pParent = pParent;

	// create the RGB map
	CreateRGBMap();

	// create a cache bitmap
	CreatePaletteCache();

	// resize window
	SetWindowPos(NULL, 0, 0, kPaletteWidth, kPaletteHeight, SWP_NOMOVE | SWP_NOZORDER);
}

void CPaletteWinCtrl::SetColor(COLORREF clrCurrent)
{
	m_clrCurrent = clrCurrent;
	m_ptCurrent = MapColorToPoint(clrCurrent);
	Invalidate(FALSE);
}

void CPaletteWinCtrl::CreateRGBMap()
{
	// create array
	m_prgbMap = new RGBTRIPLE[kPaletteWidth][kPaletteHeight];

	// zero it out (makes it all black)
	ZeroMemory(m_prgbMap, sizeof(RGBTRIPLE) * kPaletteWidth * kPaletteHeight);

	int nSectionWidth = (int)(kPaletteWidth / kColorSections);
	int nColorIncrement = (kMaxColor / nSectionWidth);

	// first loop - R=kMaxColor, G->kMaxColor, B=0
	BYTE nSection = 0;
	BYTE nRed = kMaxColor;
	BYTE nGreen = 0;
	BYTE nBlue = 0;
	for (int x = (nSection * nSectionWidth); x < nSectionWidth; x++)
	{
		nGreen = (BYTE)(kMaxColor * (float)((float)x / (float)nSectionWidth));
		FillUp(x, nRed, nGreen, nBlue);
		FillDown(x, nRed, nGreen, nBlue);
	}

	// second loop - R->0, G=kMaxColor, B=0
	nSection++;
	int nSectionIndex = 0;
	nGreen = kMaxColor;
	for (int x = (nSection * nSectionWidth); x < nSectionWidth * (nSection + 1); x++)
	{
		nRed = (BYTE)(kMaxColor - (kMaxColor * (float)((float)nSectionIndex / (float)nSectionWidth)));
		FillUp(x, nRed, nGreen, nBlue);
		FillDown(x, nRed, nGreen, nBlue);
		nSectionIndex++;
	}

	// third loop - R=0, G=kMaxColor, B->kMaxColor
	nSection++;
	nSectionIndex = 0;
	nRed = 0;
	for (int x = (nSection * nSectionWidth); x < nSectionWidth * (nSection + 1); x++)
	{
		nBlue = (BYTE)(kMaxColor * (float)((float)nSectionIndex / (float)nSectionWidth));
		FillUp(x, nRed, nGreen, nBlue);
		FillDown(x, nRed, nGreen, nBlue);
		nSectionIndex++;
	}

	// fourth loop - R=0, G->0, B=kMaxColor
	nSection++;
	nSectionIndex = nSectionWidth;
	nBlue = kMaxColor;
	for (int x = (nSection * nSectionWidth); x < nSectionWidth * (nSection + 1); x++)
	{
		nGreen = (BYTE)(kMaxColor * (float)((float)nSectionIndex / (float)nSectionWidth));
		FillUp(x, nRed, nGreen, nBlue);
		FillDown(x, nRed, nGreen, nBlue);
		nSectionIndex--;
	}

	// fifth loop - R->kMaxColor, G=0, B=kMaxColor
	nSection++;
	nSectionIndex = 0;
	nGreen = 0;
	for (int x = (nSection * nSectionWidth); x < nSectionWidth * (nSection + 1); x++)
	{
		nRed = (BYTE)(kMaxColor * (float)((float)nSectionIndex / (float)nSectionWidth));
		FillUp(x, nRed, nGreen, nBlue);
		FillDown(x, nRed, nGreen, nBlue);
		nSectionIndex++;
	}

	// sixth and final loop - R=kMaxColor, G=0, B->0
	nSection++;
	nSectionIndex = kMaxColor;
	nRed = kMaxColor;
	for (int x = (nSection * nSectionWidth); x < nSectionWidth * (nSection + 1); x++)
	{
		nBlue = (BYTE)(kMaxColor * (float)((float)nSectionIndex / (float)nSectionWidth));
		FillUp(x, nRed, nGreen, nBlue);
		FillDown(x, nRed, nGreen, nBlue);
		nSectionIndex--;
	}
}

void CPaletteWinCtrl::FillUp(const int& x, const int& nRed, const int& nGreen, const int& nBlue)
{
	int y = (kPaletteHeight / 2) - 1;

	int nColor = 0;
	m_prgbMap[x][y].rgbtRed = nRed;
	m_prgbMap[x][y].rgbtGreen = nGreen;
	m_prgbMap[x][y].rgbtBlue = nBlue;

	for (y--; y >= 0; y--)
	{
		nColor++;
		float fPercent = (float)((float)nColor / (float)kHalfColor);
		m_prgbMap[x][y].rgbtRed = (BYTE)(nRed + (fPercent * (float)((float)kMaxColor - (float)nRed)));
		m_prgbMap[x][y].rgbtGreen = (BYTE)(nGreen + (fPercent * (float)((float)kMaxColor - (float)nGreen)));
		m_prgbMap[x][y].rgbtBlue = (BYTE)(nBlue + (fPercent * (float)((float)kMaxColor - (float)nBlue)));

	}
}

void CPaletteWinCtrl::FillDown(const int& x, const int& nRed, const int& nGreen, const int& nBlue)
{
	int nColor = 255;

	for (int y = (kPaletteHeight / 2); y < kPaletteHeight; y++)
	{
		m_prgbMap[x][y].rgbtRed = (BYTE)(nRed * (float)((float)nColor  / (float)kMaxColor));
		m_prgbMap[x][y].rgbtGreen = (BYTE)(nGreen * (float)((float)nColor  / (float)kMaxColor));
		m_prgbMap[x][y].rgbtBlue = (BYTE)(nBlue * (float)((float)nColor  / (float)kMaxColor));

		nColor -= 2;
	}
}

void CPaletteWinCtrl::CreatePaletteCache()
{
	// do nothing if already cached
	if (m_bCached)
		return;

	ATLASSERT(IsWindow());

	m_bmpPaletteSelect = ::LoadBitmap(_AtlBaseModule.GetResourceInstance(), MAKEINTRESOURCE(IDB_PALSEL_BITMAP));

	m_nSelectBitmapWidth = kSelectorWidth;
	m_nSelectBitmapHeight = kSelectorWidth;

	// get the DC
	HDC hDC = GetDC();

	// need a memory dc
	HDC hdcMemory = CreateCompatibleDC(hDC);

	// create compatible bitmap
	m_bmpPaletteCache = CreateCompatibleBitmap(hDC, kPaletteWidth, kPaletteHeight);

	// select the bitmap cache into the DC so we can write on it
	HBITMAP bmpOld = (HBITMAP)SelectObject(hdcMemory, m_bmpPaletteCache);

	// draw the palette
	for (int x = 0; x < kPaletteWidth; x++)
		for (int y = 0; y < kPaletteHeight; y++)
			SetPixel(hdcMemory, x, y, RGB(m_prgbMap[x][y].rgbtRed, m_prgbMap[x][y].rgbtGreen, m_prgbMap[x][y].rgbtBlue)); 

	// restore old bitmap, also "saves" our bitmap
	SelectObject(hdcMemory, bmpOld);

	// delete memory DC
	DeleteDC(hdcMemory);

	// detach from desktop DC
	ReleaseDC(hDC);

	// cache created
	m_bCached = true;
}

POINT CPaletteWinCtrl::MapColorToPoint(COLORREF clr)
{
	POINT ptReturn = { 0xFFFF, 0xFFFF };;

	// find color in map
	for (int x = 0; x < kPaletteWidth; x++)
		for (int y = 0; y < kPaletteHeight; y++)
		{
			if (((m_prgbMap[x][y].rgbtRed <= (GetRValue(clr) + kColorSections)) && (m_prgbMap[x][y].rgbtRed >= (GetRValue(clr) - kColorSections))) &&
				((m_prgbMap[x][y].rgbtGreen <= (GetGValue(clr) + kColorSections)) && (m_prgbMap[x][y].rgbtGreen >= (GetGValue(clr)- kColorSections))) &&
				((m_prgbMap[x][y].rgbtBlue <= (GetBValue(clr) + kColorSections)) && (m_prgbMap[x][y].rgbtBlue >= (GetBValue(clr) - kColorSections))))
			{
				ptReturn.x = x;
				ptReturn.y = y;
				return ptReturn;
			}
		}

	// point not found
	return ptReturn;
}

// CPaletteWinCtrl message handlers

LRESULT CPaletteWinCtrl::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	// draw bitmap if cached
	if (m_bCached)
	{
		// start painting
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(&ps);

		// create a clipping region
		HRGN hrgn = CreateRectRgn(0, 0, kPaletteWidth - 1, kPaletteHeight - 1);
		SelectClipRgn(hdc, hrgn);

		// create memory dc
		HDC dcMem = CreateCompatibleDC(hdc);

		HBITMAP bmpOld = (HBITMAP)SelectObject(dcMem, m_bmpPaletteCache);

		// blit
		BitBlt(hdc, 0, 0, kPaletteWidth, kPaletteHeight, dcMem, 0, 0, SRCCOPY);

		// draw selection bitmaps if mouse is not down
		int nOffset = (int)(m_nSelectBitmapHeight / 2);

		// draw selection
		SelectObject(dcMem, m_bmpPaletteSelect);
		BitBlt(hdc,m_ptCurrent.x - nOffset,
				  m_ptCurrent.y - nOffset,
				  m_nSelectBitmapWidth,
				  m_nSelectBitmapHeight,
				  dcMem,
				  0, 0,
				  SRCINVERT);

		// clean up
		SelectObject(dcMem, bmpOld);
		ReleaseDC(dcMem);

		// done painting
		EndPaint(&ps);
	}

	return 0L;
}
LRESULT CPaletteWinCtrl::OnLButtonDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	POINT point = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };

	// save new foreground point
	m_ptCurrent = point;
	Invalidate(FALSE);

	// let parent know about click
	m_pParent->OnMouseDownColor(m_clrOver);

	// mouse is down
	m_bMouseDown = true;

	// restrict movement to this window
	RECT rectScreen;
	GetWindowRect(&rectScreen);
	::GetClipCursor(&m_rectOriginalClipArea);
	::ClipCursor(&rectScreen);

	return 0;
}

LRESULT CPaletteWinCtrl::OnLButtonUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	m_bMouseDown = false;

	// restore old cursor clip area
	::ClipCursor(&m_rectOriginalClipArea);

	return 0;
}

LRESULT CPaletteWinCtrl::OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	POINT point = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };

	m_clrOver = RGB(m_prgbMap[point.x][point.y].rgbtRed,
					m_prgbMap[point.x][point.y].rgbtGreen,
					m_prgbMap[point.x][point.y].rgbtBlue);

	m_pParent->OnMouseOverColor(m_clrOver);

	// also send color change if mouse is captured
	if (m_bMouseDown)
	{
		// let parent know about click
		m_pParent->OnMouseDownColor(m_clrOver);
		
		// save new foreground point
		m_ptCurrent = point;
		Invalidate(FALSE);
	}

	return 0;
}
