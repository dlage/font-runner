/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// FontRunner.h : main header file for the FontRunner application
//
#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

#include <vector>
#include <boost/scoped_ptr.hpp>

// used for ensuring there is only one instance
#define FONTRUNNER_MUTEX_NAME		_T("FONTRUNNERPRO-MUTEX-BE4DD254-09FB-4501-9038-10296DCCEF30")
#define FONTRUNNER_REGWM_NAME		_T("FONTRUNNERPRO-REGWM-BE4DD254-09FB-4501-9038-10296DCCEF30")

#define WM_SEARCHINGFOLDER			WM_USER + 101
#define WM_SEARCHFILEFOUND			WM_USER + 102
#define WM_SEARCHCOMPLETE			WM_USER + 103

typedef std::vector<unsigned short> commonsizelist_type;

class CProgramOptions;
class CFontRunnerSignals;
class CFavoriteFolders;
class CFontProjects;
class CFontManager;

// CFontRunnerApp:
// See FontRunner.cpp for the implementation of this class
//

class CFontRunnerApp : public CWinApp
{
public:
	CFontRunnerApp();

// public interface
public:
	// creates a fully-qualified filename using the app data path and the
	// supplied filename.  If bCreateAppDataFolder is true, the path is created
	// if it does not exist.
	CString GetAppDataFilename(UINT nFileID, bool bCreateAppDataFolder = true, UINT nAppDataFolder = IDS_MISC_APPDATAFOLDER) const;

	void RunUpdaterOnShutdown(LPCTSTR szUpdaterLocation);
	
	inline CProgramOptions* GetProgramOptions()	{ return m_pProgramOptions.get(); }
	inline CString GetVersionString() { return m_strVersion; }
	inline CFont* GetDialogFont() { return &m_fontDialog; }
	inline CFont* GetBoldDialogFont() { return &m_fontBoldDialog; }
	inline CFontRunnerSignals* GetSignals() const { return m_pSignals.get(); }
	inline const commonsizelist_type& GetCommonSizeList() const { return m_commonsizelist; }
	inline CFavoriteFolders* GetFavoriteFolders() const { return m_pFavoriteFolders.get(); }
	inline CFontProjects* GetFontProjects() const { return m_pFontProjects.get(); }
	inline CFontManager* GetFontManager() const { return m_pFontManager.get(); }
	inline bool InitDone() const { return m_bInitialized; }
	LPCTSTR GetWindowsFontFolder() const;
	void SwitchToDroppedItem(HDROP hDrop);
	bool VistaOrLater();
	inline bool InPrintPreview() const { return m_bInPrintPreview; }
	inline void InPrintPreview(bool bPreview) { m_bInPrintPreview = bPreview; }

	static UINT	s_uwmAreYouFontRunner;

// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// internals
protected:
	void InitOptions();
	void CreateDialogFont();
	void OnFontListPreviewSizeChange(unsigned short nSize);
	
	static BOOL CALLBACK InstanceSearcher(HWND hWnd, LPARAM lParam);
	HWND FirstInstance();

	void ImportFontRunnerProProjects();
	void ImportFontRunnerProFavorites();

protected:
	CString		m_strUpdater;
	CString		m_strWindowsFontFolder;
	bool		m_bSetup;		// registry setup is complete
	bool		m_bInitialized; // program initialization is complete
	CString		m_strVersion;
	CString		m_strAppPath;
	HANDLE		m_hAppMutex;
	CFont		m_fontDialog;
	CFont		m_fontBoldDialog;
	commonsizelist_type m_commonsizelist;
	DWORD		m_dwMajorOSVersion;
	bool		m_bInPrintPreview;

	boost::scoped_ptr<CFontProjects> m_pFontProjects;
	boost::scoped_ptr<CFontRunnerSignals> m_pSignals;
	boost::scoped_ptr<CFavoriteFolders> m_pFavoriteFolders;
	boost::scoped_ptr<CProgramOptions> m_pProgramOptions;
	boost::scoped_ptr<CFontManager> m_pFontManager;

// Implementation
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void HtmlHelp(DWORD_PTR dwData, UINT nCmd = 0x000F);
};

extern CFontRunnerApp theApp;