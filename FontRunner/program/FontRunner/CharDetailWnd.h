/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include "SizeBox.h"
#include "ThemeSupport.h"
#include "DWMSupport.h"

#include <boost/shared_ptr.hpp>
#include <boost/signals2.hpp>

// CCharDetailWnd frame

class CFontItem;

class CCharDetailWnd : public CMiniFrameWnd, public boost::signals2::trackable
{
	DECLARE_DYNCREATE(CCharDetailWnd)
public:
	CCharDetailWnd();
	virtual ~CCharDetailWnd();

public:
	bool Create(CWnd* pParentWnd, bool bVisible);

// signal handlers
private:
	void OnSelectedFontChange(const CFontItem* pFontItem);
	void OnSelectedCodeChange(wchar_t charCode, const wchar_t* );
	void OnShutdown();
	void OnOptionsChange();
	int OnUnManageFontItem(const CFontItem* pFontItem);
	void OnFontFileDeleted(const CFontItem* pFontItem);

private:
	std::string GetRTFFormattedChar() const;
	void SetDWMSupport();

private:
	boost::shared_ptr<CFontItem> m_pFontItem;
	const int m_nMinWidth;
	wchar_t m_charCurrent;
	const wchar_t* m_pszCurrent;
	bool m_bShuttingDown;
	COLORREF m_clrBackground, m_clrInfoArea;
	CFont m_fntChar, m_fntInfo, *m_pfntInfo;
	long m_nFontHeight;
	bool m_bFontChanged;
	CSizeBox m_wndSizeBox;
	CDWMSupport m_dwmSupport;
	CThemeSupport m_themeSupport;
	bool m_bDWMSupport;
	HTHEME m_hTheme;

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg int OnCreate(LPCREATESTRUCT lpcs);
	afx_msg BOOL OnEraseBkgnd(CDC*);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT, int, int);
	afx_msg void OnClose();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnSettingChange(UINT uFlags, LPCTSTR lpszSection);
	afx_msg LRESULT OnThemeChanged();
	afx_msg LRESULT OnDWMCompositionChanged(WPARAM wParam, LPARAM lParam);
	afx_msg void OnFontMapMenu_CopyCharacter();
	afx_msg void OnFontMapMenu_PreviewCharacter();
	afx_msg void OnFontMapMenu_FindCharacter();
};


