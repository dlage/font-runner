/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "StdAfx.h"
#include "FontRunnerSignals.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CFontRunnerSignals::CFontRunnerSignals()
{
}

CFontRunnerSignals::~CFontRunnerSignals()
{
}

void CFontRunnerSignals::Fire_SelectedFolderChange(const CString& strFontName, const CString* pstrFontSelect) const
{
	m_sigSelectedFolderChange(strFontName, pstrFontSelect);
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_SelectedFolderChange(const SelectedFolderChange_SignalType::slot_type &slot)
{
	return m_sigSelectedFolderChange.connect(slot);
}

void CFontRunnerSignals::Fire_SelectedProjectChange(const std::wstring& strProjectName) const
{
	m_sigSelectedProjectChange(strProjectName);
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_SelectedProjectChange(const SelectedProjectChange_SignalType::slot_type& slot)
{
	return m_sigSelectedProjectChange.connect(slot);
}

void CFontRunnerSignals::Fire_SelectedFontChange(const CFontItem* pData) const
{
	m_sigSelectedFontChange(pData);
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_SelectedFontChange(const SelectedFontChange_SignalType::slot_type& slot)
{
	return m_sigSelectedFontChange.connect(slot);
}

void CFontRunnerSignals::Fire_SelectedCodeChange(wchar_t charCode, const wchar_t* pszDescription) const
{
	m_sigSelectedCodeChange(charCode, pszDescription);
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_SelectedCodeChange(const SelectedCodeChange_SignalType::slot_type& slot)
{
	return m_sigSelectedCodeChange.connect(slot);
}

void CFontRunnerSignals::Fire_PreviewSizeChange(unsigned short nSize) const
{
	m_sigPreviewSizeChange(nSize);
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_PreviewSizeChange(const SizeChange_SignalType::slot_type& slot)
{
	return m_sigPreviewSizeChange.connect(slot);
}

void CFontRunnerSignals::Fire_TempAutoInstallChange(bool bAutoInstall) const
{
	m_sigTempAutoInstall(bAutoInstall);
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_TempAutoInstallChange(const TempAutoInstall_SignalType::slot_type& slot)
{
	return m_sigTempAutoInstall.connect(slot);
}

void CFontRunnerSignals::Fire_Shutdown() const
{
	m_sigShutdown();
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_Shutdown(const Shutdown_SignalType::slot_type& slot)
{
	return m_sigShutdown.connect(slot);
}

void CFontRunnerSignals::Fire_Refresh() const
{
	m_sigRefresh();
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_Refresh(const Refresh_SignalType::slot_type& slot)
{
	return m_sigRefresh.connect(slot);
}

bool CFontRunnerSignals::Query_FolderUp() const
{
	return m_sigQueryFolderUp();
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_FolderUpQuery(const FolderUpQuery_SignalType::slot_type& slot)
{
	return m_sigQueryFolderUp.connect(slot);
}

void CFontRunnerSignals::Fire_FolderUp() const
{
	m_sigFolderUp();
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_FolderUp(const FolderUp_SignalType::slot_type& slot)
{
	return m_sigFolderUp.connect(slot);
}

void CFontRunnerSignals::Fire_OptionsChange() const
{
	m_sigOptionsChange();
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_OptionsChange(const OptionsChange_SignalType::slot_type& slot)
{
	return m_sigOptionsChange.connect(slot);
}

void CFontRunnerSignals::Fire_NavigateToFolder(const CString& strFolder) const
{
	m_sigNavigateToFolder(strFolder);
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_NavigateToFolder(const NavigateToFolder_SignalType::slot_type& slot)
{
	return m_sigNavigateToFolder.connect(slot);
}

void CFontRunnerSignals::Fire_PreviewRTFCharacter(TCHAR ch) const
{
	m_sigPreviewRTFCharacter(ch);
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_PreviewRTFCharacter(const PreviewRTFCharacter_SignalType::slot_type& slot)
{
	return m_sigPreviewRTFCharacter.connect(slot);
}

int CFontRunnerSignals::Fire_UnManageFontItem(const CFontItem* pFontItem) const
{
	return m_sigUnManageFontItem(pFontItem);
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_UnManageFontItem(const CFontRunnerSignals::UnManageFontItem_SignalType::slot_type& slot)
{
	return m_sigUnManageFontItem.connect(slot);
}

void CFontRunnerSignals::Fire_FontDereferenced(const CFontItem* pFontItem) const
{
	m_sigFontDereferenced(pFontItem);
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_FontDereferenced(const FontDereferenced_SignalType::slot_type& slot)
{
	return m_sigFontDereferenced.connect(slot);
}

void CFontRunnerSignals::Fire_AddToProject(const std::vector<std::wstring>& list) const
{
	m_sigAddToProject(list);
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_AddToProject(const AddToProject_SignalType::slot_type& slot)
{
	return m_sigAddToProject.connect(slot);
}

void CFontRunnerSignals::Fire_RemovedFromProject(const CFontItem* pFontItem) const
{
	m_sigRemovedFromProject(pFontItem);
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_RemovedFromProject(const RemovedFromProject_SignalType::slot_type& slot)
{
	return m_sigRemovedFromProject.connect(slot);
}

void CFontRunnerSignals::Fire_FontFileDeleted(const CFontItem* pFontItem) const
{
	m_sigFontFileDeleted(pFontItem);
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_FontFileDeleted(const CFontRunnerSignals::FontFileDeleted_SignalType::slot_type& slot)
{
	return m_sigFontFileDeleted.connect(slot);
}

void CFontRunnerSignals::Fire_DetailSizeChange(unsigned short nSize) const
{
	m_sigDetailSizeChange(nSize);
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_DetailSizeChange(const SizeChange_SignalType::slot_type& slot)
{
	return m_sigDetailSizeChange.connect(slot);
}

void CFontRunnerSignals::Fire_FontMapSizeChange(unsigned short nSize) const
{
	m_sigFontMapSizeChange(nSize);
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_FontMapSizeChange(const SizeChange_SignalType::slot_type& slot)
{
	return m_sigFontMapSizeChange.connect(slot);
}

void CFontRunnerSignals::Fire_DetailColorChange(COLORREF color, bool bForeground) const
{
	m_sigDetailColor(color, bForeground);
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_DetailColorChange(const Color_SignalType::slot_type& slot)
{
	return m_sigDetailColor.connect(slot);
}

void CFontRunnerSignals::Fire_ColorPaletteClosed() const
{
	m_sigColorPaletteClosed();
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_ColorPaletteClosed(const ColorPaletteClosed_SignalType::slot_type& slot)
{
	return m_sigColorPaletteClosed.connect(slot);
}

void CFontRunnerSignals::Fire_OpenColorPalette(bool bForeground) const
{
	m_sigOpenColorPalette(bForeground);
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_OpenColorPalette(const OpenColorPalette_SignalType::slot_type& slot)
{
	return m_sigOpenColorPalette.connect(slot);
}

void CFontRunnerSignals::Fire_AddToMainFontListView(const std::vector<std::wstring>& list) const
{
	m_sigAddToMainFontListView(list);
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_AddToMainFontListView(const AddToMainFontListView_SignalType::slot_type& slot)
{
	return m_sigAddToMainFontListView.connect(slot);
}

void CFontRunnerSignals::Fire_ClearLocationBar() const
{
	m_sigClearLocationBar();
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_ClearLocationBar(const ClearLocationBar_SignalType::slot_type& slot)
{
	return m_sigClearLocationBar.connect(slot);
}

void CFontRunnerSignals::Fire_ViewingSystemFonts(bool bSystemFonts) const
{
	m_sigViewingSystemFonts(bSystemFonts);
}

boost::signals2::connection CFontRunnerSignals::ConnectTo_ViewingSystemFonts(const CFontRunnerSignals::ViewingSystemFonts_SignalType::slot_type& slot)
{
	return m_sigViewingSystemFonts.connect(slot);
}
