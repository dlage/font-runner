/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// FontProjectFrame.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "FontProjectFrame.h"
#include "PrettyToolbar.h"
#include "FontProjectView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CFontProjectFrame

IMPLEMENT_DYNCREATE(CFontProjectFrame, CFrameWnd)

CFontProjectFrame::CFontProjectFrame()
	: m_pwndToolbar(NULL)
{
}

CFontProjectFrame::~CFontProjectFrame()
{
	if (m_pwndToolbar)
		delete m_pwndToolbar;
}

BOOL CFontProjectFrame::OnCreateClient(LPCREATESTRUCT /*lpcs*/, CCreateContext* pContext)
{
	// assign new view to new context
	CCreateContext context;
	pContext = &context;
	pContext->m_pNewViewClass = RUNTIME_CLASS(CFontProjectView);

	// create the new font preview view
	CFontProjectView* pView = NULL;
	pView = (CFontProjectView*)CreateView(pContext, AFX_IDW_PANE_FIRST);

	// set this one and only view as the active view
	SetActiveView(pView, FALSE);

	return TRUE;
}


BEGIN_MESSAGE_MAP(CFontProjectFrame, CFrameWnd)
	ON_WM_CREATE()
END_MESSAGE_MAP()


// CFontProjectFrame message handlers
int CFontProjectFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// create the rebar
	if (!m_wndRebar.Create(this, 0))
	{
		TRACE0("Failed to create rebar\n");
		return -1;      // fail to create
	}

	// create the toolbar
	if (!m_pwndToolbar)
		m_pwndToolbar = new CPrettyToolbar();

	if ((!m_pwndToolbar->CreateEx(this, TBSTYLE_FLAT | TBSTYLE_TRANSPARENT,
							  WS_CHILD | WS_VISIBLE | CBRS_ALIGN_TOP |
							  CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC)) ||
       (!m_pwndToolbar->LoadToolBar(IDR_FONTPROJECT_TOOLBAR)))
    {
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	m_pwndToolbar->LoadToolBar(kToolbarButtonSize,
							   IDB_FONTPROJECTTOOLBAR_NORMAL,
							   IDB_FONTPROJECTTOOLBAR_HOT,
							   IDB_FONTPROJECTTOOLBAR_DISABLED,
							   IDB_FONTPROJECTTOOLBAR_NORMAL256,
							   IDB_FONTPROJECTTOOLBAR_HOT256);

	CRect rectToolbar;
	int nLastButton = m_pwndToolbar->GetToolBarCtrl().GetButtonCount() - 1;
	m_pwndToolbar->GetItemRect(nLastButton, &rectToolbar);
	rectToolbar.left = 0;

	// add this toolbar to the rebar
	m_wndRebar.AddBar(m_pwndToolbar, NULL, NULL, RBBS_NOGRIPPER);

	// set up min/max sizes and ideal sizes for pieces of the rebar
	REBARBANDINFO rbbi;

	rbbi.cbSize = sizeof(rbbi);
	rbbi.fMask = RBBIM_CHILDSIZE | RBBIM_IDEALSIZE | RBBIM_SIZE;
	rbbi.cxMinChild = rectToolbar.Width();
	rbbi.cyMinChild = rectToolbar.Height() + 1;
	rbbi.cyMaxChild = rbbi.cyMinChild;
	rbbi.cx = rbbi.cxIdeal = rectToolbar.Width();
	m_wndRebar.GetReBarCtrl().SetBandInfo(0, &rbbi);

	return 0;
}