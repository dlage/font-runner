/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "StdAfx.h"
#include "DropTargetHandler.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CDropTargetHandler::CDropTargetHandler()
	: m_nRefCount(0)
{
}

CDropTargetHandler::~CDropTargetHandler()
{
}

HRESULT __stdcall CDropTargetHandler::QueryInterface(REFIID iid, void ** ppvObject)
{
	// check to see what interface has been requested
	if(iid == IID_IDropSource || iid == IID_IUnknown)
	{
		AddRef();
		*ppvObject = this;
		return S_OK;
	}
	else
	{
		*ppvObject = 0;
		return E_NOINTERFACE;
	}
}
ULONG __stdcall CDropTargetHandler::AddRef()
{
	return ::InterlockedIncrement(&m_nRefCount);
}

ULONG __stdcall CDropTargetHandler::Release()
{
	LONG nCount = ::InterlockedDecrement(&m_nRefCount);

	if (nCount == 0)
	{
		delete this;
		return 0;
	}
	else
		return nCount;
}

void CDropTargetHandler::Connect(const DragEnterSignalType::slot_type& dragenter,
								 const DragOverSignalType::slot_type& dragover,
								 const DragLeaveSignalType::slot_type& dragleave,
								 const DropSignalType::slot_type& drop)
{
	m_sigDragEnter.connect(dragenter);
	m_sigDragOver.connect(dragover);
	m_sigDragLeave.connect(dragleave);
	m_sigDrop.connect(drop);
}

HRESULT __stdcall CDropTargetHandler::DragEnter(IDataObject *pDataObj, DWORD grfKeyState, POINTL pt, DWORD *pdwEffect)
{
	TRACE("Entering CDropTargetHandler::DragEnter()\n");
	m_sigDragEnter(pDataObj, grfKeyState, pt, pdwEffect);
	return S_OK;
}

HRESULT __stdcall CDropTargetHandler::DragOver(DWORD grfKeyState, POINTL pt, DWORD *pdwEffect)
{
	TRACE("Entering CDropTargetHandler::DragOver()\n");
	m_sigDragOver(grfKeyState, pt, pdwEffect);
	return S_OK;
}

HRESULT __stdcall CDropTargetHandler::DragLeave()
{
	TRACE("Entering CDropTargetHandler::DragLeave()\n");
	m_sigDragLeave();
	return S_OK;
}

HRESULT __stdcall CDropTargetHandler::Drop(IDataObject *pDataObj, DWORD grfKeyState, POINTL pt, DWORD *pdwEffect)
{
	TRACE("Entering CDropTargetHandler::Drop()\n");
	m_sigDrop(pDataObj, grfKeyState, pt, pdwEffect);
	return S_OK;
}