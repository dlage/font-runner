/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// PaletteWnd.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "FontRunnerSignals.h"
#include "PaletteWnd.h"
#include "WinToolbox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CPaletteWnd
HHOOK CPaletteWnd::s_hMouseHook = NULL;
CPaletteWnd* CPaletteWnd::s_pwndPalette = NULL;

IMPLEMENT_DYNCREATE(CPaletteWnd, CMiniFrameWnd)

CPaletteWnd::CPaletteWnd()
 : m_pdlgColor(NULL),
   m_bForeground(false),
   m_hwndRevealer(NULL),
   m_bDoNotHide(false)
{
}

CPaletteWnd::~CPaletteWnd()
{
}

bool CPaletteWnd::Create(CWnd* pParentWnd)
{
	// need a parent for this window
	ASSERT(pParentWnd != NULL);

	LPCTSTR lpszClassName = 
		AfxRegisterWndClass(CS_DBLCLKS,
							::LoadCursor(NULL, IDC_ARROW),
							::GetSysColorBrush(COLOR_3DFACE));

	DWORD dwStyle = WS_POPUP | WS_CLIPSIBLINGS | WS_DLGFRAME | MFS_SYNCACTIVE;

	// create window with zero size for now...
	// it gets resized in the OnCreate handler
	RECT rect = { 0, 0, 0, 0 };
	return (CMiniFrameWnd::Create(lpszClassName, _T("Palette"), dwStyle, rect, pParentWnd) == TRUE);
}

LRESULT CALLBACK CPaletteWnd::MouseProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode >= HC_ACTION && s_pwndPalette && (wParam == WM_LBUTTONDOWN || wParam == WM_RBUTTONDOWN))
	{
		// let window class figure what to do about click
		MOUSEHOOKSTRUCT* pMHS = reinterpret_cast<MOUSEHOOKSTRUCT*>(lParam);
		s_pwndPalette->MouseDown(pMHS->pt);
	}

	// call next hook
	return ::CallNextHookEx(s_hMouseHook, nCode, wParam, lParam);
}

void CPaletteWnd::MouseDown(POINT& pt)
{
	HWND hwndNew = ::WindowFromPoint(pt);
	m_bDoNotHide = (hwndNew == m_hwndRevealer);
}

void CPaletteWnd::Appear(const CString& strTitleText, COLORREF clrCurrent, const CRect& rectOrigin, bool bForeground, HWND hwndRevealer)
{
	// save revealer
	m_hwndRevealer = hwndRevealer;

	// save foreground/background status
	m_bForeground = bForeground;

	// re-init palette dialog
	m_pdlgColor->InitPaletteDlg(strTitleText, clrCurrent);

	// move this window to its correct location
	HMONITOR hMonitor = MonitorFromWindow(GetParent()->m_hWnd, MONITOR_DEFAULTTONEAREST);
	
	// get work area of current monitor
	MONITORINFO mi;
	mi.cbSize = sizeof(MONITORINFO);
	GetMonitorInfo(hMonitor, &mi);

	// we want this window to appear just below its owner button
	bool bSlideDown = true;
	CRect rectThis;
	GetWindowRect(&rectThis);

	// move this rectangle to the place we want it to appear
	rectThis.MoveToXY(rectOrigin.left, rectOrigin.bottom);

	// see if this rectangle fits onto our monitor's work area
	CRect rectIntersect;
	rectIntersect.IntersectRect(&rectThis, &mi.rcWork);

	// if it doesn't fit, move it above the parent
	if (rectIntersect != rectThis)
	{
		rectThis.MoveToY(rectOrigin.top - rectThis.Height());
		bSlideDown = false;
	}

	MoveWindow(rectThis);

	// it may still be off the screen if the screen is really small
	CWinToolbox::MoveWindowIntoWorkArea(m_hWnd);

	// see whether we should fade, slide or just make this window appear
	BOOL bAnim = FALSE;
	::SystemParametersInfo(SPI_GETMENUANIMATION, 0, &bAnim, 0);
	
	// always activate the frame
	ActivateFrame(SW_SHOW);

	// set foreground flag
	m_pdlgColor->SetForeground(bForeground);

	// default focus should be on "red" edit box
	m_pdlgColor->m_ctrlRedEdit.SetFocus();

	if (m_hwndRevealer)
	{
		// hook the mouse
		s_pwndPalette = this;
		s_hMouseHook = ::SetWindowsHookEx(WH_MOUSE, MouseProc, NULL, ::GetCurrentThreadId());
	}
}

void CPaletteWnd::Disappear()
{
	// unhook mouse
	::UnhookWindowsHookEx(s_hMouseHook);
	s_hMouseHook = NULL;
	s_pwndPalette = NULL;

	// hide window
	SetWindowPos(NULL, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_HIDEWINDOW);

	// fire close signal
	theApp.GetSignals()->Fire_ColorPaletteClosed();
}

bool CPaletteWnd::IsForeground() const
{
	return m_bForeground;
}


BEGIN_MESSAGE_MAP(CPaletteWnd, CMiniFrameWnd)
	ON_WM_CLOSE()
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_ACTIVATE()
END_MESSAGE_MAP()


// CPaletteWnd message handlers

void CPaletteWnd::OnClose()
{
	// don't want this window to close, just hide it
	ShowWindow(SW_HIDE);
}

void CPaletteWnd::PostNcDestroy()
{
	// we don't want this class to destroy itself, so do nothing
	return;
}

int CPaletteWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMiniFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// create the color picker dialog
	if (m_pdlgColor == NULL)
	{
		m_pdlgColor = new CPaletteDlg(this);
		m_pdlgColor->Create(this);
	}

	// resize to same size as dialog
	CRect rectDlg;
	m_pdlgColor->GetWindowRect(&rectDlg);

	// add extra space for this window's borders
	int nBorderCorrection = (::GetSystemMetrics(SM_CXDLGFRAME) * 2);

	SetWindowPos(NULL,
				 0, 0,
				 rectDlg.Width() + nBorderCorrection,
				 rectDlg.Height() + nBorderCorrection,
				 SWP_NOMOVE | SWP_NOZORDER);

	return 0;
}

void CPaletteWnd::OnDestroy()
{
	CMiniFrameWnd::OnDestroy();

	// don't forget to destroy the child dialog
	m_pdlgColor->DestroyWindow();
}

void CPaletteWnd::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CMiniFrameWnd::OnActivate(nState, pWndOther, bMinimized);

	// if losing activation, hide this window
	if (nState == WA_INACTIVE && !m_bDoNotHide)
		Disappear();

	m_bDoNotHide = false;
}
