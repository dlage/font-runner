/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

// PACKVERSION macro to help store version info
#define PACKVERSION(major,minor) MAKELONG(minor,major)

class CWinToolbox
{

// Windows common functions
public:

	// returns the version number of the current executable
	static bool GetAppVersion(unsigned long& major,
							  unsigned long& minor,
							  unsigned long& release,
							  unsigned long& build);

	// returns a string containing the version number of the current executable
	static CString GetAppVersionString();

	static DWORD GetDllVersion(LPCTSTR lpszDllName);
	static void MoveWindowIntoWorkArea(HWND hwnd, LPCRECT prcWorkArea = NULL);
	static bool IsInteger(const CString& strNumber);
	static CString BrowseForFolder(HWND hwndOwner, UINT nPromptID);
	static CString BrowseForFolder(HWND hwndOwner, CString strPrompt);
	static CString GetAppPath();
	static size_t GetFileSize(LPCTSTR szFileName);
	static int GetExtraComboWidth();
	static int GetComboHeight(CDC* pDC);
	static int GetEditHeight(CDC* pDC);
};
