/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

class CFontRunnerException
{
public:
	enum eCode
	{
		kCode_OutOfMemory,
		kCode_FileNotFound,
		kCode_FileCorrupt,
		kCode_ObjectNotFound,
		kCode_UnexpectedData,
		kCode_SeeErrorMessage,

		kNumErrorCodes
	};

public:
	CFontRunnerException(eCode nCode);
	CFontRunnerException(LPCTSTR szMessage);
	CFontRunnerException(eCode nCode, LPCTSTR szMessage);
	virtual ~CFontRunnerException();

public:
	eCode GetCode() const;
	LPCTSTR GetMessage() const;

private:
	eCode m_nCode;
	CString m_strMessage;
};

class COutOfMemoryException : public CFontRunnerException
{
public:
	COutOfMemoryException() : CFontRunnerException(kCode_OutOfMemory)
	{}
};

class CFileNotFoundException : public CFontRunnerException
{
public:
	CFileNotFoundException(LPCTSTR szFileName)
		: CFontRunnerException(kCode_FileNotFound, szFileName)
	{}
};

class CFileCorruptException : public CFontRunnerException
{
public:
	CFileCorruptException(LPCTSTR szFileName)
		: CFontRunnerException(kCode_FileCorrupt, szFileName)
	{}
};
