/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "FontRunnerSignals.h"

// CFontRunnerReBar
class CFontRunnerReBar : public CReBar
{
	DECLARE_DYNAMIC(CFontRunnerReBar)

public:
	CFontRunnerReBar();
	virtual ~CFontRunnerReBar();

public:
	boost::signals2::connection connect_to_rclick(const rclick_signal_type::slot_type& slot);

private:
	rclick_signal_type m_rclick_signal;

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
};


