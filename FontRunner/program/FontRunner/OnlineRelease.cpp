/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "StdAfx.h"
#include "OnlineRelease.h"

#include "HRESULTException.h"

#include <afxinet.h>
#include <fstream>
#include <shlwapi.h>

#include <boost/scoped_ptr.hpp>
#include <boost/scoped_array.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

const TCHAR* cstrServer		= _T("www.cruxtech.com");
const INTERNET_PORT cnPort	= 80;
const TCHAR* cstrUpdateCheckPath =		_T("CheckUpdate.aspx?product=%d");
const TCHAR* cstrUpdateDownloadPath =	_T("GetUpdate.aspx?product=%d&currentversion=%s");

COnlineRelease::COnlineRelease()
{
}

COnlineRelease::~COnlineRelease()
{
}

CString GetProductNodeName(COnlineRelease::eProduct nProduct)
{
	switch (nProduct)
	{
		case COnlineRelease::kFontRunner32:
			return _T("fontrunner32");
		case COnlineRelease::kFontRunner64:
			return _T("fontrunner64");
	}

	return _T("");
}

CComPtr<IXMLDOMNode> FindNode(LPCTSTR pszNodeName, IXMLDOMNodeList* pNodeList)
{
	// look for the requested node
	long nLength = 0;
	pNodeList->get_length(&nLength);
	for (long nDocNodeIndex = 0; nDocNodeIndex < nLength; nDocNodeIndex++)
	{
		CComPtr<IXMLDOMNode> pTest;
		pNodeList->nextNode(&pTest);

		if (pTest)
		{
			CComBSTR bstrName;
			pTest->get_nodeName(&bstrName);

			if (bstrName == pszNodeName)
				return pTest;
		}
	}

	return NULL;
}

bool GetXMLBufferFromServer(boost::scoped_array<char>& pBuffer, COnlineRelease::eProduct nProduct)
{
	bool bGotBuffer = false;

	try
	{
		// attempt to establish connection
		CInternetSession session;
		boost::scoped_ptr<CHttpConnection>
			pConnection(session.GetHttpConnection(cstrServer, cnPort));

		if (pConnection.get())
		{
			CString strUpdateCheckPath;
			strUpdateCheckPath.Format(cstrUpdateCheckPath, nProduct);

			// open request for XML file with version info
			boost::scoped_ptr<CHttpFile>
				pFile(pConnection->OpenRequest(CHttpConnection::HTTP_VERB_GET, strUpdateCheckPath));

			if (pFile)
			{
				// send request for file
				pFile->SendRequest();

				// allocate buffer big enough for it
				UINT nSize = (UINT)pFile->GetLength();
				pBuffer.reset(new char[nSize + 1]);

				// get it
				if (pFile->Read(pBuffer.get(), nSize) == nSize)
				{
					pBuffer[nSize] = 0; // NULL terminate it
					bGotBuffer = true;
				}

				// done, close file
				pFile->Close();
			}

			// close connection
			pConnection->Close();
		}
	}
	catch (CInternetException&)
	{
	}
	catch (...)
	{
	}

	return bGotBuffer;
}

std::string ParseInfoFromXML(const CComBSTR& bstrXML, version_info_type& version_info)
{
	std::string strVersion;

	try
	{
		// function to check HRESULTs
		CheckResultFunctor CheckResult;

		// create a new XML document
		CComPtr<IXMLDOMDocument> pXMLDoc = NULL;
		CheckResult(pXMLDoc.CoCreateInstance(__uuidof(DOMDocument)));

		// parse XML from buffer
		VARIANT_BOOL bSuccess = FALSE;
		pXMLDoc->loadXML(bstrXML, &bSuccess);

		if (bSuccess)
		{
			// get the document element (this is the releases node)
			CComPtr<IXMLDOMNodeList> pDocNodeList = NULL;
			pXMLDoc->get_childNodes(&pDocNodeList);

			CComPtr<IXMLDOMNode> pUpdateNode(FindNode(_T("update"), pDocNodeList));

			if (pUpdateNode)
			{
				// find version node
				CComPtr<IXMLDOMNodeList> pUpdateNodes;
				pUpdateNode->get_childNodes(&pUpdateNodes);

				CComPtr<IXMLDOMNode> pVersionNode(FindNode(_T("version"), pUpdateNodes));

				if (pVersionNode)
				{
					CComBSTR bstrVersion;
					pVersionNode->get_text(&bstrVersion);
					
					// convert to 8-bit
					size_t nVersionSize =
						::WideCharToMultiByte(CP_ACP, 0, bstrVersion, bstrVersion.Length(), NULL, 0, NULL, NULL);

					if (nVersionSize)
					{
						boost::scoped_array<char> temp(new char[nVersionSize + 1]);
						memset(temp.get(), 0, nVersionSize + 1);
						::WideCharToMultiByte(CP_ACP, 0, bstrVersion, bstrVersion.Length(), temp.get(), (DWORD)nVersionSize, NULL, NULL);
						strVersion = temp.get();
					}
				}

				CComPtr<IXMLDOMNode> pQuickInfoNode(FindNode(_T("quickinfo"), pUpdateNodes));

				if (pQuickInfoNode)
				{
					CComBSTR bstrQuickInfo;
					pQuickInfoNode->get_text(&bstrQuickInfo);

					version_info.strQuickInfoURL = bstrQuickInfo;
				}
			}
		}
	}
	catch (CHRESULTException&)
	{
	}

	return strVersion;
}

bool COnlineRelease::ObtainCurrentReleaseInfo(eProduct nProduct,
											  version_info_type& version_info) const
{
	boost::scoped_array<char> pBuffer;
	
	if (GetXMLBufferFromServer(pBuffer, nProduct))
	{
		// convert XML to BSTR
		CComBSTR bstrXML(pBuffer.get());

		// get version from XML
		std::string versionstring = ParseInfoFromXML(bstrXML, version_info);

		if (!versionstring.empty())
		{
			// parse version
			CruxTechnologies::string_to_version(versionstring, version_info.version);
			return true;
		}
	}

	return false;
}

bool COnlineRelease::DownloadRelease(eProduct nProduct,
									 LPCTSTR szVersion,
									 LPTSTR pszDownloadTo,
									 abortcallback_type abortcallback,
									 progresscallback_type progresscallback) const
{
	try
	{
		// attempt to establish connection
		CInternetSession session;
		boost::scoped_ptr<CHttpConnection>
			pConnection(session.GetHttpConnection(cstrServer, cnPort));

		if (pConnection.get())
		{
			CString strDownloadPath;
			strDownloadPath.Format(cstrUpdateDownloadPath, nProduct, szVersion);

			// open request for update file
			boost::scoped_ptr<CHttpFile>
				pFile(pConnection->OpenRequest(CHttpConnection::HTTP_VERB_GET, strDownloadPath));

			if (pFile)
			{
				// send request for file
				pFile->SendRequest();

				// make sure there is data
				if (pFile->GetLength() == 0)
					return false;

				// read 32KB at a time
				const size_t cnBufferSize = 32768;
				boost::scoped_array<char> pBuffer(new char[cnBufferSize]);

				CString strFileName;
				strFileName.Format(_T("%s_update.exe"), GetProductNodeName(nProduct));

				// create filename
				::PathAppend(pszDownloadTo, strFileName);
				std::ofstream file;
				file.open(pszDownloadTo, std::ios::out | std::ios::binary | std::ios::trunc);

				if (file.is_open())
				{
					bool bDone = false;
					while ((abortcallback ? !abortcallback() : true) && !bDone)
					{
						UINT nRead = pFile->Read(pBuffer.get(), cnBufferSize);

						// write data
						file.write(pBuffer.get(), nRead);

						// invoke progress callback if supplied
						if (progresscallback)
							progresscallback(nRead, cnBufferSize);

						// we're done when we've read less from the file the requested
						bDone = (nRead < cnBufferSize);
					}

					// close file
					file.close();
				}

				// done, close files
				pFile->Close();
			}

			// close connection
			pConnection->Close();
		}
	}
	catch (CInternetException&)
	{
	}
	catch (...)
	{
	}

	// execution down here means it was successful
	return true;
}
