/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// CharDetailWnd.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "CharDetailWnd.h"

#include "ProgramOptions.h"
#include "FontRunnerDoc.h"
#include "FontRunnerSignals.h"
#include "FontManager.h"
#include "FontItem.h"
#include "OpenTypeData.h"
#include "FindCharacterFrameWnd.h"

#include <sstream>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// From WinUser.h in the Windows 6 SDK
#ifndef WM_DWMCOMPOSITIONCHANGED
#define WM_DWMCOMPOSITIONCHANGED        0x031E
#endif

// CCharDetailWnd

IMPLEMENT_DYNCREATE(CCharDetailWnd, CMiniFrameWnd)

CCharDetailWnd::CCharDetailWnd()
  : m_nMinWidth(150),
	m_charCurrent(0),
	m_pszCurrent(NULL),
	m_bShuttingDown(false),
	m_clrBackground(::GetSysColor(COLOR_WINDOW)),
	m_clrInfoArea(::GetSysColor(COLOR_3DFACE)),
	m_nFontHeight(0),
	m_bFontChanged(false),
	m_bDWMSupport(false),
	m_hTheme(NULL)
{
}

CCharDetailWnd::~CCharDetailWnd()
{
	if (m_hTheme)
		m_themeSupport.CloseThemeData(m_hTheme);
}

bool CCharDetailWnd::Create(CWnd* pParentWnd, bool bVisible)
{
	// need a parent for this window
	ASSERT(pParentWnd != NULL);

	// use regular arrow cursor for this window
	HCURSOR hArrowCursor = (HCURSOR)::LoadImage(NULL, IDC_ARROW, IMAGE_CURSOR, 0, 0, LR_DEFAULTSIZE | LR_SHARED);

	DWORD dwStyle = WS_POPUP | WS_THICKFRAME | WS_CAPTION | WS_SYSMENU | MFS_SYNCACTIVE;

	// if we want this window visible, add the visible flag
	if (bVisible)
		dwStyle |= WS_VISIBLE;

	// load title from resources
	CString strTitle;
	strTitle.LoadString(IDS_CHARDETAILWND_TITLE);

	// create window with arbitrary size for now...
	// it gets resized in later
	RECT rect = { 0, 0, m_nMinWidth, 100 };
	bool bResult = (CMiniFrameWnd::CreateEx(WS_EX_NOPARENTNOTIFY,
				AfxRegisterWndClass(0, hArrowCursor),
				strTitle, dwStyle, rect, pParentWnd) != 0);

	// clean up cursor
	::DestroyCursor(hArrowCursor);

	// return result
	return bResult;
}

void CCharDetailWnd::OnSelectedFontChange(const CFontItem* pFontItem)
{
	if (pFontItem)
	{
		using CruxTechnologies::OpenTypeFontData::CNameRecord;

		// add reference because we're keeping one
		m_pFontItem.reset(theApp.GetFontManager()->AddReference(pFontItem),
								 boost::bind(&CFontManager::RemoveReference, theApp.GetFontManager(), _1));
		m_bFontChanged = true;
		Invalidate();
	}
	else
	{
		m_pFontItem.reset();

		// just hide if nothing selected
		ShowWindow(SW_HIDE);
		return;
	}
}

void CCharDetailWnd::OnSelectedCodeChange(wchar_t charCode, const wchar_t* pszDescription)
{
	// save new current code
	m_charCurrent = charCode;
	m_pszCurrent = pszDescription;

	// show window depending on whether selection is valid
	ShowWindow((charCode == 0) ? SW_HIDE : SW_SHOW);

	Invalidate();
}

void CCharDetailWnd::OnShutdown()
{
	// save window position
	CRect rect;
	GetWindowRect(&rect);

	CProgramOptions* pOptions = theApp.GetProgramOptions();
	pOptions->SetCharDetailWindowPos(CPoint(rect.left, rect.top));
	pOptions->SetCharDetailWindowSize(CSize(rect.Width(), rect.Height()));

	// set shutdown flag to allow close to be processed properly
	m_bShuttingDown = true;
	SendMessage(WM_CLOSE);
}

void CCharDetailWnd::OnOptionsChange()
{
	m_bFontChanged = true;
	Invalidate();
}

int CCharDetailWnd::OnUnManageFontItem(const CFontItem* pFontItem)
{
	if (m_pFontItem.get() == pFontItem)
	{
		m_pFontItem.reset();

		// just hide if nothing selected
		ShowWindow(SW_HIDE);

		return 1;
	}

	return 0;
}

void CCharDetailWnd::OnFontFileDeleted(const CFontItem* pFontItem)
{
	if (m_pFontItem.get() == pFontItem)
	{
		m_pFontItem.reset();

		// just hide if nothing selected
		ShowWindow(SW_HIDE);
	}
}

std::string CCharDetailWnd::GetRTFFormattedChar() const
{
	// have to convert font name to ANSI
	using CruxTechnologies::OpenTypeFontData::CNameRecord;
	const std::wstring* pstrFontName = m_pFontItem->GetOpenTypeData()->GetName(CNameRecord::kNameID_FullFontName);
	int nFontNameSize = ::WideCharToMultiByte(CP_ACP, 0, pstrFontName->c_str(), -1, NULL, 0, NULL, NULL);
	boost::scoped_array<char> pFontName(new char[nFontNameSize]);
	::WideCharToMultiByte(CP_ACP, 0, pstrFontName->c_str(), -1, pFontName.get(), nFontNameSize, NULL, NULL);

	// make some RTF text
	std::stringstream ssRTF;
	ssRTF << "{\\rtf1{\\fonttbl{\\f0\\fnil\\fcharset0 "
		  << pFontName.get()
		  << ";}}\\pard\\f0\\fs24 \\u"
		  << m_charCurrent
		  << "?}" << std::ends;

	return ssRTF.str();
}

void CCharDetailWnd::SetDWMSupport()
{
	// can we do desktop windows management?
	m_bDWMSupport = (m_dwmSupport.CheckAvailability() && 
		m_dwmSupport.IsCompositionEnabled() &&
		m_themeSupport.IsAppThemed() != 0 &&
		m_themeSupport.IsThemeActive() != 0 &&
		(m_hTheme = m_themeSupport.OpenThemeData(GetSafeHwnd(), _T("Window"))) != 0);

	// only show sizebox if not doing DWM
	m_wndSizeBox.ShowWindow(m_bDWMSupport ? SW_HIDE : SW_SHOW);

	// update colors
	m_clrBackground = ::GetSysColor(COLOR_WINDOW);
	m_clrInfoArea = ::GetSysColor(COLOR_3DFACE);
}


BEGIN_MESSAGE_MAP(CCharDetailWnd, CMiniFrameWnd)
	ON_WM_CREATE()
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_WM_CLOSE()
	ON_WM_RBUTTONUP()
	ON_WM_SETTINGCHANGE()
	ON_WM_THEMECHANGED()
	ON_MESSAGE(WM_DWMCOMPOSITIONCHANGED, OnDWMCompositionChanged)
	ON_COMMAND(ID_FONTMAPMENU_COPYCHARACTER, OnFontMapMenu_CopyCharacter)
	ON_COMMAND(ID_FONTMAPMENU_PREVIEWCHARACTER, OnFontMapMenu_PreviewCharacter)
	ON_COMMAND(ID_FONTMAPMENU_FINDCHARACTER, OnFontMapMenu_FindCharacter)
END_MESSAGE_MAP()


// CCharDetailWnd message handlers
int CCharDetailWnd::OnCreate(LPCREATESTRUCT /*lpcs*/)
{
	// create size box (really a scrollbar) control
	//m_wndSizeBox.Create(WS_VISIBLE | WS_CHILD | SBS_SIZEBOX | SBS_SIZEBOXBOTTOMRIGHTALIGN | SBS_SIZEBOXTOPLEFTALIGN,
	//					CRect(0,0,0,0), this, ID_CHARDETAIL_SIZEBOX);
	m_wndSizeBox.Create(this, ID_CHARDETAIL_SIZEBOX);

	// register for font change signal
	theApp.GetSignals()->ConnectTo_SelectedFontChange(boost::bind(&CCharDetailWnd::OnSelectedFontChange, this, _1));

	// register for char change signal
	theApp.GetSignals()->ConnectTo_SelectedCodeChange(boost::bind(&CCharDetailWnd::OnSelectedCodeChange, this, _1, _2));

	// register for shutdown signal
	theApp.GetSignals()->ConnectTo_Shutdown(boost::bind(&CCharDetailWnd::OnShutdown, this));

	// register for options change signal
	theApp.GetSignals()->ConnectTo_OptionsChange(boost::bind(&CCharDetailWnd::OnOptionsChange, this));

	// register for unmanage font event
	theApp.GetSignals()->ConnectTo_UnManageFontItem(boost::bind(&CCharDetailWnd::OnUnManageFontItem, this, _1));

	// register for delete font event
	theApp.GetSignals()->ConnectTo_FontFileDeleted(boost::bind(&CCharDetailWnd::OnFontFileDeleted, this, _1));

	// restore window size and position
	CProgramOptions* pOptions = theApp.GetProgramOptions();

	// if this is an uninitialized position, figure a good place for it
	CPoint pt;
	CSize size;
	pOptions->GetCharDetailWindowPos(pt);
	pOptions->GetCharDetailWindowSize(size);

	if (pt.x == -1 && pt.y == -1 && size.cx == -1 && size.cy == -1)
	{
		MoveWindow(0, 0, 120, 120);
		CenterWindow();
	}
	else
		MoveWindow(pt.x, pt.y, size.cx, size.cy, false);

	// is there a selected font?
	const CFontItem* pFontItem = NULL;
	
	if (CFontRunnerDoc::GetDoc())
		pFontItem = CFontRunnerDoc::GetDoc()->GetCurrentFontItem();

	if (pFontItem)
		OnSelectedFontChange(pFontItem);

	// setup DWM support
	SetDWMSupport();

	// try for Segoe UI, otherwise, just use the dialog font
	LOGFONT dialogfont;
	CFont* pDialogFont = theApp.GetDialogFont();
	pDialogFont->GetLogFont(&dialogfont);
	::lstrcpy(dialogfont.lfFaceName, _T("Segoe UI"));

	if (m_fntInfo.CreateFontIndirect(&dialogfont))
		m_pfntInfo = &m_fntInfo;
	else
		m_pfntInfo = pDialogFont;

	return 0;
}

BOOL CCharDetailWnd::OnEraseBkgnd(CDC*)
{
	// for flicker-free display
	return FALSE;
}

void CCharDetailWnd::OnPaint()
{
	// setup painting
	PAINTSTRUCT ps;
	CDC* pDC = BeginPaint(&ps);

	// create an offscreen surface
	CDC dcMemory;
	dcMemory.CreateCompatibleDC(pDC);

	int nSavedDC = dcMemory.SaveDC();

	// need size of client area
	CRect rectClient;
	GetClientRect(&rectClient);

	// create an offscreen bitmap
	BITMAPINFO bmi;
	memset(&bmi, 0, sizeof(RGBQUAD));
	bmi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmi.bmiHeader.biWidth = rectClient.Width();
	bmi.bmiHeader.biHeight = -rectClient.Height();
	bmi.bmiHeader.biPlanes = 1;
	bmi.bmiHeader.biBitCount = 32;
	bmi.bmiHeader.biCompression = BI_RGB;
	
	unsigned char* pixels = NULL;
	HBITMAP hBitmap =
		::CreateDIBSection(dcMemory.m_hDC, &bmi, DIB_RGB_COLORS, (void**)&pixels, NULL, 0);

	::SelectObject(dcMemory.m_hDC, hBitmap);

	// create string containing all info
	CString strInfo;
	strInfo.Format(_T("U+%04X\n%s"), m_charCurrent, m_pszCurrent);

	DTTOPTS dto;
	dto.dwSize = sizeof(DTTOPTS);
	dto.dwFlags = DTT_COMPOSITED | DTT_GLOWSIZE | DTT_CALCRECT;
	dto.iGlowSize = 10;
	DWORD dwDrawTextFlags = DT_CENTER | DT_WORDBREAK | DT_NOPREFIX;

	dcMemory.SelectObject(m_pfntInfo);

	// figure height of info area
	CRect rectInfo(0, 0, rectClient.right, 0);
	dcMemory.DrawText(strInfo, &rectInfo, dwDrawTextFlags | DT_CALCRECT);

	rectInfo.bottom += 10;

	// put info rect where it belongs
	rectInfo.top = rectClient.bottom - rectInfo.bottom;
	rectInfo.left = 0;
	rectInfo.right = rectClient.right;
	rectInfo.bottom = rectClient.bottom;

	// fill entire background
	dcMemory.FillSolidRect(rectClient, m_clrBackground);

	// compute character area size and draw the character
	dcMemory.SetBkMode(TRANSPARENT);
	CRect rectCharArea(0, 0,
		rectClient.right, rectClient.bottom - (rectInfo.bottom - rectInfo.top));

	if ((rectCharArea.Height() != m_nFontHeight) || (m_bFontChanged))
	{
		m_nFontHeight = rectCharArea.Height();
		
		if (m_fntChar.m_hObject != NULL)
			m_fntChar.DeleteObject();

		BYTE nQuality = 0;
		switch (theApp.GetProgramOptions()->GetCharDetailRenderingOption())
		{
			case CProgramOptions::kRenderingOption_UseDefault:
				nQuality = DEFAULT_QUALITY;
				break;
			case CProgramOptions::kRenderingOption_Antialiasing:
				nQuality = ANTIALIASED_QUALITY;
				break;
			case CProgramOptions::kRenderingOptions_ClearType:
				nQuality = CLEARTYPE_QUALITY;
				break;
		}

		// get font name
		using CruxTechnologies::OpenTypeFontData::CNameRecord;
		const std::wstring* pstrFontName =
			m_pFontItem->GetOpenTypeData()->GetName(CNameRecord::kNameID_FullFontName);

		m_fntChar.CreateFont(rectCharArea.Height(), 0, 0, 0, FW_NORMAL,
			FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS,
			CLIP_DEFAULT_PRECIS, nQuality, DEFAULT_PITCH | FF_DONTCARE,
			pstrFontName->c_str());

		m_bFontChanged =  false;
	}

	dcMemory.SelectObject(&m_fntChar);

	dcMemory.DrawText(&m_charCurrent, 1, rectCharArea,
					  DT_CENTER | DT_NOPREFIX | DT_SINGLELINE | DT_VCENTER);

	// draw info area
	dcMemory.SelectObject(m_pfntInfo);

	if (m_bDWMSupport)
	{
		MARGINS margins = { 0, 0, 0, rectInfo.Height() };
		m_dwmSupport.ExtendFrameIntoClientArea(m_hWnd, &margins);

		dcMemory.FillSolidRect(&rectInfo, 0);
		dto.dwFlags ^= DTT_CALCRECT; // remove CALCRECT option
		rectInfo.top += 5;
		m_themeSupport.DrawThemeTextEx(m_hTheme, dcMemory.m_hDC, WP_CAPTION, CS_ACTIVE, strInfo, strInfo.GetLength(), dwDrawTextFlags, &rectInfo, &dto);
	}
	else
	{
		dcMemory.FillSolidRect(&rectInfo, m_clrInfoArea);
		dcMemory.DrawText(strInfo, &rectInfo, dwDrawTextFlags);
	}

	// blit from offscreen
	pDC->BitBlt(0, 0, rectClient.Width(), rectClient.Height(), &dcMemory, 0, 0, SRCCOPY);

	// have to restore previous GDI objects
	dcMemory.RestoreDC(nSavedDC);

	::DeleteObject(hBitmap);

	// end painting
	EndPaint(&ps);
}

void CCharDetailWnd::OnSize(UINT, int cx, int cy)
{
	Invalidate();

	 //move the sizebox
	CRect rcSizeBox;
	m_wndSizeBox.GetClientRect(&rcSizeBox);
	m_wndSizeBox.MoveWindow(cx - rcSizeBox.Width(), cy - rcSizeBox.Height(),
							rcSizeBox.Width(), rcSizeBox.Height());
}

void CCharDetailWnd::OnClose()
{
	// don't allow this window to close unless we're shutting down,
	// just hide it
	if (m_bShuttingDown)
		CMiniFrameWnd::OnClose();
	else
		ShowWindow(SW_HIDE);
}

void CCharDetailWnd::OnRButtonUp(UINT /*nFlags*/, CPoint point)
{
	// get coordinates of click
	ClientToScreen(&point);
	
	CMenu menu;
	menu.LoadMenu(IDR_FONTMAP_MENU);

	CMenu *pSubMenu = menu.GetSubMenu(0);

	pSubMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,
							 point.x, point.y, this, NULL);
}

void CCharDetailWnd::OnSettingChange(UINT /*uFlags*/, LPCTSTR /*lpszSection*/)
{
	SetDWMSupport();
}

LRESULT CCharDetailWnd::OnThemeChanged()
{
	SetDWMSupport();
	return 0;
}

LRESULT CCharDetailWnd::OnDWMCompositionChanged(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	SetDWMSupport();
	return 0;
}

void CCharDetailWnd::OnFontMapMenu_CopyCharacter()
{
	// attempt to open clipboard
	if (!::OpenClipboard(GetSafeHwnd()))
	{
		::AfxMessageBox(IDS_GENERAL_CLIPBOARDOPEN_ERROR);
		return;
	}

	// empty the clipboard
	::EmptyClipboard();

	// get format ID for RTF
	UINT nFormat = ::RegisterClipboardFormat(_T("Rich Text Format"));

	// get RTF
	std::string strRTF = GetRTFFormattedChar();

	// allocate global memory for transfer
	const size_t nRTFSize = strRTF.size() + 1;
	HGLOBAL hRTF = ::GlobalAlloc(GMEM_MOVEABLE | GMEM_DDESHARE | GMEM_ZEROINIT, nRTFSize);

	// stick string in global memory
	char* pszGlobal = static_cast<char*>(::GlobalLock(hRTF));
	strRTF._Copy_s(pszGlobal, nRTFSize, nRTFSize); // use MS "safe" copy
	::GlobalUnlock(hRTF);

	// now put RTF string onto the clipboard
	::SetClipboardData(nFormat, hRTF);
	::GlobalFree(hRTF);

	// also copy as unicode text

	// allocate memory for unicode transfer
	const size_t nUnicodeSize = sizeof(wchar_t) * 2; // enough for a character and a NULL terminator
	HGLOBAL hUnicode = ::GlobalAlloc(GMEM_MOVEABLE | GMEM_DDESHARE | GMEM_ZEROINIT, nUnicodeSize);

	// put unicode character in global memory
	wchar_t* pszUnicodeGlobal = static_cast<wchar_t*>(::GlobalLock(hUnicode));
	pszUnicodeGlobal[0] = m_charCurrent;
	::GlobalUnlock(hUnicode);

	// stick unicode text onto the clipboard
	::SetClipboardData(CF_UNICODETEXT, hUnicode);
	::GlobalFree(hUnicode);

	// done with clipboard
	::CloseClipboard();
}

void CCharDetailWnd::OnFontMapMenu_PreviewCharacter()
{
	theApp.GetSignals()->Fire_PreviewRTFCharacter(m_charCurrent);
}

void CCharDetailWnd::OnFontMapMenu_FindCharacter()
{
	// create find character object
	CFindCharacterFrameWnd* pFindWindow =
		new CFindCharacterFrameWnd(m_pszCurrent,
								   m_charCurrent,
								   m_pFontItem.get(),
								   CFontRunnerDoc::GetDoc()->GetCurrentFolderNameFQ());
	
	// create window
	pFindWindow->Create(theApp.GetMainWnd());

	// the window will do the rest...
}

