/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

//========================================================================================
// Thrown when a failed HRESULT occurs
class CHRESULTException
{
public:
	// create with error code
	CHRESULTException(HRESULT hResult) : m_hResult(hResult)
	{}
	
	HRESULT GetHRESULT() const { return m_hResult; }

private:
	HRESULT m_hResult;
};

//========================================================================================
// Function object throws a failed HRESULT
struct CheckResultFunctor
{
	HRESULT operator()(HRESULT hResult, bool bAssert = true) const
	{
		bAssert;
		ASSERT(SUCCEEDED(hResult) || !bAssert);
		if (FAILED(hResult))
		{
#ifdef _DEBUG
			CString strDebug;
			strDebug.Format(_T("Bad HRESULT 0x%x\n"), hResult);
			OutputDebugString(strDebug);
#endif

			throw CHRESULTException(hResult);
		}
		else
			return hResult;
	}
};
