/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// FontProjectView.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "FontRunnerDoc.h"
#include "FontProjectView.h"

#include "RenameDlg.h"
#include "FontProjects.h"
#include "FontItem.h"
#include "OpenTypeData.h"
#include "DropTargetHandler.h"
#include "FontRunnerSignals.h"
#include "ProgramOptions.h"
#include "MainFrm.h"

#include <boost/scoped_array.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CFontProjectView

IMPLEMENT_DYNCREATE(CFontProjectView, CListView)

CFontProjectView::CFontProjectView()
 : m_nDropTarget(-1),
   m_bOverValidItem(false),
   m_pDropTargetHelper(NULL),
   m_pDraggedObject(NULL),
   m_nNormalIconIndex(-1),
   m_bInitialUpdateDone(false),
   m_nRClickItem(-1)
{
}

CFontProjectView::~CFontProjectView()
{
}

void CFontProjectView::OnInitialUpdate()
{
	// because this view is in a tab, this function might get called more than once
	if (m_bInitialUpdateDone)
		return;

	// call base class version of OnInitialUpdate
	CListView::OnInitialUpdate();

	// create drop target helper
	::CoCreateInstance(CLSID_DragDropHelper, NULL, CLSCTX_INPROC_SERVER,
					   IID_IDropTargetHelper, (void**)&m_pDropTargetHelper);

	CFontRunnerSignals* pSignals = theApp.GetSignals();

	// we also need to know about folder change events
	pSignals->ConnectTo_SelectedFolderChange(
		boost::bind(&CFontProjectView::OnSelectedFolderChange, this, _1, _2));

	// even need to know about project change events
	pSignals->ConnectTo_SelectedProjectChange(boost::bind(&CFontProjectView::OnSelectedProjectChange, this, _1));

	// when someone wants to add fonts to the current project
	pSignals->ConnectTo_AddToProject(boost::bind(&CFontProjectView::OnAddToProject, this, _1));

	// when a font is removed from the project
	pSignals->ConnectTo_RemovedFromProject(boost::bind(&CFontProjectView::OnRemovedFromProject, this, _1));

	// wire up for drops
	CDropTargetHandler* pDropTarget = new CDropTargetHandler();
	pDropTarget->Connect(
		boost::bind(&CFontProjectView::DragEnter, this, _1, _2, _3, _4),
		boost::bind(&CFontProjectView::DragOver, this, _1, _2, _3),
		boost::bind(&CFontProjectView::DragLeave, this),
		boost::bind(&CFontProjectView::Drop, this, _1, _2, _3, _4));

	// register for drag and drops
	::RegisterDragDrop(m_hWnd, pDropTarget);

	// set list control styles
	ModifyStyle(0, LVS_SINGLESEL | LVS_REPORT | LVS_NOCOLUMNHEADER | LVS_SHOWSELALWAYS | LVS_SORTASCENDING);

	// create an imagelist for the tree view
	m_ilProjects.Create(16, 16, ILC_COLOR32, 2, 2);
	
	// add normal icon
	HICON hIcon = AfxGetApp()->LoadIcon(IDI_FONTPROJECT_ICON);
	m_nNormalIconIndex = m_ilProjects.Add(hIcon);
	::DestroyIcon(hIcon);

	// attach imagelist to tree control
	GetListCtrl().SetImageList(&m_ilProjects, LVSIL_SMALL);

	// insert one column
	GetListCtrl().InsertColumn(0, _T("ProjectName"));

	// insert font projects
	InsertFontProjects();

	m_bInitialUpdateDone = true;
}

BOOL CFontProjectView::PreTranslateMessage(MSG* pMsg)
{
	// handle delete key because accelerator does not work for some reason
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_DELETE)
	{
		int nSelected = GetListCtrl().GetNextItem(-1, LVNI_SELECTED);
		if (nSelected != -1)
			Delete(nSelected);

		return true;
	}

	return false;
}

void CFontProjectView::DragEnter(IDataObject *pDataObject, DWORD /*grfKeyState*/, POINTL ptl, DWORD* pdwEffect)
{
	if (m_pDropTargetHelper)
	{
		POINT pt = { ptl.x, ptl.y };
		m_pDropTargetHelper->DragEnter(m_hWnd, pDataObject, &pt, *pdwEffect);
	}

	if (m_pDraggedObject)
		m_pDraggedObject->Release();

	// Do we want this object?  Look for files or other font project entries.
	UINT nFormat = ::RegisterClipboardFormat(_T("Font Runner Font Project Data"));
	FORMATETC format = { (CLIPFORMAT)nFormat, NULL, DVASPECT_CONTENT, -1, TYMED_HGLOBAL };
	HRESULT hResult = pDataObject->QueryGetData(&format);

	// not a font project entry, look for files
	if (hResult != S_OK)
	{
		format.cfFormat = (CLIPFORMAT)::RegisterClipboardFormat(CFSTR_FILENAME);
		hResult = pDataObject->QueryGetData(&format);
	}

	if (hResult == S_OK)
	{
		// save dragged object because it has something we're interested in
		m_pDraggedObject = pDataObject;
		m_pDraggedObject->AddRef();
	}
}

void CFontProjectView::DragOver(DWORD /*grfKeyState*/, POINTL ptl, DWORD* pdwEffect)
{
	if (m_pDropTargetHelper)
	{
		POINT pt = { ptl.x, ptl.y };
		m_pDropTargetHelper->DragOver(&pt, *pdwEffect);
	}

	CPoint ptScreen(ptl.x, ptl.y);
	CPoint ptClient(ptScreen);
	ScreenToClient(&ptClient);

	CImageList::DragMove(ptScreen);
	CImageList::DragShowNolock(FALSE);

	// saves its index
	UINT nFlags = 0;
	int nItem = GetListCtrl().HitTest(ptClient,  &nFlags);

	if ((nItem >= 0) && (nItem != m_nDropTarget))
	{
		m_nDropTarget = nItem;
		m_bOverValidItem = true;
	}

	if (nItem == -1)
	{
		m_nDropTarget = nItem;
		m_bOverValidItem = false;
	}

	*pdwEffect = m_bOverValidItem ? DROPEFFECT_COPY : DROPEFFECT_NONE;

	CImageList::DragShowNolock(TRUE);
}

void CFontProjectView::DragLeave()
{
	if (m_pDropTargetHelper)
		m_pDropTargetHelper->DragLeave();

	DragCleanup();
}

void CFontProjectView::Drop(IDataObject *pDataObject, DWORD /*grfKeyState*/, POINTL ptl, DWORD *pdwEffect)
{
	if (m_pDropTargetHelper)
	{
		POINT pt = { ptl.x, ptl.y };
		m_pDropTargetHelper->Drop(pDataObject, &pt, *pdwEffect);
	}
	
	// get filenames from dropped content
	FORMATETC fmt;
	fmt.cfFormat = CF_HDROP;
	fmt.ptd = NULL;
	fmt.dwAspect = DVASPECT_CONTENT;
	fmt.lindex = -1;
	fmt.tymed = TYMED_HGLOBAL;

	HRESULT hResult = pDataObject->QueryGetData(&fmt);
	if (SUCCEEDED(hResult))
	{
		// Dragged files.  That's fine.
		STGMEDIUM stgm;
		pDataObject->GetData(&fmt, &stgm);

		// the DragQueryFile function will help us get the filenames
		UINT nFileCount = ::DragQueryFile((HDROP)stgm.hGlobal, 0xFFFFFFFF, NULL, 0);

		// create a list of files names
		std::vector<std::wstring> list;
		for (UINT nFile = 0; nFile < nFileCount; ++nFile)
		{
			// get size of file name
			UINT nSize = ::DragQueryFile((HDROP)stgm.hGlobal, nFile, NULL, 0);
			boost::scoped_array<TCHAR> pszFileName(new TCHAR[nSize + 1]);

			// get filename
			::DragQueryFile((HDROP)stgm.hGlobal, nFile, pszFileName.get(), nSize + 1);

			// add to list
			list.push_back(pszFileName.get());
		}

		// add file list to project
		AddFilesToProject(m_nDropTarget, list);
	}
	else
	{
		fmt.cfFormat = (CLIPFORMAT)::RegisterClipboardFormat(_T("Font Runner Font Project Data"));
		if (SUCCEEDED(pDataObject->QueryGetData(&fmt)))
		{
			// Font project entry.  We'll take that too.
			STGMEDIUM stgm;
			pDataObject->GetData(&fmt, &stgm);

			DROPFILES* pDropFiles = reinterpret_cast<DROPFILES*>(::GlobalLock(stgm.hGlobal));
			TCHAR* pStringList = reinterpret_cast<TCHAR*>((char*)pDropFiles + pDropFiles->pFiles);

			if (pStringList)
			{
				TCHAR* pCurrent = pStringList;
				std::vector<std::wstring> list;

				while (*pCurrent)
				{
					std::wstring str(pCurrent);
					list.push_back(str);

					pCurrent += str.length() + 1;
				}

				// Done.  Unlock memory
				::GlobalUnlock(stgm.hGlobal);

				// add file list to project
				AddFilesToProject(m_nDropTarget, list);
			}
		}
	}

	DragCleanup();
	*pdwEffect = DROPEFFECT_NONE;
}

void CFontProjectView::DragCleanup()
{
	if (m_pDraggedObject)
	{
		m_pDraggedObject->Release();
		m_pDraggedObject = NULL;
	}

	m_bOverValidItem = false;
	m_nDropTarget = -1;

	Invalidate();
}

void CFontProjectView::OnSelectedFolderChange(const CString&, const CString*)
{
	// turn off projects
	CFontRunnerDoc::GetDoc()->UseFontProject(false);
}

void CFontProjectView::OnSelectedProjectChange(const std::wstring& strProjectName)
{
	// make sure projects are on
	CFontRunnerDoc::GetDoc()->UseFontProject(true);

	// show proper selection
	m_bDisableSignals = true;
	int nItems = GetListCtrl().GetItemCount();
	for (int nItem = 0; nItem < nItems; ++nItem)
	{
		CString strText = *((CString*)GetListCtrl().GetItemData(nItem));
		if (strText == strProjectName.c_str())
		{
			GetListCtrl().SetItemState(nItem, LVIS_SELECTED, LVIS_SELECTED);
			GetListCtrl().EnsureVisible(nItem, false);
		}
	}
}

void CFontProjectView::OnAddToProject(const std::vector<std::wstring>& list)
{
	AddFilesToProject(GetListCtrl().GetNextItem(-1, LVNI_SELECTED), list);
}

void CFontProjectView::OnRemovedFromProject(const CFontItem* pFontItem)
{
	// get selected item
	int nItem = GetListCtrl().GetNextItem(-1, LVNI_SELECTED);
	CString strText = *((CString*)GetListCtrl().GetItemData(nItem));

	if (nItem >= 0)
	{
		// remove the font
		theApp.GetFontProjects()->RemoveFile(strText, pFontItem->GetFullFileName());

		// find this item in the projects collection
		projectlist_type::const_iterator it = theApp.GetFontProjects()->Find(strText);

		// update the display
		CString strFormat, strDisplay;
		strFormat.LoadString(IDS_FONTPROJECTVIEW_DISPLAYSTRING);
		strDisplay.Format(strFormat, it->first, it->second->size());
		GetListCtrl().SetItemText(nItem, 0, strDisplay);
	}
}

void CFontProjectView::InsertFontProjects()
{
	std::for_each(theApp.GetFontProjects()->GetListBeginning(),
				  theApp.GetFontProjects()->GetListEnd(),
				  AddProjectsToList(GetListCtrl(), m_nNormalIconIndex));
}

void CFontProjectView::Rename(int nItem)
{
	CListCtrl& list = GetListCtrl();
	CString* pstrName = (CString*)list.GetItemData(nItem);
	CRenameDlg dlg(*pstrName, this);

	if (dlg.DoModal() == IDOK)
	{
		CString strNewName = dlg.GetNewName();

		// update name
		CFontProjects* pProjects = theApp.GetFontProjects();
		pProjects->Rename(*pstrName, strNewName);
		*pstrName = strNewName;

		// update list view item
		projectlist_type::const_iterator it = pProjects->Find(strNewName);
		
		CString strFormat, strDisplay;
		strFormat.LoadString(IDS_FONTPROJECTVIEW_DISPLAYSTRING);
		strDisplay.Format(strFormat, it->first, it->second->size());
		GetListCtrl().SetItemText(nItem, 0, strDisplay);
		GetListCtrl().SetColumnWidth(0, LVSCW_AUTOSIZE);
	}
}

void CFontProjectView::Delete(int nItem)
{
	CListCtrl& list = GetListCtrl();
	if (nItem == -1 || nItem >= list.GetItemCount())
		return;

	// get project name
	CString strProject = *((CString*)list.GetItemData(nItem));

	// load title string and format message string
	CString strMessage, strTitle;
	strTitle.LoadString(IDS_FONTPROJECTVIEW_CONFIRMDELETE_TITLE);
	AfxFormatString1(strMessage, IDS_FONTPROJECTVIEW_CONFIRMDELETE, strProject);
	
	// confirm deletion
	int nResult = MessageBox(strMessage, strTitle, MB_YESNO + MB_ICONQUESTION);
	if (nResult == IDYES)
	{
		theApp.GetFontProjects()->Remove(strProject);
		delete (CString*)list.GetItemData(nItem);
		list.DeleteItem(nItem);

		// go back to the previous history item
		if (CFontRunnerDoc::GetDoc()->GetCurrentProject() == strProject)
			((CMainFrame*)theApp.GetMainWnd())->RemoveCurrentHistoryItem();
	}
}

struct addfiles_to_project
{
	addfiles_to_project(LPCTSTR pszProjectName) : m_pszProjectName(pszProjectName)
	{}

	void operator()(const std::wstring& str)
	{
		// get size of file
		HANDLE hFile = ::CreateFile(str.c_str(), GENERIC_READ, FILE_SHARE_READ,
									NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

		if (hFile != INVALID_HANDLE_VALUE)
		{
			DWORD dwFileSizeHigh = 0;
			DWORD dwFileSize = ::GetFileSize(hFile, &dwFileSizeHigh);

			CruxTechnologies::COpenTypeData data;

			// if dwFileSizeHigh has any value then this file is out pf the question
			if (dwFileSizeHigh == 0 && data.Initialize(str.c_str(), dwFileSize) == CruxTechnologies::COpenTypeData::kStatus_OK)
			{
				using CruxTechnologies::OpenTypeFontData::CNameRecord;
				const std::wstring* pstrName = data.GetName(CNameRecord::kNameID_FullFontName);

				CFontProjects* pProjects = theApp.GetFontProjects();

				// add it to font projects structure
				if (pstrName && !pProjects->FileExists(m_pszProjectName, pstrName->c_str()))
				{
					pProjects->AddFile(m_pszProjectName, str.c_str());
					m_addedlist.push_back(str);
				}
			}

			::CloseHandle(hFile);
		}
	}

	const std::vector<std::wstring>& GetAddedFiles() const
	{
		return m_addedlist;
	}

private:
	LPCTSTR m_pszProjectName;
	std::vector<std::wstring> m_addedlist;
};

void CFontProjectView::AddFilesToProject(int nIndex, const std::vector<std::wstring> &list)
{
	CString strProjectName = *((CString*)GetListCtrl().GetItemData(nIndex));

	// add files to project
	addfiles_to_project adder =
		std::for_each(list.begin(), list.end(), addfiles_to_project(strProjectName));

	// if this project is being viewed, let main view know
	
	if (nIndex == GetListCtrl().GetNextItem(-1, LVNI_SELECTED) && CFontRunnerDoc::GetDoc()->UsingFontProject())
		theApp.GetSignals()->Fire_AddToMainFontListView(adder.GetAddedFiles());
	
	// update list view item
	projectlist_type::const_iterator it = theApp.GetFontProjects()->Find(strProjectName);
	
	CString strFormat, strDisplay;
	strFormat.LoadString(IDS_FONTPROJECTVIEW_DISPLAYSTRING);
	strDisplay.Format(strFormat, it->first, it->second->size());
	GetListCtrl().SetItemText(nIndex, 0, strDisplay);
	GetListCtrl().SetColumnWidth(0, LVSCW_AUTOSIZE);
}


BEGIN_MESSAGE_MAP(CFontProjectView, CListView)
	ON_WM_PAINT()
	ON_WM_DESTROY()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_NOTIFY_REFLECT(NM_CLICK, OnClick)
	ON_COMMAND(ID_PROJECTADD_BUTTON, OnProjectAddButton)
	ON_COMMAND(ID_PROJECTDELETE_BUTTON, OnProjectDeleteButton)
	ON_COMMAND(ID_PROJECTRENAME_BUTTON, OnProjectRenameButton)
	ON_UPDATE_COMMAND_UI(ID_PROJECTADD_BUTTON, OnUpdateProjectAddButton)
	ON_UPDATE_COMMAND_UI(ID_PROJECTDELETE_BUTTON, OnUpdateProjectDeleteButton)
	ON_UPDATE_COMMAND_UI(ID_PROJECTRENAME_BUTTON, OnUpdateProjectRenameButton)
	ON_COMMAND(ID_FONTPROJECTMENU_RENAME, OnFontProjectMenuRename)
	ON_COMMAND(ID_FONTPROJECTMENU_DELETE, OnFontProjectMenuDelete)
	ON_NOTIFY_REFLECT(LVN_ITEMCHANGED, OnLvnItemChanged)
END_MESSAGE_MAP()


// CFontProjectView diagnostics

#ifdef _DEBUG
void CFontProjectView::AssertValid() const
{
	CListView::AssertValid();
}

#ifndef _WIN32_WCE
void CFontProjectView::Dump(CDumpContext& dc) const
{
	CListView::Dump(dc);
}
#endif
#endif //_DEBUG


// CFontProjectView message handlers
void CFontProjectView::OnPaint()
{
	// makes painting during drag and drop more reliable
	if (m_pDraggedObject && m_pDropTargetHelper)
		m_pDropTargetHelper->Show(FALSE);

	CListView::OnPaint();

	if (m_pDraggedObject && m_pDropTargetHelper)
		m_pDropTargetHelper->Show(TRUE);
}

void CFontProjectView::OnDestroy()
{
	// make sure all item data is cleaned up
	for (int i = 0; i < GetListCtrl().GetItemCount(); ++i)
		delete (CString*)GetListCtrl().GetItemData(i);

	m_pDropTargetHelper->Release();
	::RevokeDragDrop(m_hWnd);
}

void CFontProjectView::OnProjectAddButton()
{
	// get new project name
	CString strNewName;
	strNewName.LoadString(IDS_FONTPROJECTVIEW_NEWPROJECT);

	CRenameDlg dlg(NULL);

	if (dlg.DoModal() == IDOK)
	{
		CString strNewName = dlg.GetNewName();
		CFontProjects* pProjects = theApp.GetFontProjects();
		pProjects->Add(strNewName);
		projectlist_type::const_iterator it = pProjects->Find(strNewName);

		CListCtrl& list = GetListCtrl();
		CString strFormat, strDisplay;
		strFormat.LoadString(IDS_FONTPROJECTVIEW_DISPLAYSTRING);
		strDisplay.Format(strFormat, it->first, it->second->size());
		int nIndex = list.InsertItem(0, strDisplay, m_nNormalIconIndex);
		list.SetItemData(nIndex, (DWORD_PTR)new CString(strNewName));
		GetListCtrl().SetColumnWidth(0, LVSCW_AUTOSIZE);
	}
}

void CFontProjectView::OnProjectDeleteButton()
{
	// get list control
	CListCtrl& list = GetListCtrl();
	int nSelected = list.GetNextItem(-1, LVNI_SELECTED);

	if (nSelected != -1)
		Delete(nSelected);
}

void CFontProjectView::OnProjectRenameButton()
{
	// get list control
	CListCtrl& list = GetListCtrl();
	int nSelected = list.GetNextItem(-1, LVNI_SELECTED);

	if (nSelected != -1)
		Rename(nSelected);
}

void CFontProjectView::OnUpdateProjectAddButton(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(true);
}

void CFontProjectView::OnUpdateProjectDeleteButton(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(GetListCtrl().GetNextItem(-1, LVNI_SELECTED) != -1);
}

void CFontProjectView::OnUpdateProjectRenameButton(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(GetListCtrl().GetNextItem(-1, LVNI_SELECTED) != -1);
}

void CFontProjectView::OnFontProjectMenuRename()
{
	Rename(m_nRClickItem);
}

void CFontProjectView::OnFontProjectMenuDelete()
{
	Delete(m_nRClickItem);
}

void CFontProjectView::OnLvnItemChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;

	// if selection state changed and we're using font projects
	if ((pNMListView->uChanged & LVIF_STATE) && (pNMListView->uNewState & LVIS_SELECTED))
	{
		CString strItem = *((CString*)GetListCtrl().GetItemData(pNMListView->iItem));

		if (CFontRunnerDoc::GetDoc()->GetCurrentProject() != strItem || !CFontRunnerDoc::GetDoc()->UsingFontProject())
		{
			CFontRunnerDoc::GetDoc()->UseFontProject(true);
			theApp.GetSignals()->Fire_SelectedProjectChange((LPCTSTR)strItem);
		}
	}

	*pResult = 0;
}

void CFontProjectView::OnClick(NMHDR* /*pNMHDR*/, LRESULT* pResult)
{
	if (!CFontRunnerDoc::GetDoc()->UsingFontProject())
	{
		DWORD dwPoint = ::GetMessagePos();
		CPoint ptScreen(GET_X_LPARAM(dwPoint), GET_Y_LPARAM(dwPoint));
		CPoint ptClient(ptScreen);

		ScreenToClient(&ptClient);

		// do a hit test
		int nItem = GetListCtrl().HitTest(ptClient);
		int nSelected = GetListCtrl().GetNextItem(-1, LVNI_SELECTED);
		if (nItem == nSelected && nItem != -1)
		{
			CFontRunnerDoc::GetDoc()->UseFontProject(true);
			CString strItem = *((CString*)GetListCtrl().GetItemData(nSelected));
			theApp.GetSignals()->Fire_SelectedProjectChange((LPCTSTR)strItem);
		}
	}

	*pResult = 0;
}

void CFontProjectView::OnRButtonDown(UINT /*nFlags*/, CPoint point)
{
	// Save the item right-clicked on.  We'll display a menu only if the mouse
	// stays on that item
	m_nRClickItem = GetListCtrl().HitTest(point);
}

void CFontProjectView::OnRButtonUp(UINT /*nFlags*/, CPoint point)
{
	int nItem = GetListCtrl().HitTest(point);

	if (nItem == m_nRClickItem && nItem != -1)
	{
		CListCtrl& list = GetListCtrl();

		// set focus
		int nLastFocused = list.GetNextItem(-1, LVIS_FOCUSED);
		list.SetItemState(nItem, LVIS_FOCUSED, LVIS_FOCUSED);

		// load and get submenu
		CMenu menu;
		CMenu* pSubMenu = NULL;
		menu.LoadMenu(IDR_FONTPROJECT_MENU);
		pSubMenu = menu.GetSubMenu(0);

		// convert client coordinates to screen coordinates
		POINT ptScreen = { point.x, point.y };
		ClientToScreen(&ptScreen);

		// show menu
		pSubMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, ptScreen.x, ptScreen.y, this);

		// restore focus
		list.SetItemState(nLastFocused, LVIS_FOCUSED, LVIS_FOCUSED);
	}
}