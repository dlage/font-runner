/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "ThemeSupport.h"
#include "PaletteDlg.h"

// CPaletteWnd frame

class CPaletteWnd : public CMiniFrameWnd, CThemeSupport
{
	DECLARE_DYNCREATE(CPaletteWnd)
public:
	CPaletteWnd();
	virtual ~CPaletteWnd();

// attributes
protected:
	CPaletteDlg*	m_pdlgColor;
	HWND			m_hwndRevealer;
	bool			m_bDoNotHide;
	bool			m_bForeground;
	static HHOOK	s_hMouseHook;
	static CPaletteWnd* s_pwndPalette;

// operations
public:
	bool Create(CWnd* pParentWnd);
	void Appear(const CString& strTitleText, COLORREF clrCurrent, const CRect& rectOrigin, bool bForeground, HWND hwndRevealer);
	void Disappear();
	bool IsForeground() const;

private:
	static LRESULT CALLBACK MouseProc(int nCode, WPARAM wParam, LPARAM lParam);
	void MouseDown(POINT& pt);

protected:
	DECLARE_MESSAGE_MAP()
	virtual void PostNcDestroy();
public:
	afx_msg void OnClose();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
};


