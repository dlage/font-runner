/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include "afxwin.h"
#include "afxcmn.h"


// COptionsPageGeneral dialog

class COptionsPageGeneral : public CPropertyPage
{
	DECLARE_DYNAMIC(COptionsPageGeneral)

public:
	COptionsPageGeneral();
	virtual ~COptionsPageGeneral();

// internals
private:
	void InitProjectCombo();
	void InitStartupCombo();

// Attributes
private:

// Dialog Data
private:
	enum { IDD = IDD_GENERAL_PAGE };
	CString m_strThisFolderEdit;
	CEdit m_ctrlThisFolderEdit;
	CComboBoxEx m_ctrlThisProjectCombo;
	CImageList m_ilProjectCombo;
	CButton m_btnBrowseThisFolder;
	CButton m_btnUseCurrentFolder, m_btnUseCurrentProject;
	CButton m_btnShowFullPathInTitleBarCheck;
	CButton m_btnClearLocationsOnExitCheck;
	CButton m_btnLimitLocationsCheck;
	int m_nLocationsLimitEdit;
	CEdit m_ctrlLocationsLimitEdit;
	CComboBox m_ctrlStartupCombo;

// overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual BOOL OnApply();
	virtual void HtmlHelp(DWORD_PTR dwData, UINT nCmd = 0x000F);

public:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnEnChangeThisFolderEdit();
	afx_msg void OnBnClickedBrowseFolder();
	afx_msg void OnCbnSelchangeThisprojectCombo();
	afx_msg void OnCbnSelchangeStartupCombo();
	afx_msg void OnHelpButton(UINT id, NMHDR* pNotifyStruct, LRESULT* result);
	afx_msg void OnBnClickedUsecurrentButton();
	afx_msg void OnBnClickedUsecurrentprojectButton();
	afx_msg void OnBnClickedFullpathCheck();
	afx_msg void OnBnClickedClearLocationsCheck();
	afx_msg void OnBnClickedLimitLocationsCheck();
	afx_msg void OnBnClickedClearLocationsNow();
	afx_msg void OnEnChangeLocationsEdit();
};
