/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "StdAfx.h"
#include "FontFileDataSource.h"

// interface map for our object
BEGIN_INTERFACE_MAP(CFontFileDataSource, COleDataSource)
	INTERFACE_PART(CFontFileDataSource, IID_IDataObject, DataObj)
END_INTERFACE_MAP()

STDMETHODIMP_(ULONG) CFontFileDataSource::XDataObj::AddRef()
{
	METHOD_PROLOGUE(CFontFileDataSource, DataObj)
	return pThis->ExternalAddRef();
}

STDMETHODIMP_(ULONG) CFontFileDataSource::XDataObj::Release()
{
	METHOD_PROLOGUE(CFontFileDataSource, DataObj)
	return pThis->ExternalRelease();
}

STDMETHODIMP CFontFileDataSource::XDataObj::QueryInterface(
	REFIID iid, LPVOID* ppvObj)
{
	METHOD_PROLOGUE(CFontFileDataSource, DataObj)
	return pThis->ExternalQueryInterface(&iid, ppvObj);
}

STDMETHODIMP CFontFileDataSource::XDataObj::GetData(LPFORMATETC pFormatetc, LPSTGMEDIUM pmedium)
{
	METHOD_PROLOGUE(CFontFileDataSource, DataObj)
	return pThis->m_xDataObject.GetData(pFormatetc,pmedium);
}

STDMETHODIMP CFontFileDataSource::XDataObj::GetDataHere(LPFORMATETC pFormatetc, LPSTGMEDIUM pmedium)
{
	METHOD_PROLOGUE(CFontFileDataSource, DataObj)
	return pThis->m_xDataObject.GetDataHere(pFormatetc,pmedium);
}

STDMETHODIMP CFontFileDataSource::XDataObj::QueryGetData(LPFORMATETC pFormatetc)
{
	METHOD_PROLOGUE(CFontFileDataSource, DataObj)
	return pThis->m_xDataObject.QueryGetData(pFormatetc);
}

STDMETHODIMP CFontFileDataSource::XDataObj::GetCanonicalFormatEtc(LPFORMATETC pFormatetcIn, LPFORMATETC pFormatetcOut)
{
	METHOD_PROLOGUE(CFontFileDataSource, DataObj)
	return pThis->m_xDataObject.GetCanonicalFormatEtc(pFormatetcIn,pFormatetcOut);
}

STDMETHODIMP CFontFileDataSource::XDataObj::SetData(LPFORMATETC pFormatetc, LPSTGMEDIUM pmedium, BOOL fRelease)
{
	METHOD_PROLOGUE(CFontFileDataSource, DataObj)
	// normal processing
	HRESULT hr = pThis->m_xDataObject.SetData(pFormatetc,pmedium,fRelease);
	if (hr==DATA_E_FORMATETC) {
		// cache the data explicitly
		pThis->CacheData(pFormatetc->cfFormat,pmedium,pFormatetc);
		return S_OK;
	}
	// normal error
	return hr;
}

STDMETHODIMP CFontFileDataSource::XDataObj::EnumFormatEtc(DWORD dwDirection, LPENUMFORMATETC* ppenumFormatetc)
{
	METHOD_PROLOGUE(CFontFileDataSource, DataObj)
	return pThis->m_xDataObject.EnumFormatEtc(dwDirection,ppenumFormatetc);
}

STDMETHODIMP CFontFileDataSource::XDataObj::DAdvise(LPFORMATETC pFormatetc, DWORD advf, LPADVISESINK pAdvSink, LPDWORD pdwConnection)
{
	METHOD_PROLOGUE(CFontFileDataSource, DataObj)
	return pThis->m_xDataObject.DAdvise(pFormatetc,advf,pAdvSink,pdwConnection);
}

STDMETHODIMP CFontFileDataSource::XDataObj::DUnadvise(DWORD dwConnection)
{
	METHOD_PROLOGUE(CFontFileDataSource, DataObj)
	return pThis->m_xDataObject.DUnadvise(dwConnection);
}

STDMETHODIMP CFontFileDataSource::XDataObj::EnumDAdvise(LPENUMSTATDATA* ppenumAdvise)
{
	METHOD_PROLOGUE(CFontFileDataSource, DataObj)
	return pThis->m_xDataObject.EnumDAdvise(ppenumAdvise);
}

