/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// FontRunnerDoc.cpp : implementation of the CFontRunnerDoc class
//

#include "stdafx.h"
#include "FontRunner.h"
#include "MainFrm.h"
#include "FontRunnerDoc.h"
#include "FontRunnerSignals.h"
#include "ProgramOptions.h"
#include "OptionsSheet.h"
#include "ShellUtils.h"
#include "Key.h"
#include "FindNameFrameWnd.h"
#include "FontItem.h"
#include "FontManager.h"

#include <shlwapi.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CFontRunnerDoc

IMPLEMENT_DYNCREATE(CFontRunnerDoc, CDocument)

BEGIN_MESSAGE_MAP(CFontRunnerDoc, CDocument)
	ON_COMMAND(ID_VIEW_OPTIONS, OnViewOptions)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, OnFilePrintPreview)
	ON_COMMAND(ID_VIEW_SEARCH, OnViewSearch)
END_MESSAGE_MAP()


// CFontRunnerDoc construction/destruction

CFontRunnerDoc::CFontRunnerDoc()
  : m_pFontItem(NULL),
    m_pCurrentShellFolder(NULL),
	m_pCurrentIDL(NULL),
	m_bUseFontProject(false)
{
	// TODO: add one-time construction code here

}

CFontRunnerDoc::~CFontRunnerDoc()
{
}

BOOL CFontRunnerDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// get name of system fonts folder from registry
	// grab location of Windows font folder
	TCHAR szFontFolder[MAX_PATH];
	::SHGetFolderPath(NULL, CSIDL_FONTS, NULL, SHGFP_TYPE_CURRENT, szFontFolder);
	::PathRemoveBackslash(szFontFolder);

	m_strSystemFontsFolder = szFontFolder;

	// sign up for some signals
	CFontRunnerSignals* pSignals = theApp.GetSignals();
	pSignals->ConnectTo_SelectedFontChange(boost::bind(&CFontRunnerDoc::OnSelectedFontChange, this, _1));
	pSignals->ConnectTo_SelectedProjectChange(boost::bind(&CFontRunnerDoc::OnSelectedProjectChange, this, _1));
	pSignals->ConnectTo_SelectedFolderChange(boost::bind(&CFontRunnerDoc::OnSelectedFolderChange, this, _1, _2));
	pSignals->ConnectTo_FontDereferenced(boost::bind(&CFontRunnerDoc::OnFontDereferenced, this, _1));
	pSignals->ConnectTo_OptionsChange(boost::bind(&CFontRunnerDoc::OnOptionsChange, this));

	return TRUE;
}

BOOL CFontRunnerDoc::OnOpenDocument(LPCTSTR /*lpszPathName*/)
{
	// just return TRUE to prevent MFC from going crazy because
	// we don't handle file opens here.
	// This is really because we handle command-line parameters in CFontRunnerApp::OnInitInstance.
	return TRUE;
}

void CFontRunnerDoc::OnCloseDocument()
{
	// just send shutdown signal and let connected classes else take care of the rest
	theApp.GetSignals()->Fire_Shutdown();

	theApp.GetProgramOptions()->UseFontProject(m_bUseFontProject);

	CDocument::OnCloseDocument();
}

CFontRunnerDoc* CFontRunnerDoc::GetDoc()
{
	CFrameWnd* pFrame = (CFrameWnd*)AfxGetApp()->m_pMainWnd;

	if (pFrame)
		return (CFontRunnerDoc*)pFrame->GetActiveDocument();
	else
		return NULL;
}

void CFontRunnerDoc::SetCurrentFolder(IShellFolder* pShellFolder, LPITEMIDLIST pidl)
{
	m_pCurrentShellFolder = pShellFolder;
	m_pCurrentIDL = pidl;

	UpdateTitle();
}

void CFontRunnerDoc::GetCurrentFolder(IShellFolder** ppShellFolder, LPITEMIDLIST* ppidl) const
{
	ASSERT(ppShellFolder && ppidl);
	*ppShellFolder = m_pCurrentShellFolder;
	*ppidl = m_pCurrentIDL;
}

CString CFontRunnerDoc::GetCurrentFolderName() const
{
	return ShellUtils::GetName(m_pCurrentShellFolder, m_pCurrentIDL, SHGDN_NORMAL);
}

CString CFontRunnerDoc::GetCurrentFolderNameFQ() const
{
	return ShellUtils::GetName(m_pCurrentShellFolder, m_pCurrentIDL, SHGDN_FORPARSING);
}

const CFontItem* CFontRunnerDoc::GetCurrentFontItem() const
{
	// verify what we think is the current font item still exists
	if (theApp.GetFontManager()->IsItemValid(m_pFontItem))
		return m_pFontItem;
	else
		return NULL;
}

void CFontRunnerDoc::SearchForFont(LPCTSTR lpszSearchRoot, bool bRootIsProject)
{
	CFindNameFrameWnd* pFindWindow = new CFindNameFrameWnd(lpszSearchRoot, bRootIsProject);
	pFindWindow->Create(theApp.GetMainWnd());
}

void CFontRunnerDoc::UpdateTitle()
{
	// figure out what to put in the title
	CString strTitle;

	// display full path in title bar unless it is a "special" folder
	// whose full path name is not human readable.
	if (theApp.GetProgramOptions()->ShowFullPathInTitleBar())
	{
		strTitle = GetCurrentFolderNameFQ();
		if (strTitle.Left(2) == _T("::"))
			strTitle = GetCurrentFolderName();
	}
	else
		strTitle = GetCurrentFolderName();

	SetTitle(strTitle);
}


// CFontRunnerDoc serialization

void CFontRunnerDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


// CFontRunnerDoc diagnostics

#ifdef _DEBUG
void CFontRunnerDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CFontRunnerDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CFontRunnerDoc commands



void CFontRunnerDoc::OnViewOptions()
{
	COptionsSheet dlgOptions(IDS_OPTIONSDIALOG_TITLE, theApp.GetMainWnd());
	dlgOptions.DoModal();
}

void CFontRunnerDoc::OnFilePrint() 
{
	// get pointer to font list
	CMainFrame* pMainFrame = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CWnd* pWnd = pMainFrame->m_wndFontViewSplit.GetPane(0,0);

	// pass this message along to the font list
	pWnd->SendMessage(WM_COMMAND, ID_FILE_PRINT);
}

void CFontRunnerDoc::OnFilePrintPreview() 
{
	// get pointer to font list
	CMainFrame* pMainFrame = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CWnd* pWnd = pMainFrame->m_wndFontViewSplit.GetPane(0,0);

	// pass this message along to the font list
	pWnd->SendMessage(WM_COMMAND, ID_FILE_PRINT_PREVIEW);
}

void CFontRunnerDoc::OnViewSearch()
{
	if (m_bUseFontProject)
		SearchForFont(m_strCurrentProject, true);
	else
		SearchForFont(GetCurrentFolderNameFQ());
}

void CFontRunnerDoc::OnSelectedFontChange(const CFontItem* pData)
{
	m_pFontItem = pData;
}

void CFontRunnerDoc::OnFontDereferenced(const CFontItem* pData)
{
	if (m_pFontItem == pData)
		m_pFontItem = NULL;
}

void CFontRunnerDoc::OnSelectedProjectChange(const std::wstring& strProjectName)
{
	SetTitle(strProjectName.c_str());
	m_strCurrentProject = strProjectName.c_str();
}

void CFontRunnerDoc::OnSelectedFolderChange(const CString& /*strFolder*/, const CString* /*pstrFile*/)
{
	UpdateTitle();
}

bool CFontRunnerDoc::UsingFontProject() const
{
	return m_bUseFontProject;
}

void CFontRunnerDoc::UseFontProject(bool bUse)
{
	m_bUseFontProject = bUse;
}

void CFontRunnerDoc::OnOptionsChange()
{
	UpdateTitle();
}