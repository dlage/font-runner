/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include "afxwin.h"


// CConfirmDeleteDialog dialog

class CConfirmDeleteDialog : public CDialog
{
	DECLARE_DYNAMIC(CConfirmDeleteDialog)

public:
	CConfirmDeleteDialog(bool bMultiple = false, CWnd* pParent = NULL);   // standard constructor
	virtual ~CConfirmDeleteDialog();

protected:
	virtual BOOL OnInitDialog();

private:
	bool m_bMultiple;

// controls
private:
	CStatic m_ctrlMessageStatic;
	CStatic m_ctrlRecycleBinIcon;
	HICON m_hRecycleBinIcon;

// Dialog Data
	enum { IDD = IDD_DELETECONFIRM_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};
