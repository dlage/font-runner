/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// FontInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "FontInfoDlg.h"
#include "FontItem.h"
#include "OpenTypeData.h"
#include <algorithm>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CFontInfoDlg dialog

IMPLEMENT_DYNAMIC(CFontInfoDlg, CDialog)
CFontInfoDlg::CFontInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFontInfoDlg::IDD, pParent),
	  m_pFontItem(NULL)
{
}

CFontInfoDlg::~CFontInfoDlg()
{
}

void CFontInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FONTINFO_LIST, m_ctrlFontInfoList);
}

void CFontInfoDlg::SetFontInfo(CFontItem* pFontItem)
{
	m_pFontItem = pFontItem;
}

// function object loads name record info into list control
class PopulateList
{
public:
	PopulateList(CListCtrl* pListCtrl)
		: m_pListCtrl(pListCtrl)
	{}

	void operator()(const CruxTechnologies::OpenTypeFontData::CNameRecord& record) const
	{
		using CruxTechnologies::OpenTypeFontData::CNameRecord;
		unsigned int nID = 0;
		switch (record.NameID())
		{
			case CNameRecord::kNameID_Copyright:
				nID = IDS_FONTINFODLG_NAME_COPYRIGHT;
				break;
			case CNameRecord::kNameID_FamilyName:
				nID = IDS_FONTINFODLG_NAME_FAMILYNAME;
				break;
			case CNameRecord::kNameID_SubFamilyName:
				nID = IDS_FONTINFODLG_NAME_SUBFAMILYNAME;
				break;
			case CNameRecord::kNameID_UniqueID:
				nID = IDS_FONTINFODLG_NAME_UNIQUEID;
				break;
			case CNameRecord::kNameID_FullFontName:
				nID = IDS_FONTINFODLG_NAME_FULLFONTNAME;
				break;
			case CNameRecord::kNameID_Version:
				nID = IDS_FONTINFODLG_NAME_VERSION;
				break;
			case CNameRecord::kNameID_PostscriptName:
				nID = IDS_FONTINFODLG_NAME_POSTSCRIPTNAME;
				break;
			case CNameRecord::kNameID_Trademark:
				nID = IDS_FONTINFODLG_NAME_TRADEMARK;
				break;
			case CNameRecord::kNameID_Manufacturer:
				nID = IDS_FONTINFODLG_NAME_MANUFACTURER;
				break;
			case CNameRecord::kNameID_Designer:
				nID = IDS_FONTINFODLG_NAME_DESIGNER;
				break;
			case CNameRecord::kNameID_Description:
				nID = IDS_FONTINFODLG_NAME_DESCRIPTION;
				break;
			case CNameRecord::kNameID_VendorURL:
				nID = IDS_FONTINFODLG_NAME_URLVENDOR;
				break;
			case CNameRecord::kNameID_DesignerURL:
				nID = IDS_FONTINFODLG_NAME_URLDESIGNER;
				break;
			case CNameRecord::kNameID_License:
				nID = IDS_FONTINFODLG_NAME_LICENSE;
				break;
			case CNameRecord::kNameID_LicenseURL:
				nID = IDS_FONTINFODLG_NAME_LICENSEURL;
				break;
			case CNameRecord::kNameID_SampleText:
				nID = IDS_FONTINFODLG_NAME_SAMPLETEXT;
				break;
		}

		// insert string
		if (nID)
		{
			CString strText;
			strText.LoadString(nID);
			int nIndex = m_pListCtrl->InsertItem(m_pListCtrl->GetItemCount(), strText);
			m_pListCtrl->SetItemText(nIndex, 1, record.Name()->c_str());
		}
	}

private:
	CListCtrl* m_pListCtrl;
};


BEGIN_MESSAGE_MAP(CFontInfoDlg, CDialog)
END_MESSAGE_MAP()

// CFontInfoDlg message handlers
BOOL CFontInfoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// m_pFontItem must be set using SetFontInfo (see header)
	ASSERT(m_pFontItem);

	// set extended styles
	m_ctrlFontInfoList.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES | LVS_EX_LABELTIP);

	// get font name for dialog title
	using CruxTechnologies::OpenTypeFontData::CNameRecord;
	const std::wstring* pstrText = 
		m_pFontItem->GetOpenTypeData()->GetName(CNameRecord::kNameID_FullFontName);
	SetWindowText(pstrText->c_str());

	// insert two columns
	CString strText;
	strText.LoadString(IDS_FONTINFODLG_NAME);
	m_ctrlFontInfoList.InsertColumn(0, strText, LVCFMT_LEFT, 150, 0);
	
	strText.LoadString(IDS_FONTINFODLG_VALUE);
	m_ctrlFontInfoList.InsertColumn(1, strText, LVCFMT_LEFT, 215, 1);

	// insert file name
	strText.LoadString(IDS_FONTINFODLG_NAME_FILENAME);
	m_ctrlFontInfoList.InsertItem(0, strText);
	m_ctrlFontInfoList.SetItemText(0, 1, m_pFontItem->GetFileName());

	// get name list
	using CruxTechnologies::OpenTypeFontData::CNamingTable;
	const CNamingTable::NameRecordContainer_Type& namelist = m_pFontItem->GetOpenTypeData()->GetNameRecords();

	// populate list control with every name we found
	PopulateList populate(&m_ctrlFontInfoList);
	std::for_each(namelist.begin(), namelist.end(), populate);

	// insert panose number
	strText.LoadString(IDS_FONTINFODLG_PANOSE);
	int nIndex = m_ctrlFontInfoList.InsertItem(m_ctrlFontInfoList.GetItemCount(), strText);

	PANOSE* pPanose = (PANOSE*)&m_pFontItem->GetOpenTypeData()->GetOS2Table().m_table.part0.panose;
	strText.Format(_T("%d%d%d%d%d%d%d%d%d%d"),
				   pPanose->bFamilyType,
				   pPanose->bSerifStyle,
				   pPanose->bWeight,
				   pPanose->bProportion,
				   pPanose->bContrast,
				   pPanose->bStrokeVariation,
				   pPanose->bArmStyle,
				   pPanose->bLetterform,
				   pPanose->bMidline,
				   pPanose->bXHeight);
	m_ctrlFontInfoList.SetItemText(nIndex, 1, strText);

	// adjust column size
	m_ctrlFontInfoList.SetColumnWidth(0, LVSCW_AUTOSIZE);
	m_ctrlFontInfoList.SetColumnWidth(1, LVSCW_AUTOSIZE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
