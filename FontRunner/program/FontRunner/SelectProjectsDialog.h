/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include "afxcmn.h"
#include "afxwin.h"


// CSelectProjectsDialog dialog

class CSelectProjectsDialog : public CDialog
{
	DECLARE_DYNAMIC(CSelectProjectsDialog)

public:
	CSelectProjectsDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSelectProjectsDialog();

// Dialog Data
	enum { IDD = IDD_SELECTPROJECTS_DIALOG };

public:
	const CString& GetSelectedProject() const { return m_strSelectedProject; }

// overrides
protected:
	virtual BOOL OnInitDialog();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

private:
	CListCtrl m_ctrlProjectList;
	CImageList m_ilProjects;
	CButton m_btnOk;
	CString m_strSelectedProject;

private:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnDestroy();
	afx_msg void OnLvnItemchangedProjectSelectionList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkProjectSelectionList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedOk();
};
