/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// SelectProjectsDialog.cpp : implementation file
//
#include "stdafx.h"
#include "FontRunner.h"
#include "SelectProjectsDialog.h"

#include "FontProjects.h"
#include "FontProjectView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CSelectProjectsDialog dialog

IMPLEMENT_DYNAMIC(CSelectProjectsDialog, CDialog)

CSelectProjectsDialog::CSelectProjectsDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectProjectsDialog::IDD, pParent)
{

}

CSelectProjectsDialog::~CSelectProjectsDialog()
{
}

void CSelectProjectsDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROJECT_SELECTION_LIST, m_ctrlProjectList);
	DDX_Control(pDX, IDOK, m_btnOk);
}

BOOL CSelectProjectsDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// add one column to list control
	m_ctrlProjectList.InsertColumn(0, _T("Project"));

	// setup imagelist
	m_ilProjects.Create(16, 16, ILC_COLOR32, 1, 0);

	HICON hIcon = theApp.LoadIcon(IDI_FONTPROJECT_ICON);
	int nProjectIcon = m_ilProjects.Add(hIcon);
	::DestroyIcon(hIcon);
	
	// attach it to the list control
	m_ctrlProjectList.SetImageList(&m_ilProjects, LVSIL_SMALL);

	// add projects to list
	std::for_each(theApp.GetFontProjects()->GetListBeginning(),
				  theApp.GetFontProjects()->GetListEnd(),
				  AddProjectsToList(m_ctrlProjectList, nProjectIcon));

	return TRUE;
}

BEGIN_MESSAGE_MAP(CSelectProjectsDialog, CDialog)
	ON_WM_DESTROY()
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_PROJECT_SELECTION_LIST, &CSelectProjectsDialog::OnLvnItemchangedProjectSelectionList)
	ON_NOTIFY(NM_DBLCLK, IDC_PROJECT_SELECTION_LIST, &CSelectProjectsDialog::OnNMDblclkProjectSelectionList)
	ON_BN_CLICKED(IDOK, &CSelectProjectsDialog::OnBnClickedOk)
END_MESSAGE_MAP()


// CSelectProjectsDialog message handlers

void CSelectProjectsDialog::OnDestroy()
{
	// don't forget to delete CStrings
	for (int i = 0; i < m_ctrlProjectList.GetItemCount(); ++i)
		delete (CString*)m_ctrlProjectList.GetItemData(i);

	CDialog::OnDestroy();
}

void CSelectProjectsDialog::OnLvnItemchangedProjectSelectionList(NMHDR* /*pNMHDR*/, LRESULT *pResult)
{
	//LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	
	//m_btnOk.EnableWindow(m_ctrlProjectList.GetSelectedCount());

	*pResult = 0;
}

void CSelectProjectsDialog::OnNMDblclkProjectSelectionList(NMHDR* pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	
	if (pNMItemActivate->iItem >= 0)
		OnBnClickedOk();

	*pResult = 0;
}

void CSelectProjectsDialog::OnBnClickedOk()
{
	int n = m_ctrlProjectList.GetNextItem(-1, LVNI_SELECTED);
	if (n != -1)
	{
		CString* pstr = reinterpret_cast<CString*>(m_ctrlProjectList.GetItemData(n));
		m_strSelectedProject = *pstr;
	}

	OnOK();
}
