/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// FontDetailFrame.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "FontRunnerDoc.h"
#include "FontDetailFrame.h"
#include "RichTextPreviewView.h"
#include "FontMapView.h"
#include "FontRunnerSignals.h"
#include "ProgramOptions.h"
#include "FontItem.h"
#include "OpenTypeData.h"
#include "WinToolbox.h"
#include "PaletteWnd.h"

#pragma warning(push, 3)
#include <boost/lexical_cast.hpp>
#pragma warning(pop)

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// Generic function for creating a view
template <class T>
T* CreateFontViewClass(CFontDetailFrame* pFrame)
{
	// setup creation context
	CCreateContext context;
	context.m_pCurrentDoc = NULL;
	context.m_pCurrentFrame = pFrame;
	context.m_pLastView = NULL;
	context.m_pNewDocTemplate = NULL;
	context.m_pNewViewClass = RUNTIME_CLASS(T);

	// create the new font preview view
	return static_cast<T*>(pFrame->CreateView(&context, AFX_IDW_PANE_FIRST));
}


// CFontDetailFrame

IMPLEMENT_DYNCREATE(CFontDetailFrame, CFrameWnd)

CFontDetailFrame::CFontDetailFrame()
 : m_nCurrentView((CProgramOptions::eFontDetailMode)theApp.GetProgramOptions()->GetFontDetailMode()),
   m_pPreviewView(NULL),
   m_pMapView(NULL),
   m_nFormatToolbarIndex(-1),
   m_nForegroundIndex(-1),
   m_nBackgroundIndex(-1),
   m_pwndPalette(new CPaletteWnd())
{
}

CFontDetailFrame::~CFontDetailFrame()
{
}

struct AddSizeToCombo
{
	AddSizeToCombo(CComboBox* pCombo, CDC* pDC)
		: m_pCombo(pCombo),
		  m_pDC(pDC),
		  m_nLongestStringWidth(0)
	{
		m_strPixels.LoadString(IDS_FONTLIST_PX);
	}

	void operator ()(unsigned short nSize)
	{
		CString strPixels;
		strPixels.Format(m_strPixels, nSize);
		m_pCombo->AddString(strPixels);

		m_nLongestStringWidth = std::max<int>(m_nLongestStringWidth, m_pDC->GetTextExtent(strPixels).cx);
	}

	int GetLongestStringWidth() const
	{
		return m_nLongestStringWidth;
	}

private:
	CComboBox* m_pCombo;
	CString m_strPixels;
	CDC* m_pDC;
	int m_nLongestStringWidth;
};

bool CFontDetailFrame::CreatePreviewToolbarBand()
{
	bool bSuccess = true;
	boost::shared_ptr<CDC> pDC(GetDC(), boost::bind(&CWnd::ReleaseDC, this, _1));
	int nSavedDC = pDC->SaveDC();

	// set dialog font for text measurement
	pDC->SelectObject(theApp.GetDialogFont());

	try
	{
		CString strText;
		int nRebarHeight = 0;
		UINT nBand = 0;

		// add mode combo
		if (m_ctrlModeCombo.Create(WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_CLIPSIBLINGS | CBS_DROPDOWNLIST,
								   CRect(0, 0, 0, 0),
								   this,
								   ID_FONTDETAILMODE_COMBO))
		{
			int nComboWidth = 0;
			m_ctrlModeCombo.SetFont(theApp.GetDialogFont());

			strText.LoadString(IDS_PREVIEW_TAB_PREVIEW);
			m_ctrlModeCombo.InsertString(CProgramOptions::kMode_Preview, strText);
			nComboWidth = std::max<int>(nComboWidth, pDC->GetTextExtent(strText).cx);

			strText.LoadString(IDS_PREVIEW_TAB_MAP);
			m_ctrlModeCombo.InsertString(CProgramOptions::kMode_FontMap, strText);
			nComboWidth = std::max<int>(nComboWidth, pDC->GetTextExtent(strText).cx);

			m_ctrlModeCombo.SetCurSel(theApp.GetProgramOptions()->GetFontDetailMode());

			strText.LoadString(IDS_PREVIEWVIEW_MODE);
			m_wndPreviewRebar.AddBar(&m_ctrlModeCombo, strText, NULL, RBBS_NOGRIPPER);

			// set up min/max sizes and ideal sizes for preview toolbar
			REBARBANDINFO rbbi;
			rbbi.cbSize = sizeof(rbbi);
			rbbi.fMask = RBBIM_CHILDSIZE | RBBIM_IDEALSIZE | RBBIM_SIZE;
			rbbi.cx = rbbi.cxIdeal = rbbi.cxMinChild = nComboWidth + CWinToolbox::GetExtraComboWidth();
			rbbi.cyMinChild = CWinToolbox::GetComboHeight(pDC.get());
			m_wndPreviewRebar.GetReBarCtrl().SetBandInfo(nBand++, &rbbi);

			nRebarHeight = std::max<int>(nRebarHeight, rbbi.cyMinChild);

			m_ctrlModeCombo.SetWindowPos(NULL, 0, 0, nComboWidth, 100, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER);
		}

		// add size combo
		strText.LoadString(IDS_PREVIEWVIEW_SIZE);

		// create combobox
		int nComboWidth = 0;
		if (m_ctrlSizeCombo.Create(WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_CLIPSIBLINGS | CBS_DROPDOWN,
								   CRect(0, 0, 0, 0),
								   this,
								   ID_SIZE_COMBO))
		{
			// setup combo
			m_ctrlSizeCombo.SetFont(theApp.GetDialogFont());
			m_ctrlSizeCombo.LimitText(3);

			// add values to combo
			const commonsizelist_type& sizelist = theApp.GetCommonSizeList();
			AddSizeToCombo comboadder = std::for_each(sizelist.begin(), sizelist.end(), AddSizeToCombo(&m_ctrlSizeCombo, pDC.get()));

			nComboWidth = CWinToolbox::GetExtraComboWidth() + comboadder.GetLongestStringWidth();

			// get program options
			CString strFormat, strPixels;
			strPixels.LoadString(IDS_FONTLIST_PX);
			strFormat.Format(strPixels, theApp.GetProgramOptions()->GetPreviewFontSize());
			m_ctrlSizeCombo.SetWindowText(strFormat);

			m_wndPreviewRebar.AddBar(&m_ctrlSizeCombo, strText, NULL, RBBS_NOGRIPPER);

			// set up min/max sizes and ideal sizes for preview toolbar
			REBARBANDINFO rbbi;
			rbbi.cbSize = sizeof(rbbi);
			rbbi.fMask = RBBIM_CHILDSIZE | RBBIM_IDEALSIZE | RBBIM_SIZE;
			rbbi.cx = rbbi.cxIdeal = rbbi.cxMinChild = nComboWidth;
			rbbi.cyMinChild = CWinToolbox::GetComboHeight(pDC.get());
			m_wndPreviewRebar.GetReBarCtrl().SetBandInfo(nBand++, &rbbi);

			nRebarHeight = std::max<int>(nRebarHeight, rbbi.cyMinChild);

			m_ctrlSizeCombo.SetWindowPos(NULL, 0, 0, nComboWidth, 150, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER);
		}
		else
			throw std::runtime_error("Could not create size combo");

		// create color toolbar
		if (m_wndColorToolbar.CreateEx(this, TBSTYLE_FLAT | TBSTYLE_TRANSPARENT,
				WS_CHILD | WS_VISIBLE | CBRS_ALIGN_TOP | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC))
		{
			// add buttons
			TBBUTTON buttons[2];
			buttons[0].iBitmap = 0;
			buttons[0].idCommand = ID_PREVIEW_FGCOLOR;
			buttons[0].fsState = 0;
			buttons[0].fsStyle = TBBS_CHECKBOX;
			buttons[0].dwData = NULL;
			buttons[0].iString = NULL;

			buttons[1].iBitmap = 1;
			buttons[1].idCommand = ID_PREVIEW_BGCOLOR;
			buttons[1].fsState = 0;
			buttons[1].fsStyle = TBBS_CHECKBOX;
			buttons[1].dwData = NULL;
			buttons[1].iString = NULL;

			m_wndColorToolbar.GetToolBarCtrl().AddButtons(2, buttons);

			CBitmap bitmapForeground, bitmapBackground;
			m_bitmapMask.LoadBitmap(IDB_COLORSELECTMASK_BITMAP);

			if (theApp.GetProgramOptions()->GetFontDetailMode() == CProgramOptions::kMode_Preview)
			{
				CreateColorSelectBitmap(theApp.GetProgramOptions()->GetPreviewFontColor(), bitmapForeground);
				CreateColorSelectBitmap(theApp.GetProgramOptions()->GetPreviewFontBackgroundColor(), bitmapBackground);
			}
			else
			{
				CreateColorSelectBitmap(theApp.GetProgramOptions()->GetFontMapFontColor(), bitmapForeground);
				CreateColorSelectBitmap(theApp.GetProgramOptions()->GetFontMapFontBackgroundColor(), bitmapBackground);
			}

			m_ilColorToolbarNormal.Create(16, 16, ILC_COLOR24 | ILC_MASK, 2, 0);
			m_nForegroundIndex = m_ilColorToolbarNormal.Add(&bitmapForeground, &m_bitmapMask);
			m_nBackgroundIndex = m_ilColorToolbarNormal.Add(&bitmapBackground, &m_bitmapMask);

			m_wndColorToolbar.GetToolBarCtrl().SetImageList(&m_ilColorToolbarNormal);

			m_wndPreviewRebar.AddBar(&m_wndColorToolbar, NULL, NULL, RBBS_NOGRIPPER);

			CRect rectToolbar;
			int nLastButton = m_wndColorToolbar.GetToolBarCtrl().GetButtonCount() - 1;
			m_wndColorToolbar.GetItemRect(nLastButton, &rectToolbar);
			rectToolbar.left = 0;

			// set up min/max sizes and ideal sizes for preview toolbar
			REBARBANDINFO rbbi;
			rbbi.cbSize = sizeof(rbbi);
			rbbi.fMask = RBBIM_CHILDSIZE | RBBIM_IDEALSIZE | RBBIM_SIZE;
			rbbi.cx = rbbi.cxIdeal = rbbi.cxMinChild = rectToolbar.Width();
			rbbi.cyMinChild = rectToolbar.Height() + 2; // the toolbar looks a little funny without some padding
			m_wndPreviewRebar.GetReBarCtrl().SetBandInfo(nBand++, &rbbi);

			nRebarHeight = std::max<int>(nRebarHeight, rbbi.cyMinChild);
		}

		// create format toolbar
		if (m_wndFormatToolbar.CreateEx(this, TBSTYLE_FLAT | TBSTYLE_TRANSPARENT,
				WS_CHILD | WS_VISIBLE | CBRS_ALIGN_TOP | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC))
		{
			m_wndFormatToolbar.LoadToolBar(IDR_FONTPREVIEW_TOOLBAR);

			int nIndex = 0, nBitmapIndex = 0;
			m_wndFormatToolbar.SetButtonInfo(nIndex++, ID_PREVIEW_BOLD, TBBS_CHECKBOX, nBitmapIndex++);
			m_wndFormatToolbar.SetButtonInfo(nIndex++, ID_PREVIEW_ITALIC, TBBS_CHECKBOX, nBitmapIndex++);
			m_wndFormatToolbar.SetButtonInfo(nIndex++, ID_PREVIEW_UNDERLINE, TBBS_CHECKBOX, nBitmapIndex++);

			m_wndPreviewRebar.AddBar(&m_wndFormatToolbar, NULL, NULL, RBBS_NOGRIPPER);

			CRect rectToolbar;
			int nLastButton = m_wndFormatToolbar.GetToolBarCtrl().GetButtonCount() - 1;
			m_wndFormatToolbar.GetItemRect(nLastButton, &rectToolbar);
			rectToolbar.left = 0;

			// save the index of the format toolbar
			m_nFormatToolbarIndex = nBand;

			// set up min/max sizes and ideal sizes for preview toolbar
			REBARBANDINFO rbbi;
			rbbi.cbSize = sizeof(rbbi);
			rbbi.fMask = RBBIM_CHILDSIZE | RBBIM_IDEALSIZE | RBBIM_SIZE;
			rbbi.cx = rbbi.cxIdeal = rbbi.cxMinChild = rectToolbar.Width();
			rbbi.cyMinChild = rectToolbar.Height() + 2; // the toolbar looks a little funny without some padding
			m_wndPreviewRebar.GetReBarCtrl().SetBandInfo(nBand++, &rbbi);

			nRebarHeight = std::max<int>(nRebarHeight, rbbi.cyMinChild);
		}

		// get font name
		CString strFontName;
		if (CFontRunnerDoc::GetDoc() && CFontRunnerDoc::GetDoc()->GetCurrentFontItem())
		{
			using CruxTechnologies::OpenTypeFontData::CNameRecord;
			const CFontItem* pFontItem = CFontRunnerDoc::GetDoc()->GetCurrentFontItem();
			const std::wstring* pstr =
				pFontItem->GetOpenTypeData()->GetName(CNameRecord::kNameID_FullFontName);
			strFontName = pstr->c_str();
		}
		else
			strFontName.LoadString(IDS_FONTMAPVIEW_NOSELECTION);

		// create font name static
		if (m_wndFontNameStatic.Create(strFontName,
									   SS_RIGHT | SS_CENTERIMAGE | SS_ENDELLIPSIS | SS_NOPREFIX | WS_VISIBLE | WS_CHILD,
									   CRect(0, 0, 0, 0), this))
		{
			// use bold font for measuring
			pDC->SelectObject(theApp.GetBoldDialogFont());
			CSize sizeText = pDC->GetTextExtent(strFontName);

			::SetWindowLongPtr(m_wndFontNameStatic.GetSafeHwnd(), GWL_EXSTYLE, WS_EX_TRANSPARENT);
			m_wndFontNameStatic.SetFont(theApp.GetBoldDialogFont());

			m_wndPreviewRebar.AddBar(&m_wndFontNameStatic, NULL, NULL, RBBS_NOGRIPPER);

			// set up min/max sizes and ideal sizes for static font name text
			REBARBANDINFO rbbi;
			rbbi.cbSize = sizeof(rbbi);
			rbbi.fMask = RBBIM_CHILDSIZE | RBBIM_IDEALSIZE | RBBIM_SIZE;
			rbbi.cx = rbbi.cxIdeal = sizeText.cx * 10;  // force this to be the biggest
			rbbi.cyMinChild = nRebarHeight + 5;
			m_wndPreviewRebar.GetReBarCtrl().SetBandInfo(nBand++, &rbbi);
		}
		else
			throw std::runtime_error("Failed to create font name static");
	}
	catch (std::runtime_error& re)
	{
		re; // unused in release mode
		TRACE(re.what());
		bSuccess = false;
	}

	// restore DC
	pDC->RestoreDC(nSavedDC);

	return bSuccess;
}

bool CFontDetailFrame::CreateColorSelectBitmap(COLORREF clrCurrent, CBitmap& bitmap)
{
	// an auto-release smart pointer
	boost::shared_ptr<CDC> pDC(GetDC(), boost::bind(&CWnd::ReleaseDC, this, _1));

	// size is 16x16
	const int cnSize = 16;

	CDC dcMemory;
	dcMemory.CreateCompatibleDC(pDC.get());

	// create and select bitmap
	bitmap.CreateCompatibleBitmap(pDC.get(), cnSize, cnSize);
	CBitmap* pbmpOld = dcMemory.SelectObject(&bitmap);
	
	// make a solid black border
	CPen penSolidBlack;
	penSolidBlack.CreatePen(PS_SOLID, 1, RGB(0, 0, 0));
	CPen* pOldPen = dcMemory.SelectObject(&penSolidBlack);

	// make a brush for the inside of the rect
	CBrush brushColor(clrCurrent);
	CBrush* pOldBrush = dcMemory.SelectObject(&brushColor);
	
	// draw filled rectangle
	dcMemory.Rectangle(1, 1, cnSize - 1, cnSize - 1);
	
	// restore old GDI objects
	dcMemory.SelectObject(pOldBrush);
	dcMemory.SelectObject(pOldPen);
	dcMemory.SelectObject(pbmpOld);

	return true;
}

CView* CFontDetailFrame::GetView(CProgramOptions::eFontDetailMode nView)
{
	CView* pView = NULL;

	// create view if it does not exist
	if (nView == CProgramOptions::kMode_Preview)
	{
		if (m_pPreviewView == NULL)
		{
			m_pPreviewView = CreateFontViewClass<CRichTextPreviewView>(this);
			InitialUpdateFrame(CFontRunnerDoc::GetDoc(), TRUE);
		}

		pView = static_cast<CView*>(m_pPreviewView);
	}

	if (nView == CProgramOptions::kMode_FontMap)
	{
		if (m_pMapView == NULL)
		{
			m_pMapView = CreateFontViewClass<CFontMapView>(this);
			InitialUpdateFrame(CFontRunnerDoc::GetDoc(), TRUE);
		}

		pView = static_cast<CView*>(m_pMapView);
	}

	return pView;
}

void CFontDetailFrame::SizeFrame()
{
	// get active view
	CView* pView = GetActiveView();
	if (!pView || !pView->m_hWnd)
		return;

	// get size of this frame
	CRect rectClient;
	GetClientRect(&rectClient);

	// get size of rebar control
	CRect rectRebar;
	m_wndPreviewRebar.GetWindowRect(&rectRebar);

	// moving 1 window (the view)
	HDWP hdwp = ::BeginDeferWindowPos(1);
	
	// view area
	::DeferWindowPos(hdwp, pView->m_hWnd, HWND_TOP,
					 rectClient.left, rectRebar.Height(),
					 rectClient.Width(), rectClient.Height() - rectRebar.Height(),
					 SWP_SHOWWINDOW);
	
	::EndDeferWindowPos(hdwp);
}

void CFontDetailFrame::OnColorPaletteClosed()
{
	CheckPreviewToolbarButton(ID_PREVIEW_FGCOLOR, false);
	CheckPreviewToolbarButton(ID_PREVIEW_BGCOLOR, false);
}

void CFontDetailFrame::OnSelectedFontChange(const CFontItem* pData)
{
	// get font name
	CString strFontName;
	if (pData)
	{
		using CruxTechnologies::OpenTypeFontData::CNameRecord;
		const std::wstring* pstr =
			pData->GetOpenTypeData()->GetName(CNameRecord::kNameID_FullFontName);
		strFontName = pstr->c_str();
	}
	else
		strFontName.LoadString(IDS_FONTMAPVIEW_NOSELECTION);

	m_wndFontNameStatic.SetWindowText(strFontName);
	m_wndFontNameStatic.Invalidate();
}

void CFontDetailFrame::OnDetailColorChange(COLORREF color, bool bForeground)
{
	CBitmap bmp;
	CreateColorSelectBitmap(color, bmp);
	m_ilColorToolbarNormal.Replace(bForeground ? m_nForegroundIndex : m_nBackgroundIndex, &bmp, &m_bitmapMask);
	m_wndColorToolbar.Invalidate();
}

void CFontDetailFrame::OnOpenColorPalette(bool bForeground)
{
	if (bForeground)
		OnForegroundButton();
	else
		OnBackgroundButton();
}


BEGIN_MESSAGE_MAP(CFontDetailFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_CBN_SELCHANGE(ID_FONTDETAILMODE_COMBO, OnModeSelChange)
	ON_CBN_SELCHANGE(ID_SIZE_COMBO, OnSizeSelChange)
	ON_CBN_EDITCHANGE(ID_SIZE_COMBO, OnSizeEditChange)
	ON_BN_CLICKED(ID_PREVIEW_FGCOLOR, OnForegroundButton)
	ON_BN_CLICKED(ID_PREVIEW_BGCOLOR, OnBackgroundButton)
END_MESSAGE_MAP()


// CFontDetailFrame message handlers

BOOL CFontDetailFrame::OnCreateClient(LPCREATESTRUCT /*lpcs*/, CCreateContext* /*pContext*/)
{
	SetActiveView(GetView((CProgramOptions::eFontDetailMode)theApp.GetProgramOptions()->GetFontDetailMode()), FALSE);

	return TRUE;
}

int CFontDetailFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// create the rebar
	if (!m_wndPreviewRebar.Create(this, 0))
	{
		TRACE0("Failed to create font detail frame rebar\n");
		return -1;      // fail to create
	}

	// create toolbar band
	if (!CreatePreviewToolbarBand())
		return -1;

	// create palette window
	if (!m_pwndPalette->Create(this))
		return -1;

	// hook up signals
	CFontRunnerSignals* pSignals = theApp.GetSignals();
	pSignals->ConnectTo_ColorPaletteClosed(boost::bind(&CFontDetailFrame::OnColorPaletteClosed, this));
	pSignals->ConnectTo_SelectedFontChange(boost::bind(&CFontDetailFrame::OnSelectedFontChange, this, _1));
	pSignals->ConnectTo_DetailColorChange(boost::bind(&CFontDetailFrame::OnDetailColorChange, this, _1, _2));
	pSignals->ConnectTo_OpenColorPalette(boost::bind(&CFontDetailFrame::OnOpenColorPalette, this, _1));

	return 0;
}

void CFontDetailFrame::OnDestroy()
{
	if (m_nCurrentView == CProgramOptions::kMode_FontMap && m_pPreviewView)
		m_pPreviewView->DestroyWindow();
	else if (m_pMapView)
		m_pMapView->DestroyWindow();
}

void CFontDetailFrame::OnModeSelChange()
{
	// get selection
	CProgramOptions::eFontDetailMode nView = (CProgramOptions::eFontDetailMode)m_ctrlModeCombo.GetCurSel();

	// swtich view
	SwitchView(nView);

	// resize frame
	SizeFrame();
}

void CFontDetailFrame::OnSizeSelChange()
{
	int nIndex = m_ctrlSizeCombo.GetCurSel();

	CString strText;
	m_ctrlSizeCombo.GetLBText(nIndex, strText);

	// strip off pixels
	strText.Remove(_T('p'));
	strText.Remove(_T('x'));
	strText.Trim();

	int nSize = _wtoi(strText);

	if (m_nCurrentView == CProgramOptions::kMode_Preview)
		theApp.GetSignals()->Fire_DetailSizeChange((unsigned short)nSize);
	else
		theApp.GetSignals()->Fire_FontMapSizeChange((unsigned short)nSize);
}

void CFontDetailFrame::OnSizeEditChange()
{
	CString strText;
	m_ctrlSizeCombo.GetWindowText(strText);

	strText.Remove(_T('p'));
	strText.Remove(_T('x'));
	strText.Trim();

	try
	{
		unsigned short nSize = boost::lexical_cast<unsigned short>((LPCTSTR)strText);

		if (nSize < 6 || nSize > 300)
			throw std::out_of_range("out of range");

		if (m_nCurrentView == CProgramOptions::kMode_Preview)
			theApp.GetSignals()->Fire_DetailSizeChange((unsigned short)nSize);
		else
			theApp.GetSignals()->Fire_FontMapSizeChange((unsigned short)nSize);
	}
	catch (boost::bad_lexical_cast&)
	{
	}
	catch (std::out_of_range&)
	{
	}
}

CProgramOptions::eFontDetailMode CFontDetailFrame::SwitchView(CProgramOptions::eFontDetailMode nView)
{
	// nothing to change
	if (m_nCurrentView == nView)
		return m_nCurrentView;
	else
	{
		// save old view
		CProgramOptions::eFontDetailMode nOldView = m_nCurrentView;
		m_nCurrentView = nView;

		// set program option
		theApp.GetProgramOptions()->SetFontDetailMode(nView);

		// set this one and only view as the active view
		CView* pOldView = GetActiveView();
		CView* pNewView = GetView(nView);
		SetActiveView(pNewView, TRUE);

		if (!m_pPreviewView)
			m_pPreviewView = static_cast<CRichTextPreviewView*>(GetView(CProgramOptions::kMode_Preview));

		m_pPreviewView->ShowWindow((nOldView == CProgramOptions::kMode_Preview) ? SW_HIDE : SW_SHOW);
		m_pMapView->ShowWindow((nOldView == CProgramOptions::kMode_FontMap) ? SW_HIDE : SW_SHOW);
		m_wndPreviewRebar.GetReBarCtrl().ShowBand(m_nFormatToolbarIndex, (nOldView == CProgramOptions::kMode_FontMap));

		::SetWindowLong(pOldView->m_hWnd, GWL_ID, 0);
		::SetWindowLong(pNewView->m_hWnd, GWL_ID, AFX_IDW_PANE_FIRST);

		// update size combo
		CString strPixels, strSize;
		strPixels.LoadString(IDS_FONTLIST_PX);
		if (nView == CProgramOptions::kMode_FontMap)
			strSize.Format(strPixels, theApp.GetProgramOptions()->GetFontMapSize());
		else
			strSize.Format(strPixels, theApp.GetProgramOptions()->GetPreviewFontSize());

		m_ctrlSizeCombo.SetWindowText(strSize);

		// update color bitmap
		CBitmap bmpFore, bmpBack;
		if (nView == CProgramOptions::kMode_FontMap)
		{
			CreateColorSelectBitmap(theApp.GetProgramOptions()->GetFontMapFontColor(), bmpFore);
			CreateColorSelectBitmap(theApp.GetProgramOptions()->GetFontMapFontBackgroundColor(), bmpBack);
		}
		else
		{
			CreateColorSelectBitmap(theApp.GetProgramOptions()->GetPreviewFontColor(), bmpFore);
			CreateColorSelectBitmap(theApp.GetProgramOptions()->GetPreviewFontBackgroundColor(), bmpBack);
		}
		m_ilColorToolbarNormal.Replace(m_nForegroundIndex, &bmpFore, &m_bitmapMask);
		m_ilColorToolbarNormal.Replace(m_nBackgroundIndex, &bmpBack, &m_bitmapMask);
		m_wndColorToolbar.Invalidate();

		// return old view
		return nOldView;
	}
}

void CFontDetailFrame::CheckPreviewToolbarButton(int nID, bool bCheck)
{
	m_wndColorToolbar.GetToolBarCtrl().CheckButton(nID, bCheck);
}

void CFontDetailFrame::OnForegroundButton()
{
	// uncheck other button
	if (m_pwndPalette->IsWindowVisible() && !m_pwndPalette->IsForeground())
		m_wndColorToolbar.GetToolBarCtrl().CheckButton(ID_PREVIEW_BGCOLOR, false);

	else if (m_pwndPalette->IsWindowVisible() && m_pwndPalette->IsForeground())
	{
		// close palette
		m_pwndPalette->Disappear();
		return;
	}

	CRect rect;
	m_wndColorToolbar.GetToolBarCtrl().GetItemRect(0, &rect);
	m_wndColorToolbar.GetToolBarCtrl().ClientToScreen(&rect);

	CString strTitle;
	strTitle.LoadString(IDS_PREVIEWVIEW_FGCOLOR);
	m_pwndPalette->Appear(strTitle, theApp.GetProgramOptions()->GetPreviewFontColor(), rect, true, m_wndColorToolbar.GetSafeHwnd());
}

void CFontDetailFrame::OnBackgroundButton()
{
	// uncheck other button
	if (m_pwndPalette->IsWindowVisible() && m_pwndPalette->IsForeground())
		m_wndColorToolbar.GetToolBarCtrl().CheckButton(ID_PREVIEW_FGCOLOR, false);
	
	else if (m_pwndPalette->IsWindowVisible() && !m_pwndPalette->IsForeground())
	{
		// close palette
		m_pwndPalette->Disappear();
		return;
	}

	CRect rect;
	m_wndColorToolbar.GetToolBarCtrl().GetItemRect(1, &rect);
	m_wndColorToolbar.GetToolBarCtrl().ClientToScreen(&rect);

	CString strTitle;
	strTitle.LoadString(IDS_PREVIEWVIEW_BGCOLOR);
	m_pwndPalette->Appear(strTitle, theApp.GetProgramOptions()->GetPreviewFontBackgroundColor(), rect, false, m_wndColorToolbar.GetSafeHwnd());
}
