/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "FindFrameBase.h"
#include "FontRunnerSignals.h"
#include "TransparentStatic.h"

class CFontItem;
class CFontListView;

// CFindSimilarFrameWnd frame
class CFindSimilarFrameWnd : public CFindFrameBase, public boost::signals2::trackable
{
	DECLARE_DYNCREATE(CFindSimilarFrameWnd)
public:
	CFindSimilarFrameWnd(CFontItem* pItem = NULL, LPCTSTR pszCurrentFolder = NULL, bool bProject = false);
	virtual ~CFindSimilarFrameWnd();

public:
	bool Create(CWnd* pParentWnd);

private:
	void SetLookForPreviewFont(CStatic* pwndFontNameStatic);
	void OnOptionsChange();
	int UnManageFontItem(const CFontItem* pItem);
	void OnFontFileDeleted(const CFontItem* pItem);

// functions for the search thread
private:
	bool Searching() const;
	void EnteringFolder(LPCTSTR szPath);
	void Found(CFontItem* pItem);

// overrides
protected:
	CFontListView::eMode GetFontListViewMode() const;

private:
	CString m_strLookForFont;
	boost::shared_ptr<CFontItem> m_pFontItem;
	CFont		m_fntLookFor;
	CTransparentStatic	m_wndFontNameStatic;
	bool m_bSearchSubFolders;
	bool m_bSearchRemovableDisks;

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg BOOL OnEraseBkgnd(CDC*);
	afx_msg void OnPaint();
	afx_msg void OnLookInDeleteItem(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnToolbarDropDown(NMHDR* pNMHDR, LRESULT *pResult);
	afx_msg void OnUpdateOptions(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSearchButton(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCancelButton(CCmdUI* pCmdUI);
	afx_msg void OnOptionsSearchSubFolders();
	afx_msg void OnOptionsSearchRemovableDisks();
	afx_msg void OnSearchButton();
	afx_msg void OnCancelButton();
	afx_msg void OnLookInSelChange();
	afx_msg LRESULT OnFontFound(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSearchFinished(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnEnteringFolder(WPARAM wParam, LPARAM lParam);
};
