/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

class CFontMatcher
{
public:
	CFontMatcher();
	virtual ~CFontMatcher();

public:
	// Font distance -- lower is a better match
	static int ComputeDistance(const PANOSE* p1, const PANOSE* p2);

	static bool SansSerif(unsigned char nValue);
	static bool Serif(unsigned char nValue);
};
