// PaletteCtrl.cpp : Implementation of CPaletteCtrl
#include "stdafx.h"
#include "PaletteCtrl.h"

// CPaletteCtrl
CPaletteCtrl::CPaletteCtrl()
 : m_clrCurrent(RGB(0,0,0)),
   m_hTabFont(NULL)
{
	m_bWindowOnly = TRUE;
	CalcExtent(m_sizeExtent);
}

void CPaletteCtrl::OnMouseDownColor(COLORREF clrDown)
{
	// save new color
	m_clrCurrent = clrDown;

	// let host know about change
	Fire_ColorChange((ULONG)clrDown);
}

void CPaletteCtrl::OnMouseOverColor(COLORREF clrOver)
{
	// let host know about change
	Fire_ColorOver((ULONG)clrOver);
}

// CPaletteCtrl message handlers

LRESULT CPaletteCtrl::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	// subclass our placeholder static controls
	HWND hwndStatic = GetDlgItem(IDC_PALETTE_STATIC);
	m_wndPaletteCtrl.SubclassWindow(hwndStatic);
	m_wndPaletteCtrl.InitPalette(this);

	hwndStatic = GetDlgItem(IDC_BASICPALETTE_STATIC);
	m_wndBasicPaletteCtrl.SubclassWindow(hwndStatic);
	m_wndBasicPaletteCtrl.InitPalette(this);

	// cache tab control handle
	m_wndTabCtrl.Attach(GetDlgItem(IDC_PALETTE_TAB));

	// set tab control font
	if (!m_hTabFont)
	{
		// try and create a dialog font
		// get a font for use on tabs
		int nSize = 12;
		BYTE nCharSet = ANSI_CHARSET;
		HFONT hFont = (HFONT)::GetStockObject(DEFAULT_GUI_FONT);
		LOGFONT lf;
		if ((hFont != NULL) && (::GetObject(hFont, sizeof(LOGFONT), &lf) != 0))
		{
			nSize = lf.lfHeight;
			nCharSet = lf.lfCharSet;
		}

		LPCTSTR lpszFacename = _T("MS Shell Dlg 2");
		m_hTabFont = ::CreateFont(nSize, 0, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, nCharSet,
									OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
									DEFAULT_PITCH | FF_DONTCARE, lpszFacename);
		if (!m_hTabFont)
		{
			// creation of MS Shell Dlg 2 was not successful, use old reliable system font
			lpszFacename = _T("MS Shell Dlg");
			m_hTabFont = ::CreateFont(nSize, 0, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, nCharSet,
										OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
										DEFAULT_PITCH | FF_DONTCARE, lpszFacename);
		}
	}

	// use tab font
	m_wndTabCtrl.SetFont(m_hTabFont);

	// get strings for tab
	TCHAR szBasicTabCaption[64] = _T(""), szAdvancedTabCaption[64] = _T("");
	::LoadString(_AtlBaseModule.GetResourceInstance(), IDS_PALETTE_TAB_BASIC, szBasicTabCaption, 64);
	::LoadString(_AtlBaseModule.GetResourceInstance(), IDS_PALETTE_TAB_ADVANCED, szAdvancedTabCaption, 64);

	// insert basic tab
	TCITEM tcItem;
	tcItem.mask = TCIF_TEXT;
	tcItem.pszText = szBasicTabCaption;
	m_wndTabCtrl.SendMessage(TCM_INSERTITEM, kTabPage_Basic, (LPARAM)&tcItem);

	// insert advanced tab
	tcItem.mask = TCIF_TEXT;
	tcItem.pszText = szAdvancedTabCaption;
	m_wndTabCtrl.SendMessage(TCM_INSERTITEM, kTabPage_Advanced, (LPARAM)&tcItem);

	// want basic tab selected by default
	m_wndTabCtrl.SendMessage(TCM_SETCURSEL, kTabPage_Basic);

	// init tab pages by faking a selection change
	OnPaletteTabSelChange(IDC_PALETTE_TAB, NULL, bHandled);

	RECT rectWindow;
	m_wndPaletteCtrl.GetClientRect(&rectWindow);

	return 0L;
}

LRESULT CPaletteCtrl::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	// don't forget to unsubclass the static control or it will crash
	m_wndPaletteCtrl.UnsubclassWindow();

	// delete the tab font
	if (m_hTabFont)
		::DeleteObject(m_hTabFont);

	return 0L;
}

LRESULT CPaletteCtrl::OnPaletteTabSelChange(int idCtrl, LPNMHDR pnmh, BOOL& bHandled)
{
	// get selected tab
	eTabPage nCurSel = (eTabPage)m_wndTabCtrl.SendMessage(TCM_GETCURSEL);

	// hide / show controls depending on selection
	m_wndBasicPaletteCtrl.ShowWindow((nCurSel == kTabPage_Basic) ? SW_SHOW : SW_HIDE);
	m_wndPaletteCtrl.ShowWindow((nCurSel == kTabPage_Advanced) ? SW_SHOW : SW_HIDE);

	return 0L;
}
STDMETHODIMP CPaletteCtrl::get_SelectedColor(ULONG* pVal)
{
	*pVal = m_clrCurrent;

	return S_OK;
}

STDMETHODIMP CPaletteCtrl::put_SelectedColor(ULONG newVal)
{
	// save new color
	m_clrCurrent = (COLORREF)newVal;

	// update palettes
	m_wndPaletteCtrl.SetColor(m_clrCurrent);
	m_wndBasicPaletteCtrl.SetColor(m_clrCurrent);

	return S_OK;
}
