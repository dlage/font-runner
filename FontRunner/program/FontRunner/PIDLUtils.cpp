/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "StdAfx.h"
#include "PIDLUtils.h"

#include "HRESULTException.h"

#include <shlwapi.h>
#include <boost/shared_ptr.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

UINT pidlutils::getsize(LPCITEMIDLIST pidl)
{
    UINT cbTotal = 0;
    if (pidl)
    {
        cbTotal += sizeof(pidl->mkid.cb);       // Null terminator
        while (pidl->mkid.cb)
        {
            cbTotal += pidl->mkid.cb;
            pidl = ::ILGetNext(pidl);
        }
    }

    return cbTotal;
}

LPITEMIDLIST pidlutils::createpidl(UINT cbSize)
{
    LPITEMIDLIST pidl = NULL;

	pidl = (LPITEMIDLIST)::CoTaskMemAlloc(cbSize);

    if (pidl)
        memset(pidl, 0, cbSize);      // zero-init for external task alloc

    return pidl;
}

LPITEMIDLIST pidlutils::concatpidls(LPCITEMIDLIST pidl1, LPCITEMIDLIST pidl2)
{
    LPITEMIDLIST pidlNew;
    UINT cb1;
    UINT cb2;

    if (pidl1)  //May be NULL
       cb1 = getsize(pidl1) - sizeof(pidl1->mkid.cb);
    else
       cb1 = 0;

    cb2 = getsize(pidl2);

    pidlNew = createpidl(cb1 + cb2);
    if (pidlNew)
    {
        if (pidl1)
           memcpy(pidlNew, pidl1, cb1);
        memcpy(((LPSTR)pidlNew) + cb1, pidl2, cb2);
    }
    return pidlNew;
}

LPITEMIDLIST pidlutils::copyitemid(LPITEMIDLIST lpi)
{
   LPITEMIDLIST lpiTemp;

	lpiTemp = (LPITEMIDLIST)::CoTaskMemAlloc(lpi->mkid.cb + sizeof(lpi->mkid.cb));
	memcpy(lpiTemp, lpi, lpi->mkid.cb + sizeof(lpi->mkid.cb));

	return lpiTemp;
}

bool pidlutils::pidls_are_equal(IShellFolder* pShellFolder, LPCITEMIDLIST pidl1, LPCITEMIDLIST pidl2)
{
	// NULL in, NULL out
	if (!pShellFolder || !pidl1 || !pidl2)
		return NULL;

	// smart pointers
	boost::shared_ptr<char> pidlCompare1((char*)::CoTaskMemAlloc(pidl1->mkid.cb + 2), ::CoTaskMemFree);
	boost::shared_ptr<char> pidlCompare2((char*)::CoTaskMemAlloc(pidl2->mkid.cb + 2), ::CoTaskMemFree);

	// zero buffers
	memset(pidlCompare1.get(), 0, pidl1->mkid.cb + 2);
	memset(pidlCompare2.get(), 0, pidl2->mkid.cb + 2);

	// copy buffers
	memcpy(pidlCompare1.get(), pidl1, pidl1->mkid.cb);
	memcpy(pidlCompare2.get(), pidl2, pidl2->mkid.cb);

	HRESULT hResult =
		pShellFolder->CompareIDs(0, (LPCITEMIDLIST)pidlCompare1.get(), (LPCITEMIDLIST)pidlCompare2.get());

	return (HRESULT_CODE(hResult) == 0);
}

LPITEMIDLIST pidlutils::copy1st(LPITEMIDLIST lpi)
{
	LPITEMIDLIST lpiTemp;

	size_t nSize = lpi->mkid.cb + 2;
	lpiTemp = (LPITEMIDLIST)::CoTaskMemAlloc(nSize);
	memset(lpiTemp, 0, nSize);
	memcpy(lpiTemp, lpi, nSize - 2);

	return lpiTemp;
}

// this converts an "abnormal" pidl that comes in though a shell notification into
// a "normal" one that all the shell functions can use reliably
LPITEMIDLIST pidlutils::to_normal(IShellFolder* pShellFolder, LPCITEMIDLIST pidl)
{
	 if (!pShellFolder || !pidl)
		return NULL; // NULL in, NULL out

	LPITEMIDLIST pidlOut = NULL;

	try
	{
		CheckResultFunctor CheckResult;

		// convert to parsing name
		STRRET strResult;
		CheckResult(pShellFolder->GetDisplayNameOf(pidl, SHGDN_INFOLDER | SHGDN_FORPARSING, &strResult));

		// get wide-character name from STRRET
		WCHAR* pName = NULL;
		CheckResult(::StrRetToStrW(&strResult, pidl, &pName));

		// give ownership to a smart pointer
		boost::shared_ptr<WCHAR> pNamePtr(pName, &CoTaskMemFree);

		// parse the name to produce a "normal" pidl
		CheckResult(pShellFolder->ParseDisplayName(NULL, NULL, pNamePtr.get(), NULL, &pidlOut, NULL));
	}
	catch (CHRESULTException&)
	{ // caught, but we don't need to do anything with it
	}

	return pidlOut;
}
