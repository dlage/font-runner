/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>

namespace ShellUtils
{
	// returns name of shell object ID'd by a shell folder and PIDL
	bool GetName(IShellFolder* lpsf,
				 LPCITEMIDLIST lpi,
				 DWORD dwFlags,
				 LPTSTR lpFriendlyName,
				 size_t nFriendlyNameSize);

	// returns name of shell object ID'd by a shell folder and PIDL
	CString GetName(IShellFolder* lpsf, LPITEMIDLIST lpi, DWORD dwFlags);

	// Runs a command verb on item
	HRESULT InvokeContextMenuCommand(HWND hwnd, LPCTSTR szFullFileName, LPCSTR lpCommand);

	// returns the effect performed on the data object after it is dragged
	DROPEFFECT CheckPerformedEffect(IDataObject* pDataObject);

	// puts file names into a data object
	// returns number of files put into data object
	int SetDataObjectWithFileNames(IDataObject* pDataObject,
								   const std::vector<LPCTSTR>& filenames,
								   size_t nStringListLength,
								   CLIPFORMAT cf = CF_HDROP);

	// puts PIDLs into a data object
	// returns number of PIDLs put into data object
	int SetDataObjectWithPIDLs(IDataObject* pDataObject, const std::vector<LPCTSTR>& filenames);
}
