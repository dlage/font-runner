/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include <boost/shared_ptr.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/signals2.hpp>

// prototype classes from CruxTechnologies namespace
namespace CruxTechnologies
{
	class COpenTypeData;
	class CUnicodeCharDescriptions;
}
class CCharDetailWnd;
class CFontItem;

// CFontMapView view

class CFontMapView : public CScrollView, public boost::signals2::trackable
{
	DECLARE_DYNCREATE(CFontMapView)

protected:
	CFontMapView();           // protected constructor used by dynamic creation
	virtual ~CFontMapView();

// signal handlers
private:
	void OnSelectedFontChange(const CFontItem* pData);
	void OnSizeChange(unsigned short nSize);
	void OnOptionsChange();
	int OnUnManageFontItem(const CFontItem* pFont);
	void OnFontFileDeleted(const CFontItem* pFontItem);
	void OnDetailColorChange(COLORREF color, bool bForeground);

// internal functionality
private:
	uint16_t HitTest(const CPoint& pt, RECT* pRect) const;
	CRect MapCell(unsigned int nCell) const;
	void DrawCell(CDC* pDC, uint16_t nCharIndex);
	void ResizeScrollArea();
	void ChangeCharSelection();
	std::string GetRTFFormattedChar(int nIndex) const;
	void CreateDisplayFont();
	void InvalidateAtIndex(int index, const CPoint& pt);
	UINT ChangeScrollPosition(int nBar, UINT nSBCode, UINT nPos);
	void EnsureVisible(int nIndex);

private:
	const int m_nToolTipWidth;
	boost::shared_ptr<CFontItem> m_pFontItem;
	boost::scoped_ptr<CruxTechnologies::CUnicodeCharDescriptions> m_pUnicodeData;
	const uint16_t* m_pCurrentMap;
	uint16_t m_nCurrentMapLength;
	CFont m_fntCurrent;
	int m_nFontHeight;
	bool m_bInitialUpdated;
	
	// variables
	CSize m_sizCell, m_sizFullMap;
	unsigned int m_nColumns;
	COLORREF m_clrForeground, m_clrBackground;

	unsigned short m_nCurrentEncodingID;
	const int m_nScrollbarWidth;
	const COLORREF m_clrHot, m_clrPressed;
	int m_nMouseOverIndex;
	int m_nSelectedIndex;
	bool m_bTrackingMouse;
	bool m_bMouseDown;
	int m_nRButtonDownIndex;
	int m_nContextMenuIndex;
	bool m_bShowingPopupMenu;

	// character detail window
	CCharDetailWnd* m_pCharDetailWnd;

public:
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

// overrides
protected:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnInitialUpdate();     // first time after construct
	virtual void PreSubclassWindow();
	virtual INT_PTR OnToolHitTest(CPoint point, TOOLINFO* pTI) const;

	DECLARE_MESSAGE_MAP()
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnFontMapMenu_CopyCharacter();
	afx_msg void OnFontMapMenu_PreviewCharacter();
	afx_msg void OnFontMapMenu_FindCharacter();

	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	afx_msg BOOL OnTTNNeedText(UINT nID, NMHDR* pTTTStruct, LRESULT* pResult);
};


