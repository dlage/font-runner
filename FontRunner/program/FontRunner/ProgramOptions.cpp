/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "StdAfx.h"
#include "ProgramOptions.h"
#include "Key.h"

#include "Resource.h"

#include <boost/scoped_ptr.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// registry information
static const TCHAR* cstrFontRunnerRegSection	= _T("SOFTWARE\\Crux Technologies, Inc\\Font Runner\\3.0");
const TCHAR* cstrMainWindowMaximized	= _T("MainWindowMaximized");
const TCHAR* cstrWindowMetrics			= _T("WindowMetrics");
const TCHAR* cstrRestoredMetrics		= _T("RestoredMetrics");
const TCHAR* cstrFontNameColumnWidth	= _T("FontNameColumnWidth");
const TCHAR* cstrFileNameColumnWidth	= _T("FileNameColumnWidth");
const TCHAR* cstrSizeColumnWidth		= _T("SizeColumnWidth");
const TCHAR* cstrFontListCharSize		= _T("FontListCharSize");
const TCHAR* cstrFontListSortColumn		= _T("FontListSortColumn");
const TCHAR* cstrFontListSortDescending = _T("FontListSortDescending");
const TCHAR* cstrFontListText			= _T("FontListText");
const TCHAR* cstrAutoInstall			= _T("AutoInstall");
const TCHAR* cstrShowFullPathInTitleBar	= _T("ShowFullPathInTitleBar");
const TCHAR* cstrFontDetailMode			= _T("FontDetailMode");
const TCHAR* cstrPreviewFontSize		= _T("PreviewFontSize");
const TCHAR* cstrPreviewFontColor		= _T("PreviewFontColor");
const TCHAR* cstrPreviewBackgroundColor = _T("PreviewBackgroundColor");
const TCHAR* cstrPreviewFontItalic		= _T("PreviewFontItalic");
const TCHAR* cstrPreviewFontBold		= _T("PreviewFontBold");
const TCHAR* cstrPreviewFontUnderlined	= _T("PreviewFontUnderlined");
const TCHAR* cstrPreviewText			= _T("PreviewText");
const TCHAR* cstrFontMapSize			= _T("FontMapSize");
const TCHAR* cstrFontMapFontColor		= _T("FontMapFontColor");
const TCHAR* cstrFontMapBackgroundColor = _T("FontMapBackgroundColor");
const TCHAR* cstrPrintCharSize			= _T("PrintCharSize");
const TCHAR* cstrPrintSampleTextLine1	= _T("PrintSampleTextLine1");
const TCHAR* cstrPrintSampleTextLine2	= _T("PrintSampleTextLine2");
const TCHAR* cstrCharDetailWindowPos	= _T("CharDetailWindowPos");
const TCHAR* cstrCharDetailWindowSize	= _T("CharDetailWindowSize");
const TCHAR* cstrSearchRemovableDisks	= _T("SearchRemovableDisks");
const TCHAR* cstrSearchSubFolders		= _T("SearchSubFolders");
const TCHAR* cstrSearchMatchCase		= _T("SearchMatchCase");
const TCHAR* cstrSearchMatchWord		= _T("SearchMatchWord");
const TCHAR* cstrSearchSimilar			= _T("SearchSimilar");
const TCHAR* cstrSearchUsingRegex		= _T("SearchUsingRegex");
const TCHAR* cstrStartupOption			= _T("StartupOption");
const TCHAR* cstrDefaultFolder			= _T("DefaultFolder");
const TCHAR* cstrDefaultProject			= _T("DefaultProject");
const TCHAR* cstrLastFolder				= _T("LastFolder");
const TCHAR* cstrLastProject			= _T("LastProject");
const TCHAR* cstrUseFontProject			= _T("UseFontProject");
const TCHAR* cstrFontListRenderingOption	= _T("FontListRenderingOption");
const TCHAR* cstrFontMapRenderingOption	= _T("FontMapRenderingOption");
const TCHAR* cstrCharDetailRenderingOption	= _T("CharDetailRenderingOption");
const TCHAR* cstrFindWindowPos			= _T("FindWindowPos");
const TCHAR* cstrFindWindowSize			= _T("FindWindowSize");
const TCHAR* cstrMainRebarLayout		= _T("MainRebarLayout");
const TCHAR* cstrUpdaterLocation		= _T("UpdaterLocation");
const TCHAR* cstrClearLocationsOnExit	= _T("ClearLocationsOnExit");
const TCHAR* cstrLimitLocations			= _T("LimitLocations");
const TCHAR* cstrLocationsLimit			= _T("LocationsLimit");
const TCHAR* cstrFontListDropOption		= _T("FontListDropOption");

struct framemetrics_type
{
	POINT ptMainWindowPos;
	SIZE sizeMainWindowSize;
	DWORD dwTreeViewWidth;
	DWORD dwTreeViewHeight;
	DWORD dwFontListHeight;
};

struct CProgramOptions::_impl
{
	_impl() :
		bMainWindowMaximized(false),
		dwFontNameColWidth(225),
		dwFileNameColWidth(140),
		dwSizeColWidth(60),
		dwFontListCharSize(24),
		dwFontListSortColumn(0),
		bFontListSortDescending(false),
		bAutoInstall(false),
		bShowFullPathInTitleBar(false),
		dwFontDetailMode(kMode_Preview),
		dwPreviewFontSize(24),
		dwPreviewFontColor(RGB(0, 0, 0)),
		dwPreviewBackgroundColor(RGB(255, 255, 255)),
		bPreviewFontItalic(false),
		bPreviewFontBold(false),
		bPreviewFontUnderlined(false),
		dwFontMapSize(24),
		dwFontMapFontColor(RGB(0, 0, 0)),
		dwFontMapBackgroundColor(RGB(255, 255, 255)),
		dwPrintCharSize(20),
		bSearchRemovableDisks(false),
		bSearchSubFolders(true),
		bSearchMatchCase(false),
		bSearchMatchWord(false),
		bSearchUsingRegex(false),
		nStartupOption(kStartupOption_LastItem),
		bUseFontProject(false),
		strDefaultFolder(_T("C:\\")),
		strLastFolder(_T("C:\\")),
		nFontListRenderingOption(kRenderingOption_UseDefault),
		nFontMapRenderingOption(kRenderingOption_UseDefault),
		nCharDetailRenderingOption(kRenderingOption_UseDefault),
		bClearLocationsOnExit(false),
		bLimitLocations(false),
		nLocationsLimit(20),
		nFontListDropOption(0)
	{
		memset(&windowmetrics, 0xff, sizeof(framemetrics_type));
		memset(&restoredmetrics, 0xff, sizeof(framemetrics_type));
		ptCharDetailWindowPos.x = -1;
		ptCharDetailWindowPos.y = -1;
		sizeCharDetailWindowSize.cx = -1;
		sizeCharDetailWindowSize.cy = -1;
		strFontListText.LoadString(IDS_FONTLIST_DEFAULTSAMPLE);
		strPreviewText.LoadString(IDS_PREVIEWVIEW_DEFAULTTEXT);
		strPrintSampleTextLine1.LoadString(IDS_SAMPLEPRINT_LINE1);
		strPrintSampleTextLine2.LoadString(IDS_SAMPLEPRINT_LINE2);
		ptFindWindow.x = -1;
		ptFindWindow.y = -1;
		sizeFindWindow.cx = -1;
		sizeFindWindow.cy = -1;
	}

	void Load()
	{
		boost::scoped_ptr<CKey> pKey(new CKey());

		if (pKey->OpenKey(HKEY_CURRENT_USER, cstrFontRunnerRegSection))
		{
			pKey->GetBoolean(cstrMainWindowMaximized, bMainWindowMaximized);
			pKey->GetBinary(cstrWindowMetrics, (LPBYTE)&windowmetrics, sizeof(framemetrics_type));
			pKey->GetBinary(cstrRestoredMetrics, (LPBYTE)&restoredmetrics, sizeof(framemetrics_type));
			pKey->GetDWORD(cstrFontNameColumnWidth, &dwFontNameColWidth);
			pKey->GetDWORD(cstrFileNameColumnWidth, &dwFileNameColWidth);
			pKey->GetDWORD(cstrSizeColumnWidth, &dwSizeColWidth);
			pKey->GetDWORD(cstrFontListCharSize, &dwFontListCharSize);
			pKey->GetDWORD(cstrFontListSortColumn, &dwFontListSortColumn);
			pKey->GetBoolean(cstrFontListSortDescending, bFontListSortDescending);
			pKey->GetString(cstrFontListText, strFontListText);
			pKey->GetBoolean(cstrAutoInstall, bAutoInstall);
			pKey->GetBoolean(cstrShowFullPathInTitleBar, bShowFullPathInTitleBar);
			pKey->GetDWORD(cstrFontDetailMode, &dwFontDetailMode);
			pKey->GetDWORD(cstrPreviewFontSize, &dwPreviewFontSize);
			pKey->GetDWORD(cstrPreviewFontColor, &dwPreviewFontColor);
			pKey->GetDWORD(cstrPreviewBackgroundColor, &dwPreviewBackgroundColor);
			pKey->GetBoolean(cstrPreviewFontItalic, bPreviewFontItalic);
			pKey->GetBoolean(cstrPreviewFontBold, bPreviewFontBold);
			pKey->GetBoolean(cstrPreviewFontUnderlined, bPreviewFontUnderlined);
			pKey->GetString(cstrPreviewText, strPreviewText);
			pKey->GetDWORD(cstrFontMapSize, &dwFontMapSize);
			pKey->GetDWORD(cstrFontMapFontColor, &dwFontMapFontColor);
			pKey->GetDWORD(cstrFontMapBackgroundColor, &dwFontMapBackgroundColor);
			pKey->GetDWORD(cstrPrintCharSize, &dwPrintCharSize);
			pKey->GetString(cstrPrintSampleTextLine1, strPrintSampleTextLine1);
			pKey->GetString(cstrPrintSampleTextLine2, strPrintSampleTextLine2);
			pKey->GetBinary(cstrCharDetailWindowPos, (LPBYTE)&ptCharDetailWindowPos, sizeof(POINT));
			pKey->GetBinary(cstrCharDetailWindowSize, (LPBYTE)&sizeCharDetailWindowSize, sizeof(SIZE));
			pKey->GetBoolean(cstrSearchRemovableDisks, bSearchRemovableDisks);
			pKey->GetBoolean(cstrSearchSubFolders, bSearchSubFolders);
			pKey->GetBoolean(cstrSearchMatchCase, bSearchMatchCase);
			pKey->GetBoolean(cstrSearchMatchWord, bSearchMatchWord);
			pKey->GetBoolean(cstrSearchUsingRegex, bSearchUsingRegex);
			pKey->GetDWORD(cstrStartupOption, (DWORD*)&nStartupOption);
			pKey->GetString(cstrDefaultFolder, strDefaultFolder);
			pKey->GetString(cstrDefaultProject, strDefaultProject);
			pKey->GetString(cstrLastFolder, strLastFolder);
			pKey->GetString(cstrLastProject, strLastProject);
			pKey->GetBoolean(cstrUseFontProject, bUseFontProject);
			pKey->GetDWORD(cstrFontListRenderingOption, (DWORD*)&nFontListRenderingOption);
			pKey->GetDWORD(cstrFontMapRenderingOption, (DWORD*)&nFontMapRenderingOption);
			pKey->GetDWORD(cstrCharDetailRenderingOption, (DWORD*)&nCharDetailRenderingOption);
			pKey->GetBinary(cstrFindWindowPos, (LPBYTE)&ptFindWindow, sizeof(POINT));
			pKey->GetBinary(cstrFindWindowSize, (LPBYTE)&sizeFindWindow, sizeof(SIZE));
			pKey->GetString(cstrUpdaterLocation, strUpdaterLocation);
			pKey->GetBoolean(cstrClearLocationsOnExit, bClearLocationsOnExit);
			pKey->GetBoolean(cstrLimitLocations, bLimitLocations);
			pKey->GetDWORD(cstrLocationsLimit, (DWORD*)&nLocationsLimit);
			pKey->GetDWORD(cstrFontListDropOption, &nFontListDropOption);
		}
	}

	void Save()
	{
		boost::scoped_ptr<CKey> pKey(new CKey());

		if (!pKey->OpenKey(HKEY_CURRENT_USER, cstrFontRunnerRegSection))
			pKey->CreateKey(HKEY_CURRENT_USER, cstrFontRunnerRegSection);

		if (pKey->IsOpen())
		{
			pKey->SetDWORD(cstrMainWindowMaximized, bMainWindowMaximized);
			pKey->SetBinary(cstrWindowMetrics, &windowmetrics, sizeof(framemetrics_type));
			pKey->SetBinary(cstrRestoredMetrics, &restoredmetrics, sizeof(framemetrics_type));
			pKey->SetDWORD(cstrFontNameColumnWidth, dwFontNameColWidth);
			pKey->SetDWORD(cstrFileNameColumnWidth, dwFileNameColWidth);
			pKey->SetDWORD(cstrSizeColumnWidth, dwSizeColWidth);
			pKey->SetDWORD(cstrFontListCharSize, dwFontListCharSize);
			pKey->SetDWORD(cstrFontListSortColumn, dwFontListSortColumn);
			pKey->SetDWORD(cstrFontListSortDescending, bFontListSortDescending);
			pKey->SetString(cstrFontListText, strFontListText);
			pKey->SetDWORD(cstrAutoInstall, bAutoInstall);
			pKey->SetDWORD(cstrShowFullPathInTitleBar, bShowFullPathInTitleBar);
			pKey->SetDWORD(cstrFontDetailMode, dwFontDetailMode);
			pKey->SetDWORD(cstrPreviewFontSize, dwPreviewFontSize);
			pKey->SetDWORD(cstrPreviewFontColor, dwPreviewFontColor);
			pKey->SetDWORD(cstrPreviewBackgroundColor, dwPreviewBackgroundColor);
			pKey->SetDWORD(cstrPreviewFontItalic, bPreviewFontItalic);
			pKey->SetDWORD(cstrPreviewFontBold, bPreviewFontBold);
			pKey->SetDWORD(cstrPreviewFontUnderlined, bPreviewFontUnderlined);
			pKey->SetString(cstrPreviewText, strPreviewText);
			pKey->SetDWORD(cstrFontMapSize, dwFontMapSize);
			pKey->SetDWORD(cstrFontMapFontColor, dwFontMapFontColor);
			pKey->SetDWORD(cstrFontMapBackgroundColor, dwFontMapBackgroundColor);
			pKey->SetDWORD(cstrPrintCharSize, dwPrintCharSize);
			pKey->SetString(cstrPrintSampleTextLine1, strPrintSampleTextLine1);
			pKey->SetString(cstrPrintSampleTextLine2, strPrintSampleTextLine2);
			pKey->SetBinary(cstrCharDetailWindowPos, &ptCharDetailWindowPos, sizeof(POINT));
			pKey->SetBinary(cstrCharDetailWindowSize, &sizeCharDetailWindowSize, sizeof(SIZE));
			pKey->SetDWORD(cstrSearchRemovableDisks, bSearchRemovableDisks);
			pKey->SetDWORD(cstrSearchSubFolders, bSearchSubFolders);
			pKey->SetDWORD(cstrSearchMatchCase, bSearchMatchCase);
			pKey->SetDWORD(cstrSearchMatchWord, bSearchMatchWord);
			pKey->SetDWORD(cstrSearchUsingRegex, bSearchUsingRegex);
			pKey->SetDWORD(cstrStartupOption, nStartupOption);
			pKey->SetString(cstrDefaultFolder, strDefaultFolder);
			pKey->SetString(cstrDefaultProject, strDefaultProject);
			pKey->SetString(cstrLastFolder, strLastFolder);
			pKey->SetString(cstrLastProject, strLastProject);
			pKey->SetDWORD(cstrUseFontProject, bUseFontProject);
			pKey->SetDWORD(cstrFontListRenderingOption, nFontListRenderingOption);
			pKey->SetDWORD(cstrFontMapRenderingOption, nFontMapRenderingOption);
			pKey->SetDWORD(cstrCharDetailRenderingOption, nCharDetailRenderingOption);
			pKey->SetBinary(cstrFindWindowPos, &ptFindWindow, sizeof(POINT));
			pKey->SetBinary(cstrFindWindowSize, &sizeFindWindow, sizeof(SIZE));
			pKey->SetString(cstrUpdaterLocation, strUpdaterLocation);
			pKey->SetDWORD(cstrClearLocationsOnExit, bClearLocationsOnExit);
			pKey->SetDWORD(cstrLimitLocations, bLimitLocations);
			pKey->SetDWORD(cstrLocationsLimit, nLocationsLimit);
			pKey->SetDWORD(cstrFontListDropOption, nFontListDropOption);
		}
	}

	bool bMainWindowMaximized;
	framemetrics_type windowmetrics, restoredmetrics;
	DWORD dwFontNameColWidth;
	DWORD dwFileNameColWidth;
	DWORD dwSizeColWidth;
	DWORD dwFontListCharSize;
	DWORD dwFontListSortColumn;
	bool bFontListSortDescending;
	CString strFontListText;
	bool bAutoInstall;
	bool bShowFullPathInTitleBar;
	DWORD dwFontDetailMode;
	DWORD dwPreviewFontSize;
	DWORD dwPreviewFontColor;
	DWORD dwPreviewBackgroundColor;
	bool bPreviewFontItalic;
	bool bPreviewFontBold;
	bool bPreviewFontUnderlined;
	CString strPreviewText;
	DWORD dwFontMapSize;
	DWORD dwFontMapFontColor, dwFontMapBackgroundColor;
	DWORD dwPrintCharSize;
	CString strPrintSampleTextLine1;
	CString strPrintSampleTextLine2;
	POINT ptCharDetailWindowPos;
	SIZE sizeCharDetailWindowSize;
	bool bSearchRemovableDisks;
	bool bSearchSubFolders;
	bool bSearchMatchCase;
	bool bSearchMatchWord;
	bool bSearchUsingRegex;
	eStartupOption nStartupOption;
	CString strDefaultFolder;
	CString strDefaultProject;
	CString strLastFolder;
	CString strLastProject;
	bool bUseFontProject;
	eRenderingOption nFontListRenderingOption;
	eRenderingOption nFontMapRenderingOption;
	eRenderingOption nCharDetailRenderingOption;
	POINT ptFindWindow;
	SIZE sizeFindWindow;
	CString strUpdaterLocation;
	bool bClearLocationsOnExit;
	bool bLimitLocations;
	int nLocationsLimit;
	DWORD nFontListDropOption;
};

CProgramOptions::CProgramOptions() : m_pImpl(new _impl)
{
}

CProgramOptions::~CProgramOptions()
{
}

void CProgramOptions::Load()
{
	m_pImpl->Load();
}

void CProgramOptions::Save()
{
	m_pImpl->Save();
}

CString CProgramOptions::GetRegistryLocation()
{
	return cstrFontRunnerRegSection;
}

// frame/pane size and position
bool CProgramOptions::GetMainWindowMaximized() const
{
	return m_pImpl->bMainWindowMaximized;
}

void CProgramOptions::SetMainWindowMaximized(bool bMaximized)
{
	m_pImpl->bMainWindowMaximized = bMaximized;
}

void CProgramOptions::GetMainWindowPosition(POINT& pt) const
{
	pt = m_pImpl->windowmetrics.ptMainWindowPos;
}

void CProgramOptions::SetMainWindowPosition(const POINT& pt)
{
	m_pImpl->windowmetrics.ptMainWindowPos = pt;
}

void CProgramOptions::GetMainWindowSize(SIZE& size) const
{
	size = m_pImpl->windowmetrics.sizeMainWindowSize;
}

void CProgramOptions::SetMainWindowSize(const SIZE& size)
{
	m_pImpl->windowmetrics.sizeMainWindowSize = size;
}

DWORD CProgramOptions::GetTreeViewWidth() const
{
	return m_pImpl->windowmetrics.dwTreeViewWidth;
}

void CProgramOptions::SetTreeViewWidth(DWORD dwWidth)
{
	m_pImpl->windowmetrics.dwTreeViewWidth = dwWidth;
}

DWORD CProgramOptions::GetTreeViewHeight() const
{
	return m_pImpl->windowmetrics.dwTreeViewHeight;
}

void CProgramOptions::SetTreeViewHeight(DWORD dwHeight)
{
	m_pImpl->windowmetrics.dwTreeViewHeight = dwHeight;
}

DWORD CProgramOptions::GetFontListHeight() const
{
	return m_pImpl->windowmetrics.dwFontListHeight;
}

void CProgramOptions::SetFontListHeight(DWORD dwHeight)
{
	m_pImpl->windowmetrics.dwFontListHeight = dwHeight;
}

void CProgramOptions::GetRestoredWindowPosition(POINT& pt) const
{
	pt = m_pImpl->restoredmetrics.ptMainWindowPos;
}

void CProgramOptions::GetRestoredWindowSize(SIZE& size) const
{
	size = m_pImpl->restoredmetrics.sizeMainWindowSize;
}

DWORD CProgramOptions::GetRestoredTreeViewWidth() const
{
	return m_pImpl->restoredmetrics.dwTreeViewWidth;
}

DWORD CProgramOptions::GetRestoredTreeViewHeight() const
{
	return m_pImpl->restoredmetrics.dwTreeViewHeight;
}

DWORD CProgramOptions::GetRestoredFontListHeight() const
{
	return m_pImpl->restoredmetrics.dwFontListHeight;
}

void CProgramOptions::SaveRestoredMetrics()
{
	m_pImpl->restoredmetrics = m_pImpl->windowmetrics;
}

// font list view options
DWORD CProgramOptions::GetFontNameColWidth() const
{
	return m_pImpl->dwFontNameColWidth;
}

void CProgramOptions::SetFontNameColWidth(DWORD dwWidth)
{
	m_pImpl->dwFontNameColWidth = dwWidth;
}

DWORD CProgramOptions::GetFileNameColWidth() const
{
	return m_pImpl->dwFileNameColWidth;
}

void CProgramOptions::SetFileNameColWidth(DWORD dwWidth)
{
	m_pImpl->dwFileNameColWidth = dwWidth;
}

DWORD CProgramOptions::GetSizeColWidth() const
{
	return m_pImpl->dwSizeColWidth;
}

void CProgramOptions::SetSizeColWidth(DWORD dwWidth)
{
	m_pImpl->dwSizeColWidth = dwWidth;
}

DWORD CProgramOptions::GetFontListCharSize() const
{
	return m_pImpl->dwFontListCharSize;
}

void CProgramOptions::SetFontListCharSize(DWORD dwWidth)
{
	m_pImpl->dwFontListCharSize = dwWidth;
}

DWORD CProgramOptions::GetFontListSortColumn() const
{
	return m_pImpl->dwFontListSortColumn;
}

void CProgramOptions::SetFontListSortColumn(DWORD dwColumn)
{
	m_pImpl->dwFontListSortColumn = dwColumn;
}

bool CProgramOptions::IsFontListSortDescending() const
{
	return m_pImpl->bFontListSortDescending;
}

void CProgramOptions::SetFontListSortDescending(bool bDescending)
{
	m_pImpl->bFontListSortDescending = bDescending;
}

bool CProgramOptions::ToggleFontListSortDescending()
{
	return (m_pImpl->bFontListSortDescending = !m_pImpl->bFontListSortDescending);
}

CString CProgramOptions::GetFontListText() const
{
	return m_pImpl->strFontListText;
}

void CProgramOptions::SetFontListText(const CString& strText)
{
	m_pImpl->strFontListText = strText;
}

bool CProgramOptions::AutoInstall() const
{
	return m_pImpl->bAutoInstall;
}

void CProgramOptions::SetAutoInstall(bool bSet)
{
	m_pImpl->bAutoInstall = bSet;
}

bool CProgramOptions::ShowFullPathInTitleBar() const
{
	return m_pImpl->bShowFullPathInTitleBar;
}

void CProgramOptions::ShowFullPathInTitleBar(bool bShow)
{
	m_pImpl->bShowFullPathInTitleBar = bShow;
}

// font preview options
CProgramOptions::eFontDetailMode CProgramOptions::GetFontDetailMode() const
{
	return (eFontDetailMode)m_pImpl->dwFontDetailMode;
}

void CProgramOptions::SetFontDetailMode(eFontDetailMode dwMode)
{
	m_pImpl->dwFontDetailMode = (DWORD)dwMode;
}

DWORD CProgramOptions::GetPreviewFontSize() const
{
	return m_pImpl->dwPreviewFontSize;
}

void CProgramOptions::SetPreviewFontSize(DWORD dwWidth)
{
	m_pImpl->dwPreviewFontSize = dwWidth;
}

DWORD CProgramOptions::GetPreviewFontColor() const
{
	return m_pImpl->dwPreviewFontColor;
}

void CProgramOptions::SetPreviewFontColor(DWORD dwColor)
{
	m_pImpl->dwPreviewFontColor = dwColor;
}

DWORD CProgramOptions::GetPreviewFontBackgroundColor() const
{
	return m_pImpl->dwPreviewBackgroundColor;
}

void CProgramOptions::SetPreviewFontBackgroundColor(DWORD dwColor)
{
	m_pImpl->dwPreviewBackgroundColor = dwColor;
}

bool CProgramOptions::IsPreviewFontItalic() const
{
	return m_pImpl->bPreviewFontItalic;
}

void CProgramOptions::SetPreviewFontItalic(bool bItalic)
{
	m_pImpl->bPreviewFontItalic = bItalic;
}

bool CProgramOptions::IsPreviewFontBold() const
{
	return m_pImpl->bPreviewFontBold;
}

void CProgramOptions::SetPreviewFontBold(bool bBold)
{
	m_pImpl->bPreviewFontBold = bBold;
}

bool CProgramOptions::IsPreviewFontUnderlined() const
{
	return m_pImpl->bPreviewFontUnderlined;
}

void CProgramOptions::SetPreviewFontUnderlined(bool bUnderlined)
{
	m_pImpl->bPreviewFontUnderlined = bUnderlined;
}

CString CProgramOptions::GetPreviewText() const
{
	return m_pImpl->strPreviewText;
}

void CProgramOptions::SetPreviewText(LPCTSTR lpszText)
{
	m_pImpl->strPreviewText = lpszText;
}

// font map
DWORD CProgramOptions::GetFontMapSize() const
{
	return m_pImpl->dwFontMapSize;
}

void CProgramOptions::SetFontMapSize(DWORD dwSize)
{
	m_pImpl->dwFontMapSize = dwSize;
}

DWORD CProgramOptions::GetFontMapFontColor() const
{
	return m_pImpl->dwFontMapFontColor;
}

void CProgramOptions::SetFontMapFontColor(DWORD dwColor)
{
	m_pImpl->dwFontMapFontColor = dwColor;
}

DWORD CProgramOptions::GetFontMapFontBackgroundColor() const
{
	return m_pImpl->dwFontMapBackgroundColor;
}

void CProgramOptions::SetFontMapFontBackgroundColor(DWORD dwColor)
{
	m_pImpl->dwFontMapBackgroundColor = dwColor;
}

// printing
DWORD CProgramOptions::GetPrintCharSize() const
{
	return m_pImpl->dwPrintCharSize;
}

void CProgramOptions::SetPrintCharSize(DWORD dwSize)
{
	m_pImpl->dwPrintCharSize = dwSize;
}

CString CProgramOptions::GetPrintSampleTextLine1() const
{
	return m_pImpl->strPrintSampleTextLine1;
}

void CProgramOptions::SetPrintSampleTextLine1(const CString& strText)
{
	m_pImpl->strPrintSampleTextLine1 = strText;
}

CString CProgramOptions::GetPrintSampleTextLine2() const
{
	return m_pImpl->strPrintSampleTextLine2;
}

void CProgramOptions::SetPrintSampleTextLine2(const CString& strText)
{
	m_pImpl->strPrintSampleTextLine2 = strText;
}

// character detail window
void CProgramOptions::GetCharDetailWindowPos(POINT& pt) const
{
	pt = m_pImpl->ptCharDetailWindowPos;
}

void CProgramOptions::SetCharDetailWindowPos(const POINT& pt)
{
	m_pImpl->ptCharDetailWindowPos = pt;
}

void CProgramOptions::GetCharDetailWindowSize(SIZE& size) const
{
	size = m_pImpl->sizeCharDetailWindowSize;
}

void CProgramOptions::SetCharDetailWindowSize(const SIZE& size)
{
	m_pImpl->sizeCharDetailWindowSize = size;
}

// find dialog options
bool CProgramOptions::SearchRemovableDisks() const
{
	return m_pImpl->bSearchRemovableDisks;
}

void CProgramOptions::SetSearchRemovableDisks(bool bSearch)
{
	m_pImpl->bSearchRemovableDisks = bSearch;
}

bool CProgramOptions::SearchSubFolders() const
{
	return m_pImpl->bSearchSubFolders;
}

void CProgramOptions::SetSearchSubFolders(bool bSearch)
{
	m_pImpl->bSearchSubFolders = bSearch;
}

bool CProgramOptions::SearchMatchCase() const
{
	return m_pImpl->bSearchMatchCase;
}

void CProgramOptions::SetSearchMatchCase(bool bSearch)
{
	m_pImpl->bSearchMatchCase = bSearch;
}

bool CProgramOptions::SearchMatchWord() const
{
	return m_pImpl->bSearchMatchWord;
}

void CProgramOptions::SetSearchMatchWord(bool bSearch)
{
	m_pImpl->bSearchMatchWord = bSearch;
}

bool CProgramOptions::SearchUsingRegex() const
{
	return m_pImpl->bSearchUsingRegex;
}

void CProgramOptions::SetSearchUsingRegex(bool bSearch)
{
	m_pImpl->bSearchUsingRegex = bSearch;
}

CProgramOptions::eStartupOption CProgramOptions::GetStartupOption() const
{
	return m_pImpl->nStartupOption;
}

void CProgramOptions::SetStartupOption(eStartupOption nStartupOption)
{
	m_pImpl->nStartupOption = nStartupOption;
}

CString CProgramOptions::GetDefaultFolder() const
{
	return m_pImpl->strDefaultFolder;
}

void CProgramOptions::SetDefaultFolder(LPCTSTR lpszFolder)
{
	m_pImpl->strDefaultFolder = lpszFolder;
}

CString CProgramOptions::GetDefaultProject() const
{
	return m_pImpl->strDefaultProject;
}

void CProgramOptions::SetDefaultProject(LPCTSTR lpszProject)
{
	m_pImpl->strDefaultProject = lpszProject;
}

CString CProgramOptions::GetLastFolder() const
{
	return m_pImpl->strLastFolder;
}

void CProgramOptions::SetLastFolder(LPCTSTR lpszFolder)
{
	m_pImpl->strLastFolder = lpszFolder;
}

CString CProgramOptions::GetLastProject() const
{
	return m_pImpl->strLastProject;
}

void CProgramOptions::SetLastProject(LPCTSTR lpszProject)
{
	m_pImpl->strLastProject = lpszProject;
}

bool CProgramOptions::UsingFontProject() const
{
	return m_pImpl->bUseFontProject;
}

void CProgramOptions::UseFontProject(bool bUse)
{
	m_pImpl->bUseFontProject = bUse;
}

CProgramOptions::eRenderingOption CProgramOptions::GetFontListRenderingOption() const
{
	return m_pImpl->nFontListRenderingOption;
}

void CProgramOptions::SetFontListRenderingOption(eRenderingOption nOption)
{
	m_pImpl->nFontListRenderingOption = nOption;
}

CProgramOptions::eRenderingOption CProgramOptions::GetFontMapRenderingOption() const
{
	return m_pImpl->nFontMapRenderingOption;
}

void CProgramOptions::SetFontMapRenderingOption(eRenderingOption nOption)
{
	m_pImpl->nFontMapRenderingOption = nOption;
}

CProgramOptions::eRenderingOption CProgramOptions::GetCharDetailRenderingOption() const
{
	return m_pImpl->nCharDetailRenderingOption;
}

void CProgramOptions::SetCharDetailRenderingOption(eRenderingOption nOption)
{
	m_pImpl->nCharDetailRenderingOption = nOption;
}

void CProgramOptions::GetFindWindowPos(POINT& pt) const
{
	pt = m_pImpl->ptFindWindow;
}

void CProgramOptions::SetFindWindowPos(const POINT& pt)
{
	m_pImpl->ptFindWindow = pt;
}

void CProgramOptions::GetFindWindowSize(SIZE& size) const
{
	size = m_pImpl->sizeFindWindow;
}

void CProgramOptions::SetFindWindowSize(const SIZE& size)
{
	m_pImpl->sizeFindWindow = size;
}

size_t CProgramOptions::GetMainRebarLayoutSize() const
{
	boost::scoped_ptr<CKey> pKey(new CKey());

	size_t nSize = 0;
	if (pKey->OpenKey(HKEY_CURRENT_USER, cstrFontRunnerRegSection))
		nSize = pKey->GetBinary(cstrMainRebarLayout, NULL, 0);

	return nSize;
}

void CProgramOptions::GetMainRebarLayout(unsigned char* pLayoutData, size_t nSize) const
{
	boost::scoped_ptr<CKey> pKey(new CKey());

	if (pKey->OpenKey(HKEY_CURRENT_USER, cstrFontRunnerRegSection))
		pKey->GetBinary(cstrMainRebarLayout, pLayoutData, (DWORD)nSize);
}

void CProgramOptions::SetMainRebarLayout(const unsigned char* pLayoutData, size_t nSize)
{
	boost::scoped_ptr<CKey> pKey(new CKey());

	if (pKey->OpenKey(HKEY_CURRENT_USER, cstrFontRunnerRegSection))
		pKey->SetBinary(cstrMainRebarLayout, (void*)pLayoutData, (DWORD)nSize);
}

CString CProgramOptions::GetUpdaterLocation() const
{
	return m_pImpl->strUpdaterLocation;
}

void CProgramOptions::SetUpdaterLocation(LPCTSTR szLocation)
{
	m_pImpl->strUpdaterLocation = szLocation;
}

void CProgramOptions::ClearUpdaterLocation()
{
	m_pImpl->strUpdaterLocation.Empty();
}

bool CProgramOptions::GetClearLocationsOnExit() const
{
	return m_pImpl->bClearLocationsOnExit;
}

void CProgramOptions::SetClearLocationsOnExit(bool bClear)
{
	m_pImpl->bClearLocationsOnExit = bClear;
}

bool CProgramOptions::GetLimitLocations() const
{
	return m_pImpl->bLimitLocations;
}

void CProgramOptions::SetLimitLocations(bool bLimit)
{
	m_pImpl->bLimitLocations = bLimit;
}

int CProgramOptions::GetLocationsLimit() const
{
	return m_pImpl->nLocationsLimit;
}

void CProgramOptions::SetLocationsLimit(int nLimit)
{
	m_pImpl->nLocationsLimit = nLimit;
}

DWORD CProgramOptions::GetFontListDropOption() const
{
	return m_pImpl->nFontListDropOption;
}

void CProgramOptions::SetFontListDropOption(DWORD dwOption)
{
	m_pImpl->nFontListDropOption = dwOption;
}
