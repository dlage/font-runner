/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "TransparentStatic.h"
#include "ProgramOptions.h"
#include <vector>
#include <boost/shared_ptr.hpp>

// forward declare some classes
class CPaletteWnd;
class CRichTextPreviewView;
class CFontMapView;
class CFontItem;

// CFontDetailFrame frame

class CFontDetailFrame : public CFrameWnd
{
	DECLARE_DYNCREATE(CFontDetailFrame)
protected:
	CFontDetailFrame();           // protected constructor used by dynamic creation
	virtual ~CFontDetailFrame();

// operations
public:
	// sets view mode
	CProgramOptions::eFontDetailMode SwitchView(CProgramOptions::eFontDetailMode nView);

	// for preview toolbar updates
	void CheckPreviewToolbarButton(int nID, bool bCheck);

private:
	bool CreatePreviewToolbarBand();
	bool CreateColorSelectBitmap(COLORREF clrCurrent, CBitmap& bitmap);
	CView* GetView(CProgramOptions::eFontDetailMode nView);
	void SizeFrame();

// signal handlers
private:
	void OnColorPaletteClosed();
	void OnSelectedFontChange(const CFontItem* pData);
	void OnDetailColorChange(COLORREF color, bool bForeground);
	void OnOpenColorPalette(bool bForeground);

// attributes
private:
	CToolBar			m_wndFormatToolbar;
	CToolBar			m_wndColorToolbar;
	CReBar				m_wndPreviewRebar;
	CProgramOptions::eFontDetailMode				m_nCurrentView;
	CRichTextPreviewView*	m_pPreviewView;
	CFontMapView*		m_pMapView;
	CComboBox			m_ctrlModeCombo;
	CComboBox			m_ctrlSizeCombo;
	CTransparentStatic	m_wndFontNameStatic;
	int					m_nFormatToolbarIndex;
	CImageList			m_ilColorToolbarNormal, m_ilColorToolbarHot, m_ilColorToolbarDisabled;
	int					m_nForegroundIndex, m_nBackgroundIndex;
	CBitmap				m_bitmapMask;
	boost::shared_ptr<CPaletteWnd> m_pwndPalette;

// overrides
protected:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnModeSelChange();
	afx_msg void OnSizeSelChange();
	afx_msg void OnSizeEditChange();
	afx_msg void OnForegroundButton();
	afx_msg void OnBackgroundButton();
};

