/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include "webbrowser.h"
#include "afxwin.h"


// CUpdateAvailableDialog dialog

class CUpdateAvailableDialog : public CDialog
{
	DECLARE_DYNAMIC(CUpdateAvailableDialog)

public:
	CUpdateAvailableDialog(LPCTSTR szURL, CWnd* pParent = NULL);   // standard constructor
	virtual ~CUpdateAvailableDialog();

// Dialog Data
	enum { IDD = IDD_UPDATEAVAILABLE_DIALOG };

protected:
	CString m_strURL;
	CWebbrowser m_browser;
	CButton m_ctrlContinueButton;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
	afx_msg void OnPaint();

public:
	DECLARE_EVENTSINK_MAP()
	void DownloadCompleteUpdatedetailsWebbrowser();
};
