/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "StdAfx.h"
#include "FontRunnerSplitter.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNAMIC(CFontRunnerSplitter, CSplitterWnd)

CFontRunnerSplitter::CFontRunnerSplitter()
{
}

CFontRunnerSplitter::~CFontRunnerSplitter()
{
}

void CFontRunnerSplitter::TrackRowSize(int y, int row)
{
	CSplitterWnd::TrackRowSize(y, row);
	
	if (m_pListener.get())
		m_pListener->OnTrackRowSize(m_pRowInfo[row].nIdealSize, row);
}

void CFontRunnerSplitter::TrackColumnSize(int x, int col)
{
	CSplitterWnd::TrackColumnSize(x, col);

	if (m_pListener.get())
		m_pListener->OnTrackColumnSize(m_pColInfo[col].nIdealSize, col);
}

void CFontRunnerSplitter::SetListener(ISplitterListener* pListener)
{
	m_pListener.reset(pListener);
}