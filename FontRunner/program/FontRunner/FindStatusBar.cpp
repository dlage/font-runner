/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// FindStatusBar.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "FindStatusBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

static UINT indicators[] =
{
	ID_INDICATOR_STATUSICON,	// status icon
	ID_INDICATOR_SEARCHING,		// status text
	ID_INDICATOR_PROGRESS		// indeterminate progress bar
};

// CFindStatusBar

IMPLEMENT_DYNAMIC(CFindStatusBar, CStatusBar)

CFindStatusBar::CFindStatusBar()
  : m_bSearching(false),
    m_cnIconWidth(16),
	m_cnProgressWidth(150),
	m_bProgressMarqueeOn(false)
{
}

CFindStatusBar::~CFindStatusBar()
{
}

void CFindStatusBar::Searching(LPCTSTR szFolder)
{
	if (szFolder)
	{
		CString strStatus;
		AfxFormatString1(strStatus, IDS_FINDFONTDIALOG_STATUS, szFolder);

		SetPaneText(kPane_StatusText, strStatus);
	}

	if (!m_bProgressMarqueeOn)
	{
		// start marquee
		m_wndProgressCtrl.ShowWindow(SW_SHOW);
		m_bProgressMarqueeOn = true;
		m_wndProgressCtrl.SendMessage(PBM_SETMARQUEE, m_bProgressMarqueeOn, (LPARAM)60);

		// switch to working icon
		UpdateStatusIcon(IDB_STATUS_WORKING);
	}
}

void CFindStatusBar::Canceled(int nFound)
{
	CString strFound;
	strFound.Format(_T("%d"), nFound);

	CString strStatus;
	AfxFormatString1(strStatus, IDS_FINDFONTDIALOG_STATUS_CANCELED, strFound);

	SetPaneText(kPane_StatusText, strStatus);

	if (m_bProgressMarqueeOn)
	{
		m_wndProgressCtrl.ShowWindow(SW_HIDE);
		m_bProgressMarqueeOn = false;
		m_wndProgressCtrl.SendMessage(PBM_SETMARQUEE, m_bProgressMarqueeOn);

		// switch to canceled icon
		UpdateStatusIcon(IDB_STATUS_CANCELED);
	}
}

void CFindStatusBar::Finished(int nFound)
{
	CString strFound;
	strFound.Format(_T("%d"), nFound);

	CString strStatus;
	AfxFormatString1(strStatus, IDS_FINDFONTDIALOG_STATUS_COMPLETE, strFound);

	SetPaneText(kPane_StatusText, strStatus);

	if (m_bProgressMarqueeOn)
	{
		m_wndProgressCtrl.ShowWindow(SW_HIDE);
		m_bProgressMarqueeOn = false;
		m_wndProgressCtrl.SendMessage(PBM_SETMARQUEE, m_bProgressMarqueeOn);

		// swtich to ready icon
		UpdateStatusIcon(IDB_STATUS_READY);
	}
}

void CFindStatusBar::UpdateStatusIcon(UINT nBitmapID)
{
	// delete old bitmap object if it exists
	if (m_bmpStatus.m_hObject)
		m_bmpStatus.DeleteObject();
	
	m_bmpStatus.LoadBitmap(nBitmapID);
	m_wndStatusIcon.SetBitmap((HBITMAP)m_bmpStatus.GetSafeHandle());
}

BEGIN_MESSAGE_MAP(CFindStatusBar, CStatusBar)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CFindStatusBar message handlers

int CFindStatusBar::OnCreate(LPCREATESTRUCT lpcs)
{
	// create status bar
	lpcs->style |= WS_CLIPCHILDREN;
	VERIFY(CStatusBar::OnCreate(lpcs)==0);

	// setup status bar
	SetIndicators(indicators, sizeof(indicators) / sizeof(UINT));
	SetPaneInfo(kPane_StatusIcon, ID_INDICATOR_STATUSICON, SBPS_NORMAL, m_cnIconWidth);
	SetPaneInfo(kPane_StatusText, ID_INDICATOR_SEARCHING, SBPS_STRETCH, 0);
	SetPaneInfo(kPane_Progress, ID_INDICATOR_PROGRESS, SBPS_NORMAL, m_cnProgressWidth);

	SetPaneText(kPane_StatusIcon, _T(""));
	SetPaneText(kPane_Progress, _T(""));

	// create status icon
	CRect rc;
	GetItemRect(kPane_StatusIcon, &rc);
	rc.right = rc.left + m_cnIconWidth;
	VERIFY(m_wndStatusIcon.Create(_T(""), WS_CHILD | WS_VISIBLE | SS_BITMAP | SS_REALSIZEIMAGE, rc, this, ID_FINDSTATUSBAR_STATUSICON));

	m_bmpStatus.LoadBitmap(IDB_STATUS_READY);
	m_wndStatusIcon.SetBitmap((HBITMAP)m_bmpStatus.GetSafeHandle());

	// create progress control
	VERIFY(m_wndProgressCtrl.Create(WS_CHILD | PBS_MARQUEE, CRect(), this, ID_FINDSTATUSBAR_PROGRESS));

	return 0;
}

void CFindStatusBar::OnSize(UINT nType, int cx, int cy)
{
	CStatusBar::OnSize(nType, cx, cy);

	CRect rc;
	GetItemRect(kPane_StatusIcon, &rc);
	rc.right = rc.left + m_cnIconWidth;
	m_wndStatusIcon.MoveWindow(&rc);

	GetItemRect(kPane_Progress, &rc);
	m_wndProgressCtrl.MoveWindow(&rc, FALSE);
}
