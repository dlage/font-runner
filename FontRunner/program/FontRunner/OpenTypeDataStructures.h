/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// This file contains classes that hold and manage data from
// standard OpenType files.

#pragma once

#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/shared_array.hpp>

namespace CruxTechnologies
{
	namespace OpenTypeFontData
	{
		enum ePlatformID
		{
			kPlatformID_Unicode = 0,
			kPlatformID_Macintosh,
			kPlatformID_ISO,
			kPlatformID_Microsoft,
			kPlatformID_Custom
		};

		enum eEncodingID
		{
			kEncodingID_Symbol = 0,
			kEncodingID_Unicode,
			kEncodingID_ShiftJIS,
			kEncodingID_PRC,
			kEncodingID_Big5,
			kEncodingID_Wansung,
			kEncodingID_Johab,
			kEncodingID_UCS_4 = 10
		};

		//----------------------------------------------------------------------------------
		// base interface for all structure classes
		class IDataStructure
		{
		public:
			// Prerequisite for all subclasses:
			// pBeginning must point to beginning of table
			virtual std::size_t Initialize(char* pBeginning, size_t size) = 0;
		};

		//----------------------------------------------------------------------------------
		// TTF/OTF header
		class CHeader : public IDataStructure
		{
		public:
			CHeader();
			virtual ~CHeader();
			std::size_t Initialize(char* pBeginning, size_t size);

		// read-only accessors
		public:
			uint32_t Version() const;
			uint16_t NumTables() const;
			uint16_t SearchRange() const;
			uint16_t EntrySelector() const;
			uint16_t RangeShift() const;

		private:
			struct _data;
			boost::shared_ptr<_data> m_pData;
		};

		//----------------------------------------------------------------------------------
		// table directory structure
		class CTableDirectory : public IDataStructure
		{
		public:
			CTableDirectory();
			virtual ~CTableDirectory();
			std::size_t Initialize(char* pBeginning, size_t size);

		// read-only accessors
		public:
			uint32_t Tag() const;
			uint32_t CheckSum() const;
			uint32_t Offset() const;
			uint32_t Length() const;

		private:
			struct _data;
			_data* m_pData;
		};

		//----------------------------------------------------------------------------------
		// name table record
		class CNameRecord : public IDataStructure
		{
		public:
			CNameRecord(char* pStringStorageAddress);
			CNameRecord(const CNameRecord& src);
			virtual ~CNameRecord();
			std::size_t Initialize(char* pBeginning, size_t size);

		public:
			// Name ID's listed at:
			// http://www.microsoft.com/OpenType/OTSpec/name.htm
			enum eNameID
			{
				kNameID_Copyright = 0,
				kNameID_FamilyName,
				kNameID_SubFamilyName,
				kNameID_UniqueID,
				kNameID_FullFontName,
				kNameID_Version,
				kNameID_PostscriptName,
				kNameID_Trademark,
				kNameID_Manufacturer,
				kNameID_Designer,
				kNameID_Description,
				kNameID_VendorURL,
				kNameID_DesignerURL,
				kNameID_License,
				kNameID_LicenseURL,
				kNameID_Reserved15,
				kNameID_PreferredFamily,
				kNameID_PreferredSubFamily,
				kNameID_CompatibleFull,
				kNameID_SampleText,
				kNameID_PostscriptCIDFindfontName
			};

		public:
			uint16_t PlatformID() const;
			uint16_t PlatformEncodingID() const;
			uint16_t LanguageID() const;
			uint16_t NameID() const;
			const std::wstring* Name() const;
			inline bool IsInitialized() const { return m_pData != NULL; }

		private:
			struct _data;
			_data* m_pData;
			char* m_pStringStorageAddress;
		};

		//----------------------------------------------------------------------------------
		// the naming table
		class CNamingTable : public IDataStructure
		{
		public:
			CNamingTable();
			virtual ~CNamingTable();
			std::size_t Initialize(char* pBeginning, size_t size);

		// read-only accessors
		public:
			bool Initialized() const;
			uint16_t FormatSelector() const;
			uint16_t NumberOfNameRecords() const;
			uint16_t StringStorageOffset() const;

			// list of name records
			typedef std::vector<CNameRecord> NameRecordContainer_Type;
			const NameRecordContainer_Type& GetNameRecords() const;

		private:
			struct _data;
			boost::shared_ptr<_data> m_pData;
		};


		//----------------------------------------------------------------------------------
		// the cmap table
		class CcmapTable : public IDataStructure
		{
		public:
			CcmapTable();
			std::size_t Initialize(char* pTable, size_t size);

		public:
			struct EncodingRecord
			{
				ePlatformID nPlatformID;
				eEncodingID nEncodingID;
				boost::shared_array<uint16_t> pMap;
				uint16_t	nMapLength;
			};

			typedef std::vector<EncodingRecord> EncodingRecordList_Type;

		public:
			const EncodingRecordList_Type* GetMaps() const;
			bool IsSymbol() const;

		private:
			size_t ReadFormat4Record(char* pRecordStart);

		private:
			struct _data;
			boost::shared_ptr<_data> m_pData;
		};

		//----------------------------------------------------------------------------------
		// the OS/2 (and Windows) table
		class COS2Table : public IDataStructure
		{
		public:
			COS2Table();
			std::size_t Initialize(char* pBeginning, size_t size);

		public:
			struct Part0
			{
				uint16_t 	version;
				int16_t 	xAvgCharWidth;
				uint16_t 	usWeightClass;
				uint16_t 	usWidthClass;
				uint16_t 	fsType;
				int16_t 	ySubscriptXSize;
				int16_t 	ySubscriptYSize;
				int16_t 	ySubscriptXOffset;
				int16_t 	ySubscriptYOffset;
				int16_t 	ySuperscriptXSize;
				int16_t 	ySuperscriptYSize;
				int16_t 	ySuperscriptXOffset;
				int16_t 	ySuperscriptYOffset;
				int16_t 	yStrikeoutSize;
				int16_t 	yStrikeoutPosition;
				int16_t 	sFamilyClass;
				uint8_t 	panose[10];
				uint32_t 	ulCharRange[4];
				int8_t 		achVendID[4];
				uint16_t 	fsSelection;
				uint16_t 	usFirstCharIndex;
				uint16_t 	usLastCharIndex;
				int16_t 	sTypoAscender;
				int16_t 	sTypoDescender;
				int16_t 	sTypoLineGap;
				uint16_t 	usWinAscent;
				uint16_t 	usWinDescent;
			};

			struct Part1
			{
				uint32_t 	ulCodePageRange1;
				uint32_t 	ulCodePageRange2;
			};

			struct Part2
			{
				int16_t 	sxHeight;
				int16_t 	sCapHeight;
				uint16_t 	usDefaultChar;
				uint16_t 	usBreakChar;
				uint16_t 	usMaxContext;
			};

			struct TableVersion0
			{
				Part0 part0;
			};

			struct TableVersion1
			{
				Part0 part0;
				Part1 part1;
			};

			struct TableVersion2
			{
				Part0 part0;
				Part1 part1;
				Part2 part2;
			};

			typedef TableVersion2 TableVersion3;

		public:
			bool bInitialized;
			TableVersion3 m_table;
		};
	}
}