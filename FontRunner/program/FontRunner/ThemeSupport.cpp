/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "StdAfx.h"
#include "ThemeSupport.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// init static variables
int CThemeSupport::s_nRefCount = 0;
HMODULE	CThemeSupport::s_hThemeModule = NULL;
bool CThemeSupport::s_bLibLoadAttempted = false;

// Theme function pointers
void* CThemeSupport::s_pfnIsAppThemed = NULL;
void* CThemeSupport::s_pfnIsThemeActive = NULL;
void* CThemeSupport::s_pfnEnableTheming = NULL;
void* CThemeSupport::s_pfnOpenThemeData = NULL;
void* CThemeSupport::s_pfnCloseThemeData = NULL;
void* CThemeSupport::s_pfnDrawThemeBackground = NULL;
void* CThemeSupport::s_pfnDrawThemeParentBackground = NULL;
void* CThemeSupport::s_pfnDrawThemeEdge = NULL;
void* CThemeSupport::s_pfnDrawThemeText = NULL;
void* CThemeSupport::s_pfnEnableThemeDialogTexture = NULL;
void* CThemeSupport::s_pfnGetThemeBackgroundContentRect = NULL;
void* CThemeSupport::s_pfnGetThemeColor = NULL;
void* CThemeSupport::s_pfnDrawThemeTextEx = NULL;

// theme function pointer types
typedef BOOL (*ISAPPTHEMED)();
typedef BOOL (*ISTHEMEACTIVE)();
typedef HTHEME (__stdcall *OPENTHEMEDATA)(HWND, LPCWSTR);
typedef HRESULT (__stdcall *CLOSETHEMEDATA)(HTHEME);
typedef HRESULT (__stdcall *ENABLETHEMING)(BOOL);
typedef HRESULT (__stdcall *DRAWTHEMEBACKGROUND)(HTHEME, HDC, int, int, const RECT*, const RECT*);
typedef HRESULT (__stdcall *DRAWTHEMEPARENTBACKGROUND)(HWND, HDC, RECT*);
typedef HRESULT (__stdcall *DRAWTHEMEEDGE)(HTHEME, HDC, int, int, const RECT*, UINT, UINT, RECT*);
typedef HRESULT (__stdcall *DRAWTHEMETEXT)(HTHEME, HDC, int, int, LPCWSTR, int, DWORD, DWORD, const RECT*);
typedef HRESULT (__stdcall *ENABLETHEMEDIALOGTEXTURE)(HWND, DWORD);
typedef HRESULT (__stdcall *GETTHEMEBACKGROUNDCONTENTRECT)(HTHEME hTheme, HDC hdc, int iPartId, int iStateId, const RECT* pBoundingRect, RECT* pContentRect);
typedef HRESULT (__stdcall *GETTHEMECOLOR)(HTHEME hTheme, int iPartId, int iStateId, int iPropId, COLORREF* pColor);
typedef HRESULT (__stdcall *DRAWTHEMETEXTEX)(HTHEME hTheme, HDC hdc, int iPartId, int iStateId, LPCWSTR pszText, int iCharCount, DWORD dwFlags, LPRECT pRect, const DTTOPTS *pOptions);

CThemeSupport::CThemeSupport(bool bLoadThemeLib)
{
	++s_nRefCount;

	if (bLoadThemeLib)
		LoadThemeLibrary();
}

CThemeSupport::~CThemeSupport()
{
	--s_nRefCount;

	// free theme library if open
	if ((s_hThemeModule) && (s_nRefCount == 0))
	{
		// set all function pointers to NULL
		s_pfnIsAppThemed = NULL;
		s_pfnIsThemeActive = NULL;
		s_pfnOpenThemeData = NULL;
		s_pfnCloseThemeData = NULL;
		s_pfnDrawThemeBackground = NULL;
		s_pfnDrawThemeParentBackground = NULL;
		s_pfnDrawThemeEdge = NULL;
		s_pfnDrawThemeText = NULL;
		s_pfnEnableThemeDialogTexture = NULL;
		s_pfnGetThemeBackgroundContentRect = NULL;
		s_pfnGetThemeColor = NULL;

		// free theme library
		::FreeLibrary(s_hThemeModule);
		s_hThemeModule = NULL;

		s_bLibLoadAttempted = false;
	}
}

void CThemeSupport::LoadThemeLibrary()
{
	if (s_bLibLoadAttempted)
		return;

	if (!s_hThemeModule)
	{
		// see if we can open the theme library
		s_hThemeModule = ::LoadLibrary(_T("UXTHEME.DLL"));

		// if theme library is opened, get addresses of necessary theme functions
		if (s_hThemeModule)
		{
			s_pfnIsAppThemed = ::GetProcAddress(s_hThemeModule, "IsAppThemed");
			s_pfnIsThemeActive = ::GetProcAddress(s_hThemeModule, "IsThemeActive");
			s_pfnOpenThemeData = ::GetProcAddress(s_hThemeModule, "OpenThemeData");
			s_pfnCloseThemeData = ::GetProcAddress(s_hThemeModule, "CloseThemeData");
			s_pfnDrawThemeBackground = ::GetProcAddress(s_hThemeModule, "DrawThemeBackground");
			s_pfnDrawThemeParentBackground = ::GetProcAddress(s_hThemeModule, "DrawThemeParentBackground");
			s_pfnDrawThemeEdge = ::GetProcAddress(s_hThemeModule, "DrawThemeEdge");
			s_pfnDrawThemeText = ::GetProcAddress(s_hThemeModule, "DrawThemeText");
			s_pfnEnableThemeDialogTexture = ::GetProcAddress(s_hThemeModule, "EnableThemeDialogTexture");
			s_pfnGetThemeBackgroundContentRect = ::GetProcAddress(s_hThemeModule, "GetThemeBackgroundContentRect");
			s_pfnGetThemeColor = ::GetProcAddress(s_hThemeModule, "GetThemeColor");
			s_pfnDrawThemeTextEx = ::GetProcAddress(s_hThemeModule, "DrawThemeTextEx");
		}

		s_bLibLoadAttempted = true;
	}
}

BOOL CThemeSupport::IsAppThemed()
{
	// if no theme library, return false, otherwise call XP API
	if ((!s_hThemeModule) || (!s_pfnIsAppThemed))
		return FALSE;
	else
		return ((ISAPPTHEMED)s_pfnIsAppThemed)();
}

BOOL CThemeSupport::IsThemeActive()
{
	if ((!s_hThemeModule) || (!s_pfnIsThemeActive))
		return FALSE;
	else
		return ((ISTHEMEACTIVE)s_pfnIsThemeActive)();
}

HRESULT CThemeSupport::EnableTheming(BOOL fEnable)
{
	if (s_pfnEnableTheming)
		return ((ENABLETHEMING)s_pfnEnableTheming)(fEnable);
	else
		return E_FAIL;
}

HTHEME CThemeSupport::OpenThemeData(HWND hwnd, LPCWSTR pszClassList)
{
	if (s_pfnOpenThemeData)
		return ((OPENTHEMEDATA)s_pfnOpenThemeData)(hwnd, pszClassList);
	else
		return NULL;
}

HRESULT CThemeSupport::CloseThemeData(HTHEME hThemeData)
{
	if (s_pfnCloseThemeData)
		return ((CLOSETHEMEDATA)s_pfnCloseThemeData)(hThemeData);
	else
		return E_FAIL;
}

HRESULT CThemeSupport::DrawThemeBackground(HTHEME hThemeData, HDC hdc, int iPartId, int iStateId, const RECT* pRect, const RECT* pClipRect)
{
	if (s_pfnDrawThemeBackground)
		return ((DRAWTHEMEBACKGROUND)s_pfnDrawThemeBackground)(hThemeData, hdc, iPartId, iStateId, pRect, pClipRect);
	else
		return E_FAIL;
}

HRESULT CThemeSupport::DrawThemeParentBackground(HWND hWnd, HDC hdc, RECT* prc)
{
	if (s_pfnDrawThemeParentBackground)
		return ((DRAWTHEMEPARENTBACKGROUND)s_pfnDrawThemeParentBackground)(hWnd, hdc, prc);
	else
		return E_FAIL;
}

HRESULT CThemeSupport::DrawThemeEdge(HTHEME hThemeData, HDC hdc, int iPartId, int iStateId, const RECT *pDestRect, UINT uEdge, UINT uFlags, RECT *pContentRect)
{
	if (s_pfnDrawThemeEdge)
		return ((DRAWTHEMEEDGE)s_pfnDrawThemeEdge)(hThemeData, hdc, iPartId, iStateId, pDestRect, uEdge, uFlags, pContentRect);
	else
		return E_FAIL;
}

HRESULT CThemeSupport::DrawThemeText(HTHEME hThemeData, HDC hdc, int iPartId, int iStateId, LPCWSTR pszText, int iCharCount, DWORD dwTextFlags, DWORD dwTextFlags2, const RECT *pRect)
{
	if (s_pfnDrawThemeText)
		return ((DRAWTHEMETEXT)s_pfnDrawThemeText)(hThemeData, hdc, iPartId, iStateId, pszText, iCharCount, dwTextFlags, dwTextFlags2, pRect);
	else
		return E_FAIL;
}

HRESULT CThemeSupport::EnableThemeDialogTexture(HWND hWnd, DWORD dwFlags)
{
	if (s_pfnEnableThemeDialogTexture)
		return ((ENABLETHEMEDIALOGTEXTURE)s_pfnEnableThemeDialogTexture)(hWnd, dwFlags);
	else
		return E_FAIL;
}

HRESULT CThemeSupport::GetThemeBackgroundContentRect(HTHEME hTheme, HDC hdc, int iPartId, int iStateId, const RECT *pBoundingRect, RECT *pContentRect)
{
	if (s_pfnGetThemeBackgroundContentRect)
		return ((GETTHEMEBACKGROUNDCONTENTRECT)s_pfnGetThemeBackgroundContentRect)(hTheme, hdc, iPartId, iStateId, pBoundingRect, pContentRect);
	else
		return E_FAIL;
}

HRESULT CThemeSupport::GetThemeColor(HTHEME hTheme, int nPartID, int nStateID, int nPropID, COLORREF* pclr)
{
	if (s_pfnGetThemeColor)
		return ((GETTHEMECOLOR)s_pfnGetThemeColor)(hTheme, nPartID, nStateID, nPropID, pclr);
	else
		return E_FAIL;
}

HRESULT CThemeSupport::DrawThemeTextEx(HTHEME hTheme, HDC hdc, int iPartId, int iStateId, LPCWSTR pszText, int iCharCount, DWORD dwFlags, LPRECT pRect, const DTTOPTS *pOptions)
{
	if (s_pfnDrawThemeTextEx)
		return ((DRAWTHEMETEXTEX)s_pfnDrawThemeTextEx)(hTheme, hdc, iPartId, iStateId, pszText, iCharCount, dwFlags, pRect, pOptions);
	else
		return E_FAIL;
}