/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"
#include "WinToolbox.h"

#include <shlwapi.h>
#include <boost/scoped_array.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

bool CWinToolbox::GetAppVersion(unsigned long& major,
								unsigned long& minor,
								unsigned long& release,
								unsigned long& build)
{
	// get filename of this EXE
	TCHAR szFilename[MAX_PATH];
	::GetModuleFileName(AfxGetInstanceHandle(), szFilename, MAX_PATH);

	// get version info...

	// figure size and allocate buffer for version info
	DWORD dwVersionHandle = 0;
	DWORD dwFileVersionSize = ::GetFileVersionInfoSize(szFilename, &dwVersionHandle);

	try
	{
		boost::scoped_array<BYTE> pVersionInfo(new BYTE[dwFileVersionSize]);

		// get version info
		VS_FIXEDFILEINFO* pFixedFileInfo = NULL;
		UINT nVersionSize = 0;
		if (!::GetFileVersionInfo(szFilename, 0, dwFileVersionSize, pVersionInfo.get()))
			return false;

		if (!::VerQueryValue(pVersionInfo.get(), _T("\\"), (void**)&pFixedFileInfo, &nVersionSize))
			return false;

		// info back to caller
		major = HIWORD(pFixedFileInfo->dwFileVersionMS);
		minor = LOWORD(pFixedFileInfo->dwFileVersionMS);
		release = HIWORD(pFixedFileInfo->dwFileVersionLS);
		build = LOWORD(pFixedFileInfo->dwFileVersionLS);

		return true;
	}
	catch (...)
	{
	}

	return false;
}

CString CWinToolbox::GetAppVersionString()
{
	CString strVersion;
	unsigned long major = 0, minor = 0, release = 0, build = 0;

	// if we successfully get the app version string, stuff it into a string
	if (GetAppVersion(major, minor, release, build))
		strVersion.Format(_T("%d.%d.%d.%d"), major, minor, release, build);

	return strVersion;
}

DWORD CWinToolbox::GetDllVersion(LPCTSTR lpszDllName)
{
	// get version of a DLL
    HINSTANCE hinstDll;
    DWORD dwVersion = 0;

    hinstDll = LoadLibrary(lpszDllName);
	
    if (hinstDll)
    {
        DLLGETVERSIONPROC pDllGetVersion;

        pDllGetVersion = (DLLGETVERSIONPROC)GetProcAddress(hinstDll, "DllGetVersion");

		// if DLLGetversion is supported, call it
        if (pDllGetVersion)
        {
            DLLVERSIONINFO dvi;
            HRESULT hr;

            ZeroMemory(&dvi, sizeof(dvi));
            dvi.cbSize = sizeof(dvi);

            hr = (*pDllGetVersion)(&dvi);

            if (SUCCEEDED(hr))
                dwVersion = PACKVERSION(dvi.dwMajorVersion, dvi.dwMinorVersion);
        }
        
        FreeLibrary(hinstDll);
    }

    return dwVersion;
}

void CWinToolbox::MoveWindowIntoWorkArea(HWND hwnd, LPCRECT prcWorkArea)
{
	// get window rect
	CRect rcWindow;
	::GetWindowRect(hwnd, &rcWindow);

	// get work area rect
	CRect rcWorkArea;
	if (prcWorkArea == NULL)
	{
		// see which monitor we're in
		HMONITOR hMonitor = MonitorFromRect(rcWindow, MONITOR_DEFAULTTONEAREST);

		// get coordinates of this monitor's display area
		MONITORINFO mi;
		mi.cbSize = sizeof(MONITORINFO);
		GetMonitorInfo(hMonitor, &mi);

		rcWorkArea = mi.rcWork;
		prcWorkArea = &rcWorkArea;
	}

	// set window position depending on location (change it if it is off the screen)
	bool bChanged = false;
	if (rcWindow.left < prcWorkArea->left)
	{
		rcWindow.left = prcWorkArea->left;
		bChanged = true;
	}
	if (rcWindow.right > prcWorkArea->right)
	{
		rcWindow.left = prcWorkArea->right - rcWindow.Width();
		bChanged = true;
	}
	if (rcWindow.top < prcWorkArea->top)
	{
		rcWindow.top = prcWorkArea->top;
		bChanged = true;
	}
	if (rcWindow.bottom > prcWorkArea->bottom)
	{
		rcWindow.top = prcWorkArea->bottom - rcWindow.Height();
		bChanged = true;
	}

	if (bChanged)
		::SetWindowPos(hwnd, NULL, rcWindow.left, rcWindow.top, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
}

bool CWinToolbox::IsInteger(const CString& strNumber)
{
	// go through string, character by character, and check them
	int nMax = strNumber.GetLength();
	for (int nIndex = 0; nIndex < nMax; nIndex++)
	{
		TCHAR chTest = strNumber.GetAt(nIndex);
		if ((chTest < '0') || (chTest > '9'))
			return false;	// not a number
	}

	// didn't find a non-number
	return true;
}

CString CWinToolbox::BrowseForFolder(HWND hwndOwner, UINT nPromptID)
{
	CString strPrompt;
	strPrompt.LoadString(nPromptID);

	return BrowseForFolder(hwndOwner, strPrompt);
}

CString CWinToolbox::BrowseForFolder(HWND hwndOwner, CString strPrompt)
{
	// opens a Windows standard "Browse for folder" dialog and returns
	// selected folder.  Returns empty string if no folder was selected

	// string to return
	CString strReturn;

	// BROWSEINFO structure
	BROWSEINFO bi;

	// allocate memory for BROWSEINFO structure
	memset(&bi, '\0', sizeof(BROWSEINFO));

	TCHAR szDisplayName[MAX_PATH];
	TCHAR szPath[MAX_PATH];

	bi.hwndOwner = hwndOwner;
	bi.pidlRoot = NULL;
	bi.pszDisplayName = szDisplayName;
	bi.lpszTitle = (LPCTSTR)strPrompt;
	bi.ulFlags = 0;

	LPITEMIDLIST piid = NULL;
	
	// call the shell function
	piid = ::SHBrowseForFolder(&bi);

	// process the result
	if (piid && ::SHGetPathFromIDList(piid, szPath))
		strReturn = szPath;

	// Release the ITEMIDLIST if we got one
	if (piid)
	{
		LPMALLOC lpMalloc;
		VERIFY(::SHGetMalloc(&lpMalloc) == NOERROR);
		lpMalloc->Free(piid);
		lpMalloc->Release();
	}

	return strReturn;
}

CString CWinToolbox::GetAppPath()
{
	static CString strAppPath;
	if (strAppPath.IsEmpty())
	{
		TCHAR szPath[_MAX_PATH], szNewPath[_MAX_PATH];
		::GetModuleFileName( AfxGetInstanceHandle(), szPath, _MAX_PATH );
		::GetLongPathName(szPath, szNewPath, _MAX_PATH);
		strAppPath = szNewPath;
		int nEndOfPath = strAppPath.ReverseFind( '\\' );
		if (nEndOfPath == -1)
			nEndOfPath = strAppPath.Find( ':' );
		if (nEndOfPath != -1)
		{
			strAppPath = strAppPath.Left( nEndOfPath + 1 );
			if (strAppPath.Right( 1 ) != "\\")
				strAppPath += "\\";
		}
	}

	return strAppPath;
}

size_t CWinToolbox::GetFileSize(LPCTSTR szFileName)
{
	// get size of file
	size_t nSize = 0;
	HANDLE hFile = ::CreateFile(szFileName, GENERIC_READ, FILE_SHARE_READ,
								NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	if (hFile != INVALID_HANDLE_VALUE)
	{
		nSize = (size_t)::GetFileSize(hFile, NULL);

		::CloseHandle(hFile);
	}

	return nSize;
}

int CWinToolbox::GetExtraComboWidth()
{
	return ::GetSystemMetrics(SM_CXVSCROLL) + 12;
}

int CWinToolbox::GetComboHeight(CDC* pDC)
{
	TEXTMETRIC tm;
	pDC->GetTextMetrics(&tm);

	return (int)((double)tm.tmHeight * 1.625 + 0.5);
}

int CWinToolbox::GetEditHeight(CDC* pDC)
{
	return GetComboHeight(pDC);
}