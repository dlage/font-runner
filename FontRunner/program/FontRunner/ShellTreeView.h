/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "pidlutils.h"

// CShellTreeView view
class CShellTreeView : public CTreeView
{
	DECLARE_DYNCREATE(CShellTreeView)

protected:
	CShellTreeView();           // protected constructor used by dynamic creation
	virtual ~CShellTreeView();

// Overrides
protected:
	virtual void OnInitialUpdate();

// operations
public:
	bool CurrentFolderHasParent() const;

// drag and drop ops
public:
	void DragEnter(IDataObject *pDataObject, DWORD grfKeyState, POINTL pt, DWORD* pdwEffect);
	void DragOver(DWORD grfKeyState, POINTL pt, DWORD* pdwEffect);
	void DragLeave();
	void Drop(IDataObject *pDataObject, DWORD grfKeyState, POINTL pt, DWORD *pdwEffect);
	void CreateDragBitmap(const POINT& pt, SHDRAGIMAGE* pshdi);
	bool FolderDropped(IDataObject* pDataObject) const;
	void DragCleanup();

// signal handlers
private:
	void OnSelectedFolderChange(const CString& strFolder, const CString* pstrFile);
	void OnNavigateToFolder(const CString& strFolder); // navigate to folder without selecting it
	void OnRefresh();
	void OnFolderUp();

// internals
private:
	void FillNode(HTREEITEM hNode);
	bool IsAncestor(HTREEITEM hParent, HTREEITEM hChild) const;
	HTREEITEM InsertChildItem(HTREEITEM hParent, IShellFolder* pParent, LPITEMIDLIST pidl, LPITEMIDLIST fqpidl_parent, bool bSort = false);

	typedef std::vector<CString> FolderListType;
	bool ShowContextMenu(HTREEITEM hItem, LPSHELLFOLDER lpsfParent, LPITEMIDLIST  lpi, const CPoint& point);
	HRESULT InvokeContextMenuCommand(LPCSTR lpCommand) const;
	HTREEITEM FindTreeItem(HTREEITEM hRoot, LPSHELLFOLDER pShellFolder, LPCITEMIDLIST pidl);
	HTREEITEM NavigateToFolder(const CString& strFolder);
	void SelectFolder(const CString& strFolder);
	void Refresh(HTREEITEM hItem, IShellFolder* pParent);
	void SetExpander(HTREEITEM hItem, bool bVisible);

	enum eChangeType
	{
		kChange_Add,
		kChange_Remove,
		kChange_Modify
	};

	void UpdateTree(LPCITEMIDLIST pidl1, LPCITEMIDLIST pidl2, eChangeType nChange);
	void UpdateTree(LPCITEMIDLIST pidl, eChangeType nChange);

	// Callback routine for sorting the tree 
	static int CALLBACK TreeViewCompareProc(LPARAM lparam1, LPARAM lparam2, LPARAM lparamSort);

private:
	HTREEITEM	m_hDesktop; // this is the tree root because the desktop is the root of the shell namespace
	bool		m_bInitialUpdateDone; // to prevent initialupdate from being processed more than once
	bool		m_bSelfChange;
	bool		m_bDisableFolderChangeSignal;

	// popup menu
	CMenu					m_menuPopup;
	HTREEITEM				m_hContextMenuItem;

	// drag n drop
	IDropTarget*			m_pDropTarget;
	CComPtr<IDropTargetHelper> m_pDropTargetHelper;
	IDataObject*			m_pDropDataObject;
	HTREEITEM				m_hDropSource;
	HTREEITEM				m_hDropTarget;
	bool					m_bOverValidItem;
	bool					m_bShellChangeHandled;
	CString					m_strShellChange;

	ULONG					m_nChangeNotifyID;
	static UINT				s_uwmChangeNotification;

	pidl_ptr				m_pidlControlPanel, m_pidlInternet;

public:
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnPaint();
	afx_msg void OnClick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRclick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemexpanding(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelchanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeleteitem(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBeginDrag(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnEndLabelEdit(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDestroy();
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint pos);
	afx_msg void OnFolderMenu_Expand();
	afx_msg void OnFolderMenu_Open();
	afx_msg void OnFolderMenu_Explore();
	afx_msg void OnFolderMenu_Search();
	afx_msg void OnFolderMenu_NewFolder();
	afx_msg void OnFolderMenu_Delete();
	afx_msg void OnFolderMenu_Rename();
	afx_msg void OnFolderMenu_Properties();
	afx_msg LRESULT OnChangeNotify(WPARAM wParam, LPARAM lParam);
};


