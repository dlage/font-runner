/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// FavoritesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "FavoritesDlg.h"

#include "FavoriteFolders.h"
#include "FontRunnerDoc.h"
#include "WinToolbox.h"

#include <shlwapi.h>
#include <boost/bind.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CFavoritesDlg dialog

IMPLEMENT_DYNAMIC(CFavoritesDlg, CDialog)

CFavoritesDlg::CFavoritesDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFavoritesDlg::IDD, pParent)
{

}

CFavoritesDlg::~CFavoritesDlg()
{
}

BOOL CFavoritesDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// add one column to list control
	m_ctrlFavoritesList.InsertColumn(0, _T("Folder"));

	// setup imagelist
	m_ilFavorites.Create(16, 16, ILC_COLOR32, 0, 100);
	
	// attach it to the list control
	m_ctrlFavoritesList.SetImageList(&m_ilFavorites, LVSIL_SMALL);

	// add favorites to list
	CFavoriteFolders* pFavorites = theApp.GetFavoriteFolders();
	pFavorites->ForEach(boost::bind(&CFavoritesDlg::AddFolderToListCtrl, this, _1));

	return TRUE;
}

void CFavoritesDlg::AddFolderToListCtrl(const std::wstring& str)
{
	if (!::PathIsDirectory(str.c_str()))
		return;

	// get icon from shell
	SHFILEINFO sfi;
	if (::SHGetFileInfo(str.c_str(), 0, &sfi, sizeof(SHFILEINFO), SHGFI_ICON | SHGFI_SMALLICON))
	{
		int nImage = m_ilFavorites.Add(sfi.hIcon);
		::DestroyIcon(sfi.hIcon);

		m_ctrlFavoritesList.InsertItem(0, str.c_str(), nImage);
		m_ctrlFavoritesList.SetColumnWidth(0, LVSCW_AUTOSIZE);
	}
}

void CFavoritesDlg::OnOK()
{
	CDialog::OnOK();
}

void CFavoritesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FAVORITES_LIST, m_ctrlFavoritesList);
	DDX_Control(pDX, IDC_REMOVE_BUTTON, m_btnRemove);
}


BEGIN_MESSAGE_MAP(CFavoritesDlg, CDialog)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_FAVORITES_LIST, &CFavoritesDlg::OnLvnItemchangedFavoritesList)
	ON_BN_CLICKED(IDC_ADDCURRENT_BUTTON, &CFavoritesDlg::OnBnClickedAddcurrentButton)
	ON_BN_CLICKED(IDC_ADDOTHER_BUTTON, &CFavoritesDlg::OnBnClickedAddotherButton)
	ON_BN_CLICKED(IDC_REMOVE_BUTTON, &CFavoritesDlg::OnBnClickedRemoveButton)
END_MESSAGE_MAP()


// CFavoritesDlg message handlers

void CFavoritesDlg::OnLvnItemchangedFavoritesList(NMHDR* /*pNMHDR*/, LRESULT* pResult)
{
	// enable/disable remove button
	int nSelection = m_ctrlFavoritesList.GetNextItem(-1, LVNI_SELECTED);
	m_btnRemove.EnableWindow(nSelection != -1);

	*pResult = 0;
}

void CFavoritesDlg::OnBnClickedAddcurrentButton()
{
	CString strFolder((LPCTSTR)CFontRunnerDoc::GetDoc()->GetCurrentFolderNameFQ());

	CFavoriteFolders* pFavorites =  theApp.GetFavoriteFolders();
	if (!pFavorites->Exists(strFolder))
	{
		pFavorites->AddFolder(strFolder);
		AddFolderToListCtrl((LPCTSTR)strFolder);
	}
}

void CFavoritesDlg::OnBnClickedAddotherButton()
{
	CString strFolder = CWinToolbox::BrowseForFolder(m_hWnd, IDS_FAVORITESDIALOG_ADDSTRING);

	CFavoriteFolders* pFavorites =  theApp.GetFavoriteFolders();
	if (!strFolder.IsEmpty() && !pFavorites->Exists(strFolder))
	{
		pFavorites->AddFolder(strFolder);
		AddFolderToListCtrl((LPCTSTR)strFolder);
	}
}

void CFavoritesDlg::OnBnClickedRemoveButton()
{
	int nSelected = m_ctrlFavoritesList.GetNextItem(-1, LVNI_SELECTED);

	while (nSelected != -1)
	{
		CString strFolder = m_ctrlFavoritesList.GetItemText(nSelected, 0);

		theApp.GetFavoriteFolders()->RemoveFolder(strFolder);
		
		m_ctrlFavoritesList.DeleteItem(nSelected);
		nSelected = m_ctrlFavoritesList.GetNextItem(-1, LVNI_SELECTED);
	}
}
