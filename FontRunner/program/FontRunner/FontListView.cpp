/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// FontListView.cpp : implementation file
//

#include "stdafx.h"

#include "FontRunner.h"
#include "FontRunnerDoc.h"
#include "MainFrm.h"
#include "FontListView.h"

#include "ProgressDlg.h"
#include "FontInfoDlg.h"
#include "WinToolbox.h"

#include "DropSourceHandler.h"
#include "DropTargetHandler.h"

#include "FontRunnerSignals.h"

#include "FindSimilarFrameWnd.h"
#include "FontFileDataSource.h"
#include "pidlutils.h"
#include "HRESULTException.h"
#include "OpenTypeData.h"
#include "ConfirmDeleteDialog.h"
#include "ProgramOptions.h"
#include "Key.h"

#include "FontManager.h"
#include "FontItem.h"

#include "ShellUtils.h"

#include <shlwapi.h>
#include <strsafe.h>
#include <process.h>
#include <algorithm>
#include <boost/shared_array.hpp>

#include "help/help.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// init static vars
HANDLE			CFontListView::s_hStopWatching = NULL;
CFontListView*	CFontListView::s_pwndFontListView = NULL;
bool			CFontListView::s_bThreadRunning = false;
UINT			CFontListView::s_uwmFolderChange = ::RegisterWindowMessage(_T("41A850CF-25CB-4561-B878-52B20EF9673B"));

// WPARAM = pointer to file name
UINT g_rwmGetIconIndex = ::RegisterWindowMessage(_T("7B066ABC-E420-4d46-A9A9-CCDDDBDB41C3"));

// where the fonts are installed
// HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts

/////////////////////////////////////////////////////////////////////////////
// CFontListView

IMPLEMENT_DYNCREATE(CFontListView, CListView)

CFontListView::CFontListView()
 : m_nRClickItem(-1),
   m_bViewingFontProject(false),
   m_bViewingSystemFonts(false),
   m_pGeneralFont(NULL),
   m_nSortColumn(kColumn_FontName),
   m_bSortDescending(false),
   m_dwCOMCTL32Version(0),
   m_pwndStatusBar(NULL),
   m_pWatchThread(NULL),
   m_bAdjustingHeader(false),
   m_sizIcon(16, 16),
   m_nMode(kMode_View),
   m_pDropTarget(NULL),
   m_hImageList(NULL),
   m_pszSimilarTo(NULL),
   m_bSignalingFontChange(false),
   m_bSignalFontChange(true)
{
	// init widths of columns
	for (int nColumn = 0; nColumn < kColumn_NumColumns; ++nColumn)
		m_nOptimalColumnSize[nColumn] = 0;
}

CFontListView::~CFontListView()
{
}

UINT CFontListView::WatchFileSystemThread(LPVOID pParam)
{
	// cast parameter into CString pointer
	CString* pstrFolder = (CString*)pParam;

	HANDLE hEvent[2];

	hEvent[0] = ::FindFirstChangeNotification((LPCTSTR)*pstrFolder, FALSE, FILE_NOTIFY_CHANGE_FILE_NAME);
	hEvent[1] = s_hStopWatching;

	// if change notification handle is invalid, we can't watch
	if (hEvent[0] == INVALID_HANDLE_VALUE)
	{
		s_bThreadRunning = false;
		return 1;
	}

	while (true)
	{
		// wait for notification
		DWORD dwStatus = ::WaitForMultipleObjects(2, hEvent, FALSE, INFINITE);
		
		// see which event is signalled
		if (dwStatus == WAIT_OBJECT_0)
		{
			s_bThreadRunning = false;
			s_pwndFontListView->PostMessage(s_uwmFolderChange); // the folder changed, tell view
			break;
		}
		
		if (dwStatus == WAIT_OBJECT_0 + 1)
			break;

#ifdef _DEBUG
		if (dwStatus == WAIT_FAILED)
		{
			// display any errors (in debug mode only)
			DWORD dwError = GetLastError();

			LPTSTR lpMsgBuf;
			FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | 
						FORMAT_MESSAGE_FROM_SYSTEM | 
						FORMAT_MESSAGE_IGNORE_INSERTS,
						NULL,
						dwError,
						MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
						(LPTSTR)&lpMsgBuf,
						0,
						NULL);
			OutputDebugString(lpMsgBuf);
			LocalFree(lpMsgBuf);
		}
#endif
	}

	return 0;
}

void CFontListView::WatchCurrentFolder()
{
	if (m_nMode != kMode_View)
		return;

	// stop old watcher
	StopWatchingCurrentFolder();
	
	// set static pointer to this class
	s_pwndFontListView = this;

	// begin new filesystem watch thread
	m_pWatchThread = ::AfxBeginThread(WatchFileSystemThread, (LPVOID)&m_strCurrentDirectory, THREAD_PRIORITY_IDLE);
	
	// if thread was created successfully, make sure it deletes itself
	if (m_pWatchThread)
	{
		m_pWatchThread->m_bAutoDelete = TRUE;
		s_bThreadRunning = true;
	}
}

void CFontListView::StopWatchingCurrentFolder()
{
	if (m_nMode != kMode_View)
		return;

	if (s_bThreadRunning)
	{
		// stop thread
		::SetEvent(s_hStopWatching);

		// wait for it to stop
		::WaitForSingleObject(m_pWatchThread->m_hThread, INFINITE);
	}

	// make sure "stop watching" event is not signalled
	::ResetEvent(s_hStopWatching);
}

void CFontListView::PreSubclassWindow()
{
	// enable tool tips
	EnableToolTips();

	CListView::PreSubclassWindow();
}

size_t CFontListView::GetSelectedFileNames(std::vector<LPCTSTR>& files) const
{
	size_t nStringListLength = 0;

	// initialize vector with number of files
	files.resize(GetListCtrl().GetSelectedCount());

	// grab all the filenames
	CFontItem* pFontItem = NULL;
	int nItem = GetListCtrl().GetNextItem(-1, LVNI_SELECTED);
	int n = 0;
	while (nItem != -1)
	{
		pFontItem = reinterpret_cast<CFontItem*>(GetListCtrl().GetItemData(nItem));
		
		const CString& strFileName = pFontItem->GetFullFileName();
		files[n++] = (LPCTSTR)strFileName;
		nStringListLength += strFileName.GetLength() + 1;

		nItem = GetListCtrl().GetNextItem(nItem, LVNI_SELECTED);
	}

	// one more for the extra NULL
	nStringListLength += 1;

	// multiply by size of character
	nStringListLength *= sizeof(TCHAR);

	return nStringListLength;
}

void CFontListView::FireSelectedFontChange(const CFontItem* pItem)
{
	if (m_bSignalFontChange)
	{
		m_bSignalingFontChange = true;
		theApp.GetSignals()->Fire_SelectedFontChange(pItem);
		m_bSignalingFontChange = false;
	}
}

// creates a data object from selected fonts
CFontFileDataSource* CFontListView::CreateDataSourceForDropFiles() const
{
	// get list of strings
	std::vector<LPCTSTR> filenames;
	size_t nStringListSize = GetSelectedFileNames(filenames);

	// create data source and get data object
	CFontFileDataSource* pDataSource = new CFontFileDataSource();
	IDataObject* pDataObject = dynamic_cast<IDataObject*>(pDataSource->GetInterface(&IID_IDataObject));

	// put pidls into data object first
	if (!ShellUtils::SetDataObjectWithPIDLs(pDataObject, filenames))
	{
		pDataObject->Release();
		delete pDataSource;
		return NULL;
	}
	
	// put file name strings into data object
	if (!ShellUtils::SetDataObjectWithFileNames(pDataObject, filenames, nStringListSize))
	{
		pDataObject->Release();
		delete pDataSource;
		return NULL;
	}

	return pDataSource;
}

CFontFileDataSource* CFontListView::CreateDataSourceForProjectData() const
{
	// get list of strings
	std::vector<LPCTSTR> filenames;
	size_t nStringListSize = GetSelectedFileNames(filenames);

	// create data source and get data object
	CFontFileDataSource* pDataSource = new CFontFileDataSource();
	IDataObject* pDataObject = dynamic_cast<IDataObject*>(pDataSource->GetInterface(&IID_IDataObject));
	
	CLIPFORMAT cf = (CLIPFORMAT)::RegisterClipboardFormat(_T("Font Runner Font Project Data"));

	// put file name strings into data object
	if (!ShellUtils::SetDataObjectWithFileNames(pDataObject, filenames, nStringListSize, cf))
	{
		pDataObject->Release();
		delete pDataSource;
		return NULL;
	}

	return pDataSource;
}

void CFontListView::CreateDragBitmap(const POINT& pt, SHDRAGIMAGE* pshdi)
{
	// need total size of client area
	CRect rcClient;
	GetClientRect(&rcClient);

	// size of drag bitmap is minimum rectange to encompass all visible selected items
	CRect rcItem, rcImage(rcClient.left, rcClient.bottom, rcClient.right, rcClient.top);
	const int nTotalVisible = GetListCtrl().GetCountPerPage();
	const int nTopIndex = GetListCtrl().GetTopIndex();
	for (int nItem = nTopIndex; nItem < nTopIndex + nTotalVisible; ++nItem)
	{
		if (GetListCtrl().GetItemState(nItem, LVIS_SELECTED))
		{
			GetListCtrl().GetItemRect(nItem, &rcItem, LVIR_BOUNDS);
			rcImage.top = std::min<LONG>(rcImage.top, rcItem.top);
			rcImage.bottom = std::max<LONG>(rcImage.bottom, rcItem.bottom);
		}
	}

	// get DC
	CDC* pDC = GetDC();
	
	// create a compatible DC to draw into
	CDC dcMemory;
	dcMemory.CreateCompatibleDC(pDC);

	// create a bitmap
	CBitmap bmpImage;
	bmpImage.CreateCompatibleBitmap(pDC, rcImage.Width(), rcImage.Height());

	// select it into our memory DC
	CBitmap* pOldBitmap = dcMemory.SelectObject(&bmpImage);

	// paint background
	COLORREF clrMask = RGB(0xFF, 0x00, 0xFF);
	dcMemory.FillSolidRect(&rcClient, clrMask);

	// Create and init a DRAWITEMSTRUCT.  The rest will be filled in later.
	DRAWITEMSTRUCT drawitem_struct;
	drawitem_struct.CtlType = ODT_LISTVIEW;
	drawitem_struct.CtlID = 0;
	drawitem_struct.hDC = dcMemory.m_hDC;
	drawitem_struct.hwndItem = GetListCtrl().GetSafeHwnd();
	drawitem_struct.itemAction = ODA_DRAWENTIRE;
	drawitem_struct.itemData = 0;
	drawitem_struct.itemState = ODS_SELECTED;

	// copy selected visible items into our image
	for (int nItem = nTopIndex; nItem < nTopIndex + nTotalVisible; ++nItem)
	{
		if (GetListCtrl().GetItemState(nItem, LVIS_SELECTED))
		{
			// get bounding rect of selected item
			GetListCtrl().GetItemRect(nItem, &rcItem, LVIR_BOUNDS);

			// fit it into our drag image rect
			rcItem.OffsetRect(0, -rcImage.top);

			// set ID and bounding rect for item we want drawn
			drawitem_struct.itemID = nItem;
			drawitem_struct.rcItem = rcItem;

			// call the same function we use to owner-draw the item
			DrawItem(&drawitem_struct);
		}
	}

	// have to deselect the image
	dcMemory.SelectObject(pOldBitmap);

	// done, release DC
	ReleaseDC(pDC);

	CPoint ptDrag(pt);
	ptDrag.Offset(-rcImage.left, -rcImage.top);

	// fill out drag image struct
	pshdi->sizeDragImage.cx = rcImage.Width();
	pshdi->sizeDragImage.cy = rcImage.Height();
	pshdi->ptOffset = ptDrag;
	pshdi->hbmpDragImage = (HBITMAP)bmpImage.Detach();
	pshdi->crColorKey = clrMask;
}

void CFontListView::DrawItem(LPDRAWITEMSTRUCT pDrawItemStruct)
{
	// attach a CDC to the device context handle
	CDC* pDC = new CDC();
	pDC->Attach(pDrawItemStruct->hDC);
	int nSavedDC = pDC->SaveDC();

	// make some decisions based on selected state
	COLORREF clrBackground = RGB(0xFF, 0xFF, 0xFF), clrText = RGB(0x00, 0x00, 0x00);
	if (pDrawItemStruct->itemState & ODS_SELECTED)
	{
		clrText = ::GetSysColor(COLOR_HIGHLIGHTTEXT);
		clrBackground = ::GetSysColor(COLOR_HIGHLIGHT);
	}
	else
	{
		clrText = ::GetSysColor(COLOR_WINDOWTEXT);
		clrBackground = ::GetSysColor(COLOR_WINDOW);
	}

	// paint entire area with background color
	pDC->FillSolidRect(&pDrawItemStruct->rcItem, clrBackground);

	// get item
	LVITEM lvi;
	::memset(&lvi, 0, sizeof(LVITEM));
	lvi.iItem = pDrawItemStruct->itemID;
	lvi.mask = LVIF_IMAGE | LVIF_PARAM;
	GetListCtrl().GetItem(&lvi);
	CFontItem* pFontItem = reinterpret_cast<CFontItem*>(lvi.lParam);

	if (!pFontItem)
	{
		// cleanup CDC object
		pDC->RestoreDC(nSavedDC);
		pDC->Detach();
		delete pDC;
		return;
	}

	//////////////////////////////
	// in find-character mode, draw the character
	CRect rcChar(0, 0, 0, 0);
	if (m_nMode == kMode_FindCharacter)
	{
		CPoint ptTopLeft(pDrawItemStruct->rcItem.left + m_im.nMargin,
						 pDrawItemStruct->rcItem.top + m_im.nMargin);
		rcChar.SetRect(ptTopLeft,
					   CPoint((ptTopLeft.x + m_im.nInfoLineHeight + m_im.nPreviewLineHeight) - (m_im.nMargin + m_im.nPadding),
							  (ptTopLeft.y + m_im.nInfoLineHeight + m_im.nPreviewLineHeight) - (m_im.nMargin + m_im.nPadding)));
		pDC->FillSolidRect(&rcChar, ::GetSysColor(COLOR_WINDOW));

		// make a solid black pen
		CPen penSolid;
		penSolid.CreatePen(PS_SOLID, 1, RGB(0, 0, 0));
		pDC->SelectObject(&penSolid);
		pDC->Rectangle(&rcChar);

		// create a font for the character
		BYTE nQuality = 0;
		switch (theApp.GetProgramOptions()->GetCharDetailRenderingOption())
		{
			case CProgramOptions::kRenderingOption_UseDefault:
				nQuality = DEFAULT_QUALITY;
				break;
			case CProgramOptions::kRenderingOption_Antialiasing:
				nQuality = ANTIALIASED_QUALITY;
				break;
			case CProgramOptions::kRenderingOptions_ClearType:
				nQuality = CLEARTYPE_QUALITY;
				break;
		}

		using CruxTechnologies::OpenTypeFontData::CNameRecord;
		const std::wstring* pstrName = pFontItem->GetOpenTypeData()->GetName(CNameRecord::kNameID_FullFontName);

		CFont fntChar;
		fntChar.CreateFont(rcChar.Height(), 0, 0, 0, FW_NORMAL,
			FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS,
			CLIP_DEFAULT_PRECIS, nQuality, DEFAULT_PITCH | FF_DONTCARE,
			pstrName->c_str());

		pDC->SetTextColor(::GetSysColor(COLOR_WINDOWTEXT));
		pDC->SetBkColor(::GetSysColor(COLOR_WINDOW));

		CFont* pOldFont = pDC->SelectObject(&fntChar);
		CString str;
		str.Format(_T("%c"), m_chLookFor);
		pDC->DrawText(str, rcChar, DT_CENTER | DT_NOPREFIX | DT_SINGLELINE | DT_VCENTER);
		pDC->SelectObject(pOldFont);
	}

	// set colors
	pDC->SetTextColor(clrText);
	pDC->SetBkColor(clrBackground);

	//////////////////////////////
	// draw font information line

	// get header control
	CHeaderCtrl* pHeader = GetListCtrl().GetHeaderCtrl();

	// Font Name Column
	HDITEM hditem;
	hditem.mask = HDI_WIDTH;
	pHeader->GetItem(kColumn_FontName, &hditem);

	CPoint pos(pDrawItemStruct->rcItem.left + m_im.nMargin,
			   pDrawItemStruct->rcItem.top + m_im.nMargin + m_im.nIconPos);

	// move line over because of character
	if (m_nMode == kMode_FindCharacter)
		pos.x += rcChar.Width() + m_im.nPadding;

	bool bFontOk = (pFontItem->GetStatus() == CruxTechnologies::COpenTypeData::kStatus_OK);

	// we want to draw the icon first
	HICON hIcon = NULL;
	if (bFontOk)
	{
		if (m_hImageList)
			hIcon = ImageList_ExtractIcon(NULL, m_hImageList, lvi.iImage);
	}
	else
		hIcon = theApp.LoadIcon(IDI_FONTERROR_ICON);

	if (hIcon)
	{
		::DrawIconEx(pDrawItemStruct->hDC,
					 pos.x, pos.y,
					 hIcon,
					 hditem.cxy >= m_sizIcon.cx ? m_sizIcon.cx : hditem.cxy,
					 m_sizIcon.cy, 0, NULL, DI_NORMAL);
		::DestroyIcon(hIcon);
	}

	pos.x += m_sizIcon.cx + m_im.nPadding;
	pos.y = m_im.nMargin + m_im.nTextPos;

	// set regular font
	pDC->SelectObject(theApp.GetDialogFont());

	// get metrics of normal font
	TEXTMETRIC tm;
	pDC->GetTextMetrics(&tm);

	// preset DrawText format
	const UINT cnFormat = DT_LEFT | DT_WORD_ELLIPSIS | DT_VCENTER | DT_SINGLELINE | DT_NOPREFIX;

	// font name
	CRect rectText(pos.x, pos.y, hditem.cxy, pos.y + m_im.nNormalFontHeight);
	rectText.OffsetRect(pDrawItemStruct->rcItem.left, pDrawItemStruct->rcItem.top);
	CString strText = GetListCtrl().GetItemText(pDrawItemStruct->itemID, 0);
	pDC->DrawText(strText, &rectText, cnFormat);

	// file name
	rectText.left = pDrawItemStruct->rcItem.left + hditem.cxy;
	pHeader->GetItem(kColumn_FileName, &hditem);
	rectText.right = rectText.left + hditem.cxy;
	strText = GetListCtrl().GetItemText(pDrawItemStruct->itemID, 1);
	pDC->DrawText(strText, &rectText, cnFormat);

	// file size
	rectText.left += hditem.cxy;
	pHeader->GetItem(kColumn_Size, &hditem);
	rectText.right = rectText.left + hditem.cxy;
	strText = GetListCtrl().GetItemText(pDrawItemStruct->itemID, 2);
	pDC->DrawText(strText, &rectText, cnFormat);

	//////////////////////////////
	// draw the font preview line

	if (bFontOk)
	{
		// set font for preview
		pDC->SelectObject(pFontItem->GetDisplayFont());

		CRect rcText(pDrawItemStruct->rcItem);
		if (m_nMode == kMode_FindCharacter)
			rcText.left += m_im.nMargin + m_im.nPadding + rcChar.Width();
		else
			rcText.left += m_im.nMargin;
		rcText.top += m_im.nInfoLineHeight + m_im.nPadding;

		pDC->DrawText(theApp.GetProgramOptions()->GetFontListText(), &rcText, cnFormat);
	}

	//////////////////////////////
	// draw solid gray line along bottom
	CPen pen(PS_SOLID, 1, RGB(0x80, 0x80, 0x80));
	pDC->SelectObject(&pen);
	pDC->MoveTo(pDrawItemStruct->rcItem.left, pDrawItemStruct->rcItem.bottom - 1);
	pDC->LineTo(pDrawItemStruct->rcItem.right, pDrawItemStruct->rcItem.bottom - 1);

	// cleanup CDC object
	pDC->RestoreDC(nSavedDC);
	pDC->Detach();
	delete pDC;
}

void CFontListView::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	ComputeItemMetrics();
	lpMeasureItemStruct->itemHeight = m_im.nInfoLineHeight + m_im.nPreviewLineHeight;
}

void CFontListView::ComputeItemMetrics()
{
	CDC* pDC = GetListCtrl().GetDC();
	int nSavedDC = pDC->SaveDC();
	pDC->SelectObject(theApp.GetDialogFont());

	// get metrics of normal font
	TEXTMETRIC tm;
	pDC->GetTextMetrics(&tm);

	m_im.nMargin = 2;
	m_im.nPadding = 2;
	m_im.nNormalFontHeight = tm.tmHeight;
	m_im.nInfoLineHeight = std::max<int>(m_sizIcon.cx, tm.tmHeight) +
								m_im.nMargin + m_im.nPadding;
	m_im.nPreviewLineHeight = theApp.GetProgramOptions()->GetFontListCharSize() +
								m_im.nMargin + m_im.nPadding;

	if (m_im.nNormalFontHeight > m_sizIcon.cy)
	{
		m_im.nIconPos = (int)((m_im.nNormalFontHeight - m_sizIcon.cy) / 2);
		m_im.nTextPos = 0;
	}
	else
	{
		m_im.nIconPos = 0;
		m_im.nTextPos = (int)((m_sizIcon.cy - m_im.nNormalFontHeight) / 2);
	}

	// clean up
	pDC->RestoreDC(nSavedDC);
	GetListCtrl().ReleaseDC(pDC);
}

struct add_filename_to_buffer
{
	add_filename_to_buffer(TCHAR* pBuffer, size_t nSize)
		: m_pBuffer(pBuffer), m_nSize(nSize)
	{}

	void operator()(const CString& strFileName)
	{
		int nLength = strFileName.GetLength() + 1;
		::StringCchCopy(m_pBuffer, m_nSize, strFileName);
		m_pBuffer += nLength;
		m_nSize -= nLength;
	}

private:
	TCHAR* m_pBuffer;
	size_t m_nSize;
};

void CFontListView::DeleteSelectedFiles()
{
	if (m_bViewingSystemFonts)
		return;

	CListCtrl& listctrl = GetListCtrl();

	// see how many selected
	UINT nSelected = listctrl.GetSelectedCount();

	// do nothing if no selection
	if (nSelected == 0)
		return;

	// if user cancels, just return
	CConfirmDeleteDialog dlgConfirm(nSelected > 1, this);
	if (dlgConfirm.DoModal() != IDOK)
		return;

	// no selection, and make sure the rest of the app knows it
	FireSelectedFontChange(NULL);

	try
	{
		CheckResultFunctor CheckResult;

		// need shell folder interface for this folder
		CComPtr<IShellFolder> pDesktopFolder, pFontsFolder;
		CheckResult(SHGetDesktopFolder(&pDesktopFolder));

		// grab pointer to font manager
		CFontManager* pManager = theApp.GetFontManager();

		// at this point, we should clean up the selected items on the list
		int nItem = listctrl.GetNextItem(-1, LVNI_SELECTED);
		int i = 0;
		std::vector<CString> filelist(nSelected);
		std::vector<CString> regnames(nSelected);
		size_t filelist_size = 0;
		while (nItem != -1)
		{
			// mark item for deletion
			CFontItem* pItem = reinterpret_cast<CFontItem*>(listctrl.GetItemData(nItem));
			pItem->MarkForDeletion();

			filelist[i] = pItem->GetFullFileName();
			regnames[i] = GetListCtrl().GetItemText(nItem, 0) + _T(" (TrueType)");
			filelist_size += pItem->GetFullFileName().GetLength() + 1;
			++i;

			// Mark font as private.  The OS will release it faster that way.
			pItem->SetTempAutoInstall(false);

			// delete list item, this will remove our own reference too
			listctrl.DeleteItem(nItem);

			// unmanage the font, if it fails, it is an error
			if (!pManager->UnManage(pItem))
				return;

			// get next selected item
			nItem = listctrl.GetNextItem(-1, LVNI_SELECTED);
		}

		// post font change message
		::PostMessage(HWND_BROADCAST, WM_FONTCHANGE, 0, 0);

		// one more for extra NULL
		++filelist_size;

		// fill buffer
		boost::scoped_ptr<TCHAR> pBuffer(new TCHAR[filelist_size]);
		memset(pBuffer.get(), 0, filelist_size * sizeof(TCHAR));
		std::for_each(filelist.begin(), filelist.end(), add_filename_to_buffer(pBuffer.get(), filelist_size));

		SHFILEOPSTRUCT fop;
		memset(&fop, 0, sizeof(SHFILEOPSTRUCT));
		fop.hwnd = GetSafeHwnd();
		fop.wFunc = FO_DELETE;
		fop.pFrom = pBuffer.get();
		fop.fFlags = FOF_NOCONFIRMATION | FOF_ALLOWUNDO; // we already asked
		::SHFileOperation(&fop);
	}
	catch (CHRESULTException& e)
	{
#ifdef _DEBUG
		CString strDebug;
		strDebug.Format(_T("HRESULT is 0x%08x."), e.GetHRESULT());
		MessageBox(strDebug, _T("Failure."), MB_OK + MB_ICONERROR);
#else
		e;
#endif
	}
	catch (std::bad_cast&)
	{
	}
}

void CFontListView::RemoveSelectedFilesFromProject()
{
	int nSelected = GetListCtrl().GetNextItem(-1, LVNI_SELECTED);

	if (nSelected != -1)
	{
		CString strMessage, strTitle;
		strMessage.LoadString(IDS_FONTLIST_PROJECTFONTDELETE);
		strTitle.LoadString(IDS_FONTLIST_PROJECTFONTDELETE_TITLE);

		int nResult = ::MessageBox(m_hWnd, strMessage, strTitle, MB_ICONQUESTION + MB_YESNO);

		if (nResult == IDNO)
			return;

		// no selection, and make sure the rest of the app knows it
		FireSelectedFontChange(NULL);
	}

	while (nSelected != -1)
	{
		// send removed signal
		CFontItem* pItem = reinterpret_cast<CFontItem*>(GetListCtrl().GetItemData(nSelected));
		theApp.GetSignals()->Fire_RemovedFromProject(pItem);

		// delete the item (this will remove the font reference)
		GetListCtrl().DeleteItem(nSelected);

		// get next selected item
		nSelected = GetListCtrl().GetNextItem(-1, LVNI_SELECTED);
	}
}

void CFontListView::SelectAll()
{
	// set the state of every item to selected
	CListCtrl& listctrl = GetListCtrl();
	const int cnCount = listctrl.GetItemCount();
	for (int nItem = 0; nItem < cnCount; ++nItem)
		listctrl.SetItemState(nItem, LVIS_SELECTED, LVIS_SELECTED);
}

void CFontListView::SetMode(eMode nMode)
{
	m_nMode = nMode;

	if (m_nMode != kMode_View)
		::RevokeDragDrop(GetSafeHwnd());
}

CFontListView::eMode CFontListView::GetMode() const
{
	return m_nMode;
}

void CFontListView::SetLookForCharacter(wchar_t ch)
{
	m_chLookFor = ch;
}

wchar_t CFontListView::GetLookForCharacter() const
{
	return m_chLookFor;
}

void CFontListView::MarkSelectedPrivate()
{
	int nItem = GetListCtrl().GetNextItem(-1, LVNI_SELECTED);
	while (nItem != -1)
	{
		CFontItem* pItem = reinterpret_cast<CFontItem*>(GetListCtrl().GetItemData(nItem));

		pItem->TemporarilyMarkPrivate();
		
		// get next selected item
		nItem = GetListCtrl().GetNextItem(nItem, LVNI_SELECTED);
	}
}

void CFontListView::RestoreSelectedCharacteristics()
{
	CListCtrl& listctrl = GetListCtrl();

	int nItem = listctrl.GetNextItem(-1, LVNI_SELECTED);
	while (nItem != -1)
	{
		CFontItem* pItem = reinterpret_cast<CFontItem*>(listctrl.GetItemData(nItem));

		ASSERT(pItem);
		if (pItem)
			pItem->RestorePreviousCharacteristics();
		
		// get next selected item
		nItem = listctrl.GetNextItem(nItem, LVNI_SELECTED);
	}
}


BEGIN_MESSAGE_MAP(CFontListView, CListView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, OnFilePrintPreview)
	
	ON_COMMAND(ID_FONTLIST_INSTALL, OnFontlistInstall)
	ON_COMMAND(ID_FONTLIST_OPEN, OnFontlistOpen)
	ON_COMMAND(ID_FONTLIST_EXPLORE, OnFontlistExplore)
	ON_COMMAND(ID_FONTLIST_FINDSIMILAR, OnFontlistFindSimilar)
	ON_COMMAND(ID_FONTLIST_FONTINFO, OnFontlistFontInfo)
	ON_COMMAND(ID_FONTLIST_PROPERTIES, OnFontlistProperties)
	ON_COMMAND(ID_FOLDERMENU_DELETE, OnFolderMenu_Delete)
	ON_UPDATE_COMMAND_UI(ID_FOLDERMENU_DELETE, OnUpdateFolderMenu_Delete)
	ON_COMMAND(ID_FONTLISTMENU_SHOWCONTAINING, OnFontListMenu_ShowContaining)
	ON_COMMAND(ID_BADFONTMENU_WHATSWRONG, OnBadFontMenu_WhatsWrong)
	ON_COMMAND(ID_BADFONTMENU_DELETE, OnBadFontMenu_Delete)
	ON_UPDATE_COMMAND_UI(ID_BADFONTMENU_DELETE, OnUpdateBadFontMenu_Delete)

	ON_WM_DESTROY()
	ON_WM_MOUSEMOVE()
	ON_WM_DRAWITEM()
	ON_WM_MEASUREITEM_REFLECT()
	ON_WM_SIZE()

	ON_NOTIFY_REFLECT(LVN_BEGINDRAG, OnBeginDrag)
	ON_NOTIFY_REFLECT(LVN_BEGINRDRAG, OnBeginRDrag)
	ON_NOTIFY_REFLECT(LVN_ITEMCHANGED, OnItemchanged)
	ON_NOTIFY_REFLECT(LVN_DELETEALLITEMS, OnDeleteAllItems)
	ON_NOTIFY_REFLECT(LVN_DELETEITEM, OnDeleteItem)
	ON_NOTIFY_REFLECT(NM_RCLICK, OnRclick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, OnDblclk)
	ON_NOTIFY(HDN_ITEMCLICKA, 0, OnHdnItemclick)
	ON_NOTIFY(HDN_ITEMCLICKW, 0, OnHdnItemclick)
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTW, 0, 0xFFFF, OnTTNNeedText)
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTA, 0, 0xFFFF, OnTTNNeedText)
	ON_NOTIFY(HDN_DIVIDERDBLCLICKA, 0, OnHdnDividerdblclick)
	ON_NOTIFY(HDN_DIVIDERDBLCLICKW, 0, OnHdnDividerdblclick)
	ON_NOTIFY(HDN_ITEMCHANGED, 0, OnHdnItemChanged)
	ON_REGISTERED_MESSAGE(CFontListView::s_uwmFolderChange, OnFolderChange)
	ON_REGISTERED_MESSAGE(g_rwmGetIconIndex, OnGetIconIndex)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFontListView drawing

void CFontListView::OnDraw(CDC* /*pDC*/)
{
	//CDocument* pDoc = GetDocument();
}

/////////////////////////////////////////////////////////////////////////////
// CFontListView printing

void CFontListView::SetupForPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	// get physical width and height of the page
	m_nPageWidth = pDC->GetDeviceCaps(PHYSICALWIDTH);
	m_nPageHeight = pDC->GetDeviceCaps(PHYSICALHEIGHT);

	// get pixels-per-inch
	m_nPPIX = pDC->GetDeviceCaps(LOGPIXELSX);
	m_nPPIY = pDC->GetDeviceCaps(LOGPIXELSY);

	// compute usable area
	m_rectUsableArea.left = m_nPPIX / 2;					// 1/2 inch from left
	m_rectUsableArea.right = m_nPageWidth - (m_nPPIX / 2);	// 1/2 inch from right
	m_rectUsableArea.top = m_nPPIY / 2;						// 1/2 inch from top
	m_rectUsableArea.bottom = m_nPageHeight - (m_nPPIY / 2);// 1/2 inch from bottom

	// determine real height of font
	m_nRealFontHeight = (int)(((float)(theApp.GetProgramOptions()->GetPrintCharSize() * 20) / 1440) * m_nPPIY);

	// see how high each font item should be
	CFont* pOldFont = pDC->SelectObject(m_pGeneralFont);
	TEXTMETRIC tm;
	pDC->GetTextMetrics(&tm);
	m_nFontItemHeight = tm.tmHeight + m_nRealFontHeight * 2;
	pDC->SelectObject(pOldFont);

	// compute number of fonts we can fit on page
	m_nFontsPerPage = (m_rectUsableArea.bottom - m_rectUsableArea.top) / m_nFontItemHeight;

	// compute number of pages printing will require
	int nNumItems = GetListCtrl().GetItemCount();

	if (nNumItems != 0)	// prevent division by zero
	{
		int nNumPages = (nNumItems / m_nFontsPerPage);

		// if there is a remainder from the above division, add a page
		if (nNumItems % m_nFontsPerPage)
			nNumPages++;

		pInfo->SetMaxPage(nNumPages);
	}
	else
		pInfo->SetMaxPage(1);
}

BOOL CFontListView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// we're going to create a printer DC from the default settings
	// so we can set the number of pages
	CFontRunnerApp* pApp = (CFontRunnerApp*)AfxGetApp();
	
	// get default printer
	if (!pApp->GetPrinterDeviceDefaults(&pInfo->m_pPD->m_pd))
		return DoPreparePrinting(pInfo);

	// try and create device context for default printer
	HDC hdc = NULL;
	if (pInfo->m_pPD->m_pd.hDC == NULL)
	{
		// call CreatePrinterDC if DC was not created by above
		if ((hdc = pInfo->m_pPD->CreatePrinterDC()) == NULL)
			return DoPreparePrinting(pInfo);
	}

	// create a device context class to pass to our
	// method for setting up all the print defaults
	CDC dc;
	dc.Attach(hdc);

	// change map mode to TWIPS
	dc.SetMapMode(MM_TWIPS);

	// setup for printing
	SetupForPrinting(&dc, pInfo);

	// detach from device context handle
	dc.Detach();

	// default preparation
	return DoPreparePrinting(pInfo);
}

void CFontListView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	// create general-purpose font
	m_pGeneralFont = new CFont();

	m_pGeneralFont->CreateFont((int)(0.1389f * m_nPPIY), 0, 0, 0,
							   FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET,
							   OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
							   DEFAULT_QUALITY, DEFAULT_PITCH | FF_MODERN,
							   _T("Arial")); // 10pt arial

	// make sure settings are correct
	SetupForPrinting(pDC, pInfo);
}	

void CFontListView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo) 
{
	// change mapping mode to TWIPS
	pDC->SetMapMode(MM_TEXT);

	CListView::OnPrepareDC(pDC, pInfo);
}

void CFontListView::OnPrint(CDC* pDC, CPrintInfo* pInfo) 
{
	// move position pointer to current font
	// formula for figuring the index of the font on top of this page is f(x,y) = yx - y
	// Where : x is the current page number
	//		   y is the number of fonts printed on a single page	
	int nPosCount = ((m_nFontsPerPage * pInfo->m_nCurPage) - m_nFontsPerPage);

	// nLastItem is the index of the last item on the font list
	int nLastItem = (GetListCtrl().GetItemCount() - 1);

	// nFontOnPage the current check being printed on this page
	int nFontOnPage = 0;

	// flag is set by the font position counter being higher than last element on list
	BOOL bDone = (nPosCount > nLastItem);
	while (!bDone)
	{
		// item data stored in list has all the info we need to print this font
		CFontItem* pFontItem = 
			reinterpret_cast<CFontItem*>(GetListCtrl().GetItemData(nPosCount));

		// call PrintFont to print this font
		PrintFont(pDC, nFontOnPage, pFontItem);

		// increment counter to monitor number of fonts printed on this page
		nFontOnPage++;

		// increment position counter
		nPosCount++;

		// if we've printed as many fonts as we can fit on this page, we're done
		if (nFontOnPage == m_nFontsPerPage)
			bDone = TRUE;

		// if we've gone past the last item on the list control, we're also done
		if (nPosCount > nLastItem)
			bDone = TRUE;
	}

	CListView::OnPrint(pDC, pInfo);
}

void CFontListView::OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView)
{
	CView::OnEndPrintPreview(pDC, pInfo, point, pView);

	theApp.InPrintPreview(false);
}

void CFontListView::PrintFont(CDC* pDC, int nPageItem, CFontItem* pFontListItemData)
{
	// draws a single font on the page

	// how far dowm the page we need to print
	int nStartX = m_rectUsableArea.left;
	int nStartY = m_rectUsableArea.top + (m_nFontItemHeight * nPageItem);

	// save original font while selecting our general purpose font
	CFont* pOldFont = pDC->SelectObject(m_pGeneralFont);

	CString strFontNameLine;
	strFontNameLine.Format(_T("%s (%s)"),
		pFontListItemData->GetOpenTypeData()->GetName(CruxTechnologies::OpenTypeFontData::CNameRecord::kNameID_FullFontName)->c_str(),
		pFontListItemData->GetFileName() + _T(")"));
	pDC->TextOut(nStartX, nStartY, strFontNameLine);

	// save size of the first line
	CSize sizeLine = pDC->GetTextExtent(strFontNameLine);

	// set position for the first line of sample text
	int nSampleLine1Y = nStartY + sizeLine.cy;

	// create current font
	CFont fontCurrent;
	BOOL bSuccess = 
	fontCurrent.CreateFont(m_nRealFontHeight, 0, 0, 0,
						   FW_NORMAL, FALSE, FALSE, 0, DEFAULT_CHARSET,
						   OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
						   DEFAULT_QUALITY, DEFAULT_PITCH | FF_MODERN,
						   pFontListItemData->GetOpenTypeData()->GetName(CruxTechnologies::OpenTypeFontData::CNameRecord::kNameID_FullFontName)->c_str());

	if (bSuccess)
	{
		// switch to current font
		pDC->SelectObject(&fontCurrent);

		// set position for second line of sample text
		CProgramOptions* pOptions = theApp.GetProgramOptions();
		sizeLine = pDC->GetTextExtent(pOptions->GetPrintSampleTextLine1());
		int nSampleLine2Y = nSampleLine1Y + sizeLine.cy;

		// output sample lines
		pDC->TextOut(nStartX, nSampleLine1Y, pOptions->GetPrintSampleTextLine1());
		pDC->TextOut(nStartX, nSampleLine2Y, pOptions->GetPrintSampleTextLine2());

		// release this font object
		fontCurrent.DeleteObject();

		// restore original font
		pDC->SelectObject(pOldFont);
	}
}

void CFontListView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// dispose of general purpose font
	if (m_pGeneralFont)
	{
		m_pGeneralFont->DeleteObject();
		delete m_pGeneralFont;
		m_pGeneralFont = NULL;
	}
}

// font list sort callback function
int CALLBACK CFontListView::FontListSortProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	int nReturn = 0;

	CFontItem* pFontItem1 = (CFontItem*)lParam1;
	CFontItem* pFontItem2 = (CFontItem*)lParam2;

	CFontListView* pView = reinterpret_cast<CFontListView*>(lParamSort);

	if (pView->GetMode() == kMode_FindSimilar)
	{
		switch (pView->GetSortedColumn())
		{
			case 0: // font name
				nReturn = pFontItem1->CompareFontNames(pFontItem2);
				break;
			case 1: // similarity
				nReturn =
					(pFontItem1->GetDistance(pView->GetSimilarTo()) <= pFontItem2->GetDistance(pView->GetSimilarTo())) ? 1 : -1;
				break;
		}
	}
	else
	{
		switch (pView->GetSortedColumn())
		{
			case 0: // font name
				nReturn = pFontItem1->CompareFontNames(pFontItem2);
				break;
			case 1: // file name
				nReturn = pFontItem1->CompareFileNames(pFontItem2);
				break;
			case 2: // size
				nReturn = pFontItem1->CompareFileSizes(pFontItem2);
				break;
		}
	}

	// if not ascending, negate return value
	if (pView->IsSortDescending())
		nReturn *= -1;

	return nReturn;
}

/////////////////////////////////////////////////////////////////////////////
// CFontListView diagnostics

#ifdef _DEBUG
void CFontListView::AssertValid() const
{
	CListView::AssertValid();
}

void CFontListView::Dump(CDumpContext& dc) const
{
	CListView::Dump(dc);
}
#endif //_DEBUG

void CFontListView::SaveOptions()
{
	CProgramOptions* pOptions = theApp.GetProgramOptions();

	pOptions->SetFontListSortColumn(m_nSortColumn);
	pOptions->SetFontNameColWidth(GetListCtrl().GetColumnWidth(kColumn_FontName));
	pOptions->SetFileNameColWidth(GetListCtrl().GetColumnWidth(kColumn_FileName));
	pOptions->SetSizeColWidth(GetListCtrl().GetColumnWidth(kColumn_Size));
}

void CFontListView::OnInitialUpdate() 
{
	CListView::OnInitialUpdate();

	// set default font
	CFont* pDefaultFont = theApp.GetDialogFont();
	GetListCtrl().SetFont(pDefaultFont);

	// get program options
	CProgramOptions* pOptions = theApp.GetProgramOptions();

	// set list view styles
	ModifyStyle(0, LVS_REPORT | LVS_SHOWSELALWAYS | LVS_SHAREIMAGELISTS | LVS_OWNERDRAWFIXED);
	GetListCtrl().SetExtendedStyle(LVS_EX_FULLROWSELECT);

	// get common controls version
	m_dwCOMCTL32Version = CWinToolbox::GetDllVersion(_T("comctl32.dll"));

	// Add columns...
	CString strText;

	// a DC we'll need to measure items
	CDC* pDC = GetDC();
	CFont* pOldFont = pDC->SelectObject(pDefaultFont);
	const int cnExtraColumnWidth = 48;

	// add first column
	strText.LoadString(IDS_FONTLIST_FONTNAME);
	m_nMinHeaderSize[kColumn_FontName] = pDC->GetTextExtent(strText).cx + cnExtraColumnWidth;
	GetListCtrl().InsertColumn(kColumn_FontName, strText, LVCFMT_LEFT, pOptions->GetFontNameColWidth(), kColumn_FontName);
	
	// add second column
	if (m_nMode == kMode_FindSimilar)
		strText.LoadString(IDS_FONTLIST_SIMILARITY);
	else
		strText.LoadString(IDS_FONTLIST_FILENAME);
	m_nMinHeaderSize[kColumn_FileName] = pDC->GetTextExtent(strText).cx + cnExtraColumnWidth;
	GetListCtrl().InsertColumn(kColumn_FileName, strText, LVCFMT_LEFT, pOptions->GetFileNameColWidth(), kColumn_FileName);
	
	// add third column
	if (m_nMode != kMode_FindSimilar)
	{
		strText.LoadString(IDS_FONTLIST_FILESIZE);
		m_nMinHeaderSize[kColumn_Size] = pDC->GetTextExtent(strText).cx + cnExtraColumnWidth;
		GetListCtrl().InsertColumn(kColumn_Size, strText, LVCFMT_LEFT, pOptions->GetSizeColWidth(), kColumn_Size);
	}

	// don't need DC anymore, clean up
	pDC->SelectObject(pOldFont);
	ReleaseDC(pDC);

	if (m_nMode == kMode_View)
	{
		m_nSortColumn = pOptions->GetFontListSortColumn();
		m_bSortDescending = pOptions->IsFontListSortDescending();
	}
	else if (m_nMode == kMode_FindSimilar) // sort (ascending) by similarity
	{
		m_nSortColumn = 1;
		m_bSortDescending = true;
	}

	// setup up/down arrows in sortable header
	SetHeaderArrow();

	// create a stop event
	if (!s_hStopWatching && m_nMode == kMode_View)
		s_hStopWatching = ::CreateEvent(NULL, TRUE, FALSE, _T("StopWatchingFolder"));

	// grab pointer to status bar
	CMainFrame* pMainFrame =(CMainFrame*)AfxGetMainWnd();
	m_pwndStatusBar = pMainFrame->GetStatusBar();

	// connect to program events (signals)
	CFontRunnerSignals* pSignals = theApp.GetSignals();

	// only want certain signals in search mode
	if (m_nMode == kMode_View)
	{
		// connect to folder change event
		pSignals->ConnectTo_SelectedFolderChange(boost::bind(&CFontListView::OnSelectedFolderChange, this, _1, _2));

		// connect to font change event
		pSignals->ConnectTo_SelectedFontChange(boost::bind(&CFontListView::OnSelectedFontChange, this, _1));

		// connect to project change event
		pSignals->ConnectTo_SelectedProjectChange(boost::bind(&CFontListView::OnSelectedProjectChange, this, _1));

		// connect to refresh event
		pSignals->ConnectTo_Refresh(boost::bind(&CFontListView::OnRefresh, this));

		// connect to "add files to main view" event
		pSignals->ConnectTo_AddToMainFontListView(boost::bind(&CFontListView::OnAddToMainFontListView, this, _1));
	}

	// connect to preview size change event
	pSignals->ConnectTo_PreviewSizeChange(boost::bind(&CFontListView::OnPreviewSizeChange, this, _1));

	// connect to temp install change event
	pSignals->ConnectTo_TempAutoInstallChange(boost::bind(&CFontListView::OnTempAutoInstallChange, this, _1));

	// connect to shutdown event
	pSignals->ConnectTo_Shutdown(boost::bind(&CFontListView::OnFontRunnerShutdown, this));
	
	// connect to options change event
	pSignals->ConnectTo_OptionsChange(boost::bind(&CFontListView::OnOptionsChange, this));

	// connect to un-managed font event
	pSignals->ConnectTo_UnManageFontItem(boost::bind(&CFontListView::OnUnManageFontItem, this, _1));

	// connect to font delete event
	pSignals->ConnectTo_FontFileDeleted(boost::bind(&CFontListView::OnFontFileDeleted, this, _1));

	// create drop target helper
	::CoCreateInstance(CLSID_DragDropHelper, NULL, CLSCTX_INPROC_SERVER,
					   IID_IDropTargetHelper, (void**)&m_pDropTargetHelper);

	// create a drop target handler and connect this class to it
	CDropTargetHandler* pDropTarget = new CDropTargetHandler();
	pDropTarget->Connect(
		boost::bind(&CFontListView::DragEnter, this, _1, _2, _3, _4),
		boost::bind(&CFontListView::DragOver, this, _1, _2, _3),
		boost::bind(&CFontListView::DragLeave, this),
		boost::bind(&CFontListView::Drop, this, _1, _2, _3, _4));

	// register for drag and drops
	::RegisterDragDrop(m_hWnd, pDropTarget);
}

BOOL CFontListView::PreTranslateMessage(MSG* pMsg)
{
	// handle delete key because accelerator does not work for some reason
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_DELETE)
	{
		int nSelected = GetListCtrl().GetNextItem(-1, LVNI_SELECTED);
		if (nSelected != -1)
			OnFolderMenu_Delete();

		return true;
	}

	return CListView::PreTranslateMessage(pMsg);
}

void CFontListView::BeginDrag()
{
	// grab mouse cursor position
	POINT pt = {0, 0};
	::GetCursorPos(&pt);
	ScreenToClient(&pt);

	bool bForFontProject =
		(CFontRunnerDoc::GetDoc()->UsingFontProject() && m_nMode == kMode_View);
	
	// create data object from current font selection
	CFontFileDataSource* pDataSource =
		bForFontProject ? CreateDataSourceForProjectData() : CreateDataSourceForDropFiles();

	// if we got a data object, start drag and drop op
	if (pDataSource)
	{
		// create SHDRAGIMAGE object
		SHDRAGIMAGE shdi;
		memset(&shdi, 0, sizeof(SHDRAGIMAGE));

		try
		{
			CheckResultFunctor CheckResult;

			CComPtr<IDragSourceHelper> pDragSourceHelper;
			CheckResult(::CoCreateInstance(CLSID_DragDropHelper, NULL, CLSCTX_INPROC_SERVER,
							   IID_IDragSourceHelper, (void**)&pDragSourceHelper));

			
			CreateDragBitmap(pt, &shdi);

			// It's ok if InitializeFromBitmap fails because
			// if "show window contents while dragging is turned off, it will fail.
			IDataObject* pDataObject = dynamic_cast<IDataObject*>(pDataSource->GetInterface(&IID_IDataObject));
			HRESULT hResult = pDragSourceHelper->InitializeFromBitmap(&shdi, pDataObject);

			if (FAILED(hResult) && shdi.hbmpDragImage)
				::DeleteObject(shdi.hbmpDragImage); // have to delete bitmap if call fails

			// mark selected as private temporarily
			MarkSelectedPrivate();
			
			// do drag-and-drop operation
			pDataSource->DoDragDrop(DROPEFFECT_COPY | DROPEFFECT_MOVE);

			// restore the old characteristics for the selected items if drag did not result in a move
			DROPEFFECT dwPerformedEffect = ShellUtils::CheckPerformedEffect(pDataObject);
			if (dwPerformedEffect != DROPEFFECT_MOVE)
				RestoreSelectedCharacteristics();

			pDataObject->Release();

			if (pDataSource->m_dwRef == 1)
				pDataObject->Release();
		}
		catch (...)
		{		
		}
	}
}

void CFontListView::OnBeginDrag(NMHDR* /*pNMHDR*/, LRESULT* pResult)
{
	if (!m_bViewingSystemFonts)
		BeginDrag();

	*pResult = 0;
}

void CFontListView::OnBeginRDrag(NMHDR* /*pNMHDR*/, LRESULT* pResult)
{
	if (!m_bViewingSystemFonts)
		BeginDrag();

	*pResult = 0;
}

void CFontListView::DragEnter(IDataObject *pDataObject, DWORD grfKeyState, POINTL ptl, DWORD* pdwEffect)
{
	CFontRunnerDoc* pDoc = CFontRunnerDoc::GetDoc();

	if (pDoc->UsingFontProject())
		*pdwEffect = DROPEFFECT_COPY;
	else if (m_nMode == kMode_View)
	{
		IShellFolder* pShellFolder = NULL;
		LPITEMIDLIST lpi = NULL;
		pDoc->GetCurrentFolder(&pShellFolder, &lpi);

		HRESULT hResult =
			pShellFolder->GetUIObjectOf(::GetParent(GetListCtrl().GetSafeHwnd()),
										1,
										(const struct _ITEMIDLIST **)&lpi,
										IID_IDropTarget,
										0,
										(void**)&m_pDropTarget);

		if (SUCCEEDED(hResult))
		{
			m_pDropTarget->DragEnter(pDataObject, grfKeyState, ptl, pdwEffect);

			// since we're hanging onto this pointer, increase its reference count
			m_pDropTarget->AddRef();
		}
	}
	else
		*pdwEffect = DROPEFFECT_NONE;

	if (m_pDropTargetHelper)
	{
		POINT pt = { ptl.x, ptl.y };
		m_pDropTargetHelper->DragEnter(m_hWnd, pDataObject, &pt, *pdwEffect);
	}
}

void CFontListView::DragOver(DWORD grfKeyState, POINTL ptl, DWORD *pdwEffect)
{
	if (m_pDropTargetHelper)
	{
		POINT pt = { ptl.x, ptl.y };
		m_pDropTargetHelper->DragOver(&pt, *pdwEffect);
	}

	if (CFontRunnerDoc::GetDoc()->UsingFontProject())
		*pdwEffect = DROPEFFECT_COPY;
	else if (m_pDropTarget)
		m_pDropTarget->DragOver(grfKeyState, ptl, pdwEffect);
	else
		*pdwEffect = DROPEFFECT_NONE;
}

void CFontListView::DragLeave()
{
	if (m_pDropTargetHelper)
		m_pDropTargetHelper->DragLeave();

	if (m_pDropTarget)
	{
		m_pDropTarget->Release();
		m_pDropTarget = NULL;
	}
}

void CFontListView::Drop(IDataObject *pDataObject, DWORD grfKeyState, POINTL ptl, DWORD *pdwEffect)
{
	if (m_pDropTargetHelper)
	{
		POINT pt = { ptl.x, ptl.y };
		m_pDropTargetHelper->Drop(pDataObject, &pt, *pdwEffect);
	}

	// get filenames from dropped content
	FORMATETC fmt;
	fmt.cfFormat = CF_HDROP;
	fmt.ptd = NULL;
	fmt.dwAspect = DVASPECT_CONTENT;
	fmt.lindex = -1;
	fmt.tymed = TYMED_HGLOBAL;

	// see if these are files
	HRESULT hResult = pDataObject->QueryGetData(&fmt);
	if (SUCCEEDED(hResult))
	{
		STGMEDIUM stgm;
		pDataObject->GetData(&fmt, &stgm);

		// the DragQueryFile function will help us get the filenames
		UINT nFileCount = ::DragQueryFile((HDROP)stgm.hGlobal, 0xFFFFFFFF, NULL, 0);

		if (theApp.GetProgramOptions()->GetFontListDropOption() == 0)
		{

			// need a buffer for filenames
			boost::scoped_array<TCHAR> pszFileName;

			if (CFontRunnerDoc::GetDoc()->UsingFontProject())
			{
				std::vector<std::wstring> list;

				for (UINT nFile = 0; nFile < nFileCount; ++nFile)
				{
					// get size of file name
					UINT nSize = ::DragQueryFile((HDROP)stgm.hGlobal, nFile, NULL, 0);
					pszFileName.reset(new TCHAR[nSize + 1]);

					// get filename
					::DragQueryFile((HDROP)stgm.hGlobal, nFile, pszFileName.get(), nSize + 1);

					std::wstring strTemp(pszFileName.get());
					list.push_back(strTemp);
				}

				// Add files to project.  The project view will let us know
				// if we indeed have to add them to this list view
				theApp.GetSignals()->Fire_AddToProject(list);
			}
			else if (m_pDropTarget)
			{
				// If all the files being droppped are already in this folder,
				// the user probably intended on canceling the drag.
				bool bAllInThisFolder = true;
				for (UINT nFile = 0; nFile < nFileCount; ++nFile)
				{
					// get size of file name
					UINT nSize = ::DragQueryFile((HDROP)stgm.hGlobal, nFile, NULL, 0);
					pszFileName.reset(new TCHAR[nSize + 1]);

					// get filename
					::DragQueryFile((HDROP)stgm.hGlobal, nFile, pszFileName.get(), nSize + 1);

					// remove filespec
					::PathRemoveFileSpec(pszFileName.get());

					// if this file is not in this folder, then this is probably
					// a legit attempt to drag files in here
					if (_tcscmp(pszFileName.get(), m_strCurrentDirectory) != 0)
					{
						bAllInThisFolder = false;
						break;
					}
				}

				if (!bAllInThisFolder)
					m_pDropTarget->Drop(pDataObject, grfKeyState, ptl, pdwEffect);
				else
					*pdwEffect = DROPEFFECT_NONE;

				m_pDropTarget->Release();
				m_pDropTarget = NULL;
			}
		}
		else
			theApp.SwitchToDroppedItem((HDROP)stgm.hGlobal);
	}
	else
	{
		// files were not dragged -- see if it is font project data
		fmt.cfFormat = (CLIPFORMAT)::RegisterClipboardFormat(_T("Font Runner Font Project Data"));
		if (SUCCEEDED(pDataObject->QueryGetData(&fmt)))
		{
			// Font project entry.  We'll take that too.
			STGMEDIUM stgm;
			pDataObject->GetData(&fmt, &stgm);

			TCHAR* pStringList = reinterpret_cast<TCHAR*>(::GlobalLock(stgm.hGlobal));

			if (pStringList)
			{
				TCHAR* pCurrent = pStringList;
				std::vector<std::wstring> list;

				while (*pCurrent)
				{
					std::wstring str(pCurrent);
					list.push_back(str);

					pCurrent += str.length() + 1;
				}

				// Done.  Unlock memory
				::GlobalUnlock(stgm.hGlobal);

				// Add files to project.  The project view will let us know
				// if we indeed have to add them to this list view
				theApp.GetSignals()->Fire_AddToProject(list);
			}
		}
	}
}

void CFontListView::OnSelectedFolderChange(const CString& strFolder, const CString* pstrFile)
{
	// don't do anything if we're already in this folder
	if (m_strCurrentDirectory != strFolder || m_bViewingFontProject)
		ShowFontsInDirectory(strFolder, pstrFile);

	if (pstrFile && !pstrFile->IsEmpty())
		SetFontSelection(*pstrFile);
}

void CFontListView::OnSelectedFontChange(const CFontItem* pFontItem)
{
	// if this signal originated from this class, do nothing
	if (m_bSignalingFontChange)
		return;

	// find font in list
	int nNumItems = GetListCtrl().GetItemCount();

	for (int i = 0; i < nNumItems; ++i)
	{
		CFontItem* pItem = reinterpret_cast<CFontItem*>(GetListCtrl().GetItemData(i));
		if (pItem == pFontItem)
		{
			GetListCtrl().EnsureVisible(i, FALSE);

			m_bSignalFontChange = false;
			GetListCtrl().SetItemState(i, LVNI_SELECTED, LVNI_SELECTED);
			m_bSignalFontChange = true;
		}
	}
}

void CFontListView::OnSelectedProjectChange(const std::wstring& strProject)
{
	boost::weak_ptr<filelist_type> wpFileList = theApp.GetFontProjects()->GetFileList(strProject.c_str());
	if (boost::shared_ptr<filelist_type> pFileList = wpFileList.lock())
	{
		m_bViewingFontProject = true;

		if (m_bViewingSystemFonts)
		{
			m_bViewingSystemFonts = false;
			theApp.GetSignals()->Fire_ViewingSystemFonts(m_bViewingSystemFonts);
		}

		ShowFontsInList(*(pFileList.get()), NULL);
	}
}

void CFontListView::OnPreviewSizeChange(unsigned short nSize)
{
	// re-create all fonts in the list
	for (int i = 0; i < GetListCtrl().GetItemCount(); ++i)
	{
		CFontItem* pFontItem =
			reinterpret_cast<CFontItem*>(GetListCtrl().GetItemData(i));

		// get logical font info from old font.  We'll
		// use it to create the new font too.
		LOGFONT lf;
		pFontItem->GetDisplayFont().GetLogFont(&lf);

		// delete old font
		pFontItem->GetDisplayFont().DeleteObject();

		// create new font with new size
		lf.lfHeight = nSize;
		pFontItem->GetDisplayFont().CreateFontIndirect(&lf);
	}

	theApp.GetProgramOptions()->SetFontListCharSize(nSize);

	// have to send WM_WINDOWPOSCHANGED, to get MeasureItem called again
	CRect rectWindow;
    GetWindowRect(&rectWindow);

    WINDOWPOS wp;
    wp.hwnd = GetSafeHwnd();
    wp.cx = rectWindow.Width();
    wp.cy = rectWindow.Height();
    wp.flags = SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER;
    SendMessage(WM_WINDOWPOSCHANGED, 0, reinterpret_cast<LPARAM>(&wp));
}

CString CFontListView::GetSelFontName()
{
	CString strReturn("");

	// if there are no items on the list, return nothing
	if (GetListCtrl().GetItemCount() == 0)
		return strReturn;

	int nSelectedItem = GetListCtrl().GetNextItem(-1, LVNI_SELECTED);

	// if nothing is selected, return blank string
	if (nSelectedItem == -1)
		return strReturn;

	// get font name and return it to caller
	strReturn = GetListCtrl().GetItemText(nSelectedItem, 0);
	return strReturn;
}

void CFontListView::CleanUpFontList()
{
	// empty the list
	GetListCtrl().DeleteAllItems();
}

void CFontListView::OnFontRunnerShutdown()
{
	if (!m_nMode == kMode_View)
		SaveOptions();
}

// for automatic cleanup of LVITEMs allocated by the CreateListItem function object
typedef boost::shared_ptr<LVITEM> lvitem_ptr;

// LVITEM and its text are created together and are destroyed together
struct lvitem
{
	lvitem(lvitem_ptr p, boost::shared_array<TCHAR> t)
		: pItem(p),
		  pText(t)
	{}

	lvitem_ptr					pItem;
	boost::shared_array<TCHAR>	pText;
};
typedef std::vector<lvitem> lvitem_list;

struct ThreadParams
{
	const filelist_type*	pFileList;
	CProgressDlg*			pProgressDlg;
	CFontListView*			pFontListView;
	boost::shared_ptr<lvitem_list>	pLVItemlist;	// out param
};

// function object creates a lvitem_list
struct CreateListItem
{
	CreateListItem(ThreadParams* pParams)
		: m_pParams(pParams)
	{}

	void operator()(const CString& strFileName)
	{
		CFontManager* pFontManager = theApp.GetFontManager();

		CFontItem* pFontItem = pFontManager->AddReference(strFileName);

		bool bFontOk = (pFontItem->GetStatus() == CruxTechnologies::COpenTypeData::kStatus_OK);
		
		std::wstring strName;
		if (bFontOk)
		{
			// get font name
			using CruxTechnologies::OpenTypeFontData::CNameRecord;
			strName = *(pFontItem->GetOpenTypeData()->GetName(CNameRecord::kNameID_FullFontName));
		}
		else
		{
			using CruxTechnologies::COpenTypeData;
			UINT nMessageID = IDS_FONTLIST_CORRUPTFILE;
			switch (pFontItem->GetStatus())
			{
				case COpenTypeData::kStatus_OpenFileError:
					nMessageID = IDS_FONTLIST_OPENERROR;
					break;
				case COpenTypeData::kStatus_ReadFileError:
					nMessageID = IDS_FONTLIST_READERROR;
					break;
			}

			CString strMessage;
			strMessage.LoadString(nMessageID);
			strName = (LPCTSTR)strMessage;
		}

		// get icon
		int nIcon = 
			(int)m_pParams->pFontListView->SendMessage(g_rwmGetIconIndex, (WPARAM)(LPCTSTR)strFileName);

		// LVITEM re-used to add items to the list
		lvitem_ptr pLVItem(new LVITEM);
		ZeroMemory(pLVItem.get(), sizeof(LV_ITEM));
		pLVItem->mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;

		// finish filling out LVITEM struct
		std::wstring::size_type nNameLength = strName.length();
		boost::shared_array<TCHAR> pNameBuffer(new TCHAR[nNameLength + 1]);
		pLVItem->pszText = pNameBuffer.get();
		strName._Copy_s(pLVItem->pszText, nNameLength + 1, nNameLength);
		pLVItem->pszText[nNameLength] = '\0';	// make sure last char is NULL terminator
		pLVItem->lParam = reinterpret_cast<LPARAM>(pFontItem);
		pLVItem->iItem = 0;
		pLVItem->iImage = nIcon;

		m_pParams->pLVItemlist->push_back(lvitem(pLVItem, pNameBuffer));
	}

private:
	ThreadParams* m_pParams;
};

struct AddToList
{
	AddToList(CListCtrl* pListCtrl)
		: m_pListCtrl(pListCtrl)
	{}

	void operator()(const lvitem& pLVItem) const
	{
		// insert item
		int nIndex = m_pListCtrl->InsertItem(pLVItem.pItem.get());

		CFontItem* pFontItem = reinterpret_cast<CFontItem*>(pLVItem.pItem->lParam);

		// add filename and size
		m_pListCtrl->SetItemText(nIndex, 1, pFontItem->GetFileName());
		m_pListCtrl->SetItemText(nIndex, 2, pFontItem->GetFileSizeString());
	}

private:
	CListCtrl* m_pListCtrl;
};

struct AddToList_WithSimilarity
{
	AddToList_WithSimilarity(CListCtrl* pListCtrl, LPCTSTR szFontName)
		: m_pListCtrl(pListCtrl),
		  m_szFontName(szFontName)
	{}

	void operator()(const lvitem& pLVItem) const
	{
		// insert item
		int nIndex = m_pListCtrl->InsertItem(pLVItem.pItem.get());

		CFontItem* pFontItem = reinterpret_cast<CFontItem*>(pLVItem.pItem->lParam);

		UINT nID = IDS_FONTLIST_SIMILARITY_SIMILAR;

		int nDistance = pFontItem->GetDistance(m_szFontName);
		if (nDistance == 0)
			nID = IDS_FONTLIST_SIMILARITY_IDENTICAL;
		else if (nDistance <= 15)
			nID = IDS_FONTLIST_SIMILARITY_VERYSIMILAR;
		else if (nDistance <= 30)
			nID = IDS_FONTLIST_SIMILARITY_REASONABLY;

		// add filename and size
		CString strSimilarity;
		strSimilarity.LoadString(nID);
		m_pListCtrl->SetItemText(nIndex, 1, strSimilarity);
	}

private:
	CListCtrl*	m_pListCtrl;
	LPCTSTR		m_szFontName;
};

UINT __stdcall CFontListView::ThreadedCreateList(LPVOID pParam)
{
	ASSERT(pParam);
	ThreadParams* pParams = reinterpret_cast<ThreadParams*>(pParam);

	ASSERT(pParams->pFontListView && pParams->pFileList);

	CreateListItem create_listitem(pParams);
	for (filelist_type::const_iterator it = pParams->pFileList->begin();
		 it !=  pParams->pFileList->end();
		 ++it)
	{
		create_listitem(*it);

		if (pParams->pProgressDlg)
		{
			pParams->pProgressDlg->Increment();
			if (pParams->pProgressDlg->IsCanceled())
				break;
		}
	}

	// Done.  Let progress dialog know
	if (pParams->pProgressDlg)
		pParams->pProgressDlg->Finish();

	return 0;
};

void CFontListView::ShowFontsInList(const filelist_type& filelist, const CString* pstrSelect)
{
	// first, clean up old list
	CleanUpFontList();

	// if the font list is empty, update the status bar but do nothing else
	if (filelist.empty())
	{
		CString strStatusBarMessage, strMessage;

		// udpate status bar pane 0
		strMessage.LoadString(IDS_STATUSBAR_NOFONTS);
		m_pwndStatusBar->SetPaneText(0, strMessage);

		// update status bar pane 1
		strMessage.LoadString(IDS_STATUSBAR_FONTSDISPLAYED);
		strStatusBarMessage.Format(strMessage, 0);
		m_pwndStatusBar->SetPaneText(1, strStatusBarMessage);
		
		return;
	}

	// set mouse cursor to wait
	CWaitCursor wait;

	// time it
	LARGE_INTEGER liStart;
	::QueryPerformanceCounter(&liStart);

	// how many processors?
	SYSTEM_INFO si;
	::GetSystemInfo(&si);

	// ensure at least 50 fonts per thread, but no more than 1 per processor
	DWORD nThreads = std::min<DWORD>((DWORD)(filelist.size() / 50), si.dwNumberOfProcessors);

	// can't be zero
	if (nThreads == 0)
		++nThreads;

	// how many files per thread?
	size_t nFilesPerThread = (filelist.size() / nThreads) + 1;

	// first, break the list up
	std::vector<filelist_type> lists(nThreads);
	
	filelist_type::const_iterator it = filelist.begin();
	for (DWORD p = 0; p < nThreads; ++p)
	{
		size_t nListIndex = 0;
		while (nListIndex < nFilesPerThread && it != filelist.end())
		{
			lists[p].insert(*it);

			++it;
			++nListIndex;
		}
	}

	// parameters for each thread
	std::vector<ThreadParams> params(nThreads);

	// handles
	boost::scoped_array<HANDLE> pHandles(new HANDLE[nThreads]);

	// the progress dialog
	CProgressDlg dlgProgress;

	unsigned nThreadID;
	for (DWORD t = 0; t < nThreads; ++t)
	{
		params[t].pProgressDlg = &dlgProgress;
		params[t].pFileList = &lists.at(t);
		params[t].pLVItemlist.reset(new lvitem_list);
		params[t].pFontListView = this;
		pHandles[t] = (HANDLE)::_beginthreadex(NULL, 0, ThreadedCreateList, &params[t], 0, &nThreadID);
	}

	// show progress
	dlgProgress.Start((int)filelist.size(), nThreads);
	if (dlgProgress.DoModal() == IDCANCEL)
	{
		// pump messages while waiting for threads to finish
		MSG message;
		while (::WaitForMultipleObjects(nThreads, pHandles.get(), true, 50) == WAIT_TIMEOUT)
		{
			if (::PeekMessage(&message, NULL, 0, 0, PM_REMOVE))
			{
				::TranslateMessage(&message);
				::DispatchMessage(&message);
			}
		}
	}

	// stick all the LVITEMs on the list
	for (DWORD t = 0; t < nThreads; ++t)
		std::for_each(params[t].pLVItemlist->begin(),
					  params[t].pLVItemlist->end(),
					  AddToList(&GetListCtrl()));

	GetListCtrl().SortItems(FontListSortProc, (DWORD_PTR)this);

	// need to know how wide this control is
	CRect rcClient, rcLastItem;
	GetClientRect(&rcClient);
	GetListCtrl().GetItemRect(GetListCtrl().GetItemCount() - 1, &rcLastItem, LVIR_BOUNDS);

	if (rcLastItem.bottom <= rcClient.bottom)
	{
		// resize the columns
		int nOtherColumns =
			(GetListCtrl().GetColumnWidth(kColumn_FileName) +
			 GetListCtrl().GetColumnWidth(kColumn_Size));
		GetListCtrl().SetColumnWidth(kColumn_FontName, rcClient.Width() - nOtherColumns);
	}

	// let other apps know we installed these fonts
	::PostMessage(HWND_BROADCAST, WM_FONTCHANGE, 0, 0);

	LARGE_INTEGER liFinish, liFrequency;
	::QueryPerformanceCounter(&liFinish);
	::QueryPerformanceFrequency(&liFrequency);

	double lfTime = (double)(liFinish.QuadPart - liStart.QuadPart) / (double)(liFrequency.QuadPart);

	// create status bar message
	CString strStatusBarMessage, strMessage;
	if (dlgProgress.IsCanceled())
	{
		strMessage.LoadString(IDS_STATUSBAR_LOADCANCELED);
		strStatusBarMessage.Format(strMessage, dlgProgress.GetCurrent());
	}
	else
	{
		strMessage.LoadString(IDS_STATUSBAR_LOADCOMPLETE);
		strStatusBarMessage.Format(strMessage, dlgProgress.GetCurrent(), lfTime);
	}

	// update status bar
	m_pwndStatusBar->SetPaneText(0, strStatusBarMessage);

	// update number of fonts shown
	strMessage.LoadString(IDS_STATUSBAR_FONTSDISPLAYED);
	strStatusBarMessage.Format(strMessage, dlgProgress.GetCurrent());
	m_pwndStatusBar->SetPaneText(1, strStatusBarMessage);

	if (pstrSelect)
		SetFontSelection(*pstrSelect);

	if (CFontRunnerDoc::GetDoc()->UsingFontProject())
		theApp.GetProgramOptions()->SetLastProject(CFontRunnerDoc::GetDoc()->GetCurrentProject());
}

void CFontListView::SetSimilarTo(LPCTSTR pszSimilarTo)
{
	m_pszSimilarTo = pszSimilarTo;
}

LPCTSTR CFontListView::GetSimilarTo() const
{
	return m_pszSimilarTo;
}

int CFontListView::GetSortedColumn() const
{
	return m_nSortColumn;
}

bool CFontListView::IsSortDescending() const
{
	return m_bSortDescending;
}

void CFontListView::AddSimilarItem(const CFontItem* pItem)
{
	// create ThreadParams for direct call to CreateListItem function object
	ThreadParams params;
	params.pFileList = NULL; // file list not needed
	params.pLVItemlist.reset(new lvitem_list);
	params.pProgressDlg = NULL;
	params.pFontListView = this;
	CreateListItem create_listitem(&params);
	create_listitem(pItem->GetFullFileName());

	// just add first item to list because it is the only item
	AddToList_WithSimilarity add_to_list(&GetListCtrl(), m_pszSimilarTo);
	add_to_list(*(params.pLVItemlist.get()->begin()));

	GetListCtrl().SortItems(FontListSortProc, (DWORD_PTR)this);
}

void CFontListView::AddFoundItem(const CFontItem* pItem)
{
	// create ThreadParams for direct call to CreateListItem function object
	ThreadParams params;
	params.pFileList = NULL; // file list not needed
	params.pLVItemlist.reset(new lvitem_list);
	params.pProgressDlg = NULL;
	params.pFontListView = this;
	CreateListItem create_listitem(&params);
	create_listitem(pItem->GetFullFileName());

	// just add first item to list because it is the only item
	AddToList add_to_list(&GetListCtrl());
	add_to_list(*(params.pLVItemlist.get()->begin()));

	GetListCtrl().SortItems(FontListSortProc, (DWORD_PTR)this);
}

void CFontListView::AddNewFile(const CString& strFileName)
{
	// create ThreadParams for direct call to CreateListItem function object
	ThreadParams params;
	params.pFileList = NULL; // file list not needed
	params.pLVItemlist.reset(new lvitem_list);
	params.pProgressDlg = NULL;
	params.pFontListView = this;
	CreateListItem create_listitem(&params);
	create_listitem(strFileName);

	AddToList add_to_list(&GetListCtrl());
	add_to_list(*(params.pLVItemlist.get()->begin()));

	GetListCtrl().SortItems(FontListSortProc, (DWORD_PTR)this);
}

void CFontListView::AddNewFile(const std::wstring& strFileName)
{
	CString strTemp(strFileName.c_str());
	AddNewFile(strTemp);
}

void PopulateList(LPCTSTR pszSearchString, const CString* pstrDirectory, filelist_type& filelist)
{
	TCHAR szSearchString[MAX_PATH];
	::StringCbCopy(szSearchString, MAX_PATH, *pstrDirectory);
	::PathAppend(szSearchString, pszSearchString);

	// get list of TTF files from this directory
	// we're going to use the Win32 API functions FindFirstFile, FindNextFile
	// and FindClose to enumerate the files

	// found file information
	WIN32_FIND_DATA FoundData;
	HANDLE hFindHandle = FindFirstFile(szSearchString, &FoundData);
	if (hFindHandle != INVALID_HANDLE_VALUE)
	{
		TCHAR szFileName[MAX_PATH];
		do
		{
			::StringCbCopy(szFileName, MAX_PATH, *pstrDirectory);
			::PathAppend((LPTSTR)&szFileName, FoundData.cFileName);

			filelist.insert(szFileName);

		} while (FindNextFile(hFindHandle, &FoundData));
	}
}

void CFontListView::ShowFontsInDirectory(const CString& strDirectory, const CString* pstrSelect)
{
	// read fonts from given directory and display them in this list view
	m_bViewingFontProject = false;

	// save current directory
	// if NULL was passed in, then we are just refreshing the current directory
	const CString* pstrDirectory = &m_strCurrentDirectory;
	if (!strDirectory.IsEmpty())
		m_strCurrentDirectory = strDirectory;

	// if the directory name we're working with is empty, there's nothing we can do
	if (pstrDirectory->IsEmpty())
		return;

	if (!strDirectory.IsEmpty())
	{
		try
		{
			CheckResultFunctor CheckResult;

			// get fully-qualified PIDL of incoming path
			CComPtr<IShellFolder> pDesktop;
			CheckResult(::SHGetDesktopFolder(&pDesktop));

			// see if we're viewing the system fonts folder
			pidl_ptr pidlFonts;
			pidlFonts.useILFree();
			CheckResult(::SHGetFolderLocation(NULL, CSIDL_FONTS, NULL, NULL, &pidlFonts));

			pidl_ptr pidlDirectory;
			TCHAR szCompare[MAX_PATH];
			CheckResult(::StringCchCopy(szCompare, MAX_PATH, strDirectory));
			CheckResult(pDesktop->ParseDisplayName(NULL, NULL, szCompare, NULL, &pidlDirectory, NULL));

			m_bViewingSystemFonts = (::ILIsEqual(pidlFonts.get(), pidlDirectory.get()) != 0);

			theApp.GetSignals()->Fire_ViewingSystemFonts(m_bViewingSystemFonts);
		}
		catch (CHRESULTException&)
		{
			m_bViewingSystemFonts = false;
		}
	}

	filelist_type filelist;

	PopulateList(_T("*.ttf"), pstrDirectory, filelist);
	PopulateList(_T("*.otf"), pstrDirectory, filelist);

	ShowFontsInList(filelist, pstrSelect);

	// save current directory to options struct
	theApp.GetProgramOptions()->SetLastFolder(m_strCurrentDirectory);

	// if there is no new font selected, signal it
	if (pstrSelect == NULL)
		FireSelectedFontChange(NULL);

	// watch for changes
	WatchCurrentFolder();
}

bool CFontListView::SetHeaderArrow(const int* pnLastItem)
{
	// see if we can get header control
	CHeaderCtrl* pHeader = GetListCtrl().GetHeaderCtrl();
	if (!pHeader)
		return false;

	// get major version of common controls library
	WORD nMajorVersion = HIWORD(m_dwCOMCTL32Version);

	// prevents any sizing
	m_bAdjustingHeader = true;

	// common controls 6 will draw the arrow for us
	if (nMajorVersion >= 6)
	{
		// turn arrow off on last item
		if ((pnLastItem) && (*pnLastItem != m_nSortColumn))
		{
			// get item
			HDITEM hdLastItem;
			hdLastItem.mask = HDI_FORMAT;
			pHeader->GetItem(*pnLastItem, &hdLastItem);

			// set item
			hdLastItem.mask = HDI_FORMAT;
			hdLastItem.fmt &= ~(HDF_SORTUP | HDF_SORTDOWN);
			pHeader->SetItem(*pnLastItem, &hdLastItem);
		}

		// get item
		HDITEM hdItem;
		hdItem.mask = HDI_FORMAT;
		pHeader->GetItem(m_nSortColumn, &hdItem);

		// set item
		hdItem.mask = HDI_FORMAT;
		hdItem.fmt &= ~(HDF_SORTUP | HDF_SORTDOWN);
		hdItem.fmt |= m_bSortDescending ? HDF_SORTDOWN : HDF_SORTUP;
		pHeader->SetItem(m_nSortColumn, &hdItem);
	}
	else // common controls version is < 6, we have to draw the arrow ourselves
	{
		// delete bitmap from last item
		if ((pnLastItem) && (*pnLastItem != m_nSortColumn))
		{
			HDITEM hdLastItem;
			hdLastItem.mask = HDI_FORMAT | HDI_BITMAP;
			pHeader->GetItem(*pnLastItem, &hdLastItem);

			if (hdLastItem.hbm != NULL)
			{
				::DeleteObject(hdLastItem.hbm);
				hdLastItem.hbm = NULL;
				hdLastItem.fmt ^= HDF_BITMAP;
				pHeader->SetItem(*pnLastItem, &hdLastItem);
			}
		}

		// setup bitmap on header control
		HDITEM hdItem;
		hdItem.mask = HDI_FORMAT | HDI_BITMAP;

		pHeader->GetItem(m_nSortColumn, &hdItem);

		// delete header item bitmap if exists
		if (hdItem.hbm != NULL)
		{
			::DeleteObject(hdItem.hbm);
			hdItem.hbm = NULL;
		}

		hdItem.fmt |= HDF_BITMAP | HDF_BITMAP_ON_RIGHT;
		hdItem.hbm =
			(HBITMAP)::LoadImage(AfxGetInstanceHandle(),
								 MAKEINTRESOURCE(m_bSortDescending ? IDB_SORTDOWN_BITMAP : IDB_SORTUP_BITMAP), IMAGE_BITMAP, 0, 0, LR_LOADMAP3DCOLORS);
		pHeader->SetItem(m_nSortColumn, &hdItem);
	}

	m_bAdjustingHeader = false;

	return true;
}

void CFontListView::SetFontSelection(const CString& strFilename)
{
	int nLastItem = GetListCtrl().GetItemCount() - 1;

	for (int nItem = 0; nItem <= nLastItem; nItem++)
	{
		CFontItem* pFontItem =
			reinterpret_cast<CFontItem*>(GetListCtrl().GetItemData(nItem));

		if (strFilename.CompareNoCase(pFontItem->GetFileName()) == 0)
		{
			// if already selected, do nothing
			if (GetListCtrl().GetItemState(nItem, LVIS_SELECTED) == LVIS_SELECTED)
				break;

			GetListCtrl().SetItemState(nItem, LVIS_SELECTED, LVIS_SELECTED);

			LRESULT lResult = 0;
			NM_LISTVIEW NMListView;
			NMListView.uChanged = LVIF_STATE;
			NMListView.uNewState = LVIS_SELECTED;
			NMListView.lParam = GetListCtrl().GetItemData(nItem);

			OnItemchanged((NMHDR*)&NMListView, &lResult);

			// make sure it is visible
			GetListCtrl().EnsureVisible(nItem, FALSE);

			// found font, stop looping
			break;
		}
		else
			GetListCtrl().SetItemState(nItem, 0, LVIS_SELECTED);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CFontListView message handlers

void CFontListView::OnDestroy() 
{
	// unregister for drag and drops
	::RevokeDragDrop(GetSafeHwnd());

	// stop watching this folder
	StopWatchingCurrentFolder();
	
	// clean up event handle
	if (s_hStopWatching && m_nMode == kMode_View)
	{
		::CloseHandle(s_hStopWatching);
		s_hStopWatching = NULL;
	}

	// can't allow this window to be destroyed without first
	// cleaning up the font list
	CleanUpFontList();

	// make sure the arrow bitmap in the header is deleted
	if ((HIWORD(m_dwCOMCTL32Version) < 6))
	{
		// see if we can get header control
		CHeaderCtrl* pHeader = GetListCtrl().GetHeaderCtrl();
		if (pHeader)
		{
			HDITEM hdItem;
			hdItem.mask = HDI_FORMAT | HDI_BITMAP;

			pHeader->GetItem(m_nSortColumn, &hdItem);

			// delete header item bitmap if exists
			if (hdItem.hbm != NULL)
			{
				::DeleteObject(hdItem.hbm);
				hdItem.hbm = NULL;
			}
		}
	}

	CListView::OnDestroy();
}

void CFontListView::OnItemchanged(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// don't care unless this is the main view
	if (m_nMode != kMode_View)
		return;

	NM_LISTVIEW* pNMListView = reinterpret_cast<NM_LISTVIEW*>(pNMHDR);

	// see if selection state changed
	if ((pNMListView->uChanged & LVIF_STATE) &&
		(pNMListView->uNewState & LVIS_SELECTED))
	{
		// get item data
		CFontItem* pData = reinterpret_cast<CFontItem*>(pNMListView->lParam);

		if (pData->GetStatus() != CruxTechnologies::COpenTypeData::kStatus_OK)
			FireSelectedFontChange(NULL);
		else
			FireSelectedFontChange(pData);
	}
	
	*pResult = 0;
}

void CFontListView::OnRclick(NMHDR* /*pNMHDR*/, LRESULT* pResult) 
{
	// get coordinates of click
	POINT point;
	GetCursorPos(&point);
	ScreenToClient(&point);
	
	// get item clicked on
	m_nRClickItem = GetListCtrl().HitTest(point);

	// convert point back to screen coordinates
	ClientToScreen(&point);
	
	if (m_nRClickItem != -1)
	{
		CMenu menu;
		CMenu *pSubMenu = NULL;

		UINT nMenu = IDR_FONTLIST_MENU, nDefaultItem = ID_FONTLIST_OPEN;

		CFontItem* pFontItem = (CFontItem*)GetListCtrl().GetItemData(m_nRClickItem);

		// different menu if bad font
		if (pFontItem->GetStatus() != CruxTechnologies::COpenTypeData::kStatus_OK)
		{
			nMenu = IDR_BADFONT_MENU;
			nDefaultItem = ID_BADFONTMENU_WHATSWRONG;
		}

		menu.LoadMenu(nMenu);
		pSubMenu = menu.GetSubMenu(0);

		if (pFontItem->GetStatus() == CruxTechnologies::COpenTypeData::kStatus_OK)
		{
			// if no panose information, disable "Find Similar" item
			PANOSE* pPanose = (PANOSE*)pFontItem->GetOpenTypeData()->GetOS2Table().m_table.part0.panose;

			if (pPanose->bFamilyType == 0 || pPanose->bFamilyType == 1)
			{
				MENUITEMINFO mii;
				memset(&mii, 0, sizeof(MENUITEMINFO));
				mii.cbSize = sizeof(MENUITEMINFO);
				mii.fMask |= MIIM_STATE;
				mii.fState = MFS_DISABLED;
				pSubMenu->SetMenuItemInfo(ID_FONTLIST_FINDSIMILAR, &mii);
			}

			// insert a "Go to location" menu item if not already viewing a directory
			// this means any find window (not view mode) or when viewing a font project
			if (m_nMode != kMode_View || CFontRunnerDoc::GetDoc()->UsingFontProject())
			{
				MENUITEMINFO mii;

				CString strMenuItem;
				strMenuItem.LoadString(IDS_FONTLIST_SHOWFOLDER);
				::memset(&mii, 0, sizeof(MENUITEMINFO));
				mii.cbSize = sizeof(MENUITEMINFO);
				mii.fMask = MIIM_ID | MIIM_STRING;
				mii.fType = MFT_STRING;
				mii.wID = ID_FONTLISTMENU_SHOWCONTAINING;
				mii.cch = strMenuItem.GetLength();
				mii.dwTypeData = strMenuItem.GetBuffer();

				menu.InsertMenuItem(ID_FOLDERMENU_DELETE, &mii);
				strMenuItem.ReleaseBuffer();

				::memset(&mii, 0, sizeof(MENUITEMINFO));
				mii.cbSize = sizeof(MENUITEMINFO);
				mii.fType = MFT_SEPARATOR;
				menu.InsertMenuItem(ID_FOLDERMENU_DELETE, &mii);
			}

			if (m_bViewingSystemFonts)
			{
				// remove "install" and "delete"
				pSubMenu->DeleteMenu(ID_FONTLIST_INSTALL, MF_BYCOMMAND);
				pSubMenu->DeleteMenu(ID_FOLDERMENU_DELETE, MF_BYCOMMAND);

				// remove extra separator
				pSubMenu->DeleteMenu(3, MF_BYPOSITION);
			}
		}

		// set default item
		pSubMenu->SetDefaultItem(nDefaultItem);

		// show menu
		pSubMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this);
	}

	*pResult = 0;
}

void CFontListView::OnFilePrintPreview()
{
	theApp.InPrintPreview(true);
	CView::OnFilePrintPreview();
}

void CFontListView::OnFontlistInstall() 
{
	bool bPlural = (GetListCtrl().GetSelectedCount() > 1);

	// first confirm this with user
	CString strMessage, strTitle;
	strMessage.LoadString(bPlural ? IDS_GENERAL_INSTALL_MULTIPLE : IDS_GENERAL_INSTALL);
	strTitle.LoadString(bPlural ? IDS_GENERAL_INSTALL_MULTIPLE_TITLE : IDS_GENERAL_INSTALL_TITLE);
	int nResult = MessageBox(strMessage, strTitle, MB_YESNO + MB_ICONQUESTION);
	if (nResult == IDNO)
		return;

	try
	{
		CheckResultFunctor CheckResult;

		// get desktop folder
		CComPtr<IShellFolder> pDesktopFolder;
		CheckResult(::SHGetDesktopFolder(&pDesktopFolder));

		// get fonts PIDL
		pidl_ptr pidlFonts;
		CheckResult(::SHGetFolderLocation(NULL, CSIDL_FONTS, NULL, NULL, &pidlFonts));

		// get fonts folder drop target
		CComPtr<IDropTarget> pDropTarget;
		CheckResult(pDesktopFolder->GetUIObjectOf(AfxGetMainWnd()->GetSafeHwnd(),
												  1,
												  (LPCITEMIDLIST*)&pidlFonts,
												  IID_IDropTarget,
												  NULL,
												  (void**)&pDropTarget));

		// create data object from selected files
		CFontFileDataSource* pDataSource = CreateDataSourceForDropFiles();

		// grab data object from data source
		CComPtr<IDataObject> pDataObject(dynamic_cast<IDataObject*>(pDataSource->GetInterface(&IID_IDataObject)));

		// attempt drop
		POINTL pt = { 0, 0 };
		DWORD dwEffect = DROPEFFECT_COPY;
		CheckResult(pDropTarget->DragEnter(pDataObject, 0, pt, &dwEffect));
		CheckResult(pDropTarget->DragOver(0, pt, &dwEffect));
		CheckResult(pDropTarget->Drop(pDataObject, 0, pt, &dwEffect));

		// one more thing, update selected font's item data so that it doesn't get un-installed.
		int nItem = GetListCtrl().GetNextItem(-1, LVNI_SELECTED);
		while (nItem != -1)
		{
			CFontItem* pFontItem = reinterpret_cast<CFontItem*>(GetListCtrl().GetItemData(nItem));
			pFontItem->Installed(true);

			nItem = GetListCtrl().GetNextItem(nItem, LVNI_SELECTED);
		}
	}
	catch (CHRESULTException& e)
	{
#ifdef _DEBUG
		CString strDebug;
		strDebug.Format(_T("HRESULT is 0x%08x."), e.GetHRESULT());
		MessageBox(strDebug, _T("Failure."), MB_OK + MB_ICONERROR);
#else
		e;
#endif
	}


	//// first confirm this with user
	//CString strMessage, strTitle;
	//strMessage.LoadString(IDS_GENERAL_INSTALL);
	//strTitle.LoadString(IDS_GENERAL_INSTALL_TITLE);
	//int nResult = MessageBox(strMessage, strTitle, MB_YESNO + MB_ICONQUESTION);
	//if (nResult == IDNO)
	//	return;

	//// get index of currently selected item
	//int nItem = GetListCtrl().GetNextItem(-1, LVNI_SELECTED);

	//CString strName = GetListCtrl().GetItemText(nItem, 0) + _T(" (TrueType)");
	//CString strFileName = GetListCtrl().GetItemText(nItem, 1);
	//CFontItem* pItem = reinterpret_cast<CFontItem*>(GetListCtrl().GetItemData(nItem));
	//CString strFullFileName = pItem->GetFullFileName();

	//// add font to registry
	//boost::scoped_ptr<CKey> pKey(new CKey());
	//pKey->OpenKey(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Fonts"));
	//pKey->SetString(strName, strFileName);
	//pKey->CloseKey();

	//// get name of system fonts folder from registry
	//pKey->OpenKey(HKEY_CURRENT_USER, _T("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders"));
	//CString strSystemFontsFolder;
	//pKey->GetString(_T("Fonts"), strSystemFontsFolder);
	//pKey->CloseKey();

	//// copy file to windows fonts folder
	//CString strDest = strSystemFontsFolder + _T("\\") + strFileName;

	//// copy file into fonts folder
	//BOOL bSuccess = CopyFile(strFullFileName, strDest, FALSE);

	//// Add this font to the system
	//bSuccess = ::AddFontResource(strDest);
	//
	//// let other apps know we installed this font
	//::PostMessage(HWND_BROADCAST, WM_FONTCHANGE, 0, 0);
}

void CFontListView::OnFontlistOpen() 
{
	CFontItem* pItem = reinterpret_cast<CFontItem*>(GetListCtrl().GetItemData(m_nRClickItem));

	if (pItem)
	{
		if (pItem->GetStatus() == CruxTechnologies::COpenTypeData::kStatus_OK)
		{
			int result =
				reinterpret_cast<int>(::ShellExecute(NULL, _T("open"), pItem->GetFullFileName(), NULL, NULL, SW_SHOW));

			// if "open" doesn't work, try "preview"
			if (result <= 32)
				::ShellExecute(NULL, _T("preview"), pItem->GetFullFileName(), NULL, NULL, SW_SHOW);
		}
	}
}

void CFontListView::OnFontlistExplore()
{
	CFontItem* pItem = reinterpret_cast<CFontItem*>(GetListCtrl().GetItemData(m_nRClickItem));

	if (pItem)
	{
		if (pItem->GetStatus() == CruxTechnologies::COpenTypeData::kStatus_OK)
		{
			CString strParams;
			strParams.Format(_T("/select,\"%s\""), pItem->GetFullFileName());
			ShellExecute(NULL, _T("open"), _T("explorer"), strParams, NULL, SW_SHOW);
		}
	}
}

void CFontListView::OnFontlistFindSimilar()
{
	CFontItem* pItem = reinterpret_cast<CFontItem*>(GetListCtrl().GetItemData(m_nRClickItem));

	CFontRunnerDoc* pDoc = CFontRunnerDoc::GetDoc();

	CFindSimilarFrameWnd* pFindWindow =
		new CFindSimilarFrameWnd(pItem,
								 pDoc->UsingFontProject() ? pDoc->GetCurrentProject() : m_strCurrentDirectory,
								 pDoc->UsingFontProject());
	
	pFindWindow->Create(theApp.GetMainWnd());
}

void CFontListView::OnFontlistFontInfo()
{
	// get index of currently selected item
	int nItem = GetListCtrl().GetNextItem(-1, LVNI_SELECTED);

	// get FONTLISTITEMDATA from list
	CFontItem* pFontListItemData =
		reinterpret_cast<CFontItem*>(GetListCtrl().GetItemData(nItem));

	// display info
	CFontInfoDlg dlgFontInfo;
	dlgFontInfo.SetFontInfo(pFontListItemData);
	dlgFontInfo.DoModal();
}

void CFontListView::OnFontlistProperties() 
{
	CFontItem* pItem = reinterpret_cast<CFontItem*>(GetListCtrl().GetItemData(m_nRClickItem));
	ShellUtils::InvokeContextMenuCommand(GetSafeHwnd(), pItem->GetFullFileName(), "properties");
}

void CFontListView::OnDblclk(NMHDR* /*pNMHDR*/, LRESULT* pResult) 
{
	// get coordinates of click
	POINT point;
	GetCursorPos(&point);
	ScreenToClient(&point);
	
	// get item clicked on
	int nItem = GetListCtrl().HitTest(point);

	if (nItem != -1)
	{
		CFontItem* pItem = reinterpret_cast<CFontItem*>(GetListCtrl().GetItemData(nItem));
		
		if (pItem && pItem->GetStatus() == CruxTechnologies::COpenTypeData::kStatus_OK)
		{
			int result =
				reinterpret_cast<int>(::ShellExecute(NULL, _T("open"), pItem->GetFullFileName(), NULL, NULL, SW_SHOW));

			// if "open" doesn't work, try "preview"
			if (result <= 32)
				::ShellExecute(NULL, _T("preview"), pItem->GetFullFileName(), NULL, NULL, SW_SHOW);
		}
	}
	
	*pResult = 0;
}

void CFontListView::OnHdnItemclick(NMHDR *pNMHDR, LRESULT *pResult)
{
	NMLISTVIEW *pLV = (NMLISTVIEW*) pNMHDR;
	
	int nLastItem = m_nSortColumn;

	// determine column and sort order
	if (m_nSortColumn == pLV->iItem)
		m_bSortDescending = !m_bSortDescending;
	else
	{
		m_nSortColumn = pLV->iItem;
		m_bSortDescending = (m_nMode == kMode_FindSimilar && pLV->iItem == kColumn_Similarity);
	}

	if (m_nMode == kMode_View)
		theApp.GetProgramOptions()->SetFontListSortDescending(m_bSortDescending);

	// set the appropriate bitmap
	SetHeaderArrow(&nLastItem);
	
	// sort the list
	GetListCtrl().SortItems(FontListSortProc, (DWORD_PTR)this);

	*pResult = 0;
}

LRESULT CFontListView::OnFolderChange(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	// not re-entrant
	CSingleLock lock(&m_csFolderChange, true);

	// we're going to reconcile the changes
	const CListCtrl& listctrl = GetListCtrl();
	typedef std::vector<CString> fflist_type;
	fflist_type fflCurrent, fflNew;

	// make a list of current files
	CFontItem* pFontItem = NULL;
	for (int i = 0; i < listctrl.GetItemCount(); ++i)
	{
		pFontItem = reinterpret_cast<CFontItem*>(listctrl.GetItemData(i));
		fflCurrent.push_back(pFontItem->GetFullFileName());
	}
	std::sort(fflCurrent.begin(), fflCurrent.end());

	// make a list of new files
	TCHAR szFolder[MAX_PATH];
	_tcscpy_s(szFolder, MAX_PATH, m_strCurrentDirectory);
	::PathAppend(szFolder, _T("*.ttf"));

	WIN32_FIND_DATA FoundData;
	HANDLE hFindHandle = ::FindFirstFile(szFolder, &FoundData);

	if (hFindHandle != INVALID_HANDLE_VALUE)
	{
		TCHAR szFileName[MAX_PATH];
		do
		{
			_tcscpy_s(szFileName, MAX_PATH, m_strCurrentDirectory);
			::PathAppend(szFileName, FoundData.cFileName);
			fflNew.push_back(szFileName);
		} while (::FindNextFile(hFindHandle, &FoundData));

		::FindClose(hFindHandle);
	}
	std::sort(fflNew.begin(), fflNew.end());

	// find files to remove
	fflist_type fflRemove(fflCurrent.size());
	fflist_type::iterator itUnsorted =
		std::set_difference(fflCurrent.begin(),	fflCurrent.end(),
							fflNew.begin(),	fflNew.end(), fflRemove.begin());
	fflRemove.erase(itUnsorted, fflRemove.end());

	// remove files
	std::for_each(fflRemove.begin(),
				  fflRemove.end(),
				  boost::bind(&CFontManager::FontFileDeleted, theApp.GetFontManager(), _1));

	// find files to add
	fflist_type fflAdd(fflNew.size());
	itUnsorted =
		std::set_difference(fflNew.begin(), fflNew.end(),
							fflCurrent.begin(), fflCurrent.end(), fflAdd.begin());
	fflAdd.erase(itUnsorted, fflAdd.end());

	// add files
	std::for_each(fflAdd.begin(),
				  fflAdd.end(),
				  boost::bind(static_cast<void (CFontListView::*)(const CString&)>(&CFontListView::AddNewFile), this, _1));

	return 0L;
}

INT_PTR CFontListView::OnToolHitTest(CPoint point, TOOLINFO* pTI) const
{
	LVHITTESTINFO lvHitTest;
	lvHitTest.pt = point;

	int nItem = GetListCtrl().HitTest(&lvHitTest);

	if (nItem >= 0)
	{
		pTI->hwnd = m_hWnd;
		GetListCtrl().GetItemRect(lvHitTest.iItem, &pTI->rect, LVIR_BOUNDS);
		pTI->uId = lvHitTest.iItem;
		pTI->lpszText = LPSTR_TEXTCALLBACK;

		return nItem;
	}
	else
		return -1;
}

BOOL CFontListView::OnTTNNeedText(UINT /*nID*/, NMHDR* pTTTStruct, LRESULT* pResult)
{
	*pResult = 0;

	// get list item
	LVHITTESTINFO lvHitTest;
	lvHitTest.pt = m_ptMouse;

	// make sure there is plenty of room for our tooltip
	::SendMessage(pTTTStruct->hwndFrom, TTM_SETMAXTIPWIDTH, 0, kToolTipWidth);

	CString strTipText;

	// cast to TOOLTIPTEXT structure
	TOOLTIPTEXT* pTTT = reinterpret_cast<TOOLTIPTEXT*>(pTTTStruct);

	// If ID is invalid, don't show tool tip
	// If the flags are a specific value, skip it too.  Otherwise we get an oddball tooltip
	// in the top corner of the screen when using the keyboard to select items
	if (pTTT->hdr.idFrom == 0xFFFFFFFF || (pTTT->hdr.idFrom == 0 && pTTT->uFlags == 0x1E0))
		return FALSE;

	if (pTTTStruct->idFrom < 32768)
	{
		// this is a list control item

		CFontItem* pData =
			reinterpret_cast<CFontItem*>(GetListCtrl().GetItemData((int)pTTTStruct->idFrom));

		// grab name pointer
		using CruxTechnologies::OpenTypeFontData::CNameRecord;
		const std::wstring* pstrName =
			pData->GetOpenTypeData()->GetName(CNameRecord::kNameID_FullFontName);

		if (!pstrName)
			return FALSE;

		CString strFormat;
		if (!CFontRunnerDoc::GetDoc()->UsingFontProject() && m_nMode == kMode_View)
		{
			// load formatting text and format it
			strFormat.LoadString(IDS_FONTLIST_VIEW_TOOLTIP);
			strTipText.Format(strFormat, pstrName->c_str(), pData->GetFileName(), (int)(pData->GetFileSize() / 1024));
		}
		else
		{
			// get string containing just the path
			TCHAR szPath[MAX_PATH];
			::StringCchCopy(szPath, MAX_PATH, pData->GetFullFileName());
			::PathRemoveFileSpec(szPath);

			// load formatting text and format it
			strFormat.LoadString(IDS_FONTLIST_FIND_TOOLTIP);
			strTipText.Format(strFormat, pstrName->c_str(), pData->GetFileName(), szPath, (int)(pData->GetFileSize() / 1024));
		}

		// recreate current tip text string and copy our formatted string into it
		m_pszCurrentTipText.reset(new TCHAR[strTipText.GetLength() + 1]);
		::StringCchCopy(m_pszCurrentTipText.get(), strTipText.GetLength() + 1, strTipText);
	}
	else
	{
		// this is a toolbar ID
		strTipText.LoadString((UINT)pTTTStruct->idFrom);
		int nBreak = strTipText.Find(_T("\n"));

		if (nBreak != -1)
		{
			m_pszCurrentTipText.reset(new TCHAR[strTipText.GetLength() + 1]);
			::StringCchCopy(m_pszCurrentTipText.get(), strTipText.GetLength() + 1, strTipText.Mid(nBreak + 1));
		}
	}

	// send the tip text back to the system
	pTTT->lpszText = m_pszCurrentTipText.get();

	return TRUE;
}

void CFontListView::OnMouseMove(UINT /*nFlags*/, CPoint point)
{
	// just save mouse position
	m_ptMouse = point;
}

void CFontListView::OnHdnDividerdblclick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);

	if (phdr->iItem == kColumn_FontName)
	{
		int nTotal = GetListCtrl().GetColumnWidth(kColumn_FontName) +
			GetListCtrl().GetColumnWidth(kColumn_FileName);
		GetListCtrl().SetColumnWidth(kColumn_FontName,
			nTotal - m_nOptimalColumnSize[kColumn_FileName]);
		GetListCtrl().SetColumnWidth(kColumn_FileName,
			m_nOptimalColumnSize[kColumn_FileName]);
	}
	else if (phdr->iItem == kColumn_FileName)
	{
		int nTotal = GetListCtrl().GetColumnWidth(kColumn_FileName) +
			GetListCtrl().GetColumnWidth(kColumn_Size);
		GetListCtrl().SetColumnWidth(kColumn_FileName,
			nTotal - m_nOptimalColumnSize[kColumn_Size]);
		GetListCtrl().SetColumnWidth(kColumn_Size,
			m_nOptimalColumnSize[kColumn_Size]);
	}

	*pResult = 0;
}

void CFontListView::OnHdnItemChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	if (m_bAdjustingHeader)
	{
		*pResult = 0;
		return;
	}

	m_bAdjustingHeader = true;
	NMHEADER* pNMHeader = reinterpret_cast<NMHEADER*>(pNMHDR);

	CRect rcClient;
	GetClientRect(&rcClient);

	// if resizing name column, stretch filename column
	if (pNMHeader->iItem == kColumn_FontName)
	{
		GetListCtrl().SetColumnWidth(
			kColumn_FileName,
			rcClient.Width() - (pNMHeader->pitem->cxy + GetListCtrl().GetColumnWidth(kColumn_Size)));
	}
	else if (pNMHeader->iItem == kColumn_FileName)
	{
		GetListCtrl().SetColumnWidth(
			kColumn_Size,
			rcClient.Width() - (pNMHeader->pitem->cxy + GetListCtrl().GetColumnWidth(kColumn_FontName)));
	}

	*pResult = 0;
	m_bAdjustingHeader = false;
	Invalidate(FALSE);
}

void CFontListView::OnSize(UINT /*nType*/, int cx, int /*cy*/)
{
	// resize the name column
	int nOtherColumns =
		(GetListCtrl().GetColumnWidth(kColumn_FileName) +
		 GetListCtrl().GetColumnWidth(kColumn_Size));
	GetListCtrl().SetColumnWidth(kColumn_FontName, cx - nOtherColumns);
}

void CFontListView::OnTempAutoInstallChange(bool bAutoInstall)
{
	CListCtrl& ctrlList = GetListCtrl();

	int nCount = ctrlList.GetItemCount();
	if (nCount)
	{
		CString strTitle;
		strTitle.LoadString(IDS_FONTLIST_TEMPINSTALLPROGRESSTITLE);

		// show wait cursor
		CWaitCursor wait;

		// don't bother progress window unless there is really alot to do
		bool bProgressWindow = (GetListCtrl().GetItemCount() > 500);

		CProgressDlg dlgProgress;
		if (bProgressWindow)
		{
			// show working dialog
			dlgProgress.Create(theApp.m_pMainWnd);
			dlgProgress.CenterWindow();
			dlgProgress.UpdateProgressText(false);
			dlgProgress.ShowCancelButton(false);
			dlgProgress.SetWindowText(strTitle);
			dlgProgress.SetProgressText(
				bAutoInstall ? IDS_FONTLIST_TEMPINSTALLING : IDS_FONTLIST_TEMPUNINSTALLING);
			dlgProgress.ShowWindow(SW_SHOW);
		}

		theApp.GetFontManager()->SetTempAutoInstall(bAutoInstall);
		
		// cleanup progress window
		if (bProgressWindow)
			dlgProgress.DestroyWindow();

		// let other apps know what we did
		::PostMessage(HWND_BROADCAST, WM_FONTCHANGE, 0, 0);
	}
}

void CFontListView::OnRefresh()
{
	if (!CFontRunnerDoc::GetDoc()->UsingFontProject())
		ShowFontsInDirectory(_T(""));
}

void CFontListView::OnAddToMainFontListView(const std::vector<std::wstring>& list)
{
	// add each item to this view
	std::for_each(list.begin(), list.end(),
		boost::bind(static_cast<void (CFontListView::*)(const std::wstring&)>(&CFontListView::AddNewFile), this, _1));
}

void CFontListView::OnOptionsChange()
{
	// re-create all fonts in the list
	for (int i = 0; i < GetListCtrl().GetItemCount(); ++i)
	{
		CFontItem* pFontItem =
			reinterpret_cast<CFontItem*>(GetListCtrl().GetItemData(i));

		// get logical font info from old font.  We'll
		// use it to create the new font too.
		LOGFONT lf;
		pFontItem->GetDisplayFont().GetLogFont(&lf);

		// delete old font
		pFontItem->GetDisplayFont().DeleteObject();

		// create font with new quality
		//switch (m_pOptions->m_nFontListRenderingOption)
		switch (theApp.GetProgramOptions()->GetFontListRenderingOption())
		{
			case CProgramOptions::kRenderingOption_UseDefault:
				lf.lfQuality = DEFAULT_QUALITY;
				break;
			case CProgramOptions::kRenderingOption_Antialiasing:
				lf.lfQuality = ANTIALIASED_QUALITY;
				break;
			case CProgramOptions::kRenderingOptions_ClearType:
				lf.lfQuality = CLEARTYPE_QUALITY;
				break;
		}

		pFontItem->GetDisplayFont().CreateFontIndirect(&lf);
	}

	Invalidate();
}

void CFontListView::OnDeleteAllItems(NMHDR* /*pNMHDR*/, LRESULT* pResult)
{
	// remove all these fonts from the manager
	if (DWORD dwItemCount = GetListCtrl().GetItemCount())
	{
		CFontItem* pFontItem = NULL;

		// loop through list uninstalling each font and deleting each item's data
		for (DWORD dwIndex = 0; dwIndex <= (dwItemCount - 1); dwIndex++)
		{
			// get this item's data
			pFontItem =	reinterpret_cast<CFontItem*>(GetListCtrl().GetItemData(dwIndex));

			if (pFontItem)
			{
				theApp.GetFontManager()->RemoveReference(pFontItem);

				// set item data to NULL
				GetListCtrl().SetItemData(dwIndex, (DWORD_PTR)NULL);
			}
		}

		// let other apps know we removed these fonts
		::PostMessage(HWND_BROADCAST, WM_FONTCHANGE, 0, 0);
	}

	*pResult = 0;
}

void CFontListView::OnDeleteItem(NMHDR* pNMHDR, LRESULT* pResult)
{
	NM_LISTVIEW* pNMListView = reinterpret_cast<NM_LISTVIEW*>(pNMHDR);
	
	// remove this font from the manager
	CFontItem* pItem = reinterpret_cast<CFontItem*>(pNMListView->lParam);

	if (pItem)
		theApp.GetFontManager()->RemoveReference(pItem);

	*pResult = 0;
}

LRESULT CFontListView::OnGetIconIndex(WPARAM wParam, LPARAM /*lParam*/)
{
	LPCTSTR szFilename = (LPCTSTR)wParam;

	SHFILEINFO sfi;
	HIMAGELIST himl =
		(HIMAGELIST)::SHGetFileInfo(szFilename, 0, &sfi, sizeof(SHFILEINFO),
									SHGFI_SYSICONINDEX | SHGFI_SMALLICON);

	if (m_hImageList == NULL)
	{
		// instead of setting the image list directly to the
		// control, we'll hang onto it with a CImageList object.
		// Setting it directly causes problems with the row height
		// that interferes with MeasureItem.
		m_hImageList = himl;
	}

	return sfi.iIcon;
}

int CFontListView::OnUnManageFontItem(const CFontItem* pFontItem)
{
	for (int n = 0; n < GetListCtrl().GetItemCount(); ++n)
	{
		CFontItem* pTest = reinterpret_cast<CFontItem*>(GetListCtrl().GetItemData(n));

		if (pTest == pFontItem)
		{
			// remove item from list, the delete item handler will dereference it
			GetListCtrl().DeleteItem(n);

			// deleted it, we're done
			return 1;
		}
	}

	return 0;
}

void CFontListView::OnFontFileDeleted(const CFontItem* pFontItem)
{
	for (int n = 0; n < GetListCtrl().GetItemCount(); ++n)
	{
		CFontItem* pTest = reinterpret_cast<CFontItem*>(GetListCtrl().GetItemData(n));

		// remove item from list, the delete item handler will dereference it
		if (pTest == pFontItem)
		{
			GetListCtrl().SetItemData(n, (DWORD_PTR)NULL);
			GetListCtrl().DeleteItem(n);
		}
	}
}

void CFontListView::OnFolderMenu_Delete()
{
	if (CFontRunnerDoc::GetDoc()->UsingFontProject())
		RemoveSelectedFilesFromProject();
	else
		DeleteSelectedFiles();
}

void CFontListView::OnUpdateFolderMenu_Delete(CCmdUI* pCmdUI)
{
	if (CFontRunnerDoc::GetDoc()->UsingFontProject())
	{
		CString strText;
		strText.LoadString(IDS_FONTPROJECTVIEW_MENUREMOVE);

		pCmdUI->SetText(strText);
	}
}

void CFontListView::OnBadFontMenu_WhatsWrong()
{
	AfxGetApp()->HtmlHelp(HIDP_ERRORS, HH_HELP_CONTEXT);
}

void CFontListView::OnBadFontMenu_Delete()
{
	OnFolderMenu_Delete();
}

void CFontListView::OnUpdateBadFontMenu_Delete(CCmdUI* pCmdUI)
{
	OnUpdateFolderMenu_Delete(pCmdUI);
}

void CFontListView::OnFontListMenu_ShowContaining()
{
	CFontItem* pItem = reinterpret_cast<CFontItem*>(GetListCtrl().GetItemData(m_nRClickItem));

	if (pItem)
	{
		TCHAR szPath[MAX_PATH];
		::StringCchCopy(szPath, MAX_PATH, pItem->GetFullFileName());

		::PathRemoveFileSpec(szPath);

		CString strFullName = pItem->GetFileName();
		theApp.GetSignals()->Fire_SelectedFolderChange(szPath, &strFullName);
	}
}
